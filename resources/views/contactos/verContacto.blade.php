@extends('adminlte::layouts.app')

@section('htmlheader_title')
	{{ trans('adminlte_lang::message.home') }}
@endsection


@section('main-content')
	<script type="text/javascript">
		$("#contentHeader1").append('<a href={{url("contactos")}}><i class="fa fa-address-book"></i> Contactos</a>');
		$("#contentHeader2").text('Detalle de Contacto');
	</script>
	<?php 
    	$permisos = $_SESSION['permisos']; 
	?>
	<div class="container-fluid spark-screen">
		<div class="row">
			<div class="col-md-12 col-md-offset-0">
				<!-- Default box -->
				<div class="box box-primary">
					<div class="box-header with-border">
						<h3 class="box-title">Ver Contacto</h3>

						<div class="box-tools pull-right">
							<button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
								<i class="fa fa-minus"></i></button>
							<button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
								<i class="fa fa-times"></i></button>
						</div>
					</div>

					<div class="box-body">

						<div class="form-group col-md-4 col-md-offset-0">
							<label for="nombre">Nombre</label>
							<input class="form-control" name="nombre" id="nombre" type="text" value="{{$contacto->nombre}}" disabled="disabled">
						</div>
						<div class="form-group col-md-4 col-md-offset-0">
							<label for="nit">Nit</label>
							<input class="form-control" name="nit" id="nit" type="text" value="{{$contacto->nit}}" disabled="disabled">
						</div>
						<div class="form-group col-md-4 col-md-offset-0">
							<label for="ciudad">Ciudad</label>
							<input class="form-control" name="ciudad" id="ciudad" type="text" value="{{$contacto->ciudad}}" disabled="disabled">
						</div>
						
						<div class="form-group col-md-6 col-md-offset-0">
							<label for="direccion">Direccion</label>
							<input class="form-control" name="direccion" id="direccion" type="text" value="{{$contacto->direccion}}" disabled="disabled">
						</div>
						<div class="form-group col-md-6 col-md-offset-0">
							<label for="email">Email</label>
							<input class="form-control" name="email" id="email" type="text" value="{{$contacto->email}}" disabled="disabled">
						</div>
						
						<div class="form-group col-md-4 col-md-offset-0">
							<label for="telefono">Telefono 1</label>
							<input class="form-control" name="telefono" id="telefono" type="text" value="{{$contacto->telefono}}" disabled="disabled">
						</div>
						<div class="form-group col-md-4 col-md-offset-0">
							<label for="telefono2">Telefono 2</label>
							<input class="form-control" name="telefono2" id="telefono2" type="text" value="{{$contacto->telefono2}}" disabled="disabled">
						</div>
						<div class="form-group col-md-4 col-md-offset-0">
							<label for="celular">Celular</label>
							<input class="form-control" name="celular" id="celular" type="text" value="{{$contacto->celular}}" disabled="disabled">
						</div>
						
						<div class="form-group col-md-4 col-md-offset-0">
							<label for="fax">Fax</label>
							<input class="form-control" name="fax" id="fax" type="text" value="{{$contacto->fax}}" disabled="disabled">
						</div>
						
						<div class="form-group col-md-2 col-md-offset-0">
							<label>Cliente</label>
							<div class="checkbox">
								<?php $checked = ($contacto->cliente==1) ? 'checked=checked': ''; ?>
			                    <label><input type="checkbox" name="cliente" id="cliente" value="1" disabled="disabled" {{$checked}}></label>
			                 </div>
						</div>
						<div class="form-group col-md-2 col-md-offset-0">
							<label>Proveedor</label>
							<div class="checkbox">
								<?php $checked = ($contacto->proveedor==1) ? 'checked=checked': ''; ?>
			                    <label><input type="checkbox" name="proveedor" id="proveedor" value="1" disabled="disabled" {{$checked}}></label>
			                 </div>
						</div>
						<div class="form-group col-md-4 col-md-offset-0">
							<label for="observaciones">Observaciones</label>
							<textarea class="form-control" rows="2" name="observaciones" id="observaciones" disabled="disabled">{{$contacto->observaciones}}</textarea>
						</div>
						<!--
						<div class="form-group col-md-4 col-md-offset-0">
							<label for="tipo">Termino de pago preterminado</label>
							<select class="form-control" name="id_terminoPago" id="id_terminoPago" disabled="disabled">
							@foreach($terminos as $termino)
					        	<option value="{{ $termino->id_terminoPago }}">{{ $termino->nombre}}</option>
					        @endforeach
		                  	</select>
						</div>
						<div class="form-group col-md-4 col-md-offset-0">
							<label for="id_vendedor">Vendedor preterminado</label>
							<select class="form-control" name="id_vendedor" id="id_vendedor" disabled="disabled">
					        	@foreach($vendedores as $vendedor)
					        		<option value="{{ $vendedor->id_usuario }}">{{ $vendedor->nombre}}</option>
					        	@endforeach
		                  	</select>
						</div>

						<div class="form-group col-md-4 col-md-offset-0">
							<label for=id_lista>Lista de precios preterminada</label>
							<select class="form-control" name=id_lista id=id_lista disabled="disabled" >
					        	<option value="1">General</option>
		                  	</select>
						</div>
						

						<div class="form-group col-md-12 col-md-offset-0">
							<label>Notificaciones(de pagos de facturas)</label>
							<div class="checkbox">
			                    <label><input type="checkbox" name="notificacion" id="notificacion" value="1" disabled="disabled"></label>
			                 </div>
						</div><br>
						-->
						<h4>Personas relacionadas</h4>

						@foreach($personas as $persona)
						<div class="row rowPersona" id="rowPersona">
							<div class="form-group col-md-2 col-md-offset-0">
								<label for="nombreP">Nombre</label>
								<input class="form-control" name="nombreP[]" id="nombreP" type="text" value="{{$persona->nombre}}" disabled="disabled" >
							</div>
							<div class="form-group col-md-2 col-md-offset-0">
								<label for="apellidoP">Apellido</label>
								<input class="form-control" name="apellidoP[]" id="apellidoP" type="text" value="{{$persona->apellido}}" disabled="disabled">
							</div>
							<div class="form-group col-md-2 col-md-offset-0">
								<label for="emailP">Email</label>
								<input class="form-control" name="emailP[]" id="emailP" type="email" value="{{$persona->email}}" disabled="disabled">
							</div>
							<div class="form-group col-md-2 col-md-offset-0">
								<label for="telefonoP">Telefono</label>
								<input class="form-control" name="telefonoP[]" id="telefonoP" type="text" value="{{$persona->telefono}}" disabled="disabled">
							</div>
							<div class="form-group col-md-2 col-md-offset-0">
								<label for="celularP">Celular</label>
								<input class="form-control" name="celularP[]" id="celularP" type="text" value="{{$persona->celular}}" disabled="disabled">
							</div>
							<!--
							<div class="form-group col-md-1 col-md-offset-0">
								<label>Notificaciones</label>
								<div class="checkbox">
									@if($persona->notificacion==1)
				                    <label><input type="checkbox" name="notificacionP[]" value="1" checked="checked" disabled="disabled"></label>
				                    @else
				                    <label><input type="checkbox" name="notificacionP[]" value="1" disabled="disabled"></label>
				                    @endif
				                 </div>
							</div>
							-->
							
						</div>

						@endforeach


						@if(isset($permisos[18]) && $permisos[18]->ver==1)
						<h4 class="box-title form-group col-md-12 col-md-offset-0">Versiones</h3>
						<table class="table table-bordered table-hover dataTable" 
							data-toggle="table"
							data-pagination="true"       						
							>
			                <thead>
				                <tr>
				                	<th>Nit</th>
				                  	<th>Nombre</th>
				                  	<th>Ciudad</th>
				                  	<th>Fecha</th>
				                  	<th>Version</th>
				                  	<th>Responsable</th>
				                  	<th>Detalle</th>
				                </tr>
				            </thead>
				        	<tbody>
				                @foreach($versiones as $version) 
				                	<?php  $referenciaPrincipal = ($version->id_contacto==$contacto->id_contacto) ? 'id=referenciaPrincipal' : ''; 
				                	?>
						    		<tr {{$referenciaPrincipal}} >
						    			<td> {{$version->nombre }} 		</td>
						    			<td> {{$version->nit }} 		</td>
						    			<td> {{$version->ciudad }} 	</td>
						    			<td> {{$version->updated_at }} </td>
						    			<td> {{$version->numeroVersion }} </td>
						    			<td> <a href="{{ url('/verUsuario/'.$version->responsableID) }}">  {{$version->responsableNombre.' ('.$version->responsableIdentificacion.')' }}</a> </td>
						    			<td> 
						    				<a class="btn btn-info" style="padding: 2px 10px;" href="{{ url('/verContacto/'.$version->id_contacto) }}" aria-label="Ver Detalle">
  												<i class="fa fa-eye" aria-hidden="true"></i>
  											</a>
						    			</td>
						    		</tr>
								@endforeach
			              	</tbody>	
              			</table>
              			@endif					

					</div>
					
				</div>
				<!-- /.box -->
			</div>
		</div>
	</div>

	<script type="text/javascript">

		$( document ).ready(function() {
			$("#referenciaPrincipal").css("background-color", "#DDFCDA");
		});
	</script>

	
@endsection