@extends('adminlte::layouts.app')

@section('htmlheader_title')
	{{ trans('adminlte_lang::message.home') }}
@endsection


@section('main-content')
	<script type="text/javascript">
		$("#contentHeader2").append('<i class="fa fa-address-book"></i> Contactos');
	</script>

	<?php $permisos = $_SESSION['permisos']; ?>
	<div class="container-fluid spark-screen">
		<div class="row">
			<div class="col-md-12 col-md-offset-0">
				<!-- Default box -->
				<div class="box box-primary">
					<div class="box-header with-border">
						<h3 class="box-title">Contactos</h3>

						<div class="box-tools pull-right">
							<button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
								<i class="fa fa-minus"></i></button>
							<button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
								<i class="fa fa-times"></i></button>
						</div>
					</div>
					<div class="box-body">
						@if($permisos['11']->insertar==1)
						<div class="col-md-2 col-md-offset-0" style="margin-bottom: 10px;">
							<a class="btn btn-primary btn-block btn-flat" href="{{ url('nuevoContacto')}}"><i class="fa fa-plus" aria-hidden="true"></i> Nuevo</a>
						</div>
						@endif
						

						<!--Errores y exito mensajes-->
						@if(count($errors)>0)
						<div class="form-group col-md-8 col-md-offset-2">
							<div class="alert alert-danger alert-dismissible">
				                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
				                <h4><i class="icon fa fa-ban"></i> Error en el formulario</h4>
				                @foreach ($errors->all() as $message) 
	    							<span > {{$message}}</span><br>
								@endforeach
				             </div>
						</div>
						@endif
						@if(session('mensaje'))
						<div class="form-group col-md-8 col-md-offset-2">
							<div class="alert alert-success alert-dismissible">
						        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
						        <h4><i class="icon fa fa-check"></i> {{ session('mensaje') }}</h4>
						    </div>
						</div>
						@endif

						<div class="form-group col-md-2 col-md-offset-2">
							<label>Clientes</label>
			                <label><input type="checkbox" name="cliente" id="clientes" onclick="mostrarClientes();" checked="checked"></label>
						</div>
						<div class="form-group col-md-2 col-md-offset-0">
							<label>Proveedores</label>
			                <label><input type="checkbox" name="proveedor" id="proveedores" onclick="mostrarProveedores();" checked="checked"></label>
			                
						</div>
						<!--/Errores y exito mensajes-->

						<table class="table table-bordered table-hover dataTable" 
							data-toggle="table"
							data-pagination="true"
							data-search="true"
							data-show-refresh="true"
       						data-show-columns="true">
			                <thead>
				                <tr>
				                  <th>Nombre</th>
				                  <th>Nit</th>
				                  <th>Telefono</th>
				                  <th>Observacion</th>
				                  <th data-halign="center" data-align="center" data-width="17%">Acciones</th>
				                </tr>
				            </thead>
				        	<tbody>
				                @foreach($contactos as $contacto) 
				                	@if($contacto->cliente==1 && $contacto->proveedor==1)
						    		<tr class="proveedor cliente">
						    		@elseif($contacto->proveedor==1)
						    		<tr class="proveedor">
						    		@elseif($contacto->cliente==1)
						    		<tr class="cliente">
						    		@endif
						    			<td> {{$contacto->nombre }} 		</td>
						    			<td> {{$contacto->nit }} 	</td>
						    			<td> {{$contacto->telefono }} </td>
						    			<td> {{$contacto->observaciones }} </td>
						    			<td>
						    				@if($permisos['11']->ver==1)
						    				<a class="btn btn-success" style="padding: 2px 10px;" href="{{ url('/verContacto/'.$contacto->id_contacto) }}">
  												<i class="fa fa-eye" aria-hidden="true"></i>
  											</a>
  											@endif
  											@if($permisos['11']->modificar==1)
											<a class="btn btn-primary" style="padding: 2px 10px;" href="{{ url('/editContacto/'.$contacto->id_contacto) }}" aria-label="editar">
											  <i class="fa fa-pencil" aria-hidden="true"></i>
											</a>
											@endif
											@if($permisos['11']->eliminar==1)
											<a class="btn btn-danger" style="padding: 2px 10px;" href="{{ url('/eliminarContacto/'.$contacto->id_contacto) }}" aria-label="Eliminar" id="eliminar">
											 <i class="fa fa-times" aria-hidden="true"></i>
											</a>
											@endif
										</td>
						    		</tr>
								@endforeach
			              	</tbody>	
              			</table>						
					</div>
					
				</div>
				<!-- /.box -->
			</div>
		</div>
	</div>


	<script type="text/javascript">
		
		$( document ).ready(function() {
  			$('#eliminar').click(function(event) {
		    	if(!confirm('¿Deseas realmente eliminar este Contacto?'))
					event.preventDefault();
			});
		});

		function mostrarClientes(){
			if($( "#clientes").is(":checked")){
				$(".cliente").css("display", "");
			}
			else if(!$( "#proveedores").is(":checked")){
				$(".cliente").css("display", "none");
			}
			else{
				$(".cliente:not(.proveedor)").css("display", "none");	
			}
		}
		function mostrarProveedores(){
			if($( "#proveedores").is(":checked")){
				$(".proveedor").css("display", "");
			}
			else if(!$( "#clientes").is(":checked")){
				$(".proveedor").css("display", "none");
			}
			else{
				$(".proveedor:not(.cliente)").css("display", "none");	
			}
		}
	</script>
@endsection

