@extends('adminlte::layouts.app')

@section('htmlheader_title')
	{{ trans('adminlte_lang::message.home') }}
@endsection


@section('main-content')
	<script type="text/javascript">
		$("#contentHeader1").append('<a href={{url("contactos")}}><i class="fa fa-address-book"></i> Contactos</a>');
		$("#contentHeader2").text('Crear Contacto');
	</script>
	<div class="container-fluid spark-screen">
		<div class="row">
			<div class="col-md-12 col-md-offset-0">
				<!-- Default box -->
				<div class="box box-primary">
					<div class="box-header with-border">
						<h3 class="box-title">Nuevo Contacto</h3>

						<div class="box-tools pull-right">
							<button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
								<i class="fa fa-minus"></i></button>
							<button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
								<i class="fa fa-times"></i></button>
						</div>
					</div>

					<form action="{{ url('/nuevoContacto') }}" method="post">
					<div class="box-body">
						<p class="help-block">Campos marcados con asterisco (<i class="fa fa-asterisk" style="color: #DD4B39;"></i>) son obligatorios.</p>
						<!--Errores y exito mensajes-->
						@if(count($errors)>0)
						<div class="form-group col-md-12 col-md-offset-0">
							<div class="alert alert-danger alert-dismissible">
				                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
				                <h4><i class="icon fa fa-ban"></i> Error en el formulario</h4>
				                @foreach ($errors->all() as $message) 
	    							<span > {{$message}}</span><br>
								@endforeach
				             </div>
						</div>
						@endif

						<div class="form-group col-md-4 col-md-offset-0">
							<label for="nombre">Nombre</label> (<i class="fa fa-asterisk" style="color: #DD4B39;"></i>)
							<input class="form-control" name="nombre" id="nombre" type="text" value="{{ Input::old('nombre') }}">
						</div>
						<div class="form-group col-md-4 col-md-offset-0">
							<label for="nit">Nit</label>
							<input class="form-control" name="nit" id="nit" type="text" value="{{ Input::old('nit') }}">
						</div>
						<div class="form-group col-md-4 col-md-offset-0">
							<label for="ciudad">Ciudad</label>
							<input class="form-control" name="ciudad" id="ciudad" type="text" value="{{ Input::old('ciudad')}}">
						</div>
						
						<div class="form-group col-md-6 col-md-offset-0">
							<label for="direccion">Direccion</label>
							<input class="form-control" name="direccion" id="direccion" type="text" value="{{ Input::old('direccion')}}">
						</div>
						<div class="form-group col-md-6 col-md-offset-0">
							<label for="email">Email</label>
							<input class="form-control" name="email" id="email" type="text" value="{{ Input::old('email')}}">
						</div>
						
						<div class="form-group col-md-4 col-md-offset-0">
							<label for="telefono">Telefono 1</label>
							<input class="form-control" name="telefono" id="telefono" type="text" value="{{ Input::old('telefono')}}">
						</div>
						<div class="form-group col-md-4 col-md-offset-0">
							<label for="telefono2">Telefono 2</label>
							<input class="form-control" name="telefono2" id="telefono2" type="text" value="{{ Input::old('telefono2')}}">
						</div>
						<div class="form-group col-md-4 col-md-offset-0">
							<label for="celular">Celular</label>
							<input class="form-control" name="celular" id="celular" type="text" value="{{ Input::old('celular')}}">
						</div>
						
						<div class="form-group col-md-4 col-md-offset-0">
							<label for="fax">Fax</label>
							<input class="form-control" name="fax" id="fax" type="text" value="{{ Input::old('fax')}}">
						</div>
						
						<div class="form-group col-md-2 col-md-offset-0">
							<label>Cliente</label>
							<div class="checkbox">
			                    <label><input type="checkbox" name="cliente" value="1"></label>
			                 </div>
						</div>
						<div class="form-group col-md-2 col-md-offset-0">
							<label>Proveedor</label>
							<div class="checkbox">
			                    <label><input type="checkbox" name="proveedor" value="1"></label>
			                 </div>
						</div>
						<div class="form-group col-md-4 col-md-offset-0">
							<label for="observaciones">Observaciones</label>
							<textarea class="form-control" rows="2" name="observaciones" id="observaciones" >{{ Input::old('observaciones')}}</textarea>
						</div>
						<!--
						<div class="form-group col-md-4 col-md-offset-0">
							<label for="tipo">Termino de pago preterminado</label>
							<select class="form-control" name="id_terminoPago" id="id_terminoPago">
							@foreach($terminos as $termino)
					        	<option value="{{ $termino->id_terminoPago }}">{{ $termino->nombre}}</option>
					        @endforeach
		                  	</select>
						</div>
						<div class="form-group col-md-4 col-md-offset-0">
							<label for="id_vendedor">Vendedor preterminado</label>
							<select class="form-control" name="id_vendedor" id="id_vendedor">
					        	@foreach($vendedores as $vendedor)
					        		<option value="{{ $vendedor->id_usuario }}">{{ $vendedor->nombre}}</option>
					        	@endforeach
		                  	</select>
						</div>

						<div class="form-group col-md-4 col-md-offset-0">
							<label for=id_lista>Lista de precios preterminada</label>
							<select class="form-control" name=id_lista id=id_lista>
					        	<option value="1">General</option>
		                  	</select>
						</div>
						

						<div class="form-group col-md-12 col-md-offset-0">
							<label>Notificaciones(de pagos de facturas)</label>
							<div class="checkbox">
			                    <label><input type="checkbox" name="notificacion" value="1"></label>
			                 </div>
						</div><br>
						-->
						<h4>Personas relacionadas</h4>

						<div class="col-md-1 col-md-offset-0" style="margin-bottom: 5px;">
						<button type="button" class="btn btn-block btn-success " onclick="nuevaPersona();"><i class="fa fa-plus-circle" aria-hidden="true"></i></button>
						</div>
						<div class="col-md-1 col-md-offset-0" style="margin-bottom: 5px;">
						<button type="button" class="btn btn-block btn-danger " onclick="menosPersona();"><i class="fa fa-minus-circle" aria-hidden="true"></i></button>
						</div>

						<div class="col-md-11 col-md-offset-2">
						</div>
						<div class="row rowPersona" id="rowPersona">
							<div class="form-group col-md-2 col-md-offset-0">
								<label for="nombreP">Nombre</label>
								<input class="form-control" name="nombreP[]" id="nombreP" type="text" value="" >
							</div>
							<div class="form-group col-md-2 col-md-offset-0">
								<label for="apellidoP">Apellido</label>
								<input class="form-control" name="apellidoP[]" id="apellidoP" type="text" value="">
							</div>
							<div class="form-group col-md-2 col-md-offset-0">
								<label for="emailP">Email</label>
								<input class="form-control" name="emailP[]" id="emailP" type="email" value="">
							</div>
							<div class="form-group col-md-2 col-md-offset-0">
								<label for="telefonoP">Telefono</label>
								<input class="form-control" name="telefonoP[]" id="telefonoP" type="text" value="">
							</div>
							<div class="form-group col-md-2 col-md-offset-0">
								<label for="celularP">Celular</label>
								<input class="form-control" name="celularP[]" id="celularP" type="text" value="">
							</div>
							<!--
							<div class="form-group col-md-1 col-md-offset-0">
								<label>Notificaciones</label>
								<div class="checkbox">
				                    <label><input type="checkbox" name="notificacionP[]" value="1"></label>
				                 </div>
							</div>
							-->
						</div>
						
					


					</div>
					<div class="box-footer">
                		<div class="col-md-2 col-md-offset-10">
							<button type="submit" class="btn btn-primary btn-block btn-flat">Crear</button>
						</div>
						<input type="hidden" name="_token" value="{{ csrf_token() }}">
						<input type="hidden" name="id_empresa" value="{{ Auth::user()->id_empresa }}">

              		</div>
              		</form>
					
				</div>
				<!-- /.box -->
			</div>
		</div>
	</div>

	<script type="text/javascript">

		//funciones usadas para agregar o quitar rows de personas haciendo click en los botones
		function nuevaPersona(){
			$( ".rowPersona:last" ).clone().insertAfter( ".rowPersona:last" );
		}
		function menosPersona(){
			if($(".rowPersona").length>1)
				$(".rowPersona:last").remove();
		}
	</script>
@endsection