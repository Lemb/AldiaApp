@extends('adminlte::layouts.app')

@section('htmlheader_title')
	{{ trans('adminlte_lang::message.home') }}
@endsection


@section('main-content')
	<script type="text/javascript">
		$("#contentHeader1").append('<a href={{url("items")}}><i class="fa fa-cubes"></i> Items</a>');
		$("#contentHeader2").text('Crear Item');
	</script>
	@include('adminlte::layouts.partials.functions')
	<div class="container-fluid spark-screen">
		<div class="row">
			<div class="col-md-12 col-md-offset-0">
				<!-- Default box -->
				<div class="box box-primary">
					<div class="box-header with-border">
						<h3 class="box-title">Nuevo Item</h3>  
						<!--<button type="button" class="btn btn-secondary" data-toggle="tooltip" data-placement="right" title="Campos marcados con asterisco son obligatorios.">
						  <i class="fa fa-info-circle"></i>
						</button>-->

						<div class="box-tools pull-right">
							<button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
								<i class="fa fa-minus"></i></button>
							<button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
								<i class="fa fa-times"></i></button>
						</div>
					</div>

					<form action="{{ url('/nuevoItem') }}" method="post"  enctype="multipart/form-data"  accept-charset="UTF-8">
					<div class="box-body">
						<p class="help-block">Campos marcados con asterisco (<i class="fa fa-asterisk" style="color: #DD4B39;"></i>) son obligatorios.</p>
						
						<!--Errores y exito mensajes-->
						@if(count($errors)>0)
						<div class="form-group col-md-12 col-md-offset-0">
							<div class="alert alert-danger alert-dismissible">
				                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
				                <h4><i class="icon fa fa-ban"></i> Error en el formulario</h4>
				                @foreach ($errors->all() as $message) 
	    							<span > {{$message}}</span><br>
								@endforeach
				             </div>
						</div>
						@endif
						<div class="row">
						<div class="form-group col-md-3 col-md-offset-0">
							<label for="nombre">Nombre (<i class="fa fa-asterisk" style="color: #DD4B39;"></i>)</label>
							<input class="form-control" name="nombre" id="nombre" type="text" value="{{ Input::old('nombre') }}">
						</div>
						<div class="form-group col-md-3 col-md-offset-0">
							<label for="referencia">Referencia</label>
							<input class="form-control" name="referencia" id="referencia" type="text" value="{{ Input::old('referencia') }}">
						</div>
						<div class="form-group col-md-3 col-md-offset-0">
							<label for="costo">Costo</label>
							<input class="form-control" name="costo" id="costo" type="text" value="{{ Input::old('costo')}}">
						</div>
						
						<div class="form-group col-md-3 col-md-offset-0">
							<label for="precio">Precio (<i class="fa fa-asterisk" style="color: #DD4B39;"></i>)</label>
							<input class="form-control" name="precio" id="precio" type="text" value="{{ Input::old('precio')}}">
						</div>
						</div>
						
						<div class="row">
						<div class="form-group col-md-3 col-md-offset-0">
							<label for="id_impuesto">Impuestos</label>
							<select class="form-control" name=id_impuesto[] id=id_impuesto multiple="multiple" style="display:none;">
								@foreach($impuestos as $impuesto)
									@if($impuesto->activa==1)
						        	<option value="{{ $impuesto->id_impuesto}}">{{ $impuesto->nombre }} ({{$impuesto->porcentaje}}%)</option>
						        	@endif
					        	@endforeach
		                  	</select>
						</div>						
						<div class="form-group col-md-3 col-md-offset-0">
							<label for="categoria">Categoria</label>
							<input type="hidden" name="id_categoria" id="categoriaIDModal" value="53">
							<input class="form-control" id="categoriaModal" type="text" value="Ventas" data-toggle="modal" data-target="#myModal" type="text" readonly="readonly">
						</div>
						
						<div class="form-group col-md-3 col-md-offset-0">
							<label for="descripcion">Descripcion</label>
							<textarea class="form-control" rows="1" name="descripcion" id="descripcion" >{{ Input::old('descripcion')}}</textarea>
						</div>
						<div class="form-group col-md-2 col-md-offset-0">
							<label>Inventariable</label>
							<div>
			                    <label><input type="checkbox" id="inventariable" name="inventariable" value="1" onclick="inventariableShow();"></label>
			                </div>
						</div>
						
						</div>
						<div class="row">
						<div class="form-group col-md-4 col-md-offset-0">
							<label>Imagen</label>
							<input id="imagen" name="imagen" type="file" class="file" data-show-upload="false" data-show-cancel="false" data-show-remove="false" data-allowed-file-extensions='["png", "jpeg", "jpg","bmp", "gif"]'>
						</div>
						<div class="form-group col-md-2 col-md-offset-0">
							<label for="referencia">Codigo de barras</label>
							<input class="form-control" name="codigoBarras" id="codigoBarras" type="text" onkeypress="return enter(event);">
						</div>

						
						<div class="form-group col-md-3 col-md-offset-3" id="divMedida" style="display: none;">
							<label for="id_medida" >Medida</label>
							<select class="form-control" name=id_medida id=id_medida>
								@foreach($medidas as $medida)
					        	<option value="{{ $medida->id_medida}}">{{ $medida->nombre }}</option>
					        	@endforeach
		                  	</select>
						</div>
						
						
						
						<div class="col-md-3 col-md-offset-2 divBodega" style="display: none;">
							@foreach($bodegas as $bodega)
							<label for="cantidad">Bodega</label>
							<input type="hidden" name="id_bodega[]" value="{{$bodega->id_bodega}}">
							<input class="form-control" type="text" readonly="" value="{{$bodega->nombre}}">
							@endforeach
						</div>
						<div class="col-md-3 col-md-offset-0 divCantidad" style="display: none;">
							@foreach($bodegas as $bodega)
							<label for="cantidad">Cantidad inicial</label>
							<input class="form-control" type="text" name="cantidad[]" id="cantidad">
							@endforeach
						</div>
						
						
						</div>
					</div>
					<div class="box-footer">
                		<div class="col-md-2 col-md-offset-10">
							<button type="submit" class="btn btn-primary btn-block btn-flat">Crear</button>
						</div>
						<input type="hidden" name="_token" value="{{ csrf_token() }}">
						<input type="hidden" name="id_empresa" value="{{ Auth::user()->id_empresa }}">

              		</div>
              		</form>
					
				</div>
				<!-- /.box -->
			</div>
		</div>
	</div>

	<!-- modal -->
	<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
		<div class="modal-dialog modal-lg" role="document">
	    	<div class="modal-content">
	      		<div class="modal-header">
	        		<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
	        		</button>
	        		<h4 class="modal-title" id="myModalLabel">Categorias</h4>	        
	      		</div>
	      
	    		<div class="modal-body">
	    			
	      			<div >
				    @foreach($categorias as $categoria) 
				    	@if( empty($categoria->id_categoriaP) && $categoria->id_categoria ==3)
				    	<ul>
	                		<li class="list-group-item justify-content-between"><i class="fa fa-plus-square-o" aria-hidden="true" style="cursor:pointer;" id="i{{$categoria->id_categoria}}" onclick="expandir({{$categoria->id_categoria}});"></i> {{$categoria->nombre}}
	                			<span class="badge badge-default badge-pill" style="background-color: white; padding: 0 0; ">
		                			<a class='btn' data-dismiss="modal" style='padding: 2px 10px; margin-right:4px;' onclick='seleccionarCategoria({{ $categoria->id_categoria }}, "{{ $categoria->nombre }}");'>
										<i class='fa fa-chevron-circle-up fa-lg' aria-hidden='true' style='background-color:white; color:black;'></i>
									</a>
								</span>
							</li>
							{!!subcategorias($categoria, $categorias, "items")!!}
							
	                	</ul>
				    	@endif
					@endforeach
					</div>
					
	      		</div>
				<div class="modal-footer">	      	

				</div>
	      
	    	</div>
		</div>
	</div>
	<!-- /modal -->

	<script type="text/javascript">
		
		$(function () {	
			$('#id_impuesto').css('display','');
		 	$('#id_impuesto').select2({theme: "bootstrap"});		
			$(".fa-plus-square-o").click();//automaticamente click en expandir categorias del modal
			$('form').submit(function(){//quitar puntos de los campos de costo y precio
				$('#costo').number(true, 2, '.', '');	
				$('#precio').number(true, 2, '.', '');	
			});
			//colocar formato a campos de texto	
			$('#costo').number(true, 2, '.', ',');
			$('#precio').number(true, 2, '.', ',');	
        });


		//funcion usada para mostrar o no opciones extras una vez checkeado inventariable
		function inventariableShow() {
			if($("#inventariable").is(":checked")){
				$(".divCantidad").css("display","");
				$("#divMedida").css("display","");
				$(".divBodega").css("display","");
				//$("#divCantidadCostoPrecio").css("display","");
			}
			else{
				$(".divCantidad").css("display","none");
				$("#divMedida").css("display","none");
				$(".divBodega").css("display","none");
				//$("#divCantidadCostoPrecio").css("display","none");
			}
		}

		//MODAL DE CATEGORIAS

		//funcion usada para expandir la categorias relacionadas a la expandida
		function expandir(id_categoriaP){
			if($( "."+id_categoriaP).css( "display")=="none"){
				$( "."+id_categoriaP).css( "display", "");
				$( "#i"+id_categoriaP ).attr( "class", "fa fa-minus-square-o");
			}
			else{
				$( "."+id_categoriaP).css( "display", "none");
				$( "#i"+id_categoriaP ).attr( "class", "fa fa-plus-square-o");
			}
		}

		function seleccionarCategoria(id_categoria, nombre){
			$("#categoriaIDModal").val(id_categoria);
			$("#categoriaModal").val(nombre);			
		}

		function enter(e) {//no permitir que al undir enter envie el formulario
    		if (e.keyCode == 13) {
		    	return false;
		    }
		}
	</script>
@endsection