@extends('adminlte::layouts.app')

@section('htmlheader_title')
	{{ trans('adminlte_lang::message.home') }}
@endsection


@section('main-content')
	<script type="text/javascript">
		$("#contentHeader2").append('<i class="fa fa-cubes"></i> Items');
	</script>

	<?php $permisos = $_SESSION['permisos']; ?>
	<div class="container-fluid spark-screen">
		<div class="row">
			<div class="col-md-12 col-md-offset-0">
				<!-- Default box -->
				<div class="box box-primary">
					<div class="box-header with-border">
						<h3 class="box-title">Items</h3>

						<div class="box-tools pull-right">
							<button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
								<i class="fa fa-minus"></i></button>
							<button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
								<i class="fa fa-times"></i></button>
						</div>
					</div>
					<div class="box-body">
						@if($permisos['12']->insertar==1)
						<div class="col-md-2 col-md-offset-0" style="margin-bottom: 10px;">
							<a class="btn btn-primary btn-block btn-flat" href="{{ url('nuevoItem')}}"><i class="fa fa-plus" aria-hidden="true"></i> Nuevo</a>
						</div>
						@endif
						

						<!--Errores y exito mensajes-->
						@if(count($errors)>0)
						<div class="form-group col-md-8 col-md-offset-2">
							<div class="alert alert-danger alert-dismissible">
				                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
				                <h4><i class="icon fa fa-ban"></i> Error en el formulario</h4>
				                @foreach ($errors->all() as $message) 
	    							<span > {{$message}}</span><br>
								@endforeach
				             </div>
						</div>
						@endif
						@if(session('mensaje'))
						<div class="form-group col-md-8 col-md-offset-2">
							<div class="alert alert-success alert-dismissible">
						        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
						        <h4><i class="icon fa fa-check"></i> {{ session('mensaje') }}</h4>
						    </div>
						</div>
						@endif


						<table class="table table-bordered table-hover dataTable" 
							data-toggle="table"
							data-pagination="true"
							data-search="true"
							data-show-refresh="true"
       						data-show-columns="true">
			                <thead>
				                <tr>
				                  <th>Nombre</th>
				                  <th>Referencia</th>
				                  <th>Costo</th>
				                  <th>Precio</th>
				                  <th>Disponibilidad</th>
				                  <th>Categoria</th>
				                  <th data-halign="center" data-align="center" data-width="17%">Acciones</th>
				                </tr>
				            </thead>
				        	<tbody>
				                @foreach($items as $item) 
				                	<tr>
						    			<td> {{$item->nombre }} 		</td>
						    			<td> {{$item->referencia }} 	</td>
						    			<td> {{number_format($item->costo,2) }} </td>
						    			<td> {{number_format($item->precio,2) }} </td>
						    			<td> {{$item->cantidad}} ({{$item->medida}}) </td>
						    			<td> {{$item->categoria }} </td>
						    			<td>
						    				@if($permisos['12']->ver==1)
						    				<a class="btn btn-success" style="padding: 2px 5px;" href="{{ url('/verItem/'.$item->id_item) }}">
  												<i class="fa fa-eye" aria-hidden="true"></i>
  											</a>
  											@endif
  											@if($permisos['12']->modificar==1)
											<a class="btn btn-primary" style="padding: 2px 5px;" href="{{ url('/editItem/'.$item->id_item) }}" aria-label="editar">
											  <i class="fa fa-pencil" aria-hidden="true"></i>
											</a>
											@endif
											@if($permisos['12']->eliminar==1)
											<a class="btn btn-danger" style="padding: 2px 5px;" href="{{ url('/eliminarItem/'.$item->id_item) }}" aria-label="Eliminar" id="eliminar">
											 <i class="fa fa-times" aria-hidden="true"></i>
											</a>
											@endif
										</td>
						    		</tr>
								@endforeach
			              	</tbody>	
              			</table>						
					</div>
					
				</div>
				<!-- /.box -->
			</div>
		</div>
	</div>

	
	
	<script type="text/javascript">
		
		$( document ).ready(function() {
			
			

  			$('#eliminar').click(function(event) {
		    	if(!confirm('¿Deseas realmente eliminar este Contacto?'))
					event.preventDefault();
			});

		});
	</script>
@endsection

