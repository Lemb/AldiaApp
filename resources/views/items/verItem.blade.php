@extends('adminlte::layouts.app')

@section('htmlheader_title')
	{{ trans('adminlte_lang::message.home') }}
@endsection


@section('main-content')
	<script type="text/javascript">
		$("#contentHeader1").append('<a href={{url("items")}}><i class="fa fa-cubes"></i> Items</a>');
		$("#contentHeader2").text('Detalle de Item');
	</script>
	<div class="container-fluid spark-screen">
		<div class="row">
			<div class="col-md-12 col-md-offset-0">
				<!-- Default box -->
				<div class="box box-primary">
					<div class="box-header with-border">
						<h3 class="box-title">Ver Item</h3>

						<div class="box-tools pull-right">
							<button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
								<i class="fa fa-minus"></i></button>
							<button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
								<i class="fa fa-times"></i></button>
						</div>
					</div>

					<div class="box-body">
						<div class="row">
						<div class="form-group col-md-3 col-md-offset-0">
							<label for="nombre">Nombre</label>
							<input class="form-control" name="nombre" id="nombre" type="text" value="{{ $item->nombre }}" disabled="disabled">
						</div>
						<div class="form-group col-md-3 col-md-offset-0">
							<label for="referencia">Referencia</label>
							<input class="form-control" name="referencia" id="referencia" type="text" value="{{ $item->referencia }}" disabled="disabled">
						</div>
						<div class="form-group col-md-3 col-md-offset-0">
							<label for="costo">Costo</label>
							<input class="form-control" name="costo" id="costo" type="text" value="{{ $item->costo}}" disabled="disabled">
						</div>
						
						<div class="form-group col-md-3 col-md-offset-0">
							<label for="precio">Precio</label>
							<input class="form-control" name="precio" id="precio" type="text" value="{{ $item->precio}}" disabled="disabled">
						</div>
						
						
						<div class="form-group col-md-3 col-md-offset-0">
							<label for="id_impuesto">Impuestos</label>
							<select class="form-control" name=id_impuesto[] multiple="multiple" id=id_impuesto disabled="disabled" style="display:none;">
								@foreach($impuestos as $impuesto)
					        	<option value="{{ $impuesto->id_impuesto}}">{{ $impuesto->nombre }} ({{ $impuesto->porcentaje }}%)</option>
					        	@endforeach
		                  	</select>
						</div>						
						<div class="form-group col-md-3 col-md-offset-0">
							<label for="categoria">Categoria</label>
							<input class="form-control" name="categoria" id="categoria" type="text" value="{{$item->nombreCategoria}}" disabled="disabled">
						</div>
						
						
						<div class="form-group col-md-3 col-md-offset-0">
							<label for="descripcion">Descripcion</label>
							<textarea class="form-control" rows="1" name="descripcion" id="descripcion" disabled="disabled">{{$item->descripcion}}</textarea>
						</div>
						<div class="form-group col-md-3 col-md-offset-0">
							<label>Inventariable</label>
							<div>
			                    <label><input type="checkbox" id="inventariable" name="inventariable" value="1" disabled="disabled"></label>
			                </div>
						</div>
						</div>

						<div class="row">
						<div class="form-group col-md-4 col-md-offset-0">
								<label>Imagen</label>
								<img class="img-responsive pad" src="{{ URL::to('/').'/storage/imagenes/'.$item->id_item.$item->imagen }}" alt="Imagen" style="box-shadow: 1px 1px 2px;">
						</div>
						<div class="form-group col-md-2 col-md-offset-0">
							<label for="referencia">Codigo de barras</label>
							<input class="form-control" name="codigoBarras" id="codigoBarras" type="text" value="{{$item->codigoBarras}}" disabled=""><br>
							<div id="barcode"></div>
						</div>	
						<div class="form-group col-md-3 col-md-offset-3" id="divMedida" style="display: none;">
							<label for="id_medida">Medida</label>
							<select class="form-control" name="id_medida" id="id_medida" disabled="disabled">
								@foreach($medidas as $medida)
					        	<option value="{{ $medida->id_medida}}" >{{ $medida->nombre }}</option>
					        	@endforeach
		                  	</select>
						</div>
						<div class="col-md-3 col-md-offset-0 divBodega" style="display: none;">
							@foreach($itemsXbodegas as $itemXbodega)
							<label for="cantidad">Bodega</label>
							<input type="hidden" name="id_bodega[]" value="{{$itemXbodega->id_bodega}}">
							<input class="form-control" type="text" disabled="" value="{{$itemXbodega->bodegaNombre}}">
							@endforeach
						</div>
						<div class="col-md-3 col-md-offset-0 divCantidad" style="display: none;">
							@foreach($itemsXbodegas as $itemXbodega)
							<label for="cantidad">Cantidad inicial</label>
							<input class="form-control" type="text" name="cantidad[]" disabled="" id="cantidad" value="{{$itemXbodega->cantidad}}">
							@endforeach
						</div>
						
						<div class="form-group col-md-3 col-md-offset-9 divCantidad" style="display: none;">
							<label for="cantidad">Total</label>
							<input class="form-control" type="text" name="cantidad" id="cantidad" value="{{ $item->cantidad}}" disabled="disabled">
						</div>	
						</div>
						
					</div>
				
					
				</div>
				<!-- /.box -->
			</div>
		</div>
	</div>

	<script type="text/javascript">


		$( document ).ready(function() {
			var settings = {
          		output:'bmp'
       		};
			$("#barcode").barcode(
				"{{$item->codigoBarras}}", // Valor del codigo de barras
				"code128", // tipo (cadena)
				settings
			);

			//IMPUESTOS
			$("#id_impuesto").css("display","");
			var id_impuestos = [];
			@foreach($itemsXimpuestos as $itemXimpuesto)
				id_impuestos.push({{$itemXimpuesto->id_impuesto}});
			@endforeach
			$('#id_impuesto').select2({theme: "bootstrap"});
		    $('#id_impuesto').val(id_impuestos);
			$('#id_impuesto').trigger('change');
			//FIN IMPUESTOS

			
			document.getElementById("id_medida").value = {{ $item->id_medida }};
			document.getElementById("inventariable").checked = {{$item->inventariable}};

		 	
			
			if($("#inventariable").is(":checked")){
				$(".divCantidad").css("display","");
				$("#divMedida").css("display","");
				$(".divBodega").css("display","");
			}
			else{
				$(".divCantidad").css("display","none");
				$(".divMedida").css("display","none");
				$(".divBodega").css("display","none");
			}
			

		});
		
	</script>
@endsection