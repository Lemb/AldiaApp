
@extends('adminlte::layouts.app')

@section('htmlheader_title')
	{{ trans('adminlte_lang::message.home') }}
@endsection


@section('main-content')
	<script type="text/javascript">
		$("#contentHeader2").append('<i class="fa fa-folder-open"></i> Categorias');
	</script>
	@include('adminlte::layouts.partials.functions')
	<?php 
    	$permisos = $_SESSION['permisos']; 
	?>
	<div class="container-fluid spark-screen">
		<div class="row">
			<div class="col-md-12 col-md-offset-0">
				<!-- Default box -->
				<div class="box">
					<div class="box-header with-border">
						<h3 class="box-title">Categorias</h3>

						<div class="box-tools pull-right">
							<button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
								<i class="fa fa-minus"></i></button>
							<button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
								<i class="fa fa-times"></i></button>
						</div>
					</div>
					<div class="box-body">
						@if(count($errors)>0)
						<div class="form-group col-md-12 col-md-offset-0">
							<div class="alert alert-danger alert-dismissible">
				                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
				                <h4><i class="icon fa fa-ban"></i> Error en el formulario</h4>
				                @foreach ($errors->all() as $message) 
	    							<span > {{$message}}</span><br>
								@endforeach
				             </div>
						</div>
						@endif
						@if(session('mensaje'))
						<div class="form-group col-md-12 col-md-offset-0">
							<div class="alert alert-success alert-dismissible">
						        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
						        <h4><i class="icon fa fa-check"></i> {{ session('mensaje') }}</h4>
						    </div>
						</div>
						@endif
						&nbsp;
						
		                @foreach($categorias as $categoria) 
		                	@empty($categoria->id_categoriaP)
		                	<ul>
		                		<li class="list-group-item justify-content-between"><i class="fa fa-plus-square-o" aria-hidden="true" style="cursor:pointer;" id="i{{$categoria->id_categoria}}" onclick="expandir({{$categoria->id_categoria}});"></i> {{$categoria->nombre}}
		                			<span class="badge" style="background-color: #222d32; padding: 0 0;">
		                				@if($permisos['9']->insertar==1)
			                			<a class="btn btn-success" style="padding: 2px 10px;" data-toggle="modal" data-target="#myModal" onclick="modal({{$categoria->id_categoria}}, '{{$categoria->nombre}}');">
												<i class="fa fa-plus" aria-hidden="true"></i>
										</a>
										@endif
									</span>
								</li>
								{!!subcategorias($categoria, $categorias, "categorias")!!}
								
		                	</ul>
		                	@endempty
						@endforeach
			              	
              					
					</div>
					
				</div>
				<!-- /.box -->
			</div>
		</div>
	</div>



	<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	  <div class="modal-dialog" role="document">
	    <div class="modal-content">
	      <div class="modal-header">
	        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
	        <h4 class="modal-title" id="myModalLabel">Nueva categoria</h4>
	        <span id="categoriaNombre"></span>
	      </div>
	      <form method="post" action="{{ url('/nuevaCategoria') }}" id="form">
	      <div class="modal-body">
	      	<div class="row">
	      		<div class="form-group col-md-10 col-md-offset-1">
					<label for="nombre">Nombre</label> (<i class="fa fa-asterisk" style="color: #DD4B39;"></i>)
					<input class="form-control" name="nombre" id="nombre" type="text" value="">
				</div>
			</div>
	      </div>
	      <div class="modal-footer">	      	
	        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
	        <input type="submit" class="btn btn-primary" value="Crear" id="submit">
	        
	        <input type="hidden" name="id_categoriaP" id="id_categoriaP">
	        <input type="hidden" name="_token" value="{{ csrf_token() }}">						
			<input type="hidden" name="id_empresa" value="{{ Auth::user()->id_empresa }}">
	      </div>
	      </form>
	    </div>
	  </div>
	</div>
	

	<script type="text/javascript">
		var nuevaCategoriaLink = "{{ url('/nuevaCategoria') }}";
		var editCategoriaLink = "{{ url('/editCategoria') }}";
	</script>
	<script type="text/javascript" src="{{ asset('/js/categorias/categorias.js') }}"></script>
@endsection

