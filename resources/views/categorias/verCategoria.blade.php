@extends('adminlte::layouts.app')

@section('htmlheader_title')
	{{ trans('adminlte_lang::message.home') }}
@endsection


@section('main-content')	
	<script type="text/javascript">
		$("#contentHeader1").append('<a href={{url("pagosI")}}><i class="fa fa-folder-open"></i> Categorias</a>');
		$("#contentHeader2").text('Detalle de categoria');
	</script>

	<?php 
    	$permisos = $_SESSION['permisos']; 
	?>
	<div class="container-fluid spark-screen">
		<div class="row">
			<div class="col-md-12 col-md-offset-0">
				<!-- Default box -->
				<div class="box box-primary">
					<div class="box-header with-border">
						
						<h3 class="box-title">Ver Categoria</h3>
						
						<div class="box-tools pull-right">
							<button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
								<i class="fa fa-minus"></i></button>
							<button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
								<i class="fa fa-times"></i></button>
						</div>
					</div>

					
					<div class="box-body">

						<div class="form-group col-md-4 col-md-offset-0">
							<label for="nombre">Nombre</label>
							<input class="form-control" id="nombre" type="text" value="{{ $categoria->nombre}}" disabled="">
						</div>

						<div class="form-group col-md-4 col-md-offset-0">
							<label for="nombre">Categoria Padre</label>
							<input class="form-control" id="nombrePadre" type="text" value="{{ $categoria->nombrePadre}}" disabled="">
						</div>
						<div class="form-group col-md-4 col-md-offset-0">
							<?php $checked = ($categoria->deleted_at !=null) ? 'checked=checked':'';?>
							<label for="eliminadoPregunta">¿Eliminado?</label><br>
							<input type="checkbox" value="1" {{$checked}} disabled="disabled">
						</div>
						
						@if(isset($permisos[18]) && $permisos[18]->ver==1)
						<br>
						<h4 class="box-title form-group col-md-12 col-md-offset-0">Versiones</h3>
						<table class="table table-bordered table-hover dataTable" 
							data-toggle="table"
							data-pagination="true"       						
							>
			                <thead>
				                <tr>
				                  	<th>Categoria</th>
				                  	<th>Fecha</th>
				                  	<th>Version</th>
				                  	<th>Responsable</th>
				                  	<th>Detalle</th>
				                </tr>
				            </thead>
				        	<tbody>
				                @foreach($versiones as $version) 
				                	<?php  
				                		$referenciaPrincipal = ($version->id_categoria==$categoria->id_categoria) ? 'id=referenciaPrincipal' : ''; 
				                	?>
						    		<tr {{$referenciaPrincipal}}>
						    			<td> {{$version->nombre }} 		</td>
						    			<td> {{$version->updated_at }} </td>
						    			<td> {{$version->numeroVersion }} </td>
						    			<td> <a href="{{ url('/verUsuario/'.$version->responsableID) }}">  {{$version->responsableNombre.' ('.$version->responsableIdentificacion.')' }}</a> </td>
						    			<td> 
						    				<a class="btn btn-info" style="padding: 2px 10px;" href="{{ url('/verCategoria/'.$version->id_categoria) }}" aria-label="Ver Detalle">
  												<i class="fa fa-eye" aria-hidden="true"></i>
  											</a>
						    			</td>
						    		</tr>
								@endforeach
			              	</tbody>	
              			</table>
              			@endif					
						
					</div>
					
				</div>
				<!-- /.box -->
			</div>
		</div>
	</div>

	<script type="text/javascript">
		
		$( document ).ready(function() {
			$("#referenciaPrincipal").css("background-color", "#DDFCDA");
		});
	</script>
@endsection