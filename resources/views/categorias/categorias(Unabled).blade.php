
@extends('adminlte::layouts.app')

@section('htmlheader_title')
	{{ trans('adminlte_lang::message.home') }}
@endsection


@section('main-content')
	
	<div class="container-fluid spark-screen">
		<div class="row">
			<div class="col-md-12 col-md-offset-0">
				<!-- Default box -->
				<div class="box box-primary">
					<div class="box-header with-border">
						<h3 class="box-title">Categorias</h3>

						<div class="box-tools pull-right">
							<button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
								<i class="fa fa-minus"></i></button>
							<button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
								<i class="fa fa-times"></i></button>
						</div>
					</div>
					<div class="box-body">
						@if(count($errors)>0)
						<div class="form-group col-md-12 col-md-offset-0">
							<div class="alert alert-danger alert-dismissible">
				                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
				                <h4><i class="icon fa fa-ban"></i> Error en el formulario</h4>
				                @foreach ($errors->all() as $message) 
	    							<span > {{$message}}</span><br>
								@endforeach
				             </div>
						</div>
						@endif
						@if(session('mensaje'))
						<div class="form-group col-md-12 col-md-offset-0">
							<div class="alert alert-success alert-dismissible">
						        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
						        <h4><i class="icon fa fa-check"></i> {{ session('mensaje') }}</h4>
						    </div>
						</div>
						@endif						
						<table id="example1" class="table table-hover">
			                <thead>
				                <tr id="headTR">
				                  <th style="width: 2%;"></th>
				                  <th>Nombre</th>
				                  <th style="width: 17%;">Acciones</th>
				                  
				                </tr>
				            </thead>
				        	<tbody>
				                @foreach($categorias as $categoria) 
				                	@if(empty($categoria->id_categoriaP))
						    		<tr id="{{ $categoria->id_categoria }}" >
						    			<?php $plus = false; ?>
						    			@foreach($categorias as $categoria2)
						    				@if($categoria->id_categoria == $categoria2->id_categoriaP && $plus==false)
						    					<?php $plus=true;?>
						    				@endif
						    			@endforeach

						    			@if($plus)
						    			<td><i id='i{{ $categoria->id_categoria }}' class="fa fa-plus-square-o" aria-hidden="true" onclick="expandir({{ $categoria->id_categoria }});"></i></td>
						    			@else
						    			<td></td>
						    			@endif
						    			<td class="nombreTD" > {{$categoria->nombre }} 		</td>				  	
						    			<td>
						    				<a class="btn btn-success" style="padding: 2px 10px;" data-toggle="modal" data-target="#myModal" onclick="modal({{$categoria->id_categoria}}, '{{$categoria->nombre}}');">
  												<i class="fa fa-plus" aria-hidden="true"></i>
  											</a>
  											@if($categoria->id_empresa!=1)
											<a class="btn btn-primary" style="padding: 2px 10px;" href="{{ url('/editCategoria/'.$categoria->id_categoria) }}" aria-label="editar">
												<i class="fa fa-pencil" a
												ria-hidden="true"></i>
											</a>
											<a class="btn btn-danger" style="padding: 2px 10px;" href="{{ url('/eliminarCategoria/'.$categoria->id_categoria.'/'.$categoria->id_empresa) }}" aria-label="Eliminar" id="eliminar">
											 <i class="fa fa-times" aria-hidden="true"></i>
											</a>
											@endif
										</td>
										
						    		</tr>
						    		@endif
								@endforeach
			              	</tbody>	
              			</table>

              					
					</div>
					
				</div>
				<!-- /.box -->
			</div>
		</div>
	</div>



	<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	  <div class="modal-dialog" role="document">
	    <div class="modal-content">
	      <div class="modal-header">
	        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
	        <h4 class="modal-title" id="myModalLabel">Nueva categoria</h4>
	        <span id="categoriaNombre"></span>
	      </div>
	      <form method="post" action="{{ url('/nuevaCategoria') }}" id="form">
	      <div class="modal-body">
	      	<div class="row">
	      		<div class="form-group col-md-10 col-md-offset-1">
					<label for="nombre">Nombre</label>
					<input class="form-control" name="nombre" id="nombre" type="text" value="">
				</div>
			</div>
	      </div>
	      <div class="modal-footer">	      	
	        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
	        <input type="submit" class="btn btn-primary" value="Crear" id="submit">
	        
	        <input type="hidden" name="id_categoriaP" id="id_categoriaP">
	        <input type="hidden" name="_token" value="{{ csrf_token() }}">						
			<input type="hidden" name="id_empresa" value="{{ Auth::user()->id_empresa }}">
	      </div>
	      </form>
	    </div>
	  </div>
	</div>

	<script type="text/javascript">
		
		$( document ).ready(function() {
			$('#eliminar').click(function(event) {
		    	if(!confirm('¿Deseas realmente eliminar esta categoria?'))
					event.preventDefault();
			});		    

			nivell=0;//nivel inicial
			@foreach($categorias as $categoria)
				//determinar si la categoria tiene subcategorias
				<?php $plus = false; ?>
				@foreach($categorias as $categoria2)
					@if($categoria->id_categoria == $categoria2->id_categoriaP && $plus==false)
						<?php $plus=true;?>
					@endif
				@endforeach

				//agregar la tabulacion basado en la jerarquia de la categoria
				td="";
				for(i=0; i<padres({{$categoria->id_categoriaP}});i++){
					td=td+"<td></td>";
				}

				//basado en si tiene o no subcategorias agregar el expand o simplemente la tabulacion
				@if($plus)
 				plus = td+"<td><i id='i{{ $categoria->id_categoria }}'class='fa fa-plus-square-o' aria-hidden='true' onclick='expandir({{$categoria->id_categoria}});'></i></td>";
    			@else
    			plus = td+"<td></td>";
    			@endif
    			
    			//usado para determinar el subnivell maximo y usarlo despues
    			if(nivell<padres({{$categoria->id_categoriaP}}))
    				nivell = padres({{$categoria->id_categoriaP}});
    			

    		//solo si la categoria tiene padre
			@if(!empty($categoria->id_categoriaP)) 
			$( "<tr id='{{ $categoria->id_categoria }}' style='display:none;' class='{{$categoria->id_categoriaP}}'>"+
							plus+
			    			"<td class='c"+padres({{$categoria->id_categoriaP}})+"'>{{$categoria->nombre}}</td>"+
			    			"<td data-halign='center' data-align='center' data-width='17%'>"+
			    				"<a class='btn btn-success' style='padding: 2px 10px; margin-right:4px;' data-toggle='modal' data-target='#myModal' onclick='modal({{$categoria->id_categoria}}, \"{{$categoria->nombre}}\");'>"+
										"<i class='fa fa-plus' aria-hidden='true'></i>"+
									"</a>"+
								@if($categoria->id_empresa!=1)
								"<a class='btn btn-primary' style='padding: 2px 10px; margin-right:4px;' data-toggle='modal' data-target='#myModal' onclick='modal({{$categoria->id_categoria}}, \"{{$categoria->nombre}}\", true);' aria-label='editar'>"+
								  "<i class='fa fa-pencil' aria-hidden='true'></i>"+
								"</a>"+
								"<a class='btn btn-danger' style='padding: 2px 10px; margin-right:4px;' href='{{url('/eliminarCategoria/'.$categoria->id_categoria.'/'.$categoria->id_empresa)}}' aria-label='Eliminar' id='eliminar'>"+
								 "<i class='fa fa-times' aria-hidden='true'></i>"+
								"</a>"+
								@endif
							"</td>"+
			    		"</tr>" ).insertAfter( "#{{$categoria->id_categoriaP}}" );//usado para insertar despues del padre
			@endif
			@endforeach

			//agregar ths basado en el subnivel maximo
			//ademas agregar colspan basado en el subnivel actual
			th="";
			for(i=0;i<nivell;i++){
				th=th+"<th style='width: 2%;'></th>";
				$( ".c"+i ).attr( "colspan", nivell+1-i);	
			}
			//usados para modificar el colspan de los padres primeros
			$( ".nombreTD" ).attr( "colspan", nivell+1);
			$( "#headTR" ).prepend(th);//agregar los th necesarios

			
		});
		//funcion usada para expandir la categorias relacionadas a la expandida
		function expandir(id_categoriaP){
			if($( "."+id_categoriaP).css( "display")=="none"){
				$( "."+id_categoriaP).css( "display", "");
				$( "#i"+id_categoriaP ).attr( "class", "fa fa-minus-square-o");
			}
			else{
				$( "."+id_categoriaP).css( "display", "none");
				$( "#i"+id_categoriaP ).attr( "class", "fa fa-plus-square-o");
			}
		}
		//funcion utilizada para retornar cuantas anidaciones tiene una categoria en este caso seria
		//la categoria hija preguntando por el padre del padre hasta no encontrar
		function padres(id_categoriaP){
			nivel = 0;
			@foreach($categorias as $categoria)	
				if(id_categoriaP == {{$categoria->id_categoria}}){
					nivel++;
					@if($categoria->id_categoriaP!=null)
						nivel=nivel+padres({{$categoria->id_categoriaP}});
					@endif
				}
			@endforeach

			return nivel;
		}

		
		//funcion usada para pasarle parametros al modal
		function modal(id_categoria, nombre, edit=false){
			if(edit){
				$("#categoriaNombre").text("");
				$("#myModalLabel").text("Editar categoria");	

				$("#nombre").attr("value",nombre);
				$("#id_categoriaP").attr("name","id_categoria");				
				$("#submit").attr("value","Actualizar");
				$("#form").attr("action","{{ url('/editCategoria') }}");
			}
			else{
				$("#categoriaNombre").text("En "+nombre);
				$("#myModalLabel").text("Nueva categoria");	

				$("#nombre").attr("value","");
				$("#id_categoriaP").attr("name","id_categoriaP");				
				$("#submit").attr("value","Crear");
				$("#form").attr("action","{{ url('/nuevaCategoria') }}");
			}
			
			$("#id_categoriaP").attr("value", id_categoria);
		}
	</script>
@endsection

