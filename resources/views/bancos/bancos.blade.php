@extends('adminlte::layouts.app')

@section('htmlheader_title')
	{{ trans('adminlte_lang::message.home') }}
@endsection


@section('main-content')
	<script type="text/javascript">
		$("#contentHeader2").append('<i class="fa fa-money"></i> Bancos');
	</script>
	<?php $permisos = $_SESSION['permisos']; ?>
	<div class="container-fluid spark-screen">
		<div class="row">
			<div class="col-md-12 col-md-offset-0">
				<!-- Default box -->
				<div class="box box-primary">
					<div class="box-header with-border">
						<h3 class="box-title">Bancos</h3>

						<div class="box-tools pull-right">
							<button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
								<i class="fa fa-minus"></i></button>
							<button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
								<i class="fa fa-times"></i></button>
						</div>
					</div>
					<div class="box-body">
						@if($permisos['10']->insertar==1)
						<div class="col-md-2 col-md-offset-0" style="margin-bottom: 10px;">
							<a class="btn btn-primary btn-block btn-flat" data-toggle="modal" data-target="#myModal" onclick="modal('', '', 1, '', '', '', false);" ><i class="fa fa-plus" aria-hidden="true"></i> Nuevo</a>
						</div>
						@endif
						@if(count($errors)>0)
						<div class="form-group col-md-8 col-md-offset-2">
							<div class="alert alert-danger alert-dismissible">
				                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
				                <h4><i class="icon fa fa-ban"></i> Error en el formulario</h4>
				                @foreach ($errors->all() as $message) 
	    							<span > {{$message}}</span><br>
								@endforeach
				             </div>
						</div>
						@endif
						@if(session('mensaje'))
						<div class="form-group col-md-8 col-md-offset-2">
							<div class="alert alert-success alert-dismissible">
						        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
						        <h4><i class="icon fa fa-check"></i> {{ session('mensaje') }}</h4>
						    </div>
						</div>
						@endif

						<table class="table table-bordered table-hover dataTable" 
							data-toggle="table"
							data-pagination="true"
							data-search="true"
							data-show-refresh="true"
       						data-show-columns="true">
			                <thead>
				                <tr>
				                  <th>Nombre</th>
				                  <th>Tipo</th>
				                  <th>Descripcion</th>
				                  <th>Saldo</th>
				                  <th data-halign="center" data-align="center" data-width="17%">Acciones</th>
				                </tr>
				            </thead>
				        	<tbody>
				        		<?php $saldoTotal = 0; ?>
				                @foreach($bancos as $banco) 
						    		<tr>
						    			<td> {{$banco->nombre }} 		</td>
						    			@if($banco->tipo==1)
						    			<td>Banco</td>
						    			@elseif($banco->tipo==2)
						    			<td>Efectivo</td>
						    			@elseif($banco->tipo==3)
						    			<td>Tarjeta de credito</td>
						    			@endif
						    			<td> {{$banco->descripcion }} </td>
						    			<td> {{number_format($banco->saldo,2) }} </td>
						    			<td>

						    				@if($permisos['18']->ver==1)
											<a class="btn btn-success" style="padding: 2px 10px;"  href="{{ url('/verBanco/'.$banco->id_banco) }}" aria-label="ver">
											  <i class="fa fa-eye" aria-hidden="true"></i>
											</a>
											@endif

						    				@if($permisos['10']->modificar==1)
											<a class="btn btn-primary" style="padding: 2px 10px;" data-toggle="modal" data-target="#myModal" onclick="modal({{$banco->id_banco}}, '{{$banco->nombre}}', {{$banco->tipo}}, '{{$banco->descripcion}}', '{{$banco->predeterminado}}', {{$banco->saldo}},true);" aria-label="editar">
											  <i class="fa fa-pencil" aria-hidden="true"></i>
											</a>
											@endif
											@if($permisos['10']->eliminar==1)
											<a class="btn btn-danger" style="padding: 2px 10px;" href="{{ url('/eliminarBanco/'.$banco->id_banco) }}" aria-label="Eliminar" id="eliminar">
											 <i class="fa fa-times" aria-hidden="true"></i>
											</a>
											@endif
										</td>
						    		</tr>
						    		<?php $saldoTotal = $saldoTotal+$banco->saldo; ?>
								@endforeach
			              	</tbody>	
              			</table>

              			<!-- saldo box -->
              			<br>
              			<div class="col-md-3 col-sm-6 col-xs-12 col-md-offset-9">
				          <div class="info-box" style="box-shadow: 1px 1px 5px #888888;">
				            <span class="info-box-icon bg-green"><i class="fa fa-money" aria-hidden="true"></i></span>

				            <div class="info-box-content">
				              <span class="info-box-text">Saldo total</span>
				              <span class="info-box-number">{{ number_format($saldoTotal,2) }}</span>
				            </div>
				            <!-- /.info-box-content -->
				          </div>
				          <!-- /.info-box -->
				        </div>						
				          <!-- /.saldo box -->
					</div>
					
				</div>
				<!-- /.box -->
			</div>
		</div>
	</div>
	





	<!-- modal -->
	<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	  <div class="modal-dialog" role="document">
	    <div class="modal-content">
	      <div class="modal-header">
	        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
	        <h4 class="modal-title" id="myModalLabel">Nuevo Banco</h4>	        
	      </div>
	      <form method="post" action="{{ url('/nuevoBanco') }}" id="form">
	      <div class="modal-body">
	      	<p class="help-block">Campos marcados con asterisco (<i class="fa fa-asterisk" style="color: #DD4B39;"></i>) son obligatorios.</p>
	      	<div class="row">
	      		<div class="form-group col-md-6 col-md-offset-0">
					<label for="nombre">Nombre</label> (<i class="fa fa-asterisk" style="color: #DD4B39;"></i>)
					<input class="form-control" name="nombre" id="nombre" type="text" value="">
				</div>
				<div class="form-group col-md-6 col-md-offset-0">
					<label for="tipo">Tipo</label>
					<select class="form-control" name="tipo" id="tipo">
			        	<option value="1">Banco</option>
			        	<option value="2">Efectivo</option>
			        	<option value="3">Tarjeta de credito</option>
                  	</select>
				</div>
				<div class="form-group col-md-6 col-md-offset-0">
					<label for="descripcion">Descripcion</label>
					<textarea class="form-control" rows="3" name="descripcion" id="descripcion" placeholder="La razon de ser de tu desposito"></textarea>
				</div>

				<div class="form-group col-md-6 col-md-offset-0" id="saldoDiv">
					<label for="saldo">Saldo inicial</label> (<i class="fa fa-asterisk" style="color: #DD4B39;"></i>)
					<input class="form-control" name="saldo" id="saldo" type="text" value="" placeholder="Sin signos intermedios" step="any" >
				</div>
				<div class="form-group col-md-6 col-md-offset-0">
	                <label>
                      <input type="checkbox" name="predeterminado" value="1" id="predeterminado">
                      Predeterminado
                    </label>
              	</div>

               
                
			</div>
	      </div>
	      <div class="modal-footer">	      	
	        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
	        <input type="submit" class="btn btn-primary" value="Crear" id="submit">
	        
	        <input type="hidden" name="id_banco" id="id_banco">
	        <input type="hidden" name="_token" value="{{ csrf_token() }}">						
			<input type="hidden" name="id_empresa" value="{{ Auth::user()->id_empresa }}">
	      </div>
	      </form>
	    </div>
	  </div>
	</div>
	<!-- /modal -->


	<script type="text/javascript">
		
		$( document ).ready(function() {
  			$('#eliminar').click(function(event) {
		    	if(!confirm('¿Deseas realmente eliminar este banco?'))
					event.preventDefault();
			});

			$('form').submit(function(){
				$('#saldo').number(true, 2, '.', '');
			});
			$('#saldo').number(true, 2, '.', ',');
		});

		//funcion usada para pasarle parametros al modal
		function modal(id_banco,nombre,tipo,descripcion,predeterminado,saldo,edit){
			
			if(edit){
				$("#myModalLabel").text("Editar banco");	
				$("#submit").attr("value","Actualizar");
				$("#form").attr("action","{{ url('/editBanco') }}");
				$("#saldoDiv").css("display","none");
			}
			else{
				$("#myModalLabel").text("Nuevo banco");	
				$("#submit").attr("value","Crear");
				$("#form").attr("action","{{ url('/nuevoBanco') }}");
				$("#saldoDiv").css("display","");
			}


			predeterminado = (predeterminado != 1) ? false : true;//checkear checkbox
			$("#id_banco").val(id_banco);
			$("#nombre").val(nombre);
			document.getElementById("tipo").value =  tipo; 
			$("#descripcion").val(descripcion);
			$("#predeterminado").prop("checked", predeterminado);
			$("#saldo").val(saldo);
		}
	</script>
@endsection

