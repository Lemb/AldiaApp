@extends('adminlte::layouts.app')

@section('htmlheader_title')
	{{ trans('adminlte_lang::message.home') }}
@endsection


@section('main-content')
	<script type="text/javascript">
		$("#contentHeader1").append('<a href={{url("bancos")}}><i class="fa fa-money"></i> Bancos</a>');
		$("#contentHeader2").text('Detalle de banco');
	</script>
	<?php 
    	$permisos = $_SESSION['permisos']; 
	?>
	<div class="container-fluid spark-screen">
		<div class="row">
			<div class="col-md-12 col-md-offset-0">
				<!-- Default box -->
				<div class="box box-primary">
					<div class="box-header with-border">
						
						<h3 class="box-title">Ver Banco</h3>
						
						<div class="box-tools pull-right">
							<button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
								<i class="fa fa-minus"></i></button>
							<button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
								<i class="fa fa-times"></i></button>
						</div>
					</div>

					
					<div class="box-body">

						<div class="form-group col-md-4 col-md-offset-0">
							<label for="nombre">Nombre</label>
							<input class="form-control" name="nombre" id="nombre" type="text" value="{{$banco->nombre}}" disabled="disabled">
						</div>
						<div class="form-group col-md-4 col-md-offset-0">
							<label for="tipo">Tipo</label>
							<select class="form-control" name="tipo" id="tipo" disabled="disabled">
								<?php  
								$selected1 = ($banco->tipo==1) ? 'selected=selected':''; 
								$selected2 = ($banco->tipo==2) ? 'selected=selected':''; 
								$selected3 = ($banco->tipo==3) ? 'selected=selected':''; 
								?>
					        	<option value="1" {{$selected1}}>Banco</option>
					        	<option value="2" {{$selected2}}>Efectivo</option>
					        	<option value="3" {{$selected3}}>Tarjeta de credito</option>
		                  	</select>
						</div>
						

						<div class="form-group col-md-4 col-md-offset-0" id="saldoDiv">
							<label for="saldo">Saldo</label>
							<input class="form-control" name="saldo" id="saldo" type="text" value="{{$banco->saldo}}" step="any" disabled="disabled">
						</div>
						<div class="form-group col-md-4 col-md-offset-0">
							<label for="descripcion">Descripcion</label>
							<textarea class="form-control" rows="3" disabled="disabled">{{$banco->descripcion}}</textarea>
						</div>
						<div class="form-group col-md-4 col-md-offset-0">
			                <label>
			                	<?php $checked = ($banco->predeterminado == 1) ? 'checked=checked': ''; ?>
			                	<label for="eliminadoPregunta">Predeterminado</label><br>
		                      	<input type="checkbox" value="1" {{$checked}} disabled="disabled">	
		                    </label>
		              	</div>
						<div class="form-group col-md-4 col-md-offset-0">
							<?php $checked = ($banco->deleted_at !=null) ? 'checked=checked':'';?>
							<label for="eliminadoPregunta">¿Eliminado?</label><br>
							<input type="checkbox" value="1" {{$checked}} disabled="disabled">
						</div>
						<br>

						
						@if(isset($permisos[18]) && $permisos[18]->ver==1)
						<h4 class="box-title form-group col-md-12 col-md-offset-0">Versiones</h3>
						<table class="table table-bordered table-hover dataTable" 
							data-toggle="table"
							data-pagination="true"       						
							>
			                <thead>
				                <tr>
				                  	<th>Banco</th>
				                  	<th>Fecha</th>
				                  	<th>Version</th>
				                  	<th>Responsable</th>
				                  	<th>Detalle</th>
				                </tr>
				            </thead>
				        	<tbody>
				                @foreach($versiones as $version) 
				                	<?php  
				                		$referenciaPrincipal = ($version->id_banco==$banco->id_banco) ? 'id=referenciaPrincipal' : ''; 
				                	?>
						    		<tr {{$referenciaPrincipal}}>
						    			<td> {{$version->nombre }} 		</td>
						    			<td> {{$version->updated_at }} </td>
						    			<td> {{$version->numeroVersion }} </td>
						    			<td> <a href="{{ url('/verUsuario/'.$version->responsableID) }}">  {{$version->responsableNombre.' ('.$version->responsableIdentificacion.')' }}</a> </td>
						    			<td> 
						    				<a class="btn btn-info" style="padding: 2px 10px;" href="{{ url('/verBanco/'.$version->id_banco) }}" aria-label="Ver Detalle">
  												<i class="fa fa-eye" aria-hidden="true"></i>
  											</a>
						    			</td>
						    		</tr>
								@endforeach
			              	</tbody>	
              			</table>
              			@endif					
						
					</div>
					
				</div>
				<!-- /.box -->
			</div>
		</div>
	</div>

	<script type="text/javascript">
		
		$( document ).ready(function() {
			$("#referenciaPrincipal").css("background-color", "#DDFCDA");
		});
	</script>
@endsection

