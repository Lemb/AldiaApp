<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        @yield('contentheader_title', 'Aldia')
        <small>@yield('contentheader_description')</small>
    </h1>
    <ol class="breadcrumb">
        <li id="contentHeader1"></li>
        <li id="contentHeader2" class="active"></li>
    </ol>
</section>