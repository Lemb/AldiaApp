<!-- REQUIRED JS SCRIPTS -->

<!-- JQuery and bootstrap are required by Laravel 5.3 in resources/assets/js/bootstrap.js-->
<!-- Laravel App -->
<script src="{{ url (mix('/js/app.js')) }}" type="text/javascript"></script>


<!--  JQUERY UI-->
<link rel="stylesheet" type="text/css" href="{{ asset('/jquery-ui/jquery-ui.min.css') }}">  
<script src="{{ asset('/jquery-ui/jquery-ui.min.js') }}"></script>

<!--  SELECT2-->
<link rel="stylesheet" type="text/css" href="{{ asset('/select2/css/select2.min.css') }}">  
<script src="{{ asset('/select2/js/select2.full.min.js') }}"></script>
<link rel="stylesheet" type="text/css" href="{{ asset('/select2/css/select2-bootstrap.min.css') }}">  


<!-- MAGNIFICPOPUP-->
<link rel="stylesheet" type="text/css" href="{{ asset('/Magnific-Popup-master/dist/magnific-popup.css') }}">  
<script src="{{ asset('/Magnific-Popup-master/dist/jquery.magnific-popup.min.js') }}"></script>

<!-- Customd-number-->
<script src="{{ asset('/customd-number/jquery.number.min.js') }}"></script>

<!-- kartik  -->

<script src="{{asset('/kartik/js/fileinput.js')}}"></script>
<script src="{{asset('/kartik/js/locales/es.js')}}"></script>

<link rel="stylesheet" type="text/css" href="{{asset('/kartik/css/fileinput.min.css')}}">  
<script type="text/javascript" src="{{ asset('/barcode-jquery-master/jquery-barcode.min.js') }}"></script>

<!-- DATETIMEPICKER-->
<link rel="stylesheet" type="text/css" href="{{asset('/datetimepicker/css/bootstrap-datetimepicker.css')}}">  
<script type="text/javascript" src="{{ asset('/datetimepicker/js/bootstrap-datetimepicker.js') }}"></script>








<!-- Optionally, you can add Slimscroll and FastClick plugins.
      Both of these plugins are recommended to enhance the
      user experience. Slimscroll is required when using the
      fixed layout. -->
