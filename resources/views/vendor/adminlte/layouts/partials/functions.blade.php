<?php

	function subcategorias($categoria, $categorias, $tipo){
		$permisos = $_SESSION['permisos']; 
		$html="";
		foreach($categorias as $categoriaHija){
			
			//comprobar si la categoria es realmente hija de la categoria y anidarlas todas
			if($categoria->id_categoria == $categoriaHija->id_categoriaP){

				$seleccionar = "";
				$agregar = "";
				$editar = "";
				$eliminar = "";
				$ver = '';
				if($tipo=="items"){
					$seleccionar = "".
					"<a class='btn' style='padding: 1px 10px; margin-right:4px;' data-dismiss='modal' onclick='seleccionarCategoria(".$categoriaHija->id_categoria.", \"".$categoriaHija->nombre."\");'>".
						"<i class='fa fa-chevron-circle-up fa-lg' aria-hidden='true' style='background-color:white; color:black;'></i>".
					"</a>";

				}
				else if($tipo=="categorias"){
					$agregar = ''.//variable para agregar boton de agregar categoria
					'<a class="btn btn-success" style="padding: 1px 10px;" data-toggle="modal" data-target="#myModal" onclick="modal('.$categoriaHija->id_categoria.', \''.$categoriaHija->nombre.'\');">'.
						'<i class="fa fa-plus" aria-hidden="true"></i>'.
					'</a> ';
					
					//si la categoria no es propia de la empresa tendra permisos para modificar o eliminar
					if($categoriaHija->id_empresa!=1){
						$editar= ''.//variable para agregar botones de editar 
						"<a class='btn btn-primary' style='padding: 1px 10px; margin-right:4px;' data-toggle='modal' data-target='#myModal' onclick='modal(".$categoriaHija->id_categoria.", \"".$categoriaHija->nombre."\", true);' aria-label='editar'>".
						  "<i class='fa fa-pencil' aria-hidden='true'></i>".
						"</a>";
						
						$eliminar=''.//y eliminar
						"<a class='btn btn-danger' style='padding: 1px 10px; margin-right:4px;' href='".url('/eliminarCategoria/'.$categoriaHija->id_categoria).";' aria-label='Eliminar' id='eliminar'>".
						 "<i class='fa fa-times' aria-hidden='true'></i>".
						"</a>";
						$ver = ''.
						'<a class="btn btn-success" style="padding: 1px 10px; margin-right:4px;"  href="'.url("/verCategoria/".$categoriaHija->id_categoria).'" aria-label="ver">'.
							'<i class="fa fa-eye" aria-hidden="true"></i>'.
						'</a>';
					}
				}


				//utilizado para definir si una categoria posee subcategorias
				foreach($categorias as $categoria2){
    				if($categoriaHija->id_categoria == $categoria2->id_categoriaP){
	    				$plus='<i class="fa fa-plus-square-o" style="cursor:pointer;" aria-hidden="true"  id="i'.$categoriaHija->id_categoria.'" onclick="expandir('.$categoriaHija->id_categoria.');"></i>  ';
	    				break;
    				}
	    			else
	    				$plus='<i class="fa"></i> ';
				}


				if($permisos['9']->insertar!=1)
					$agregar = '';
				if($permisos['9']->modificar!=1)
					$editar = '';
				if($permisos['9']->eliminar!=1)
					$eliminar ='';
				if($permisos['18']->ver!=1)
					$ver = '';
											
				$html .=
				'<ul class="'.$categoriaHija->id_categoriaP.'" style="display:none;">'.
					'<li class="list-group-item justify-content-between">'.$plus.$categoriaHija->nombre.
						'<span class="badge badge-default badge-pill" style="background-color: white; padding: 0 0;">'.
							$seleccionar.
							$ver.
                			$agregar.
							$editar.
							$eliminar.
						'</span>'.
					'</li>'.
					subcategorias($categoriaHija, $categorias, $tipo).
				'</ul>';

			}
		}
		return $html;
	}
	?>