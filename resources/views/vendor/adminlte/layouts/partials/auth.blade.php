<?php
	
	$perfilXpermisos = DB::table('perfilesXpermisos')
	->join('perfiles', 'perfilesXpermisos.id_perfil', '=', 'perfiles.id_perfil')
	->select('perfiles.nombre as perfil', 'perfilesXpermisos.*')
	->where('perfilesXpermisos.id_perfil', Auth::user()->id_perfil)
	->get();
	$permisos=array();
    foreach ($perfilXpermisos as $key) {
        $permisos[$key->id_permiso] = isset($key) ? $key : '';
        if($key!='')
            $perfilNombre = $key->perfil;
    }
   
    $_SESSION["perfilNombre"] = $perfilNombre;
	$_SESSION["permisos"] = $permisos;
?>