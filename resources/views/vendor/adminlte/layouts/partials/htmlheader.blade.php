<head>
    <meta charset="UTF-8">
    <title>Aldia</title>
    <link href="{{ asset('logo.png') }}" rel="icon">
    <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta http-equiv="X-UA-Compatible" content="IE=11"> 
    
    <!-- JQUERY-->
    <script type="text/javascript" src="{{ asset('/js/jquery-3.2.1.min.js') }}"></script>
    <script src="{{ asset('tether-master/dist/js/tether.min.js') }}"></script>

    <!-- FontAwesome-->
    <link href="{{ asset('/css/all.css') }}" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="{{ asset('/font-awesome/css/font-awesome.min.css') }}">

    <!-- Boostrap Table-->
    <link rel="stylesheet" href="{{ asset('/bootstrap-table-master/src/bootstrap-table.css')}}">
    <script src="{{ asset('/js/bootstrap.min.js') }}"></script>
    <script src="{{ asset('/bootstrap-table-master/src/bootstrap-table.js')}}"></script>
    <!-- put your locale files after bootstrap-table.js -->
    <script src="{{ asset('/bootstrap-table-master/src/locale/bootstrap-table-es-MX.js')}}"></script>
   

    

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    

</head>
