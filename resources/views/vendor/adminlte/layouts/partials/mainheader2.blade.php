<!-- Main Header -->
<header class="main-header " >

    <!-- Logo -->
    <a  class="logo">
        <!-- mini logo for sidebar mini 50x50 pixels -->
        <span class="logo-mini"><b>Ald</span>
        <!-- logo for regular state and mobile devices -->
        <span class="logo-lg">Ald&iacute;a App </span>
    </a>

    <!-- Header Navbar -->
    <nav class="navbar navbar-static-top" role="navigation">
        
        <!-- Navbar Right Menu -->
        <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">
               
            <!-- User Account Menu -->
                <li class="dropdown user user-menu" id="user_menu">
                    <!-- Menu Toggle Button -->
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <!-- The user image in the navbar-->
                        <img src="{{ Gravatar::get($user->email) }}" class="user-image" alt="User Image" id="imagenUsuarioActivo1"/>
                        <!-- hidden-xs hides the username on small devices so only the image appears. -->
                        <span class="hidden-xs" id="nombreUsuarioActivo">{{ Auth::user()->nombre }}</span>
                    </a>
                    <ul class="dropdown-menu">
                        <!-- The user image in the menu -->
                        <li class="user-header" style="height: 100%;">
                            <img src="{{ Gravatar::get($user->email) }}" class="img-circle" alt="User Image" id="imagenUsuarioActivo2" />
                            <h2 style="color: white;" id="descripcionUsuarioActivo">{{ Auth::user()->nombre.' '.Auth::user()->apellido.' ('.Auth::user()->identificacion.')' }}</h2>
                            <input type="hidden" id="id_usuarioActivo" value="{{ Auth::user()->id_usuario }}">
                        </li>
                        <!-- Menu Body
                        
                        <li class="user-body">
                            <div class="col-xs-4 text-center">
                                <a href="#">{{ trans('adminlte_lang::message.followers') }}</a>
                            </div>
                            <div class="col-xs-4 text-center">
                                <a href="#">{{ trans('adminlte_lang::message.sales') }}</a>
                            </div>
                            <div class="col-xs-4 text-center">
                                <a href="#">{{ trans('adminlte_lang::message.friends') }}</a>
                            </div>
                        </li>
                         -->
                    
                        <!-- Menu Footer-->
                        <li class="user-footer">
                            <div class="pull-left">
                                <a class="btn btn-default btn-flat" data-toggle="modal" data-target=".modalCambioUsuario">Cambiar Usuario</a>
                            </div>
                            
                            <div class="pull-right">
                                <a href="{{ url('/logout') }}" class="btn btn-default btn-flat" id="logout"
                                   onclick="event.preventDefault();
                                             document.getElementById('logout-form').submit();">
                                    {{ trans('adminlte_lang::message.signout') }}
                                </a>

                                <form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">
                                    {{ csrf_field() }}
                                    <input type="submit" value="logout" style="display: none;">
                                </form>

                            </div>
                        </li>
                    </ul>
                </li>

                <!-- Control Sidebar Toggle Button -->
                <li>
                    <a href="#" data-toggle="control-sidebar"><i class="fa fa-history"></i></a>
                </li>
            </ul>
        </div>
    </nav>
</header>
