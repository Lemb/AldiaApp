﻿@include('adminlte::layouts.partials.auth')
<?php 
    $permisos     = $_SESSION['permisos']; 
    $perfilNombre = $_SESSION["perfilNombre"];
    
    define('permisos', 'permisos');

    $_SESSION['id_usuario'] = Auth::user()->id_usuario;  
    $_SESSION['id_empresa'] = Auth::user()->id_empresa;  

?>

<!-- Left side column. contains the logo and sidebar -->
<aside class="main-sidebar">

    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">

        <!-- Sidebar user panel (optional) -->
        @if (! Auth::guest())
            <div class="user-panel">
                <div class="pull-left image">
                    <img src="{{ Gravatar::get($user->email) }}" class="img-circle" alt="User Image" />
                </div>
                <div class="pull-left info">
                    <p>{{ Auth::user()->nombre }}</p>

                    <!-- Status -->
                    <a href="#"> {{ $perfilNombre }}</a>
                </div>
            </div>
        @endif

        <!-- search form (Optional) -->
        <!--<form action="#" method="get" class="sidebar-form">
            <div class="input-group">
                <input type="text" name="q" class="form-control" placeholder="{{ trans('adminlte_lang::message.search') }}..."/>
              <span class="input-group-btn">
                <button type='submit' name='search' id='search-btn' class="btn btn-flat"><i class="fa fa-search"></i></button>
              </span>
            </div>
        </form>-->
        <!-- /.search form -->

        <!-- Sidebar Menu -->
        <ul class="sidebar-menu">
            <li class="header">OPCIONES</li>
            <!-- Optionally, you can add icons to the links -->
            <li class="active" id="inicioSidebar"><a href="{{ url('home') }}"> <span>{{ trans('adminlte_lang::message.home') }}</span></a></li>

            <li class="treeview" id="configuracionSidebar">
                <a href="{{ url('home') }}"><i class="fa fa-cogs" aria-hidden="true"></i><span>Configuraciones</span><i class="fa fa-angle-left pull-right"></i></a>
                <ul class="treeview-menu">
                  
                    <li><a href="{{ url('empresa') }}">Empresa y MiPerfil</a></li>
                   

                    @if(isset($permisos['4']))
                    @if($permisos['4']->insertar == 1 || $permisos['4']->ver == 1 || $permisos['4']->modificar == 1 || $permisos['4']->eliminar == 1)
                    <li><a href="{{ url('usuarios') }}">Usuarios</a></li>
                    @endif
                    @endif

                    @if(isset($permisos['5']))
                    @if($permisos['5']->insertar == 1 || $permisos['5']->ver == 1 || $permisos['5']->modificar == 1 || $permisos['5']->eliminar == 1)
                    <li><a href="{{ url('perfiles') }}">Perfiles</a></li>
                    @endif
                    @endif

                    @if(isset($permisos['6']))
                    @if($permisos['6']->insertar == 1 || $permisos['6']->ver == 1 || $permisos['6']->modificar == 1 || $permisos['6']->eliminar == 1)
                    <li><a href="{{ url('numeraciones') }}">Numeraciones</a></li>
                    @endif
                    @endif

                    @if(isset($permisos['7']))
                    @if($permisos['7']->insertar == 1 || $permisos['7']->ver == 1 || $permisos['7']->modificar == 1 || $permisos['7']->eliminar == 1)
                    <li><a href="{{ url('impuestos') }}">Impuestos</a></li>
                    @endif
                    @endif

                    @if(isset($permisos['8']))
                    @if($permisos['8']->insertar == 1 || $permisos['8']->ver == 1 || $permisos['8']->modificar == 1 || $permisos['8']->eliminar == 1)
                    <li><a href="{{ url('terminosPago') }}">Terminos de Pago</a></li>
                    @endif
                    @endif
                    <li><a href="{{ url('bodegas') }}">Bodegas</a></li>
                    <li><a href="{{ url('historialAcciones') }}" >Historial de Acciones</a></li>
                    
                </ul>
            </li>
           
           @if(isset($permisos['13']) || isset($permisos['14']))
           <li class="treeview" id="ingresosSidebar">
                <a href="{{ url('home') }}"><i class="fa fa-plus" aria-hidden="true"></i><span>Ingresos</span><i class="fa fa-angle-left pull-right"></i></a>
                <ul class="treeview-menu">
                    @if(isset($permisos['19']))
                    <li><a href="{{ url('pos') }}" target="_blank">POS</a></li>
                    @endif
                    @if(isset($permisos['13']))
                    @if($permisos['13']->insertar == 1 || $permisos['13']->ver == 1 || $permisos['13']->modificar == 1 || $permisos['13']->eliminar == 1)
                    <li><a href="{{ url('facturasI') }}">Facturas de venta</a></li>
                    @endif
                    @endif

                    @if(isset($permisos['14']))
                    @if($permisos['14']->insertar == 1 || $permisos['14']->ver == 1 || $permisos['14']->modificar == 1 || $permisos['14']->eliminar == 1)
                    <li><a href="{{ url('pagosI') }}">Pagos recibido</a></li>
                    @endif
                    @endif
                </ul>
            </li>
           
            @endif

            @if(isset($permisos['16']) || isset($permisos['15']))
           
            <li class="treeview" id="egresosSidebar">
                <a href="{{ url('home') }}"><i class="fa fa-minus" aria-hidden="true"></i><span>Egresos</span><i class="fa fa-angle-left pull-right"></i></a>
                <ul class="treeview-menu">
                    @if(isset($permisos['15']))
                    @if($permisos['15']->insertar == 1 || $permisos['15']->ver == 1 || $permisos['15']->modificar == 1 || $permisos['15']->eliminar == 1)
                    <li><a href="{{ url('facturasE') }}">Facturas de compra</a></li>
                    @endif
                    @endif

                    @if(isset($permisos['16']))
                    @if($permisos['16']->insertar == 1 || $permisos['16']->ver == 1 || $permisos['16']->modificar == 1 || $permisos['16']->eliminar == 1)
                    <li><a href="{{ url('pagosE') }}">Pagos realizados</a></li>
                    @endif
                    @endif

                </ul>
            </li>
            
            @endif

            @if(isset($permisos['12']))
            @if($permisos['12']->insertar == 1 || $permisos['12']->ver == 1 || $permisos['12']->modificar == 1 || $permisos['12']->eliminar == 1)
            <li id="itemsSidebar"><a href="{{ url('items') }}"> <i class="fa fa-cubes" aria-hidden="true"></i><span>Items</span></a></li>
            @endif
            @endif

            @if(isset($permisos['11']))
            @if($permisos['11']->insertar == 1 || $permisos['11']->ver == 1 || $permisos['11']->modificar == 1 || $permisos['11']->eliminar == 1)
            <li id="contactosSidebar"><a href="{{ url('contactos') }}"> <i class="fa fa-address-book" aria-hidden="true"></i><span>Contactos</span></a></li>
            @endif
            @endif

            @if(isset($permisos['10']))
            @if($permisos['10']->insertar == 1 || $permisos['10']->ver == 1 || $permisos['10']->modificar == 1 || $permisos['10']->eliminar == 1)
            <li id="bancosSidebar"><a href="{{ url('bancos') }}"> <i class="fa fa-money" aria-hidden="true"></i><span>Bancos</span></a></li>
            @endif
            @endif

            @if(isset($permisos['9']))
            @if($permisos['9']->insertar == 1 || $permisos['9']->ver == 1 || $permisos['9']->modificar == 1 || $permisos['9']->eliminar == 1)
            <li id="categoriasSidebar"><a href="{{ url('categorias') }}"> <i class="fa fa-folder-open" aria-hidden="true"></i> <span>Categorias</span></a></li>
            @endif
            @endif

            <li class="treeview" id="modulosSidebar">
                <a href="{{ url('home') }}"><i class="fa fa-university" aria-hidden="true"></i><span>Modulos</span><i class="fa fa-angle-left pull-right"></i></a>
                <ul class="treeview-menu">
                  
                    <li><a href="{{ url('codigoBarras') }}">Generador de Codigo de Barras</a></li>

                </ul>
            </li>
        </ul><!-- /.sidebar-menu -->
    </section>
    <!-- /.sidebar -->
</aside>

<script type="text/javascript">
    $('.active').removeClass('active');
    $('#{{(isset($sidebarActive))?$sidebarActive:"inicioSidebar"}}').addClass('active');
</script>