@extends('adminlte::layouts.app')

@section('htmlheader_title')
	{{ trans('adminlte_lang::message.home') }}
@endsection


@section('main-content')
	<script type="text/javascript">
		$("#contentHeader1").append('<a href={{url("usuarios")}}> Usuarios </a>');
		$("#contentHeader2").text('Detalle de Usuario');
	</script>
	<?php 
    	$permisos = $_SESSION['permisos']; 
	?>
	<div class="container-fluid spark-screen">
		<div class="row">
			<div class="col-md-12 col-md-offset-0">
				<!-- Default box -->
				<div class="box box-primary">
					<div class="box-header with-border">
						<h3 class="box-title">Informacion basica</h3>

						<div class="box-tools pull-right">
							<button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
								<i class="fa fa-minus"></i></button>
							<button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
								<i class="fa fa-times"></i></button>
						</div>
					</div>

					<div class="box-body">
						<?php 
							$style1 = (isset($usuario2)) ? 'style=background-color:#DDFCDA;' : '';
							$style2 = (isset($usuario2)) ? 'style=background-color:#FAE8E8;' : '';
						?>
						<div class="form-group col-md-4 col-md-offset-0">
							<label for="identificacion">Identificacion</label>
							<input class="form-control" name="identificacion" id="identificacion" disabled="" type="text" value="{{ $usuario->identificacion}}" {{$style1}}>
							@if(isset($usuario2))
							<input class="form-control" name="identificacion" id="identificacion" disabled="" type="text" value="{{ $usuario2->identificacion}}"  {{$style2}}>
							@endif
						</div>
						<div class="form-group col-md-4 col-md-offset-0">
							<label for="nombre">Nombres</label>
							<input class="form-control" name="nombre" id="nombre" disabled="" type="text" value="{{ $usuario->nombre }}" {{$style1}}>
							@if(isset($usuario2))
							<input class="form-control" name="nombre" id="nombre" disabled="" type="text" value="{{ $usuario2->nombre }}" {{$style2}}>
							@endif
						</div>
						<div class="form-group col-md-4 col-md-offset-0">
							<label for="apellido">Apellidos</label>
							<input class="form-control" name="apellido" id="apellido" disabled="" type="text" value="{{ $usuario->apellido }}" {{$style1}}>
							@if(isset($usuario2))
							<input class="form-control" name="apellido" id="apellido" disabled="" type="text" value="{{ $usuario2->apellido }}" {{$style2}}>
							@endif
						</div>
						<div class="form-group col-md-4 col-md-offset-0">
							<label for="direccion">Direccion</label>
							<input class="form-control" name="direccion" disabled="" id="direccion" type="text" value="{{ $usuario->direccion }}" {{$style1}}>
							@if(isset($usuario2))
							<input class="form-control" name="direccion" disabled="" id="direccion" type="text" value="{{ $usuario2->direccion }}" {{$style2}}>
							@endif
						</div>
						<div class="form-group col-md-2 col-md-offset-0">
							<label for="telefono">Telefono</label>
							<input class="form-control" name="telefono" id="telefono" disabled="" type="text" value="{{ $usuario->telefono}}" {{$style1}}>
							@if(isset($usuario2))
							<input class="form-control" name="telefono" id="telefono" disabled="" type="text" value="{{ $usuario2->telefono}}" {{$style2}}>
							@endif
						</div>
						<div class="form-group col-md-2 col-md-offset-0">
							<label for="direccionEmp">Celular</label>
							<input class="form-control" name="celular" id="celular" disabled="" type="text" value="{{ $usuario->celular }}" {{$style1}}>
							@if(isset($usuario2))
							<input class="form-control" name="celular" id="celular" disabled="" type="text" value="{{ $usuario2->celular }}" {{$style2}}>
							@endif
						</div>
						<div class="form-group col-md-4 col-md-offset-0">
							<label for="email">Email</label>
							<input class="form-control" name="email" id="email" disabled="" type="email" value="{{ $usuario->email }}" {{$style1}}>
							@if(isset($usuario2))
							<input class="form-control" name="email" id="email" disabled="" type="email" value="{{ $usuario2->email }}" {{$style2}}>
							@endif
						</div>
						
						<div class="form-group col-md-4 col-md-offset-0">
							<label for="direccion">Perfil</label>
							<select class="form-control" name="id_perfil" id="perfiles" disabled="" {{$style1}}>
								@foreach($perfiles as $perfil)
								<?php $selected = ($usuario->id_perfil == $perfil->id_perfil) ? 'selected=selected' : ''; ?>
								<option value="{{ $perfil->id_perfil }}" {{$selected}}>{{ $perfil->nombre}}</option>
								@endforeach
							</select>
							@if(isset($usuario2))
							<select class="form-control" name="id_perfil" id="perfiles" disabled="" {{$style2}}>
								@foreach($perfiles as $perfil)
								<?php $selected = ($usuario2->id_perfil == $perfil->id_perfil) ? 'selected=selected' : ''; ?>
								<option value="{{ $perfil->id_perfil }}" {{$selected}}>{{ $perfil->nombre}}</option>
								@endforeach
							</select>
							@endif
							
						</div>
						<div class="form-group col-md-4 col-md-offset-0">
							<?php $checked = ($usuario->deleted_at !=null) ? 'checked=checked':'';?>
							<label for="eliminadoPregunta">¿Eliminado?</label><br>
							<input type="checkbox" value="1" {{$checked}} disabled="disabled">
						</div>


						@if(isset($permisos[18]) && $permisos[18]->ver==1)
						<h4 class="box-title form-group col-md-12 col-md-offset-0">Versiones</h3>
						<table class="table table-bordered table-hover dataTable" 
							data-toggle="table"
							data-pagination="true"       						
							>
			                <thead>
				                <tr>
				                	<th>Identificacion</th>
				                  	<th>Nombres</th>
				                  	<th>Apellidos</th>
				                  	<th>fecha</th>
				                  	<th>Version</th>
				                  	<th>Responsable</th>
				                  	<th>Detalle</th>
				                </tr>
				            </thead>
				        	<tbody>
				                @foreach($versiones as $version) 
				                	<?php  $referenciaPrincipal = ($version->usuario==$usuario->id_usuario) ? 'id=referenciaPrincipal' : ''; 
				                		
				                		$referenciaPrevia = (isset($usuario2) && $version->usuario==$usuario2->id_usuario) ? 'id=referenciaPrevia' : '';

				                	?>
						    		<tr {{$referenciaPrincipal}} {{$referenciaPrevia}}>
						    			<td> {{$version->identificacion }} 		</td>
						    			<td> {{$version->nombre }} 		</td>
						    			<td> {{$version->apellido }} 	</td>
						    			<td> {{$version->updated_at }} </td>
						    			<td> {{$version->numeroVersion }} </td>
						    			<td> <a href="{{ url('/verUsuario/'.$version->responsableID) }}">  {{$version->responsableNombre.' ('.$version->responsableIdentificacion.')' }}</a> </td>
						    			<td> 

						    				<a class="btn btn-info" style="padding: 2px 10px;" href="{{ url('/verUsuario/'.$version->id_usuario) }}" aria-label="Ver Detalle">
  												<i class="fa fa-eye" aria-hidden="true"></i>
  											</a>
  											
						    			</td>
						    		</tr>
								@endforeach
			              	</tbody>	
              			</table>
              			@endif					
						
						
					</div>
					
				</div>
				<!-- /.box -->

			</div>
		</div>
	</div>

	<script type="text/javascript">
		
		$( document ).ready(function() {
			$("#referenciaPrincipal").css("background-color", "#DDFCDA");
			$("#referenciaPrevia").css("background-color", "#FAE8E8");
		});
	</script>

@endsection

