@extends('adminlte::layouts.app')

@section('htmlheader_title')
	{{ trans('adminlte_lang::message.home') }}
@endsection


@section('main-content')
	<script type="text/javascript">
		$("#contentHeader1").append('<a href={{url("usuarios")}}> Usuarios </a>');
		$("#contentHeader2").text('Crear Usuario');
	</script>
	<div class="container-fluid spark-screen">
		<div class="row">
			<div class="col-md-12 col-md-offset-0">
				<!-- Default box -->
				<div class="box box-primary">
					<div class="box-header with-border">
						<h3 class="box-title">Informacion basica</h3>

						<div class="box-tools pull-right">
							<button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
								<i class="fa fa-minus"></i></button>
							<button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
								<i class="fa fa-times"></i></button>
						</div>
					</div>

					<form action="{{ url('/nuevoUsuario') }}" method="post">
					<div class="box-body">
						<p class="help-block">Campos marcados con asterisco (<i class="fa fa-asterisk" style="color: #DD4B39;"></i>) son obligatorios.</p>

						@if(count($errors)>0)
						<div class="form-group col-md-12 col-md-offset-0">
							<div class="alert alert-danger alert-dismissible">
				                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
				                <h4><i class="icon fa fa-ban"></i> Error en el formulario</h4>
				                @foreach ($errors->all() as $message) 
	    							<span > {{$message}}</span><br>
								@endforeach
				             </div>
						</div>
						@endif

						<div class="form-group col-md-4 col-md-offset-0">
							<label for="identificacion">Identificacion</label>
							<input class="form-control" name="identificacion" id="identificacion" type="text" value="">
						</div>
						<div class="form-group col-md-4 col-md-offset-0">
							<label for="nombre">Nombres</label>
							<input class="form-control" name="nombre" id="nombre" type="text" value="">
						</div>
						<div class="form-group col-md-4 col-md-offset-0">
							<label for="apellido">Apellidos</label>
							<input class="form-control" name="apellido" id="apellido" type="text" value="">
						</div>
						<div class="form-group col-md-4 col-md-offset-0">
							<label for="direccion">Direccion</label>
							<input class="form-control" name="direccion" id="direccion" type="text" value="">
						</div>

						<div class="form-group col-md-2 col-md-offset-0">
							<label for="telefono">Telefono</label>
							<input class="form-control" name="telefono" id="telefono" type="text" value="">
						</div>
						<div class="form-group col-md-2 col-md-offset-0">
							<label for="direccionEmp">Celular</label>
							<input class="form-control" name="celular" id="celular" type="text" value="">
						</div>
						
						<div class="form-group col-md-4 col-md-offset-0">
							<label for="email">Email</label> (<i class="fa fa-asterisk" style="color: #DD4B39;"></i>)
							<input class="form-control" name="email" id="email" type="email" value="">
						</div>
						<div class="form-group col-md-4 col-md-offset-0">
							<label for="direccion">Perfil</label>
							<select class="form-control" name="id_perfil">
								@foreach($perfiles as $perfil)
								<option value="{{ $perfil->id_perfil }}">{{ $perfil->nombre}}</option>
								@endforeach
							</select>
						</div>
						
						<div class="form-group col-md-4 col-md-offset-4">
							<label for="permisosU">Contrase&ntilde;a</label> (<i class="fa fa-asterisk" style="color: #DD4B39;"></i>)<br>
							<input type="password" class="form-control" name="password">
						</div>
						
						
						
					</div>
					<div class="box-footer">
                		<div class="col-md-2 col-md-offset-10">
							<button type="submit" class="btn btn-primary btn-block btn-flat">Crear</button>
						</div>
						<input type="hidden" name="_token" value="{{ csrf_token() }}">
						<input type="hidden" name="activo" value="1">
						<input type="hidden" name="login" value="1">
						<input type="hidden" name="id_empresa" value="{{ Auth::user()->id_empresa }}">

              		</div>
              		</form>
					
				</div>
				<!-- /.box -->





			</div>
		</div>
	</div>

@endsection

