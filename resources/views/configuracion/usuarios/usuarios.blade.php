@extends('adminlte::layouts.app')

@section('htmlheader_title')
	{{ trans('adminlte_lang::message.home') }}
@endsection


@section('main-content')
	<script type="text/javascript">
		$("#contentHeader2").append('Usuarios');
	</script>
	<?php 
    	$permisos = $_SESSION['permisos']; 
	?>
	<div class="container-fluid spark-screen">
		<div class="row">
			<div class="col-md-12 col-md-offset-0">
				<!-- Default box -->
				<div class="box box-primary">
					<div class="box-header with-border">
						<h3 class="box-title">Usuarios</h3>

						<div class="box-tools pull-right">
							<button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
								<i class="fa fa-minus"></i></button>
							<button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
								<i class="fa fa-times"></i></button>
						</div>
					</div>
					<div class="box-body">
						@if(isset($permisos['4']) && $permisos['4']->insertar==1)
						<div class="col-md-2 col-md-offset-0" style="margin-bottom: 10px;">
							<a class="btn btn-primary btn-block btn-flat" href="{{ url('nuevoUsuario') }}"><i class="fa fa-plus" aria-hidden="true"></i> Nuevo</a>
						</div>
						@endif
						@if($mensaje)
						<div class="form-group col-md-8 col-md-offset-2">
							<div class="alert alert-success alert-dismissible">
						        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
						        <h4><i class="icon fa fa-check"></i> {{ $mensaje }}</h4>
						    </div>
						</div>
						@endif
						<table class="table table-bordered table-hover dataTable" 
							data-toggle="table"
							data-pagination="true"
							data-search="true"
							data-show-refresh="true"
       						data-show-columns="true">
			                <thead>
				                <tr>
				                	<th>Identificacion</th>
				                  	<th>Nombres</th>
				                  	<th>Apellidos</th>
				                  	<th>Perfil</th>
				                  	<th data-halign="center" data-align="center" data-width="17%">Acciones</th>
				                </tr>
				            </thead>
				        	<tbody>
				                @foreach($usuarios as $usuario) 
						    		<tr>
						    			<td> {{$usuario->identificacion }} 		</td>
						    			<td> {{$usuario->nombre }} 		</td>
						    			<td> {{$usuario->apellido }} 	</td>
						    			<td> {{$usuario->perfil }} </td>
						    			<td>
						    				@if(isset($permisos['4']) && $permisos['4']->ver==1)
						    				<a class="btn btn-success" style="padding: 2px 5px;" href="{{ url('/verUsuario/'.$usuario->id_usuario) }}" aria-label="Ver Detalle">
  												<i class="fa fa-eye" aria-hidden="true"></i>
  											</a>
  											@endif
  											@if(isset($permisos['17']) && $permisos['17']->ver==1)
						    				<a class="btn btn-info" style="padding: 2px 5px;" href="{{ url('/historialAcciones/'.$usuario->id_usuario) }}" aria-label="Historial de Acciones">
  												<i class="fa fa-history" aria-hidden="true"></i>
  											</a>
  											@endif
  											@if($usuario->login != 2)
  											@if(isset($permisos['4']) && $permisos['4']->modificar==1)
											<a class="btn btn-primary" style="padding: 2px 5px;" href="{{ url('/editUsuario/'.$usuario->id_usuario) }}" aria-label="Editar">
											  <i class="fa fa-pencil" aria-hidden="true"></i>
											</a>
											@endif
											@if(isset($permisos['4']) && $permisos['4']->eliminar==1)
											<a class="btn btn-danger" style="padding: 2px 5px;" href="{{ url('/eliminarUsuario/'.$usuario->id_usuario) }}" aria-label="Eliminar" id="eliminar">
											 <i class="fa fa-times" aria-hidden="true"></i>
											</a>
											@endif
											@endif
										</td>
						    		</tr>
								@endforeach
			              	</tbody>	
              			</table>						
					</div>
					
				</div>
				<!-- /.box -->
			</div>
		</div>
	</div>

	<script type="text/javascript">
		
		$( document ).ready(function() {
  			$('#eliminar').click(function(event) {
		    	if(!confirm('¿Deseas realmente eliminar este usuario?'))
					event.preventDefault();
			});
		});
	</script>
@endsection

