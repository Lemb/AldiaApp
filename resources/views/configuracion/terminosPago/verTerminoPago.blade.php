@extends('adminlte::layouts.app')

@section('htmlheader_title')
	{{ trans('adminlte_lang::message.home') }}
@endsection


@section('main-content')
	<div class="container-fluid spark-screen">
		<div class="row">
			<div class="col-md-12 col-md-offset-0">
				<!-- Default box -->
				<div class="box box-primary">
					<div class="box-header with-border">
						<h3 class="box-title">Ver Termino de Pago</h3>

						<div class="box-tools pull-right">
							<button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
								<i class="fa fa-minus"></i></button>
							<button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
								<i class="fa fa-times"></i></button>
						</div>
					</div>

					<div class="box-body">
						<div class="form-group col-md-4 col-md-offset-0">
							<label for="nombre">Nombre</label>
							<input class="form-control" name="nombre" id="nombre" type="text" value="{{ $terminoPago->nombre}}" disabled="">
						</div>
						<div class="form-group col-md-2 col-md-offset-0">
							<label for="dias">Dias</label>
							<input class="form-control" name="dias" id="dias" type="text" value="{{ $terminoPago->dias}}" disabled="">
						</div>
						<div class="form-group col-md-2 col-md-offset-0">
							<label for="activo">Estado</label><br>
							<input type="checkbox" id="activo" name="activo" value="1" disabled="">
						</div>
						<div class="form-group col-md-3 col-md-offset-0">
							<?php $checked = ($terminoPago->deleted_at!=null) ? 'checked="checked"' : ''; ?>
							<label for="estado">¿Eliminado?</label><br>
							<input type="checkbox" id="estado" name="estado" value="1" disabled="" {{$checked}}>
						</div>
						
					</div>					
				</div>
				<!-- /.box -->
			</div>
		</div>
	</div>

	<script type="text/javascript">
		window.onload = function ()
		{
			document.getElementById("activo").checked = {{ $terminoPago->activo }}; 
		}

	</script>

@endsection
