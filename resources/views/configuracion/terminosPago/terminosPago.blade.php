@extends('adminlte::layouts.app')

@section('htmlheader_title')
	{{ trans('adminlte_lang::message.home') }}
@endsection


@section('main-content')
	<script type="text/javascript">
		$("#contentHeader2").append('Terminos de Pago');
	</script>
	<?php 
    	$permisos = $_SESSION['permisos']; 
	?>
	<div class="container-fluid spark-screen">
		<div class="row">
			<div class="col-md-12 col-md-offset-0">
				<!-- Default box -->
				<div class="box box-primary">
					<div class="box-header with-border">
						<h3 class="box-title">Terminos de pago</h3>

						<div class="box-tools pull-right">
							<button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
								<i class="fa fa-minus"></i></button>
							<button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
								<i class="fa fa-times"></i></button>
						</div>
					</div>
					<div class="box-body">
						@if($permisos['8']->insertar==1)
						<div class="col-md-2 col-md-offset-0" style="margin-bottom: 10px;">
							<a class="btn btn-primary btn-block btn-flat" data-toggle="modal" data-target="#myModal" onclick="modal('', '', '', '', 'nuevo');"><i class="fa fa-pencil" aria-hidden="true"></i> Nuevo</a>
						</div>
						@endif
						@if(count($errors)>0)
						<div class="form-group col-md-8 col-md-offset-2">
							<div class="alert alert-danger alert-dismissible">
				                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
				                <h4><i class="icon fa fa-ban"></i> Error en el formulario</h4>
				                @foreach ($errors->all() as $message) 
	    							<span > {{$message}}</span><br>
								@endforeach
				             </div>
						</div>
						@endif
						@if(session('mensaje'))
						<div class="form-group col-md-8 col-md-offset-2">
							<div class="alert alert-success alert-dismissible">
						        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
						        <h4><i class="icon fa fa-check"></i> {{ session('mensaje') }}</h4>
						    </div>
						</div>
						@endif						
						<table id="example1" 
						data-toggle="table"
						data-pagination="true"
						data-search="true"
						data-show-refresh="true"
       					data-show-columns="true"
						>
			                <thead>
				                <tr>
				                  <th>Nombre</th>
				                  <th>Dias</th>
				                  <th>Estado</th>
				                  <th data-halign="center" data-align="center" data-width="17%">Acciones</th>
				                </tr>
				            </thead>
				        	<tbody>
				                @foreach($terminos as $termino) 
						    		<tr>
						    			<td> {{$termino->nombre }} 		</td>
						    			<td> {{$termino->dias }} 	</td>
						    			@if($termino->activo == 1)
						    			<td> Activo</td>
						    			@else
						    			<td> Inactivo</td>
						    			@endif
						    			<td>
						    				@if($permisos['8']->ver==1)
						    				<a class="btn btn-success" style="padding: 2px 10px;" data-toggle="modal" data-target="#myModal"
											onclick="modal('{{$termino->id_terminoPago}}', '{{$termino->nombre}}', '{{$termino->dias}}', '{{$termino->activo}}', 'ver');">
  												<i class="fa fa-eye" aria-hidden="true"></i>
  											</a>
  											@endif
  											@if($permisos['8']->modificar==1)
											<a class="btn btn-primary" style="padding: 2px 10px;" data-toggle="modal" data-target="#myModal"
											onclick="modal('{{$termino->id_terminoPago}}', '{{$termino->nombre}}', '{{$termino->dias}}', '{{$termino->activo}}', 'edit');" aria-label="editar">
											  <i class="fa fa-pencil" aria-hidden="true"></i>
											</a>
											@endif
											@if($permisos['8']->eliminar==1)
											<a class="btn btn-danger" style="padding: 2px 10px;" href="{{ url('/eliminarTerminoPago/'.$termino->id_terminoPago) }}" aria-label="Eliminar" id="eliminar">
											 <i class="fa fa-times" aria-hidden="true"></i>
											</a>
											@endif
										</td>
						    		</tr>
								@endforeach
			              	</tbody>	
              			</table>

              					
					</div>
					
				</div>
				<!-- /.box -->
			</div>
		</div>
	</div>

	<!-- modal -->
	<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	  <div class="modal-dialog" role="document">
	    <div class="modal-content">
	      	<div class="modal-header">
		        <h3 class="box-title" id="myModalLabel">Nuevo Termino de Pago</h3>
			</div>

			<form action="{{ url('/nuevoTerminoPago') }}" method="post" id="form">
			<div class="box-body">
				<p class="help-block">Campos marcados con asterisco (<i class="fa fa-asterisk" style="color: #DD4B39;"></i>) son obligatorios.</p>
				<div class="form-group col-md-6 col-md-offset-0">
					<label for="nombre">Nombre</label> (<i class="fa fa-asterisk" style="color: #DD4B39;"></i>)
					<input class="form-control ver" name="nombre" id="nombre" type="text" value="">
				</div>
				<div class="form-group col-md-3 col-md-offset-0">
					<label for="dias">Dias</label> (<i class="fa fa-asterisk" style="color: #DD4B39;"></i>)
					<input class="form-control ver" name="dias" id="dias" type="text" value="">
				</div>
				<div class="form-group col-md-3 col-md-offset-0 editver">
					<label for="activo">Estado</label><br>
					<input class="ver" type="checkbox" id="estado" name="activo" value="1">
				</div>
						
			</div>

			<div class="modal-footer">	
				<button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
				<button type="submit" class="btn btn-primary submit">Crear</button>

				<input type="hidden" name="id_terminoPago" id="id_terminoPago">
				<input type="hidden" name="_token" value="{{ csrf_token() }}">
				<input type="hidden" id="activo" name="activo" value="1">
				<input type="hidden" name="id_empresa" value="{{ Auth::user()->id_empresa }}">

      		</div>
      		</form>
			
	    </div>
	  </div>
	</div>
	<!-- /modal -->


	<script type="text/javascript">

		$( document ).ready(function() {
			$('#eliminar').click(function(event) {
		    	if(!confirm('¿Deseas realmente eliminar este Termino de pago?'))
					event.preventDefault();
			});		    

		});

		//funcion usada para pasarle parametros al modal
		function modal(id_terminoPago,nombre,dias,estado, tipo){
			
			if(tipo=='edit'){
				$("#myModalLabel").text("Editar Termino de Pago");	
				$("#form").attr("action","{{ url('/editTerminoPago') }}");
				$(".editver").css("display", "");
				$(".submit").css("display", "");
				$(".submit").text("Actualizar");
				$(".ver").prop("disabled", false);
				$("#activo").prop("disabled", true);
			}
			else if(tipo=='ver'){
				$("#myModalLabel").text("Ver Termino de Pago");	
				$(".editver").css("display", "");
				$(".submit").css("display", "none");
				$(".ver").prop("disabled", true);
				$("#activo").prop("disabled", true);
			}
			else{
				$("#myModalLabel").text("Nuevo Termino de Pago");	
				$("#form").attr("action","{{ url('/nuevoTerminoPago') }}");
				$(".editver").css("display", "none");
				$(".submit").css("display", "");
				$(".submit").text("Crear");
				$(".ver").prop("disabled", false);
				$("#activo").prop("disabled", false);
			}


			estado = (estado != '1') ? false : true;//checkear checkbox
			$("#id_terminoPago").attr("value", id_terminoPago);//edit/ver
			$("#nombre").attr("value",nombre);
			$("#dias").attr("value",dias);
			$("#estado").prop("checked", estado);//edit/ver
			
		}
	   
	</script>
@endsection

