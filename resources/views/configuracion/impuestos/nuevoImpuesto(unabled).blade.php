@extends('adminlte::layouts.app')

@section('htmlheader_title')
	{{ trans('adminlte_lang::message.home') }}
@endsection


@section('main-content')
	<div class="container-fluid spark-screen">
		<div class="row">
			<div class="col-md-12 col-md-offset-0">
				<!-- Default box -->
				<div class="box box-primary">
					<div class="box-header with-border">
						@if($cualidad==1)
						<h3 class="box-title">Nuevo Impuesto</h3>
						@else
						<h3 class="box-title">Nueva retencion</h3>
						@endif

						<div class="box-tools pull-right">
							<button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
								<i class="fa fa-minus"></i></button>
							<button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
								<i class="fa fa-times"></i></button>
						</div>
					</div>

					<form action="{{ url('/nuevoImpuesto') }}" method="post">
					<div class="box-body">

						@if(count($errors)>0)
						<div class="form-group col-md-12 col-md-offset-0">
							<div class="alert alert-danger alert-dismissible">
				                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
				                <h4><i class="icon fa fa-ban"></i> Error en el formulario</h4>
				                @foreach ($errors->all() as $message) 
	    							<span > {{$message}}</span><br>
								@endforeach
				             </div>
						</div>
						@endif

						<div class="form-group col-md-4 col-md-offset-0">
							<label for="nombre">Nombre</label>
							<input class="form-control" name="nombre" id="nombre" type="text" value="">
						</div>
						<div class="form-group col-md-4 col-md-offset-0">
							<label for="id_tipoImpuesto">Tipo</label>
							<select class="form-control" name="id_tipoImpuesto" id="id_tipoImpuesto">
							@foreach($tipos as $tipo)
								<option value="{{ $tipo->id_tipoImpuesto }}"> {{ $tipo->nombre }}</option>
							@endforeach
							</select>
						</div>
						<div class="form-group col-md-2 col-md-offset-0">
							<label for="porcentaje">Porcentaje</label>
							<input class="form-control" name="porcentaje" id="porcentaje" type="text" value="">
						</div>
						<div class="form-group col-md-6 col-md-offset-0">
							<label for="descripcion">Descripcion</label>
							<textarea class="form-control" rows="3" name="descripcion"></textarea>
						</div>
						
					</div>
					<div class="box-footer">
                		<div class="col-md-2 col-md-offset-10">
							<button type="submit" class="btn btn-primary btn-block btn-flat">Crear</button>
						</div>
						<input type="hidden" name="_token" value="{{ csrf_token() }}">
						<input type="hidden" name="activa" value="1">
						<input type="hidden" name="id_empresa" value="{{ Auth::user()->id_empresa }}">
						<input type="hidden" name="cualidad" value="{{ $cualidad }}">

              		</div>
              		</form>
					
				</div>
				<!-- /.box -->
			</div>
		</div>
	</div>
@endsection