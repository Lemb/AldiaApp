@extends('adminlte::layouts.app')

@section('htmlheader_title')
	{{ trans('adminlte_lang::message.home') }}
@endsection


@section('main-content')
	<script type="text/javascript">
		$("#contentHeader2").append('Impuestos');
	</script>
	<?php 
    	$permisos = $_SESSION['permisos']; 
	?>
	<div class="container-fluid spark-screen">
		<div class="row">
			<div class="col-md-12 col-md-offset-0">
				<!-- Default box -->
				<div class="box">
					<div class="box-header with-border">
						<h3 class="box-title">Impuestos</h3>

						<div class="box-tools pull-right">
							<button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
								<i class="fa fa-minus"></i></button>
							<button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
								<i class="fa fa-times"></i></button>
						</div>
					</div>
					<div class="box-body">
						@if($permisos['7']->insertar==1)
						<div class="col-md-2 col-md-offset-0" style="margin-bottom: 10px;">
							<a class="btn btn-primary btn-block btn-flat" data-toggle="modal" data-target="#myModal" onclick="modal('', 1,'','', '', '', 'nuevo');"><i class="fa fa-plus" aria-hidden="true"></i> Nuevo</a>
						</div>
						@endif
						@if(count($errors->EImpuesto)>0)
						<div class="form-group col-md-12 col-md-offset-0">
							<div class="alert alert-danger alert-dismissible">
				                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
				                <h4><i class="icon fa fa-ban"></i> Error en el formulario</h4>
				                @foreach ($errors->EImpuesto->all() as $message) 
	    							<span > {{$message}}</span><br>
								@endforeach
				             </div>
						</div>
						@endif
						@if($mensaje=='Impuesto Creado' || $mensaje=='Impuesto Eliminado' || $mensaje == 'Impuesto actualizado')
						<div class="form-group col-md-8 col-md-offset-2">
							<div class="alert alert-success alert-dismissible">
						        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
						        <h4><i class="icon fa fa-check"></i> {{ $mensaje }}</h4>
						    </div>
						</div>
						@endif
						<table class="table table-bordered table-hover dataTable"
							data-toggle="table"
							data-pagination="true"
							data-search="true"
							data-show-refresh="true"
       						data-show-columns="true">
			                <thead>
				                <tr>
				                  <th>Nombre</th>

				                  <th>Porcentaje</th>
				                  <th>Estado</th>
				                  <th data-halign="center" data-align="center" data-width="17%">Acciones</th>
				                </tr>
				            </thead>
				        	<tbody>
				                @foreach($impuestos as $impuesto) 
						    		<tr>
						    			<td> {{$impuesto->nombre }} 		</td>
						    			<td> {{$impuesto->porcentaje }} %	</td>
						    			@if($impuesto->activa==1)
						    			<td>Activo</td>
						    			@else
						    			<td>Inactivo</td>
						    			@endif
						    			<td>
						    				@if($permisos['7']->ver==1)
						    				<a class="btn btn-success" style="padding: 2px 10px;" data-toggle="modal" data-target="#myModal" onclick="modal('{{$impuesto->id_impuesto}}', 1,'{{$impuesto->nombre}}','{{$impuesto->porcentaje}}', '{{$impuesto->descripcion}}', '{{$impuesto->activa}}', 'ver');">
  												<i class="fa fa-eye" aria-hidden="true"></i>
  											</a>
  											@endif
											@if($permisos['7']->eliminar==1)
											<a class="btn btn-danger" style="padding: 2px 10px;" href="{{ url('/eliminarImpuesto/'.$impuesto->id_impuesto) }}" aria-label="Eliminar" id="eliminar">
											 <i class="fa fa-times" aria-hidden="true"></i>
											</a>
											@endif
										</td>
						    		</tr>
								@endforeach
			              	</tbody>	
              			</table>						
					</div>
					
				</div>
				<!-- /.box -->
			</div>



			<div class="col-md-12 col-md-offset-0">
				<!-- Default box -->
				<div class="box">
					<div class="box-header with-border">
						<h3 class="box-title">Retenciones</h3>

						<div class="box-tools pull-right">
							<button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
								<i class="fa fa-minus"></i></button>
							<button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
								<i class="fa fa-times"></i></button>
						</div>
					</div>
					<div class="box-body">
						<div class="col-md-2 col-md-offset-0" style="margin-bottom: 10px;">
							<a class="btn btn-primary btn-block btn-flat" data-toggle="modal" data-target="#myModal" onclick="modal('', 2,'','', '', '', 'nuevo');"><i class="fa fa-plus" aria-hidden="true"></i> Nuevo</a>
						</div>
						@if(count($errors->ERetencion)>0)
						<div class="form-group col-md-12 col-md-offset-0">
							<div class="alert alert-danger alert-dismissible">
				                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
				                <h4><i class="icon fa fa-ban"></i> Error en el formulario</h4>
				                @foreach ($errors->ERetencion->all() as $message) 
	    							<span > {{$message}}</span><br>
								@endforeach
				             </div>
						</div>
						@endif
						@if($mensaje=='Retencion Creada' || $mensaje == 'Retencion Eliminada' || $mensaje == 'Retencion actualizada')
						<div class="form-group col-md-8 col-md-offset-2">
							<div class="alert alert-success alert-dismissible">
						        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
						        <h4><i class="icon fa fa-check"></i> {{ $mensaje }}</h4>
						    </div>
						</div>
						@endif
						<table class="table table-bordered table-hover dataTable"
							data-toggle="table"
							data-pagination="true"
							data-search="true"
							data-show-refresh="true"
       						data-show-columns="true">
			                <thead>
				                <tr>
				                  <th>Nombre</th>
				                  <th>Porcentaje</th>
				                  <th>Estado</th>
				                  <th data-halign="center" data-align="center" data-width="17%">Acciones</th>
				                </tr>
				            </thead>
				        	<tbody>
				                @foreach($retenciones as $retencion) 
						    		<tr>
						    			<td> {{$retencion->nombre }} 		</td>
						    			<td> {{$retencion->porcentaje }} %	</td>
						    			@if($retencion->activa==1)
						    			<td>Activo</td>
						    			@else
						    			<td>Inactivo</td>
						    			@endif
						    			<td>
						    				@if($permisos['7']->ver==1)
						    				<a class="btn btn-success" style="padding: 2px 10px;" data-toggle="modal" data-target="#myModal" onclick="modal('{{$retencion->id_impuesto}}', 2,'{{$retencion->nombre}}','{{$retencion->porcentaje}}', '{{$retencion->descripcion}}', '{{$retencion->activa}}', 'ver');">
  												<i class="fa fa-eye" aria-hidden="true"></i>
  											</a>
  											@endif
											@if($permisos['7']->eliminar==1)
											<a class="btn btn-danger" style="padding: 2px 10px;" href="{{ url('/eliminarImpuesto/'.$retencion->id_impuesto) }}" aria-label="Eliminar" id="eliminarr">
											 <i class="fa fa-times" aria-hidden="true"></i>
											</a>
											@endif
										</td>
						    		</tr>
								@endforeach
			              	</tbody>	
              			</table>						
					</div>
					
				</div>
				<!-- /.box -->
			</div>
		</div>
	</div>



	
	<!-- modal -->
	<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	  <div class="modal-dialog" role="document">
	    <div class="modal-content">
	      	<div class="modal-header">
		        <h3 class="box-title" id="myModalLabel">Nuevo Impuesto</h3>
			</div>

			<form action="{{ url('/nuevoTerminoPago') }}" method="post" id="form">
			<div class="box-body">
				<p class="help-block">Campos marcados con asterisco (<i class="fa fa-asterisk" style="color: #DD4B39;"></i>) son obligatorios.</p>
				<div class="form-group col-md-6 col-md-offset-0">
					<label for="nombre">Nombre</label> (<i class="fa fa-asterisk" style="color: #DD4B39;"></i>)
					<input class="form-control ver" name="nombre" id="nombre" type="text" value="">
				</div>
				<div class="form-group col-md-3 col-md-offset-0">
					<label for="porcentaje">Porcentaje</label> (<i class="fa fa-asterisk" style="color: #DD4B39;"></i>)
					<input class="form-control ver" name="porcentaje" id="porcentaje" type="text" value="">
				</div>
				<div class="form-group col-md-6 col-md-offset-0">
					<label for="descripcion">Descripcion</label>
					<textarea class="form-control ver" rows="3" id="descripcion" name="descripcion"></textarea>
				</div>
				<div class="form-group col-md-2 col-md-offset-0 editver">
					<label for="activa">Estado</label><br>
					<input type="checkbox" class="ver"  id="estado" name="activa" value="1">
				</div>
						
			</div>

			<div class="modal-footer">	
				<button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
				<button type="submit" class="btn btn-primary submit">Crear</button>

				<input type="hidden" name="id_impuesto" id="id_impuesto">
				<input type="hidden" name="_token" value="{{ csrf_token() }}">
				<input type="hidden" id="activa" name="activa" value="1">
				<input type="hidden" id="cualidad" name="cualidad" value="1">
				<input type="hidden" name="id_empresa" value="{{ Auth::user()->id_empresa }}">

      		</div>
      		</form>
			
	    </div>
	  </div>
	</div>
	<!-- /modal -->


	<script type="text/javascript">

		$( document ).ready(function() {
			$('#eliminar').click(function(event) {
		    	if(!confirm('¿Deseas realmente eliminar este Impuesto?'))
					event.preventDefault();
			});		    

		});

		//funcion usada para pasarle parametros al modal
		function modal(id_impuesto, cualidad,nombre,porcentaje, descripcion, estado, tipo){
			
			if(tipo=='edit'){
				if (cualidad==1)
					$("#myModalLabel").text("Editar Impuesto");	
				else if(cualidad==2)
					$("#myModalLabel").text("Editar Retencion");
				$("#form").attr("action","{{ url('/editImpuesto') }}");
				$(".editver").css("display", "");
				$(".submit").css("display", "");
				$(".submit").text("Actualizar");
				$(".ver").prop("disabled", false);
				$("#activa").prop("disabled", true);
			}
			else if(tipo=='ver'){
				if (cualidad==1)
					$("#myModalLabel").text("Ver Impuesto");	
				else if(cualidad==2)
					$("#myModalLabel").text("Ver Retencion");
				$(".editver").css("display", "");
				$(".submit").css("display", "none");
				$(".ver").prop("disabled", true);
				$("#activa").prop("disabled", true);
			}
			else{
				if (cualidad==1)
					$("#myModalLabel").text("Nuevo Impuesto");	
				else if(cualidad==2)
					$("#myModalLabel").text("Nueva Retencion");	

				$("#form").attr("action","{{ url('/nuevoImpuesto') }}");
				$(".editver").css("display", "none");
				$(".submit").css("display", "");
				$(".submit").text("Crear");
				$(".ver").prop("disabled", false);
				$("#activa").prop("disabled", false);
			}


			estado = (estado != '1') ? false : true;//checkear checkbox
			$("#id_impuesto").attr("value", id_impuesto);//edit/ver
			$("#cualidad").attr("value",cualidad);
			$("#nombre").attr("value",nombre);
			$("#porcentaje").attr("value",porcentaje);
			$("#descripcion").text(descripcion);
			$("#estado").prop("checked", estado);//edit/ver
			
		}
	   
	</script>
@endsection
