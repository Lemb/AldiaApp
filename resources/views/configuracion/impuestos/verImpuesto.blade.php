@extends('adminlte::layouts.app')

@section('htmlheader_title')
	{{ trans('adminlte_lang::message.home') }}
@endsection


@section('main-content')
	<div class="container-fluid spark-screen">
		<div class="row">
			<div class="col-md-12 col-md-offset-0">
				<!-- Default box -->
				<div class="box box-primary">
					<div class="box-header with-border">
						@if($impuesto->cualidad==1)
						<h3 class="box-title">Ver Impuesto</h3>
						@else
						<h3 class="box-title">Ver Retencion</h3>
						@endif

						<div class="box-tools pull-right">
							<button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
								<i class="fa fa-minus"></i></button>
							<button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
								<i class="fa fa-times"></i></button>
						</div>
					</div>

					
					<div class="box-body">

						<div class="form-group col-md-4 col-md-offset-0">
							<label for="nombre">Nombre</label>
							<input class="form-control" name="nombre" id="nombre" type="text" value="{{ $impuesto->nombre}}" disabled="">
						</div>
						<div class="form-group col-md-4 col-md-offset-0">
							<label for="id_tipoImpuesto">Tipo</label>
							<select class="form-control" name="id_tipoImpuesto" id="id_tipoImpuesto" disabled="">
							@foreach($tipos as $tipo)
								<option value="{{ $tipo->id_tipoImpuesto }}"> {{ $tipo->nombre }}</option>
							@endforeach
							</select>
						</div>
						<div class="form-group col-md-2 col-md-offset-0">
							<label for="porcentaje">Porcentaje</label>
							<input class="form-control" name="porcentaje" id="porcentaje" type="text" value="{{ $impuesto->porcentaje}}" disabled="">
						</div>
						<div class="form-group col-md-6 col-md-offset-0">
							<label for="descripcion">Descripcion</label>
							<textarea class="form-control" rows="3" name="descripcion" disabled="">{{ $impuesto->descripcion }}</textarea>
						</div>
						<div class="form-group col-md-2 col-md-offset-0">
							<label for="activa">Estado</label><br>
							<input type="checkbox" id="activa" name="activa" value="1" disabled="">
						</div>
						<div class="form-group col-md-3 col-md-offset-0">
							<?php $checked = ($impuesto->deleted_at!=null) ? 'checked="checked"' : ''; ?>
							<label for="estado">¿Eliminado?</label><br>
							<input type="checkbox" id="estado" name="estado" value="1" disabled="" {{$checked}}>
						</div>
						
						
					</div>
					
				</div>
				<!-- /.box -->
			</div>
		</div>
	</div>

	<script type="text/javascript">
		window.onload = function ()
		{
			document.getElementById("activa").checked = {{ $impuesto->activa }}; 
		}

	</script>
@endsection