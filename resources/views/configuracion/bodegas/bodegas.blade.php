@extends('adminlte::layouts.app')

@section('htmlheader_title')
	{{ trans('adminlte_lang::message.home') }}
@endsection


@section('main-content')
	<script type="text/javascript">
		$("#contentHeader2").append('Bodegas');
	</script>
	<?php 
    	$permisos = $_SESSION['permisos']; 
	?>
	<div class="container-fluid spark-screen">
		<div class="row">
			<div class="col-md-12 col-md-offset-0">
				<!-- Default box -->
				<div class="box box-primary">
					<div class="box-header with-border">
						<h3 class="box-title">Bodegas</h3>

						<div class="box-tools pull-right">
							<button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
								<i class="fa fa-minus"></i></button>
							<button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
								<i class="fa fa-times"></i></button>
						</div>
					</div>
					<div class="box-body">
						@if($permisos['21']->insertar==1)
						<div class="col-md-2 col-md-offset-0" style="margin-bottom: 10px;">
							<a class="btn btn-primary btn-block btn-flat" data-toggle="modal" data-target="#myModal" onclick="modal('','','','','');"><i class="fa fa-plus" aria-hidden="true"></i> Nuevo</a>
						</div>
						@endif
						@if($mensaje)
						<div class="form-group col-md-8 col-md-offset-2">
							<div class="alert alert-success alert-dismissible">
						        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
						        <h4><i class="icon fa fa-check"></i> {{ $mensaje }}</h4>
						    </div>
						</div>
						@endif
						<table class="table table-bordered table-hover dataTable"
							data-toggle="table"
							data-pagination="true"
							data-search="true"
							data-show-refresh="true"
       						data-show-columns="true">
			                <thead>
				                <tr>
				                  <th>Nombre</th>
				                  <th>Telefono</th>
				                  <th>Direcci&oacute;n</th>
				                  <th data-halign="center" data-align="center" data-width="17%">Acciones</th>
				                </tr>
				            </thead>
				        	<tbody>
				                @foreach($bodegas as $bodega) 
						    		<tr>
						    			<td> {{$bodega->nombre }} 		</td>
						    			<td> {{$bodega->telefono }} 		</td>
						    			<td> {{$bodega->direccion }}  	</td>
						    			<td>
						    				@if($permisos['21']->ver==1)
						    				<a class="btn btn-success" style="padding: 2px 10px;"  href="{{ url('/verBodega/'.$bodega->id_bodega) }}">
  												<i class="fa fa-eye" aria-hidden="true"></i>
  											</a>
  											@endif
  											@if($permisos['21']->modificar==1)
											<a class="btn btn-primary" style="padding: 2px 10px;" data-toggle="modal" data-target="#myModal" onclick="modal('{{$bodega->id_bodega}}','{{$bodega->nombre}}','{{$bodega->direccion}}','{{$bodega->telefono}}','{{$bodega->observaciones}}', '{{$bodega->predeterminada}}', 'edit');" aria-label="editar">
											  <i class="fa fa-pencil" aria-hidden="true"></i>
											</a>
											
											@endif
											@if($permisos['21']->eliminar==1)
											<a class="btn btn-danger eliminar" style="padding: 2px 10px;" href="{{ url('/eliminarBodega/'.$bodega->id_bodega) }}" aria-label="Eliminar">
											 <i class="fa fa-times" aria-hidden="true"></i>
											</a>
											@endif
										</td>
						    		</tr>
								@endforeach
			              	</tbody>	
              			</table>						
					</div>
					
				</div>
				<!-- /.box -->
			</div>
		</div>
	</div>




	<!-- modal -->
	<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	  <div class="modal-dialog modal-lg" role="document">
	    <div class="modal-content">
	      	<div class="modal-header">
		        <h3 class="box-title" id="myModalLabel">Nueva Bodega</h3>
			</div>

			<form action="{{ url('/nuevaNumeracion') }}" method="post" id="form">
			<div class="box-body">
				<p class="help-block">Campos marcados con asterisco (<i class="fa fa-asterisk" style="color: #DD4B39;"></i>) son obligatorios.</p>
				<div class="form-group col-md-4 col-md-offset-0">
					<label for="nombre">Nombre</label> (<i class="fa fa-asterisk" style="color: #DD4B39;"></i>)
					<input class="form-control ver" name="nombre" id="nombre" type="text" value="">
				</div>
				<div class="form-group col-md-4 col-md-offset-0">
					<label for="dias">Direcci&oacute;n</label>
					<input class="form-control ver" name="direccion" id="direccion" type="text" value="">
				</div>
				<div class="form-group col-md-4 col-md-offset-0">
					<label for="nombre">Telefono</label>
					<input class="form-control ver" name="telefono" id="telefono" type="number" value="">
				</div>
				<div class="form-group col-md-6 col-md-offset-0">
					<label for="nombre">Observaciones</label>
					<textarea class="form-control ver" name="observaciones" id="observaciones"></textarea>
				</div>

				<div class="form-group col-md-6 col-md-offset-0">
	                <label>Predeterminada</label><br>
                      <input type="checkbox" name="predeterminada" value="1" id="predeterminada">
              	</div>
			</div>

			<div class="modal-footer">	
				<button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
				<button type="submit" class="btn btn-primary submit">Crear</button>

				<input type="hidden" name="id_bodega" id="id_bodega">
				<input type="hidden" name="_token" value="{{ csrf_token() }}">
				<input type="hidden" name="id_empresa" value="{{ Auth::user()->id_empresa }}">

      		</div>
      		</form>
			
	    </div>
	  </div>
	</div>
	<!-- /modal -->



	<script type="text/javascript">

		$( document ).ready(function() {
  			$('.eliminar').click(function(event) {
		    	if(!confirm('¿Deseas realmente eliminar esta Bodega?'))
					event.preventDefault();
			});
		});
		

		//funcion usada para pasarle parametros al modal
		function modal(id_bodega, nombre, direccion, telefono, observaciones, predeterminada, tipo){
			predeterminada = (predeterminada==1) ? true:false;
			if(tipo=='edit'){
				$("#myModalLabel").text("Editar Bodega");	
				$("#form").attr("action","{{ url('/editBodega') }}");				
				$(".submit").css("display", "");
				$(".submit").text("Actualizar");
				$(".ver").prop("disabled", false);
			}
			else if(tipo=='ver'){
				$("#myModalLabel").text("Ver Bodega");	
				$(".submit").css("display", "none");
				$(".ver").prop("disabled", true);
			}
			else{
				$("#myModalLabel").text("Nueva Bodega");	
				$("#form").attr("action","{{ url('/nuevaBodega') }}");
				$(".submit").css("display", "");
				$(".submit").text("Crear");
				$(".ver").prop("disabled", false);
			}

			$("#id_bodega").attr("value", id_bodega);//edit/ver
			$("#nombre").attr("value",nombre);
			$("#direccion").attr("value",direccion);
			$("#predeterminada").prop("checked",predeterminada);
			$("#telefono").attr("value",telefono);
			$("#observaciones").text(observaciones);//edit/ver
			
		}
	   
	</script>

@endsection
