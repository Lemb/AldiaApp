@extends('adminlte::layouts.app')

@section('htmlheader_title')
	{{ trans('adminlte_lang::message.home') }}
@endsection


@section('main-content')
	<?php 
    	$permisos = $_SESSION['permisos']; 
	?>
	<script type="text/javascript">
		$("#contentHeader1").append('<a href={{url("bodegas")}}> Bodegas </a>');
		$("#contentHeader2").text('Detalle de bodega');
	</script>
	<div class="container-fluid spark-screen">
		<div class="row">
			<div class="col-md-12 col-md-offset-0">
				<!-- Default box -->
				<div class="box box-primary">
					<div class="box-header with-border">
						<h3 class="box-title">Ver Bodega</h3>

						<div class="box-tools pull-right">
							<button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
								<i class="fa fa-minus"></i></button>
							<button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
								<i class="fa fa-times"></i></button>
						</div>
					</div>

					<div class="box-body">

						<div class="form-group col-md-4 col-md-offset-0">
							<label for="nombre">Nombre</label>
							<input class="form-control" name="nombre" id="nombre" type="text" value="{{$bodega->nombre}}" disabled>
						</div>
						<div class="form-group col-md-4 col-md-offset-0">
							<label for="dias">Direcci&oacute;n</label>
							<input class="form-control" name="direccion" id="direccion" type="text" value="{{$bodega->direccion}}" disabled>
						</div>
						<div class="form-group col-md-4 col-md-offset-0">
							<label for="nombre">Telefono</label>
							<input class="form-control" name="telefono" id="telefono" type="number" value="{{$bodega->telefono}}" disabled>
						</div>
						<div class="form-group col-md-6 col-md-offset-0">
							<label for="nombre">Observaciones</label>
							<textarea class="form-control" name="observaciones" id="observaciones" disabled>{{$bodega->observaciones}}</textarea>
						</div>
						<div class="form-group col-md-6 col-md-offset-0">
			                <label>Predeterminada</label><br>
		                	<input type="checkbox" name="predeterminada" value="1" id="predeterminada" disabled="">
              			</div>


						@if(isset($permisos[18]) && $permisos[18]->ver==1)
						<h4 class="box-title form-group col-md-12 col-md-offset-0">Versiones</h3>
						<table class="table table-bordered table-hover dataTable" 
							data-toggle="table"
							data-pagination="true"       						
							>
			                <thead>
				                <tr>
				                	<th>Nombre</th>
				                  	<th>Telefono</th>
				                  	<th>Direccion</th>
				                  	<th>Fecha</th>
				                  	<th>Version</th>
				                  	<th>Responsable</th>
				                  	<th>Detalle</th>
				                </tr>
				            </thead>
				        	<tbody>
				                @foreach($versiones as $version) 
				                	<?php  $referenciaPrincipal = ($version->id_bodega==$bodega->id_bodega) ? 'id=referenciaPrincipal' : ''; 
				                		$referenciaPrevia = '';
				                	?>
						    		<tr {{$referenciaPrincipal}} {{$referenciaPrevia}}>
						    			<td> {{$version->nombre }} 		</td>
						    			<td> {{$version->telefono }} 		</td>
						    			<td> {{$version->direccion }} 	</td>
						    			<td> {{$version->updated_at }} </td>
						    			<td> {{$version->numeroVersion }} </td>
						    			<td> <a href="{{ url('/verUsuario/'.$version->responsableID) }}">  {{$version->responsableNombre.' ('.$version->responsableIdentificacion.')' }}</a> </td>
						    			<td> 

						    				<a class="btn btn-info" style="padding: 2px 10px;" href="{{ url('/verBodega/'.$version->id_bodega) }}" aria-label="Ver Detalle">
  												<i class="fa fa-eye" aria-hidden="true"></i>
  											</a>
  											
						    			</td>
						    		</tr>
								@endforeach
			              	</tbody>	
              			</table>
              			@endif
					</div>

						
					
				</div>
				<!-- /.box -->

			</div>
		</div>
	</div>

<script type="text/javascript">
		
		$( document ).ready(function() {
			$("#referenciaPrincipal").css("background-color", "#DDFCDA");
			$("#referenciaPrevia").css("background-color", "#FAE8E8");
			$("#predeterminada").prop("checked", {{$bodega->predeterminada}});
		});
	</script>
@endsection
