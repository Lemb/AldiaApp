@extends('adminlte::layouts.app')

@section('htmlheader_title')
	{{ trans('adminlte_lang::message.home') }}
@endsection


@section('main-content')
	<?php 
    	$permisos = $_SESSION['permisos']; 
	?>
	<script type="text/javascript">
		@if(isset($permisos[4]))
		$("#contentHeader1").append('<a href={{url("usuarios")}}> Usuarios </a>');
		@endif
		$("#contentHeader2").text('Historial de acciones de usuario');
	</script>
	<div class="container-fluid spark-screen">
		<div class="row">
			<div class="col-md-12 col-md-offset-0">
				<!-- Default box -->
				<div class="box box-primary">
					<div class="box-header with-border">
						<h3 class="box-title">Historial de Acciones de {{$nombreUsuario}}</h3>

						<div class="box-tools pull-right">
							<button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
								<i class="fa fa-minus"></i></button>
							<button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
								<i class="fa fa-times"></i></button>
						</div>
					</div>
					<div class="box-body">

						<table class="table table-bordered table-hover dataTable" 
							data-toggle="table"
							data-pagination="true"
							data-search="true"
							data-show-refresh="true"
       						data-show-columns="true"
       						data-page-size="50"
       						data-page-list="[25, 50, 100, 500]">
			                <thead>
				                <tr>
				                	<th>Descripcion</th>
				                  	<th data-halign="center" data-align="center" data-width="17%">Fecha</th>
				                </tr>
				            </thead>
				        	<tbody>
				                @foreach($acciones as $accion) 
				                	<?php 

				                		$pos1 = strpos($accion->descripcion, '>>');
				                		$pos2 = strpos($accion->descripcion, '<<');
				                		$sub1 = substr($accion->descripcion, 0, $pos1);
				                		$sub2 = substr($accion->descripcion, $pos1, $pos2);

				                		$pos3 = strpos($sub2, '||');
				                		$link = substr($sub2, 2, $pos3-2);
				                		$visual = substr($sub2, $pos3+2, -2);

						    		?>
						    		<tr>
						    			<td> {{$sub1}}<a href="{{url($link)}}">{{$visual}}</a>  </td>
						    			<td>
						    				<?php $date = date_create($accion->created_at)?>
						    				{{date_format($date, 'Y-m-d G:iA')}}
										</td>
						    		</tr>
								@endforeach
			              	</tbody>	
              			</table>						
					</div>
					
				</div>
				<!-- /.box -->
			</div>
		</div>
	</div>
@endsection

