@extends('adminlte::layouts.app')

@section('htmlheader_title')
	{{ trans('adminlte_lang::message.home') }}
@endsection


@section('main-content')
	<?php 
    	$permisos = $_SESSION['permisos']; 
	?>
	<script type="text/javascript">
		$("#contentHeader2").append('Empresa y MiPerfil');
	</script>
	

	<div class="container-fluid spark-screen">
		@if(isset($permisos['3']))
		@if($permisos['3']->ver==1 || $permisos['3']->modificar==1)
		<div class="row">
			<div class="col-md-6 col-md-offset-0">
				<!-- Default box -->
				<div class="box box-primary">
					<div class="box-header with-border">
						<h3 class="box-title">Empresa</h3>

						<div class="box-tools pull-right">
							<button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
								<i class="fa fa-minus"></i></button>
							<button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
								<i class="fa fa-times"></i></button>
						</div>
					</div>

					<form action="{{ url('/empresa') }}" method="post">
					<div class="box-body">
						<p class="help-block">Campos marcados con asterisco (<i class="fa fa-asterisk" style="color: #DD4B39;"></i>) son obligatorios.</p>
							@if(count($errors)>0)
							<div class="form-group col-md-12 col-md-offset-0">
								<div class="alert alert-danger alert-dismissible">
					                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
					                <h4><i class="icon fa fa-ban"></i> Error en el formulario</h4>
					                @foreach ($errors->all() as $message) 
		    							<span > {{$message}}</span><br>
									@endforeach
					             </div>
							</div>
							@endif
							<!-- Bloque para mostrar que se actualizo la empresa correctamente-->
							@if(session('mensaje'))
							<div class="form-group col-md-12 col-md-offset-0">
								<div class="alert alert-success alert-dismissible">
							        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
							        <h4><i class="icon fa fa-check"></i> {{ session('mensaje') }}</h4>
							    </div>
							</div>
							@endif

							<div class="form-group col-md-6 col-md-offset-0">
								<label for="nombreEmp">Nombre</label>(<i class="fa fa-asterisk" style="color: #DD4B39;"></i>)
								<input class="form-control" name="nombre" id="nombre" type="text" value="{{ $empresa->nombre}}">
							</div>
							<div class="form-group col-md-6 col-md-offset-0">
								<label for="nitEmp">Nit</label>
								<input class="form-control" name="nit" id="nitEmp" type="text" value="{{ $empresa->nit }}">
							</div>
							<div class="form-group col-md-12 col-md-offset-0">
								<label for="email">Email</label>
								<input class="form-control" name="email" id="emailEmp" type="email" value="{{  $empresa->email}}">
							</div>
							<div class="form-group col-md-6 col-md-offset-0">
								<label for="telefono">Telefono</label>
								<input class="form-control" name="telefono" id="telefonoEmp" type="text" value="{{ $empresa->telefono}}">
							</div>
							<div class="form-group col-md-12 col-md-offset-0">
								<label for="direccionEmp">Direccion</label>
								<input class="form-control" name="direccion" id="direccionEmp" type="text" value="{{ $empresa->direccion }}">
							</div>
							<div class="form-group col-md-6 col-md-offset-0">
								<label>Regimen</label>
								<select class="form-control" name="id_regimen" id="regimen">
                          			<option value="1">Regimen comun</option>
			                    	<option value="2">Regimen simplificado</option>
                  				</select>
							</div>
							<div class="form-group col-md-6 col-md-offset-0">
								<label>Documento de Facturaci&oacute;n</label>
								<select class="form-control" name="id_documentoFacturacion" id="documentoFacturacionSelect">
									<option value="1">Factura de Venta</option>
			                    	<option value="2">Remision</option>
                  				</select>
							</div>
							<div class="form-group col-md-8 col-md-offset-0">
								<label>Moneda</label>
								<select class="form-control" name="id_moneda" id="monedaEmp">
			                    	<option value="1">Peso Colombiano (COP)</option>
                  				</select>
							</div>
							<!--
							<div class="form-group col-md-4 col-md-offset-0">
								<label>Codigo de Barras</label>
								<div class="checkbox">
				                    <label><input type="checkbox" name="codigoBarras"></label>
				                 </div>
							</div>

							<div class="form-group col-md-12 col-md-offset-0">
								<label>Logo</label>
								<input id="logoEmp" name="logo" type="file" value="{{ $empresa->logo}}" class="file" multiple data-show-upload="false" data-show-cancel="false" data-show-remove="false" data-show-details="false">

							</div>						

							-->
						
					</div>
					@if($permisos['3']->modificar==1)
					<div class="box-footer">
                		<div class="col-md-3 col-md-offset-9">
							<button type="submit" class="btn btn-primary btn-block btn-flat">Actualizar</button>
						</div>
						<input type="hidden" name="_token" value="{{ csrf_token() }}">
						<input type="hidden" name="id_empresa" value="{{ Auth::user()->id_empresa }}">
              		</div>
              		@endif
              		</form>
					
				</div>
				<!-- /.box -->
			</div>
			@endif
			@endif

			@if(isset($permisos['2']))
			@if($permisos['2']->ver==1 || $permisos['2']->modificar==1)
			<div class="col-md-6 col-md-offset-0">
				<!-- Default box -->
				<div class="box box-success">
					<div class="box-header with-border">
						<h3 class="box-title">MiPerfil</h3>

						<div class="box-tools pull-right">
							<button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
								<i class="fa fa-minus"></i></button>
							<button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
								<i class="fa fa-times"></i></button>
						</div>
					</div>

					<form action="{{ url('/perfil') }} " method="post">
					<div class="box-body">
						<p class="help-block">Campos marcados con asterisco (<i class="fa fa-asterisk" style="color: #DD4B39;"></i>) son obligatorios.</p>
							@if(count($errors->errores)>0)
							<div class="form-group col-md-12 col-md-offset-0">
								<div class="alert alert-danger alert-dismissible">
					                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
					                <h4><i class="icon fa fa-ban"></i> Error en el formulario</h4>
					                @foreach ($errors->errores->all() as $message) 
		    							<span > {{$message}}</span><br>
									@endforeach
					             </div>
							</div>
							@endif
							<!-- Bloque para mostrar que se actualizo la empresa correctamente-->
							@if(session('mensajeU'))
							<div class="form-group col-md-12 col-md-offset-0">
								<div class="alert alert-success alert-dismissible">
							        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
							        <h4><i class="icon fa fa-check"></i> {{ session('mensajeU') }}</h4>
							    </div>
							</div>
							@endif

							<div class="form-group col-md-6 col-md-offset-0">
								<label for="nombreEmp">Nombre</label> 
								<input class="form-control" name="nombre" id="nombre" type="text" value="{{ Auth::user()->nombre}}">
							</div>
							<div class="form-group col-md-6 col-md-offset-0">
								<label for="nitEmp">Apellido</label>
								<input class="form-control" name="apellido" id="apellido" type="text" value="{{ Auth::user()->apellido }}">
							</div>
							<div class="form-group col-md-12 col-md-offset-0">
								<label for="email">Email</label> (<i class="fa fa-asterisk" style="color: #DD4B39;"></i>)
								<input class="form-control" name="email" id="email" type="email" value="{{  Auth::user()->email }}">
							</div>
							<div class="form-group col-md-6 col-md-offset-0">
								<label for="telefono">Celular</label>
								<input class="form-control" name="celular" id="celular" type="text" value="{{ Auth::user()->celular}}">
							</div>
							<div class="form-group col-md-6 col-md-offset-0">
								<label for="telefono">Telefono</label>
								<input class="form-control" name="telefono" id="telefono" type="text" value="{{ Auth::user()->telefono}}">
							</div>
							<div class="form-group col-md-12 col-md-offset-0">
								<label for="direccionEmp">Direccion</label>
								<input class="form-control" name="direccion" id="direccionEmp" type="text" value="{{  Auth::user()->direccion }}">
							</div>				
						
					</div>
					@if($permisos['2']->modificar==1)
					<div class="box-footer">
                		<div class="col-md-3 col-md-offset-9">
							<button type="submit" class="btn btn-primary btn-block btn-flat">Actualizar</button>
						</div>
						<input type="hidden" name="_token" value="{{ csrf_token() }}">
						<input type="hidden" name="id_usuario" value="{{ Auth::user()->id_usuario }}">
						<input type="hidden" name="id_empresa" value="{{ Auth::user()->id_empresa }}">
              		</div>
              		@endif
              		</form>
					
				</div>
				</div>
				@endif
				@endif



				@if(isset($permisos['3']))
				@if($permisos['3']->ver==1 || $permisos['3']->modificar==1)

				<div class="col-md-6 col-md-offset-0">
				<!-- Default box -->
				<div class="box box-warning">
					<div class="box-header with-border">
						<h3 class="box-title">Configuraci&oacute;n general de facturaci&oacute;n</h3>

						<div class="box-tools pull-right">
							<button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
								<i class="fa fa-minus"></i></button>
							<button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
								<i class="fa fa-times"></i></button>
						</div>
					</div>

					<form action="{{ url('/facturacionGeneral') }} " method="post">
					<div class="box-body">
							@if(count($errors->errorF)>0)
							<div class="form-group col-md-12 col-md-offset-0">
								<div class="alert alert-danger alert-dismissible">
					                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
					                <h4><i class="icon fa fa-ban"></i> Error en el formulario</h4>
					                @foreach ($errors->errorF->all() as $message) 
		    							<span > {{$message}}</span><br>
									@endforeach
					             </div>
							</div>
							@endif
							<!-- Bloque para mostrar que se actualizo la empresa correctamente-->
							@if(session('mensajeF'))
							<div class="form-group col-md-12 col-md-offset-0">
								<div class="alert alert-success alert-dismissible">
							        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
							        <h4><i class="icon fa fa-check"></i> {{ session('mensajeF') }}</h4>
							    </div>
							</div>
							@endif

							<div class="form-group col-md-12 col-md-offset-0">
								<label for="fac_terminos">Terminos y condiciones</label>
								<textarea class="form-control" rows="5" name="fac_terminos" placeholder="Condiciones relacionadas a la factura...">{{$empresa->fac_terminos }}</textarea>
							</div>
							<!--
							<div class="form-group col-md-12 col-md-offset-0">
								<label for="fac_notas">Notas de facturas</label>
								<textarea class="form-control" rows="3" name="fac_notas" placeholder="Nota al pie de factura ...">{{ $empresa->fac_notas }}</textarea>
							</div>					
						-->
						
					</div>
					@if($permisos['3']->modificar==1)
					<div class="box-footer">
                		<div class="col-md-3 col-md-offset-9">
							<button type="submit" class="btn btn-primary btn-block btn-flat">Actualizar</button>
						</div>
						<input type="hidden" name="_token" value="{{ csrf_token() }}">
						<input type="hidden" name="id_empresa" value="{{ Auth::user()->id_empresa }}">
              		</div>
              		@endif
              		</form>

					
				</div>

				</div>
				@endif
				@endif
				<!--end box-->
	<!-- scripts para determinar que va en los selects por defect-->	
	<script type="text/javascript">
		window.onload = function ()
		{
			document.getElementById("regimen").value = {{ $empresa->id_regimen }}; 
			document.getElementById("documentoFacturacionSelect").value = {{ $empresa->id_documentoFacturacion }};
			document.getElementById("monedaEmp").value = {{ $empresa->id_moneda }};

			$("#logoEmp").fileinput({
			    overwriteInitial: true,
			    maxFileSize: 1500,
			    showClose: false,
			    showCaption: false,
			    browseLabel: '',
			    removeLabel: '',
			    browseIcon: '<i class="glyphicon glyphicon-folder-open"></i>',
			    removeIcon: '<i class="glyphicon glyphicon-remove"></i>',
			    removeTitle: 'Cancel or reset changes',
			    elErrorContainer: '#kv-avatar-errors-1',
			    msgErrorClass: 'alert alert-block alert-danger',
			    layoutTemplates: {main2: '{preview} ' +  btnCust + ' {remove} {browse}'},
			    allowedFileExtensions: ["jpg", "png", "gif"]
			});
		}
	</script>
@endsection
