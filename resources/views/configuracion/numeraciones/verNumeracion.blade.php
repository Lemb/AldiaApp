@extends('adminlte::layouts.app')

@section('htmlheader_title')
	{{ trans('adminlte_lang::message.home') }}
@endsection


@section('main-content')
	<div class="container-fluid spark-screen">
		<div class="row">
			<div class="col-md-12 col-md-offset-0">
				<!-- Default box -->
				<div class="box box-primary">
					<div class="box-header with-border">
						<h3 class="box-title">Ver Numeracion</h3>

						<div class="box-tools pull-right">
							<button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
								<i class="fa fa-minus"></i></button>
							<button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
								<i class="fa fa-times"></i></button>
						</div>
					</div>

					<div class="box-body">						

						<div class="form-group col-md-4 col-md-offset-0">
							<label for="nombre">Nombre</label>
							<input class="form-control" name="nombre" id="nombre" type="text" value="{{ $numeracion->nombre }}" disabled="">
						</div>
						<div class="form-group col-md-2 col-md-offset-0">
							<label for="prefijo">Prefijo</label>
							<input class="form-control" name="prefijo" id="prefijo" type="text" value="{{ $numeracion->prefijo }}" disabled="">
						</div>
						<div class="form-group col-md-2 col-md-offset-0">
							<label for="numeroI">Numero Inicial</label>
							<input class="form-control" name="numeroI" id="numeroI" type="text" value="{{ $numeracion->numeroI }}" disabled="">
						</div>
						<div class="form-group col-md-2 col-md-offset-0">
							<label for="numeroI">Numero Actual</label>
							<input class="form-control" name="numeroA" id="numeroA" type="text" value="{{ $numeracion->numeroA }}" disabled="">
						</div>
						<div class="form-group col-md-2 col-md-offset-0">
							<label for="numeroF">Numero Final</label>
							<input class="form-control" name="numeroF" id="numeroF" type="text" value="{{ $numeracion->numeroF }}" disabled="">
						</div>
						<div class="form-group col-md-6 col-md-offset-0">
							<label for="resolucion">Resolucion</label>
							<textarea class="form-control" rows="3" name="resolucion" placeholder="Resolucion del estatuto bla bla bla..." disabled="">{{ $numeracion->resolucion }}</textarea>
						</div>
						<div class="form-group col-md-3 col-md-offset-0">
							<?php $checked = ($numeracion->preterminada==0) ? '' : 'checked="checked"'; ?>
							<label for="preterminada">Preterminada</label><br>
							<input type="checkbox" id="preterminada" name="preterminada" value="1" disabled="" {{$checked}}>
						</div>
						<div class="form-group col-md-3 col-md-offset-0">
							<?php $checked = ($numeracion->deleted_at!=null) ? 'checked="checked"' : ''; ?>
							<label for="estado">¿Eliminada?</label><br>
							<input type="checkbox" id="estado" name="estado" value="1" disabled="" {{$checked}}>
						</div>
						
					</div>
				</div>
				<!-- /.box -->
			</div>
		</div>
	</div>

@endsection