@extends('adminlte::layouts.app')

@section('htmlheader_title')
	{{ trans('adminlte_lang::message.home') }}
@endsection


@section('main-content')
	<div class="container-fluid spark-screen">
		<div class="row">
			<div class="col-md-12 col-md-offset-0">
				<!-- Default box -->
				<div class="box box-primary">
					<div class="box-header with-border">
						<h3 class="box-title">Editar Numeracion</h3>

						<div class="box-tools pull-right">
							<button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
								<i class="fa fa-minus"></i></button>
							<button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
								<i class="fa fa-times"></i></button>
						</div>
					</div>

					<form action="{{ url('/editNumeracion') }}" method="post">
					<div class="box-body">

						@if(count($errors)>0)
						<div class="form-group col-md-12 col-md-offset-0">
							<div class="alert alert-danger alert-dismissible">
				                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
				                <h4><i class="icon fa fa-ban"></i> Error en el formulario</h4>
				                @foreach ($errors->all() as $message) 
	    							<span > {{$message}}</span><br>
								@endforeach
				             </div>
						</div>
						@endif

						<!-- Bloque para mostrar que se actualizo la empresa correctamente-->
						@if(session('mensaje'))
						<div class="form-group col-md-12 col-md-offset-0">
							<div class="alert alert-success alert-dismissible">
						        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
						        <h4><i class="icon fa fa-check"></i> {{ session('mensaje') }}</h4>
						    </div>
						</div>
						@endif

						<div class="form-group col-md-4 col-md-offset-0">
							<label for="nombre">Nombre</label>
							<input class="form-control" name="nombre" id="nombre" type="text" value="{{ $numeracion->nombre }}">
						</div>
						<div class="form-group col-md-2 col-md-offset-0">
							<label for="prefijo">Prefijo</label>
							<input class="form-control" name="prefijo" id="prefijo" type="text" value="{{ $numeracion->prefijo }}">
						</div>
						<div class="form-group col-md-2 col-md-offset-0">
							<label for="numeroI">Numero Inicial</label>
							<input class="form-control" name="numeroI" id="numeroI" type="text" value="{{ $numeracion->numeroI }}">
						</div>
						<div class="form-group col-md-2 col-md-offset-0">
							<label for="numeroI">Numero Actual</label>
							<input class="form-control" name="numeroA" id="numeroA" type="text" value="{{ $numeracion->numeroA }}">
						</div>
						<div class="form-group col-md-2 col-md-offset-0">
							<label for="numeroF">Numero Final</label>
							<input class="form-control" name="numeroF" id="numeroF" type="text" value="{{ $numeracion->numeroF }} ">
						</div>
						<div class="form-group col-md-6 col-md-offset-0">
							<label for="resolucion">Resolucion</label>
							<textarea class="form-control" rows="3" name="resolucion" placeholder="Resolucion del estatuto bla bla bla...">{{ $numeracion->resolucion }}</textarea>
						</div>
						
						<div class="form-group col-md-2 col-md-offset-0">
							<label for="preterminada">Preterminada</label><br>
							<input type="checkbox" id="preterminada" name="preterminada" value="1">
						</div>
						
						<div class="form-group col-md-2 col-md-offset-0">
							<label for="activa">Estado</label><br>
							<input type="checkbox" id="activa" name="activa" value="1">
						</div>
						
						
						
					</div>
					<div class="box-footer">
                		<div class="col-md-2 col-md-offset-10">
							<button type="submit" class="btn btn-primary btn-block btn-flat">Actualizar</button>
						</div>
						<input type="hidden" name="_token" value="{{ csrf_token() }}">						
						<input type="hidden" name="id_empresa" value="{{ Auth::user()->id_empresa }}">
						<input type="hidden" name="id_numeracion" value="{{ $numeracion->id_numeracion }}">

              		</div>
              		</form>
					
				</div>
				<!-- /.box -->
			</div>
		</div>
	</div>

	<script type="text/javascript">
		window.onload = function ()
		{
			document.getElementById("preterminada").checked = {{ $numeracion->preterminada }}; 
			document.getElementById("activa").checked = {{ $numeracion->activa }}; 
		}

	</script>
@endsection