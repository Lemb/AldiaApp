@extends('adminlte::layouts.app')

@section('htmlheader_title')
	{{ trans('adminlte_lang::message.home') }}
@endsection


@section('main-content')
	<script type="text/javascript">
		$("#contentHeader2").append('Numeraciones');
	</script>
	<?php 
    	$permisos = $_SESSION['permisos']; 
	?>
	<div class="container-fluid spark-screen">
		<div class="row">
			<div class="col-md-12 col-md-offset-0">
				<!-- Default box -->
				<div class="box box-primary">
					<div class="box-header with-border">
						<h3 class="box-title">Siguientes númeraciones de Documentos</h3>

						<div class="box-tools pull-right">
							<button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
								<i class="fa fa-minus"></i></button>
							<button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
								<i class="fa fa-times"></i></button>
						</div>
					</div>
					<form method="post" action="{{ url('/editNumeracionDoc') }}">
					<div class="box-body">


						@if(count($errors)>0)
						<div class="form-group col-md-12 col-md-offset-0">
							<div class="alert alert-danger alert-dismissible">
				                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
				                <h4><i class="icon fa fa-ban"></i> Error en el formulario</h4>
				                @foreach ($errors->all() as $message) 
	    							<span > {{$message}}</span><br>
								@endforeach
				             </div>
						</div>
						@endif

						@if(session('mensajeD'))
						<div class="form-group col-md-12 col-md-offset-0">
							<div class="alert alert-success alert-dismissible">
						        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
						        <h4><i class="icon fa fa-check"></i> {{ session('mensajeD') }}</h4>
						    </div>
						</div>
						@endif
						<div class="form-group col-md-4 col-md-offset-0">
							<label for="SigNum_reciboCaja">N&uacute;mero de recibo de caja</label>
							<input class="form-control" name="SigNum_reciboCaja" id="SigNum_reciboCaja" type="text" value="{{ $SigNum->SigNum_reciboCaja }}">
						</div>
						<div class="form-group col-md-4 col-md-offset-0">
							<label for="SigNum_comprobantePago">N&uacute;mero de comprobante de egreso</label>
							<input class="form-control" name="SigNum_comprobantePago" id="SigNum_comprobantePago" type="text" value="{{ $SigNum->SigNum_comprobantePago }}">
						</div>
						<div class="form-group col-md-4 col-md-offset-0">
							<label for="SigNum_notaCredito">N&uacute;mero de nota cr&eacute;dito</label>
							<input class="form-control" name="SigNum_notaCredito" id="SigNum_notaCredito" type="text" value="{{ $SigNum->SigNum_notaCredito }}">
						</div>
						<div class="form-group col-md-4 col-md-offset-0">
							<label for="SigNum_remision">N&uacute;mero de remisión</label>
							<input class="form-control" name="SigNum_remision" id="SigNum_remision" type="text" value="{{ $SigNum->SigNum_remision }}">
						</div>
						<div class="form-group col-md-4 col-md-offset-0">
							<label for="SigNum_cotizacion">N&uacute;mero de cotización</label>
							<input class="form-control" name="SigNum_cotizacion" id="SigNum_cotizacion" type="text" value="{{ $SigNum->SigNum_cotizacion }}">
						</div>
						<div class="form-group col-md-4 col-md-offset-0">
							<label for="SigNum_ordenCompra">N&uacute;mero de &oacute;rden de compra</label>
							<input class="form-control" name="SigNum_ordenCompra" id="SigNum_ordenCompra" type="text" value="{{ $SigNum->SigNum_ordenCompra }}">
						</div>
						
					</div>

					@if($permisos['6']->modificar==1)
					<div class="box-footer">
                		<div class="col-md-2 col-md-offset-10">
							<button type="submit" class="btn btn-primary btn-block btn-flat">Actualizar</button>
						</div>
						<input type="hidden" name="_token" value="{{ csrf_token() }}">
						<input type="hidden" name="id_empresa" value="{{ Auth::user()->id_empresa }}">
              		</div>
              		@endif

					</form>
					
				</div>
				<!-- /.box -->









				<!-- Default box -->
				<div class="box box-primary">
					<div class="box-header with-border">
						<h3 class="box-title">Numeraciones de Facturas de venta</h3>

						<div class="box-tools pull-right">
							<button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
								<i class="fa fa-minus"></i></button>
							<button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
								<i class="fa fa-times"></i></button>
						</div>
					</div>
					<div class="box-body">
						@if($permisos['6']->insertar==1)
						<div class="col-md-2 col-md-offset-0" style="margin-bottom: 10px;">
							<a class="btn btn-primary btn-block btn-flat" data-toggle="modal" data-target="#myModal" onclick="modal('','','','','','','','','', false);"><i class="fa fa-plus" aria-hidden="true"></i> Nuevo</a>
						</div>
						@endif
						@if(count($errors->errorF)>0)
						<div class="form-group col-md-12 col-md-offset-0">
							<div class="alert alert-danger alert-dismissible">
				                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
				                <h4><i class="icon fa fa-ban"></i> Error en el formulario</h4>
				                @foreach ($errors->errorF->all() as $message) 
	    							<span > {{$message}}</span><br>
								@endforeach
				             </div>
						</div>
						@endif

						@if($mensaje)
						<div class="form-group col-md-8 col-md-offset-2">
							<div class="alert alert-success alert-dismissible">
						        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
						        <h4><i class="icon fa fa-check"></i> {{ $mensaje }}</h4>
						    </div>
						</div>
						@endif
						<table class="table table-bordered table-hover dataTable"
							data-toggle="table"
							data-pagination="true"
							data-search="true"
							data-show-refresh="true"
       						data-show-columns="true">
			                <thead>
				                <tr>
				                  <th>Nombre</th>
				                  <th>Prefijo</th>
				                  <th>Numeracion</th>
				                  <th>Estado</th>
				                  <th>Resolucion</th>
				                  <th data-halign="center" data-align="center" data-width="17%">Acciones</th>
				                </tr>
				            </thead>
				        	<tbody>
				                @foreach($numeraciones as $numeracion) 
						    		<tr>
						    			<td> {{$numeracion->nombre }} 		</td>
						    			<td> {{$numeracion->prefijo }} 	</td>
						    			<td> {{$numeracion->numeroA }} </td>
						    			@if($numeracion->activa == 1)
						    			<td> Activa </td>
						    			@else
						    			<td> Inactiva </td>
						    			@endif
						    			<td> {{$numeracion->resolucion }} </td>
						    			<td>
						    				@if($permisos['6']->ver==1)
						    				<a class="btn btn-success" style="padding: 2px 10px;" data-toggle="modal" data-target="#myModal"
											onclick="modal('{{$numeracion->id_numeracion}}','{{$numeracion->nombre}}','{{$numeracion->prefijo}}','{{$numeracion->numeroI}}','{{$numeracion->numeroA}}','{{$numeracion->numeroF}}','{{$numeracion->resolucion}}', '{{$numeracion->preterminada}}','{{$numeracion->activa}}', 'ver');">
  												<i class="fa fa-eye" aria-hidden="true"></i>
  											</a>
  											@endif
  											@if($permisos['6']->modificar==1)
											<a class="btn btn-primary" style="padding: 2px 10px;"  data-toggle="modal" data-target="#myModal"
											onclick="modal('{{$numeracion->id_numeracion}}','{{$numeracion->nombre}}','{{$numeracion->prefijo}}','{{$numeracion->numeroI}}','{{$numeracion->numeroA}}','{{$numeracion->numeroF}}','{{$numeracion->resolucion}}', '{{$numeracion->preterminada}}','{{$numeracion->activa}}', 'edit');" aria-label="editar">
											  <i class="fa fa-pencil" aria-hidden="true"></i>
											</a>
											@endif
											@if($permisos['6']->eliminar==1)
											<a class="btn btn-danger" style="padding: 2px 10px;" href="{{ url('/eliminarNumeracion/'.$numeracion->id_numeracion.'/'.$numeracion->id_empresa) }}" aria-label="Eliminar" id="eliminar">
											 <i class="fa fa-times" aria-hidden="true"></i>
											</a>
											@endif
										</td>
						    		</tr>
								@endforeach
			              	</tbody>	
              			</table>						
					</div>
					
				</div>
				<!-- /.box -->
			</div>
		</div>
	</div>







	<!-- modal -->
	<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	  <div class="modal-dialog modal-lg" role="document">
	    <div class="modal-content">
	      	<div class="modal-header">
		        <h3 class="box-title" id="myModalLabel">Nueva Numeracion</h3>
			</div>

			<form action="{{ url('/nuevaNumeracion') }}" method="post" id="form">
			<div class="box-body">
				<p class="help-block">Campos marcados con asterisco (<i class="fa fa-asterisk" style="color: #DD4B39;"></i>) son obligatorios.</p>
				<div class="form-group col-md-4 col-md-offset-0">
					<label for="nombre">Nombre</label> (<i class="fa fa-asterisk" style="color: #DD4B39;"></i>)
					<input class="form-control ver" name="nombre" id="nombre" type="text" value="">
				</div>
				<div class="form-group col-md-2 col-md-offset-0">
					<label for="prefijo">Prefijo</label>
					<input class="form-control ver" name="prefijo" id="prefijo" type="text" value="">
				</div>
				<div class="form-group col-md-2 col-md-offset-0">
					<label for="numeroI">Numero Inicial</label>(<i class="fa fa-asterisk" style="color: #DD4B39;"></i>)
					<input class="form-control ver" name="numeroI" id="numeroI" type="text" value="">
				</div>
				<div class="form-group col-md-2 col-md-offset-0 editver">
					<label for="numeroA">Numero Actual</label> 
					<input class="form-control ver" name="numeroA" id="numeroA" type="text" value="">
				</div>
				<div class="form-group col-md-2 col-md-offset-0">
					<label for="numeroF">Numero Final</label>(<i class="fa fa-asterisk" style="color: #DD4B39;"></i>)
					<input class="form-control ver" name="numeroF" id="numeroF" type="text" value="">
				</div>
				<div class="form-group col-md-6 col-md-offset-0">
					<label for="resolucion">Resolucion</label>
					<textarea class="form-control ver" rows="3" id="resolucion" name="resolucion" placeholder="Resolucion del estatuto bla bla bla..."></textarea>
				</div>
				
				<div class="form-group col-md-3 col-md-offset-0">
					<label for="preterminada">Predeterminada</label><br>
					<input class="ver" type="checkbox" id="predeterminada" name="preterminada" value="1">
				</div>
				<div class="form-group col-md-2 col-md-offset-0 editver">
					<label for="activa">Estado</label><br>
					<input class="ver" type="checkbox" id="estado" name="activa" value="1">
				</div>		
			</div>

			<div class="modal-footer">	
				<button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
				<button type="submit" class="btn btn-primary submit">Crear</button>

				<input type="hidden" name="id_numeracion" id="id_numeracion">
				<input type="hidden" name="_token" value="{{ csrf_token() }}">
				<input id="activa" type="hidden" name="activa" value="1">
				<input type="hidden" name="id_empresa" value="{{ Auth::user()->id_empresa }}">

      		</div>
      		</form>
			
	    </div>
	  </div>
	</div>
	<!-- /modal -->


	<script type="text/javascript">

		$( document ).ready(function() {
  			$('#eliminar').click(function(event) {
		    	if(!confirm('¿Deseas realmente eliminar esta numeracion?'))
					event.preventDefault();
			});
		});
		

		//funcion usada para pasarle parametros al modal
		function modal(id_numeracion,nombre,prefijo,numeroI,numeroA,numeroF, resolucion, predeterminada, estado, tipo){
			
			if(tipo=='edit'){
				$("#myModalLabel").text("Editar Numeracion");	
				$("#form").attr("action","{{ url('/editNumeracion') }}");
				$(".editver").css("display", "");
				$(".submit").css("display", "");
				$(".submit").text("Actualizar");
				$(".ver").prop("disabled", false);
				$("#activa").prop("disabled", true);
			}
			else if(tipo=='ver'){
				$("#myModalLabel").text("Ver Numeracion");	
				$(".editver").css("display", "");
				$(".submit").css("display", "none");
				$(".ver").prop("disabled", true);
				$("#activa").prop("disabled", true);
			}
			else{
				$("#myModalLabel").text("Nueva Numeracion");	
				$("#form").attr("action","{{ url('/nuevaNumeracion') }}");
				$(".editver").css("display", "none");
				$(".submit").css("display", "");
				$(".submit").text("Crear");
				$(".ver").prop("disabled", false);
				$("#activa").prop("disabled", false);
			}


			predeterminada = (predeterminada != '1') ? false : true;//checkear checkbox
			estado = (estado != '1') ? false : true;//checkear checkbox
			$("#id_numeracion").attr("value", id_numeracion);//edit/ver
			$("#nombre").attr("value",nombre);
			$("#prefijo").attr("value",prefijo);
			$("#numeroI").attr("value",numeroI);
			$("#numeroA").attr("value",numeroA);//edit/ver
			$("#numeroF").attr("value",numeroF);
			$("#resolucion").text(resolucion);
			$("#predeterminada").prop("checked", predeterminada);
			$("#estado").prop("checked", estado);//edit/ver
			
		}
	   
	</script>

@endsection

