@extends('adminlte::layouts.app')

@section('htmlheader_title')
	{{ trans('adminlte_lang::message.home') }}
@endsection


@section('main-content')
	<script type="text/javascript">
		$("#contentHeader1").append('<a href={{url("perfiles")}}> Perfiles</a>');
		$("#contentHeader2").text('Crear nuevo Perfil');
	</script>
	<div class="container-fluid spark-screen">
		<div class="row">
			<div class="col-md-12 col-md-offset-0">
				<!-- Default box -->
				<div class="box box-primary">
					<div class="box-header with-border">
						<h3 class="box-title">Perfil</h3>

						<div class="box-tools pull-right">
							<button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
								<i class="fa fa-minus"></i></button>
							<button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
								<i class="fa fa-times"></i></button>
						</div>
					</div>

					<form action="{{ url('/nuevoPerfil') }}" method="post">
					<div class="box-body">
						<p class="help-block">Campos marcados con asterisco (<i class="fa fa-asterisk" style="color: #DD4B39;"></i>) son obligatorios.</p>

						@if(count($errors)>0)
						<div class="form-group col-md-12 col-md-offset-0">
							<div class="alert alert-danger alert-dismissible">
				                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
				                <h4><i class="icon fa fa-ban"></i> Error en el formulario</h4>
				                @foreach ($errors->all() as $message) 
	    							<span > {{$message}}</span><br>
								@endforeach
				             </div>
						</div>
						@endif
						<div class="row">
						<div class="form-group col-md-4 col-md-offset-0">
							<label for="nombre">Nombre</label> (<i class="fa fa-asterisk" style="color: #DD4B39;"></i>)
							<input class="form-control" name="nombre" id="nombre" type="text" value="">
						</div>
						<div class="form-group col-md-6 col-md-offset-0">
							<label for="descripcion">Descripcion</label>
							<textarea class="form-control" rows="3" name="descripcion" placeholder="Descripcion del perfil"></textarea>
						</div>
						</div>
						<h4 class="box-title">Permisos</h4>
						

						<table class="table table-bordered table-hover dataTable"
							data-toggle="table">

			                <thead>
				                <tr>
				                  <th data-halign="center">Permisos</th>
				                  <th data-halign="center" data-align="center" data-width="10%">Todo</th>
				                  <th data-halign="center" data-align="center" data-width="10%">Crear</th>
				                  <th data-halign="center" data-align="center" data-width="10%">Ver</th>
				                  <th data-halign="center" data-align="center" data-width="10%">Modificar</th>
				                  <th data-halign="center" data-align="center" data-width="10%">Eliminar</th>
				                  
				                </tr>
				            </thead>
				        	<tbody>
				        		<tr>
					    			<td><b>TODOS</b></td>
					    			<td><input type="checkbox" class="todo" onclick="checkingAll($(this));"></td>
					    			<td><input type="checkbox" class="insertar" onclick="checking($(this));"></td>
					    			<td><input type="checkbox" class="ver" onclick="checking($(this));"></td>
									<td><input type="checkbox" class="modificar" onclick="checking($(this));"></td>
									<td><input type="checkbox" class="eliminar" onclick="checking($(this));"></td>
									
					    		</tr>

				        		@foreach($permisos  as $permiso)
					    		<tr>
					    			<td>{{$permiso->descripcion}}</td>
					    			<td><input type="checkbox" class="todo" value="{{$permiso->id_permiso}}" onchange="checkrow($(this));"> </td>
					    			
					    			<td>
					    				@if($permiso->insertar==1)
					    				<input type="checkbox" class="insertar" name="insertar[]" onclick="checkrowDetect($(this));" value="{{$permiso->id_permiso}}"> 
					    				@endif
					    			</td>
					    			<td>
					    				@if($permiso->ver==1)
					    				<input type="checkbox" class="ver" name="ver[]" onclick="checkrowDetect($(this));" value="{{$permiso->id_permiso}}"> 
					    				@endif
					    			</td>
									<td>
										@if($permiso->modificar==1)
										<input type="checkbox" class="modificar" name="modificar[]" onclick="checkrowDetect($(this));" value="{{$permiso->id_permiso}}"> 
										@endif
									</td>
									<td>
										@if($permiso->eliminar==1)
										<input type="checkbox" class="eliminar" name="eliminar[]" onclick="checkrowDetect($(this));"  value="{{$permiso->id_permiso}}"> 
										@endif
									</td>
									
					    		</tr>
					    		@endforeach
			            	</tbody>	
              			</table>				
						
					</div>



					<div class="box-footer">
                		<div class="col-md-2 col-md-offset-10">
							<button type="submit" class="btn btn-primary btn-block btn-flat">Crear</button>
						</div>
						<input type="hidden" name="_token" value="{{ csrf_token() }}">
						<input type="hidden" name="id_empresa" value="{{ Auth::user()->id_empresa }}">
              		</div>
              		</form>
				</div>
				<!-- /.box -->


			</div>
		</div>
	</div>
	<script type="text/javascript" src="{{ asset('/js/Configuracion/Perfiles.js') }}"></script>
@endsection

