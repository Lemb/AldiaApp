@extends('adminlte::layouts.app')

@section('htmlheader_title')
	{{ trans('adminlte_lang::message.home') }}
@endsection


@section('main-content')
	<script type="text/javascript">
		$("#contentHeader1").append('<a href={{url("perfiles")}}> Perfiles</a>');
		$("#contentHeader2").text('Detalle de perfil');
	</script>
	<div class="container-fluid spark-screen">
		<div class="row">
			<div class="col-md-12 col-md-offset-0">
				<!-- Default box -->
				<div class="box box-primary">
					<div class="box-header with-border">
						<h3 class="box-title">Perfil</h3>

						<div class="box-tools pull-right">
							<button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
								<i class="fa fa-minus"></i></button>
							<button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
								<i class="fa fa-times"></i></button>
						</div>
					</div>

			
					<div class="box-body">
			
						<div class="row">
						<div class="form-group col-md-4 col-md-offset-0">
							<label for="nombre">Nombre</label>
							<input class="form-control" name="nombre" id="nombre" type="text" value="{{$perfil->nombre}}" disabled="">
						</div>
						<div class="form-group col-md-6 col-md-offset-0">
							<label for="descripcion">Descripcion</label>
							<textarea class="form-control" rows="3" name="descripcion" placeholder="Descripcion del perfil" disabled="">{{$perfil->descripcion}}</textarea>
						</div>
						<div class="form-group col-md-2 col-md-offset-0">
							<?php $checked = ($perfil->deleted_at!=null) ? 'checked="checked"' : ''; ?>
							<label for="estado">¿Eliminado?</label><br>
							<input type="checkbox" id="estado" name="estado" value="1" disabled="" {{$checked}}>
						</div>
						</div>

						<h4 class="box-title">Permisos</h4>
						

						<table class="table table-bordered table-hover dataTable"
							data-toggle="table">

			                <thead>
				                <tr>
				                  <th data-halign="center">Permisos</th>
				                  <th data-halign="center" data-align="center" data-width="10%">Crear</th>
				                  <th data-halign="center" data-align="center" data-width="10%">Ver</th>
				                  <th data-halign="center" data-align="center" data-width="10%">Modificar</th>
				                  <th data-halign="center" data-align="center" data-width="10%">Eliminar</th>
				                  
				                </tr>
				            </thead>
				        	<tbody>
				        		@foreach($permisos  as $permiso)
				        			<?php
				        					$insertarC 	= '';
				    						$verC 		= '';
				    						$modificarC = '';
				    						$eliminarC 	= '';
				        			foreach($permisosEdit as $permisoEdit){
				        				if($permisoEdit->id_permiso == $permiso->id_permiso){
				    						$insertarC 	= ($permisoEdit->insertar==1) ? 'checked="checked"':'';
				    						$verC 		= ($permisoEdit->ver==1) ? 'checked="checked"':'';
				    						$modificarC = ($permisoEdit->modificar==1) ? 'checked="checked"':'';
				    						$eliminarC 	= ($permisoEdit->eliminar==1) ? 'checked="checked"':'';
				        				}
				        			}
				        			?>
					    		<tr>
					    			<td>{{$permiso->descripcion}}</td>
					    			
					    			<td>
					    				@if($permiso->insertar==1)
					    				<input type="checkbox" class="insertar" name="insertar[]" onclick="checkrowDetect($(this));" value="{{$permiso->id_permiso}}" {{$insertarC}} disabled="disabled"> 
					    				 @endif
					    			</td>
					    			<td>
					    				@if($permiso->ver==1)
					    				<input type="checkbox" class="ver" name="ver[]" onclick="checkrowDetect($(this));" value="{{$permiso->id_permiso}}" {{$verC}} disabled="disabled"> 
					    				@endif
					    			</td>
									<td>
										@if($permiso->modificar==1)
										<input type="checkbox" class="modificar" name="modificar[]" onclick="checkrowDetect($(this));" value="{{$permiso->id_permiso}}" {{$modificarC}} disabled="disabled"> 
										@endif
									</td>
									<td>
										@if($permiso->eliminar==1)
										<input type="checkbox" class="eliminar" name="eliminar[]" onclick="checkrowDetect($(this));"  value="{{$permiso->id_permiso}}" {{$eliminarC}} disabled="disabled"> 
										@endif
									</td>
									
					    		</tr>
					    		@endforeach
			            	</tbody>	
              			</table>

					</div>
				</div>
				<!-- /.box -->




			</div>
		</div>
	</div>

@endsection

