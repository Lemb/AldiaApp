	<h4>Items de compra</h4>

	<div class="col-md-1 col-md-offset-0 btones" style="margin-bottom: 5px;">
	<button type="button" class="btn btn-block btn-success " onclick="nuevoItem();"><i class="fa fa-plus-circle" aria-hidden="true"></i></button>
	</div>
	<div class="col-md-1 col-md-offset-0 btones" style="margin-bottom: 5px;">
	<button type="button" class="btn btn-block btn-danger " onclick="menosItem();"><i class="fa fa-minus-circle" aria-hidden="true"></i></button>
	</div>

	<div class="col-md-11 col-md-offset-2">
	</div>

	<?php $r=0; ?>
	@foreach($itemsXgreso as $itemsX)


	<div class="row rowItem r{{$r}}" id="rowItem">
		<div class="form-group col-md-3 col-md-offset-0">
			<label for="id_item">Item</label>
			<select class="form-control input-sm item r{{$r}}" name="id_item[]" id="id_item" onchange="comboboxItemCorrecion($(this));">
			<option value="0" onclick="habilitarSubmit();"></option>
			@foreach($items as $item)
				<?php $selected = ($item->id_item == $itemsX->id_item) ? "selected='selected'" : '';?>

				@if($item->referencia)
	        	<option value="{{ $item->id_item }}" {{$selected}}>{{ $item->nombre.' ('.$item->referencia.')'}}</option>
	        	@else
	        	<option value="{{ $item->id_item }}" {{$selected}}>{{ $item->nombre}}</option>
	        	@endif
	        @endforeach
          	</select>
        
			<label for="referencia">Referencia</label>
			<input class="form-control input-sm referencia r{{$r}}" name="referencia[]" id="referencia" type="text" value="{{$itemsX->referencia}}" >
		
		</div>
		
		<div class="form-group col-md-2 col-md-offset-0">
			<label for="valor">Valor</label>
			<input class="form-control input-sm valor r{{$r}}" name="valor[]" id="valor" type="text" value="{{$itemsX->valor}}" onchange="posicion($(this));" step="any" min="0">
			<label>Cantidad</label>
			<input class="form-control input-sm cantidad r{{$r}}" type="number" name="cantidad[]" value="{{$itemsX->cantidad}}" onchange="posicion($(this));" step="any" min="0" onkeyup="itemMax($(this));">
		</div>
		
		<div class="form-group col-md-2 col-md-offset-0">
			<label for="id_descuento">Descuento %</label>
			<input class="form-control input-sm descuento r{{$r}}" name="descuento[]" id="descuento" type="number" value="{{$itemsX->descuento}}" onchange="posicion($(this));" step="any" >

			<label for="id_impuesto">Impuesto</label>
			<select class="form-control input-sm impuesto r{{$r}}" name="id_impuesto{{$r}}[]" id="0"  onchange="posicion($(this));" multiple="multiple">
			@foreach($impuestos as $impuesto)

				@if($impuesto->activa==1)
					<?php
					$selected = "";
					foreach($gresosXimpuestos as $gresoXimpuesto){
						$selected .= ($gresoXimpuesto->id_impuesto == $impuesto->id_impuesto && $gresoXimpuesto->id_gresosXitems == $itemsX->id_gresosXitems) ? "selected='selected'":'';
					}
					?>
	        		<option value="{{ $impuesto->id_impuesto }}" id="{{ $impuesto->id_impuesto }}" class="{{$impuesto->porcentaje}}" {{$selected}}>{{ $impuesto->nombre.' ('.$impuesto->porcentaje.'%)'}}</option>
	        	@endif
	        @endforeach

          	</select>
		</div>
		<div class="form-group col-md-2 col-md-offset-0">
			<label for="apunte">Apunte</label>
			<textarea class="form-control input-sm apunte r{{$r}}" rows="3" name="apunte[]" id="apunte">{{$itemsX->apunte}}</textarea>
		</div>
		<div class="form-group col-md-1 col-md-offset-0"><br>
			<button class="btn form-control input-sm images r{{$r}}" {{($itemsX->imagen!=null) ? 'href='.URL::to('/').'/storage/imagenes/'.$itemsX->id_item.str_replace(" ","",$itemsX->imagen):'disabled'}}>

  				<i class="fa fa-picture-o fa-lg"></i>
  			</button>
		</div>
		<div class="form-group col-md-2 col-md-offset-0">
			<label>Subtotal</label>
			<input class="form-control input-sm subtotal r{{$r}}" type="text" name="subtotalI[]" value="{{$itemsX->subtotal}}" readonly="readonly">
			<label>Total</label>
			<input class="form-control input-sm total r{{$r}}" type="text" name="totalI[]" value="{{$itemsX->total}}" readonly="readonly">
		</div>						
	</div>
	<?php $r++; ?>
	@endforeach
	<div class="form-group col-md-2 col-md-offset-10">
		<label>Subtotal:</label>
		<input class="form-control input-sm" type="text" name="subtotal" id="subtotal" value="{{$factura->subtotal}}" readonly="readonly">
		<div id="impuestoDiv" class="impuestoDiv">
			<label>Impuestos</label>
			<input class="form-control input-sm impuestoFinal" type="text" value="{{$factura->total-$factura->subtotal}}" disabled="disabled">
		</div>
	</div>
	<input type="hidden" value="{{$r}}" id="r">
	<script type="text/javascript">

			//utilizados con el fin de habilitar el plugin de select2
			var items = [[],[],[],[],[]];
			/*	0 = referencia
				1 = precio
				2 = id_impuesto
			*/
			<?php $i=0;  ?>
			@foreach($items as $item)

				items[0][{{$i}}]		= {{$item->id_item}};
				items[1][{{$i}}]		= '{{$item->referencia}}';
				items[2][{{$i}}]		= '{{$item->precio}}';
				items[3][{{$i}}]		= "{{ ($item->imagen!=null) ? URL::to('/').'/storage/imagenes/'.$item->id_item.$item->imagen:''}}";//imagen
				items[4][{{$i}}]		= "{{$item->medida}}";
				<?php $i++; ?>
			@endforeach


			var impuestos = [[],[],[]];
			/*
			   0 = id_impuesto
			   1 = nombre
			   2 = porcentaje
			*/
			<?php $i=0;  ?>
			@foreach($impuestos as $impuesto)
				impuestos[0][{{$i}}] = {{$impuesto->id_impuesto}};
				impuestos[1][{{$i}}] = "{{$impuesto->nombre}}";
				impuestos[2][{{$i}}] = "{{$impuesto->porcentaje}}"
				<?php $i++; ?>
			@endforeach
			
	</script>