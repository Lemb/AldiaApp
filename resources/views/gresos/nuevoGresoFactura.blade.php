	<h4>Items de venta</h4>

	<div class="col-md-1 col-md-offset-0" style="margin-bottom: 5px;">
	<button type="button" class="btn btn-block btn-success " onclick="nuevoItem();"><i class="fa fa-plus-circle" aria-hidden="true"></i></button>
	</div>
	<div class="col-md-1 col-md-offset-0" style="margin-bottom: 5px;">
	<button type="button" class="btn btn-block btn-danger " onclick="menosItem();"><i class="fa fa-minus-circle" aria-hidden="true"></i></button>
	</div>

	<div class="col-md-11 col-md-offset-2">
	</div>
	<div class="row rowItem r0" id="rowItem">
		<div class="form-group col-md-3 col-md-offset-0">
			<label for="id_item">Item</label>
			<select class="form-control input-sm item r0" name="id_item[]" id="id_item" onchange="comboboxItemCorrecion($(this));">
			<option value="" onclick="habilitarSubmit();"></option>
			<?php $i = 0; ?>
			@foreach($items as $item)
				@if($item->referencia)
	        	<option value="{{ $item->id_item }}" >{{ $item->nombre.' ('.$item->referencia.')'}}</option>
	        	@else
	        	<option value="{{ $item->id_item }}" class="{{$item->codigoBarras}}">{{ $item->nombre}}</option>
	        	@endif

	        @endforeach
          	</select>
        
			<label for="referencia">Referencia</label>
			<input class="form-control input-sm referencia r0" name="referencia[]" id="referencia" type="text" value="" >
		
		</div>
		
		
		<div class="form-group col-md-2 col-md-offset-0">
			<label for="valor">Valor</label>
			<input class="form-control input-sm valor r0" name="valor[]" id="valor" type="text" onkeyup="posicion($(this));" onchange="posicion($(this));" step="any" min="0">
			<label>Cantidad</label>
			<input class="form-control input-sm cantidad r0" type="number" name="cantidad[]" value="1" onchange="posicion($(this));" onkeyup="itemMax($(this));" step="any" min="0" readonly>
		</div>
		
		<div class="form-group col-md-2 col-md-offset-0">
			<label for="id_descuento">Descuento %</label>
			<input class="form-control input-sm descuento r0" name="descuento[]" id="descuento" type="number" value="0" onchange="posicion($(this));" step="any">

			<label for="id_impuesto">Impuesto</label>
			<select class="form-control input-sm impuesto r0" name="id_impuesto0[]" id="0" onchange="posicion($(this));" multiple="multiple">
			@foreach($impuestos as $impuesto)
				@if($impuesto->activa==1)
	        	<option value="{{ $impuesto->id_impuesto }}" id="{{ $impuesto->id_impuesto }}" class="{{$impuesto->porcentaje}}">{{ $impuesto->nombre.' ('.$impuesto->porcentaje.'%)'}}</option>
	        	@endif
	        	
	        @endforeach
          	</select>
		</div>
		<div class="form-group col-md-2 col-md-offset-0">
			<label for="apunte">Apunte</label>
			<textarea class="form-control input-sm apunte r0" rows="3" name="apunte[]" id="apunte"></textarea>
		</div>
		<div class="form-group col-md-1 col-md-offset-0"><br>
			<button class="btn form-control input-sm images r0" href="" disabled>
  				<i class="fa fa-picture-o fa-lg"></i>
  			</button>
		</div>
		<div class="form-group col-md-2 col-md-offset-0">
			<label>Subtotal</label>
			<input class="form-control input-sm subtotal r0" type="text" name="subtotalI[]" value="0" readonly="readonly">
			<label>Total</label>
			<input class="form-control input-sm total r0" type="text" name="totalI[]" value="0" readonly="readonly">
		</div>
	</div>

	<div class="form-group col-md-2 col-md-offset-10">
		<label>Subtotal:</label>
		<input class="form-control input-sm" type="text" name="subtotal" id="subtotal" value="0" readonly="readonly">
		<div id="impuestoDiv" class="impuestoDiv">
			<label>Impuestos</label>
			<input class="form-control input-sm impuestoFinal" type="text" value="0" disabled="disabled">
		</div>
	</div>

	<script type="text/javascript">

			//utilizados con el fin de habilitar el plugin de select2
			var items = [[],[],[],[],[]];
			/*	
				0 = id_item
				1 = referencia
				2 = precio
				3 = imagen
				4 = medida
			*/
			<?php $i=0;  ?>
			@foreach($items as $item)
				items[0][{{$i}}]		= {{$item->id_item}};
				items[1][{{$i}}]		= '{{$item->referencia}}';
				items[2][{{$i}}]		= '{{$item->precio}}';
				items[3][{{$i}}]		= "{{ ($item->imagen!=null) ? URL::to('/').'/storage/imagenes/'.$item->id_item.$item->imagen:''}}";//imagen
				items[4][{{$i}}]		= "{{$item->medida}}";
				
				<?php $i++; ?>
			@endforeach


			var impuestos = [[],[],[]];
			/*
			   0 = id_impuesto
			   1 = nombre
			   2 = porcentaje
			*/
			<?php $i=0;  ?>
			@foreach($impuestos as $impuesto)
				impuestos[0][{{$i}}] = {{$impuesto->id_impuesto}};
				impuestos[1][{{$i}}] = "{{$impuesto->nombre}}";
				impuestos[2][{{$i}}] = "{{$impuesto->porcentaje}}"
				<?php $i++; ?>
			@endforeach
			
	</script>

	