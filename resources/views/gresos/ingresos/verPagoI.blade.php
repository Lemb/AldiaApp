@extends('adminlte::layouts.app')

@section('htmlheader_title')
	{{ trans('adminlte_lang::message.home') }}
@endsection


@section('main-content')
	<script type="text/javascript">
		$("#contentHeader1").append('<a href={{url("pagosI")}}><i class="fa fa-money"></i> Pagos recibidos</a>');
		$("#contentHeader2").text('Detalle de Pago');
	</script>
	<?php 
    	$permisos = $_SESSION['permisos']; 
	?>
	<div class="container-fluid spark-screen">
		<div class="row">
			<div class="col-md-12 col-md-offset-0">
				<!-- Default box -->
				<div class="box box-primary">
					<div class="box-header with-border">
						<h3 class="box-title">Ver Ingreso</h3>

						<div class="box-tools pull-right">
							<button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
								<i class="fa fa-minus"></i></button>
							<button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
								<i class="fa fa-times"></i></button>
						</div>
					</div>


					<div class="box-body">
						
						<div class="row">
						<div class="form-group col-md-3 col-md-offset-0">
							<label for="numero">Numero</label>
							<input class="form-control" name="numero" id="numero" type="text" value="{{$pago->numero}}" disabled="disabled">
						</div>
						<div class="form-group col-md-3 col-md-offset-0">
							<label for="id_contacto">Cliente</label>
							<select class="form-control"  id="id_contacto" disabled="disabled">
							<option></option>
							@foreach($clientes as $cliente)
								<?php $selected = ($cliente->id_contacto== $pago->id_contacto) ? "selected='selected'" : ""; ?>
								@if($cliente->nit)
					        	<option value="{{ $cliente->id_contacto }}" onclick="ajaxClient({{$cliente->id_contacto}});" {{$selected}}>{{ $cliente->nombre.' ('.$cliente->nit.')'}}</option>
					        	@else
					        	<option value="{{ $cliente->id_contacto }}" onclick="ajaxClient({{$cliente->id_contacto}});" {{$selected}}>{{ $cliente->nombre}}</option>
					        	@endif
					        @endforeach
		                  	</select>
						</div>
						<div class="form-group col-md-3 col-md-offset-0">
							<label for="id_banco">Banco</label>
							<select class="form-control" name="id_banco" id="id_banco" disabled="disabled">
							@foreach($bancos as $banco)
							<?php $selected = ($banco->id_banco== $pago->id_banco) ? "selected='selected'" : ""; ?>
					        	<option value="{{ $banco->id_banco }}" {{$selected}}>{{ $banco->nombre}}</option>
					        @endforeach
		                  	</select>
						</div>
						<div class="form-group col-md-3 col-md-offset-0">
							<label for="id_metodoPago">Metodo de pago</label>
							<select class="form-control" name="id_metodoPago" id="id_metodoPago" disabled="disabled">
							@foreach($metodosPago as $metodos)
								<?php $selected = ($metodos->id_metodoPago== $pago->id_metodoPago) ? "selected='selected'" : ""; ?>
					        	<option value="{{ $metodos->id_metodoPago }}" {{$selected}}>{{ $metodos->nombre}}</option>
					        @endforeach
		                  	</select>
						</div>
						<div class="form-group col-md-3 col-md-offset-0">
							<label for="fechaI">Fecha</label>
							<div class="input-group date" id="date1">
			                  <div class="input-group-addon">
			                    <i class="fa fa-calendar"></i>
			                  </div>

			                  <input class="form-control pull-right" id="text" type="text" name="fechaI" value="{{ $pago->fechaI}}" disabled="disabled">
			                </div>
						</div>
						<div class="form-group col-md-3 col-md-offset-0">
							<label for="id_vendedor">Encargado</label>
							<select class="form-control" name="id_vendedor" id="id_vendedor" disabled="disabled">
							@foreach($vendedores as $vendedor)
								<?php $selected = ($vendedor->id_usuario== $pago->id_vendedor) ? 'selected=selected':''; ?>
					        	<option value="{{ $vendedor->id_usuario }}" {{$selected}}>{{ $vendedor->nombre.' ('.$vendedor->identificacion.')'}}</option>
					        @endforeach
		                  	</select>
						</div>
						
						<div class="form-group col-md-3 col-md-offset-0">
							<label for="notas">Notas</label>
							<textarea class="form-control" rows="2" name="notas" id="notas" disabled="disabled" >{{ $pago->notas}}</textarea>
						</div>
						</div>



						@if(count($gresoXgresosRef)>0)
							@include('gresos.editGresoPagoFacturasRef')
						@elseif(count($gresosXcategorias)>0)
							@include('gresos.editGresoPagoCategorias')
						@endif

						<div class="form-group col-md-4 col-md-offset-0">
							<?php $checked = ($pago->deleted_at !=null) ? 'checked=checked':'';?>
							<label for="eliminadoPregunta">¿Eliminado?</label><br>
							<input type="checkbox" value="1" {{$checked}} disabled="disabled">
						</div>

              			<div class="col-md-3 col-sm-6 col-xs-12 col-md-offset-5">
				          <div class="info-box" style="box-shadow: 1px 1px 5px #888888;">
				            <span class="info-box-icon bg-green"><i class="fa fa-money" aria-hidden="true"></i></span>

				            <div class="info-box-content">
				              <span class="info-box-text">Total</span>
				              <span class="info-box-number" id="totalTotalSpan">{{$pago->total}}</span>
				            </div>
				            <!-- /.info-box-content -->
				          </div>
				          <!-- /.info-box -->
				        </div>						
				          <!-- /.totalTotal box -->


				    	@if(isset($permisos[18]) && $permisos[18]->ver==1)
						<h4 class="box-title form-group col-md-12 col-md-offset-0">Versiones</h3>
						<table class="table table-bordered table-hover dataTable" 
							data-toggle="table"
							data-pagination="true"       						
							>
			                <thead>
				                <tr>
				                	<th>Numero</th>
				                  	<th>Contacto</th>
				                  	<th>Total</th>
				                  	<th>Fecha</th>
				                  	<th>Version</th>
				                  	<th>Responsable</th>
				                  	<th>Detalle</th>
				                </tr>
				            </thead>
				        	<tbody>
				                @foreach($versiones as $version) 
				                	<?php  $referenciaPrincipal = ($version->id_greso==$pago->id_greso) ? 'id=referenciaPrincipal' : ''; 
				                	?>
						    		<tr {{$referenciaPrincipal}} >
						    			<td> {{$version->numero }} 		</td>
						    			<td> {{$version->contacto }} 		</td>
						    			<td> {{$version->total }} 		</td>
						    			<td> {{$version->updated_at }} </td>
						    			<td> {{$version->numeroVersion }} </td>
						    			<td> <a href="{{ url('/verUsuario/'.$version->responsableID) }}">  {{$version->responsableNombre.' ('.$version->responsableIdentificacion.')' }}</a> </td>
						    			<td> 
						    				<a class="btn btn-info" style="padding: 2px 10px;" href="{{ url('/verPagoI/'.$version->id_greso) }}" aria-label="Ver Detalle">
  												<i class="fa fa-eye" aria-hidden="true"></i>
  											</a>
						    			</td>
						    		</tr>
								@endforeach
			              	</tbody>	
              			</table>
              			@endif		
					</div>
              		</form>
					
				</div>
				<!-- /.box -->
			</div>
		</div>
	</div>

	<script type="text/javascript">
		
		$( document ).ready(function() {
			$('#mas').css('display', 'none');
			$('#menos').css('display', 'none');
  			$('.valor').prop('disabled', true);
  			$('.descuento').prop('disabled', true);
  			$('.impuesto').prop('disabled', true);
  			$('.apunte').prop('disabled', true);
  			$('.cantidad').prop('disabled', true);

  			$('.valorF').prop('disabled', true);
  			$('.maximo').css('display', 'none');
			customdNumber(',');
			$("#referenciaPrincipal").css("background-color", "#DDFCDA");
		});
		
		function customdNumber(separador){
				//categorias
				$('.valor').number(true, 2, '.', separador);
				$('.subtotal').number(true, 2, '.', separador);
				$('.total').number(true, 2, '.', separador);
				$('#subtotal').number(true, 2, '.', separador);
				$('#impuestoFinal').number(true, 2, '.', separador);
				$('#totalTotalSpan').number(true, 2, '.', separador);

				//facturas
				$('.totalF').number(true, 2, '.', separador);
				$('.pagadoF').number(true, 2, '.', separador);
				$('.porpagarF').number(true, 2, '.', separador);
				$('.valorF').number(true, 2, '.', separador);
		}
	</script>
		
@endsection
