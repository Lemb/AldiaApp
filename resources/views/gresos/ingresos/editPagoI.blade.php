@extends('adminlte::layouts.app')

@section('htmlheader_title')
	{{ trans('adminlte_lang::message.home') }}
@endsection


@section('main-content')
	<script type="text/javascript">
		$("#contentHeader1").append('<a href={{url("pagosI")}}><i class="fa fa-money"></i> Pagos recibidos</a>');
		$("#contentHeader2").text('Modificar Pago');
	</script>

	<div class="container-fluid spark-screen">
		<div class="row">
			<div class="col-md-12 col-md-offset-0">
				<!-- Default box -->
				<div class="box box-primary">
					<div class="box-header with-border">
						<h3 class="box-title">Editar Ingreso</h3>

						<div class="box-tools pull-right">
							<button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
								<i class="fa fa-minus"></i></button>
							<button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
								<i class="fa fa-times"></i></button>
						</div>
					</div>

					<form action="{{ url('/editPagoI') }}" method="post" >
					<div class="box-body">

						<!--Errores y exito mensajes-->
						@if(count($errors)>0)
						<div class="form-group col-md-12 col-md-offset-0">
							<div class="alert alert-danger alert-dismissible">
				                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
				                <h4><i class="icon fa fa-ban"></i> Error en el formulario</h4>
				                @foreach ($errors->all() as $message) 
	    							<span > {{$message}}</span><br>
								@endforeach
				             </div>
						</div>
						@endif
							<!-- Bloque para mostrar que se actualizo la empresa correctamente-->
						@if(session('mensaje'))
						<div class="form-group col-md-12 col-md-offset-0">
							<div class="alert alert-success alert-dismissible">
						        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
						        <h4><i class="icon fa fa-check"></i> {{ session('mensaje') }}</h4>
						    </div>
						</div>
						@endif

						
						<div class="row">
						<div class="form-group col-md-3 col-md-offset-0">
							<label for="numero">Numero</label>
							<input class="form-control" name="numero" id="numero" type="text" value="{{$pago->numero}}" readonly="readonly">
						</div>
						<div class="form-group col-md-3 col-md-offset-0">
							<label for="id_contacto">Cliente</label>
							<select class="form-control"  id="id_contacto" disabled="disabled">
							<option></option>
							@foreach($clientes as $cliente)
								<?php $selected = ($cliente->id_contacto== $pago->id_contacto) ? "selected='selected'" : ""; ?>
								@if($cliente->nit)
					        	<option value="{{ $cliente->id_contacto }}" onclick="ajaxClient({{$cliente->id_contacto}});" {{$selected}}>{{ $cliente->nombre.' ('.$cliente->nit.')'}}</option>
					        	@else
					        	<option value="{{ $cliente->id_contacto }}" onclick="ajaxClient({{$cliente->id_contacto}});" {{$selected}}>{{ $cliente->nombre}}</option>
					        	@endif
					        @endforeach
		                  	</select>
		                  	<input type="hidden" name="id_contacto" id="id_contactoinput">
						</div>
						<div class="form-group col-md-3 col-md-offset-0">
							<label for="id_banco">Banco</label>
							<select class="form-control" name="id_banco" id="id_banco">
							@foreach($bancos as $banco)
							<?php $selected = ($banco->id_banco== $pago->id_banco) ? "selected='selected'" : ""; ?>
					        	<option value="{{ $banco->id_banco }}" {{$selected}}>{{ $banco->nombre}}</option>
					        @endforeach
		                  	</select>
						</div>
						<div class="form-group col-md-3 col-md-offset-0">
							<label for="id_metodoPago">Metodo de pago</label>
							<select class="form-control" name="id_metodoPago" id="id_metodoPago">
							@foreach($metodosPago as $metodos)
								<?php $selected = ($metodos->id_metodoPago== $pago->id_metodoPago) ? "selected='selected'" : ""; ?>
					        	<option value="{{ $metodos->id_metodoPago }}" {{$selected}}>{{ $metodos->nombre}}</option>
					        @endforeach
		                  	</select>
						</div>
						<div class="form-group col-md-3 col-md-offset-0">
							<label for="fechaI">Fecha</label>
							<div class="input-group date" id="date1">
			                  <div class="input-group-addon">
			                    <i class="fa fa-calendar"></i>
			                  </div>

			                  <input class="form-control pull-right" id="fechaI" type="text" name="fechaI" value="{{ $pago->fechaI}}">
			                </div>
						</div>
						<div class="form-group col-md-3 col-md-offset-0">
							<label for="id_vendedor">Encargado</label>
							<select class="form-control" name="id_vendedor" id="id_vendedor" disabled="disabled">
							@foreach($vendedores as $vendedor)
								<?php $selected = ($vendedor->id_usuario== Auth::user()->id_usuario) ? 'selected=selected':''; ?>
					        	<option value="{{ $vendedor->id_usuario }}" {{$selected}}>{{ $vendedor->nombre.' ('.$vendedor->identificacion.')'}}</option>
					        @endforeach
		                  	</select>
		                  	<input type="hidden" name="id_vendedor" value="{{Auth::user()->id_usuario}}">
						</div>
						
						<div class="form-group col-md-3 col-md-offset-0">
							<label for="notas">Notas</label>
							<textarea class="form-control" rows="2" name="notas" id="notas" >{{ $pago->notas}}</textarea>
						</div>
						</div>


						@if(count($gresoXgresosRef)>0)
							@include('gresos.editGresoPagoFacturasRef')
							<input type="hidden" name="facturaCategoriaPregunta"
							class="facturaCategoriaPregunta" value="Si">
						@elseif(count($gresosXcategorias)>0)
							@include('gresos.editGresoPagoCategorias')
							<input type="hidden" name="facturaCategoriaPregunta" class="facturaCategoriaPregunta" value="No">
						@endif
              			<div class="col-md-3 col-sm-6 col-xs-12 col-md-offset-9">
				          <div class="info-box" style="box-shadow: 1px 1px 5px #888888;">
				            <span class="info-box-icon bg-green"><i class="fa fa-money" aria-hidden="true"></i></span>

				            <div class="info-box-content">
				              <span class="info-box-text">Total</span>
				              <span class="info-box-number" id="totalTotalSpan">{{$pago->total}}</span>
				              <input type="hidden" name="total" id="totalTotal" value="{{$pago->total}}">
				              <input type="hidden" name="totalAntiguo" id="totalTotalA" value="{{$pago->total}}">
				            </div>
				            <!-- /.info-box-content -->
				          </div>
				          <!-- /.info-box -->
				        </div>						
				          <!-- /.totalTotal box -->
						
					


					</div>
					<div class="box-footer">
                		<div class="col-md-2 col-md-offset-10">
							<button type="submit" class="btn btn-primary btn-block btn-flat" id="submit">Actualizar</button>
						</div>
						<input type="hidden" name="_token" value="{{ csrf_token() }}">
						<input type="hidden" name="id_greso" value="{{$pago->id_greso}}">
						<input type="hidden" name="id_empresa" value="{{ Auth::user()->id_empresa }}">

              		</div>
              		</form>
					
				</div>
				<!-- /.box -->
			</div>
		</div>
	</div>
	

	<script type="text/javascript">
		var row=0;
		row = $("#r").val()-1;
		$( document ).ready(function() {
  			$("#id_contactoinput").val($("#id_contacto").val());
  			/*para editar facturas unicamente, establecer el id del select igual a la clase del option seleccionado
	    	permitiendo al aplicativo calcular el porcentaje de impuesto sin haber fijado un nuevo impuesto*/
	    	for(var i=0; i< $(".rowCategoria").length;i++){
	    		$(".impuesto.r"+i).attr("id", $(".impuesto #"+$(".impuesto.r"+i).val()).attr("class"));
			}

			
		});
		
	</script>
	<script type="text/javascript" src="{{ asset('/js/Gresos_js/gresosPagos.js') }}"></script>
@endsection
