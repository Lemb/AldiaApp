<html>
<head>
  <style>

    body{
      font-family: sans
      -serif;
      font-size: 12px;
    }
    @page {
      margin: 10px 50px;
    }
    header {
      position: static; 
      left: 0px;
      top: -160px;
      right: 0px;
      height: 100px;
      text-align: center;
    }
    header h1{
      margin: 10px 0;
    }
    header h2{
      margin: 0 0 10px 0;
    }
    footer {
      position: fixed;
      left: 0px;
      bottom: -50px;
      right: 0px;
      height: 40px;
      border-bottom: 2px solid #ddd;
    }
    footer .page:after {
      content: counter(page);
    }
    footer table {
      width: 100%;
    }
    footer p {
      text-align: right;
    }
    footer .izq {
      text-align: left;
    }


    #membrete{
        width: 40%;
        background-color: #ddd;
        margin-bottom:  0px;
        float: left;
    }
    #membretee{
        width: 40%;
        margin-bottom:  0px;
        float: right;
    }
    
    #facturaD{
      float: right;
      width: 40%;
      text-align: center;
      
    }
    #facturaN{    
      float: left;
      background-color: #8296AB;
      width: 40%;
      text-align: center;
      color: white;
    }

    #table{
      clear: left;
      margin-top: 10px;
      width: 100%;
    }
    #table table{
      width: 100%;
      text-align: center;
      border-bottom: solid;
    }
    #notas{
      float: left;
      text-align: center;
    }
    #total{
      text-align: center;
      float: right;
    }
    #footTableTerms{
      clear: left; 
      margin-top: 15px;
    }

    
  </style>
<body>
  <header>
    <div id="membrete">
      <h2>{{$empresa->nombre}}</h2>
      <span>Nit: {{$empresa->nit}}</span><br>
      <span>Telefono: {{$empresa->telefono}}</span><br>
      <span>Ciudad: {{$empresa->ciudad}}</span><br>
      <span>Direccion: {{$empresa->direccion}}</span><br>
      
    </div>
    <div id="membretee">
      
    </div>
  </header>


  <div id="content">
    <div id="info">
      
      <div id="facturaD">
        <h3 style="background-color: #8296AB; color: white;">Pagado por</h3>
        <span>{{$pago->clienteNombre}}</span><br>
        @if(isset($pago->clienteNit))
        <span>Nit: {{$pago->clienteNit}}</span><br>
        @endif
        @if(isset($pago->clienteTelefono))
        <span>Teléfono: {{$pago->clienteTelefono}}</span><br>
        @endif
        @if(isset($pago->clienteCiudad))
        <span>{{$pago->clienteCiudad}}</span><br>
        @endif
        @if(isset($pago->clienteDireccion))
        <span>{{$pago->clienteDireccion}}</span><br>
        @endif
        
      </div>

      <div id="facturaN" >
        <h4 style="margin: 3px 10px;">Recibo de caja: {{$pago->numero}}</h4>
        <h4 style="margin: 3px 10px;">Encargado: {{$pago->usuarioNombre.' '.$pago->usuarioApellido.' ('.$pago->usuarioIdentificacion.')'}}</h4>
        <h4 style="margin: 3px 10px;">Fecha: {{$pago->fechaI}}</h4>
      </div>

    </div>
    <br><br><br>
    <br><br><br>
    <br><br><br>

    @if(count($gresosX)>0)
    <div id="table">
      <table>
        <thead>
          <tr style="background-color: #8296AB; color: white;">
            
            <th style="width: 40%;">Concepto</th>
            <th style="width: 15%;">Total</th>
            <th style="width: 15%;">Pagado</th>
            <th style="width: 15%;">Por pagar (antes de valor recibido)</th>
            <th style="width: 15%;">Valor recibido</th>
          </tr>
        </thead>
        <tbody>
          @foreach($gresosX as $gresos)
          <tr>
            <td>Factura: {{$gresos->numero}}</td>
            <td>${{number_format($gresos->total,2)}}</td>
            <td>${{number_format($gresos->pagado,2)}}</td>
            <td>${{number_format($gresos->pagar,2)}}</td>
            <td>${{number_format($gresos->valor,2)}}</td>
          </tr>
          @endforeach
          
        </tbody>    
      </table>
    </div>
    @elseif(count($categoriasX)>0)
    <div id="table">
      <table>
        <thead>
          <tr style="background-color: #8296AB; color: white;">
            <th style="width: 5%;">Cantidad</th>
            <th style="width: 9%;">Valor</th>
            <th style="width: 15%;">Concepto</th>
            <th style="width: 15%;">Apunte</th>
            <th style="width: 6%;">Descuento</th>
            <th style="width: 6%;">Impuesto</th>
            <th style="width: 9%;">Subtotal</th>
            <th style="width: 9%;">Total</th>
          </tr>
        </thead>
        <tbody>
          @foreach($categoriasX as $categoria)
          <tr>
            <td>{{$categoria->cantidad}}</td>
            <td>${{number_format($categoria->valor, 2)}}</td>
            @if(isset($categoria->referencia))
            <td>{{$categoria->categoriaNombre}} ({{$categoria->referencia}})</td>
            @else
            <td>{{$categoria->categoriaNombre}}</td>
            @endif
            <td>{{$categoria->apunte}}</td>
            <td>{{$categoria->descuento}}%</td>
            <td>{{$categoria->tipoimpuestoNombre}}({{$categoria->impuestoPorcentaje}}%)</td>
            <td>${{number_format($categoria->subtotal, 2)}}</td>
            <td>${{number_format($categoria->total, 2)}}</td>
          </tr>
          @endforeach
          
        </tbody>    
      </table>
    </div>
    @endif

    <div id="footTable">
      @if(isset($pago->notas))
      <div id="notas">
        <table style="width: 56%;">
          <thead>
            <tr>
              <th style="background-color: #ddd; text-align: center;"><span>NOTAS</span></td>
            </tr>
          </thead>
          <tbody>
            <tr>
              <td>{{$pago->notas}}</td>
            </tr>
          </tbody>
        </table>
      </div>
      @endif
      
  
      <div id="total">
        <table style="width: 30%;">
          <tbody>
            <tr style="background-color: #ddd;">
              <td style="width: 20%;">TOTAL:</td>
              <td style="width: 17%; text-align: center;">${{number_format($pago->total,2)}}</td>
            </tr>
          </tbody>
        </table>
      </div>

    </div>


  </div>
</body>
</html>