@extends('adminlte::layouts.app')

@section('htmlheader_title')
	{{ trans('adminlte_lang::message.home') }}
@endsection


@section('main-content')
	<script type="text/javascript">
		$("#contentHeader1").append('<a href={{url("facturasI")}}><i class="fa fa-tags"></i> Facturas de venta</a>');
		$("#contentHeader2").text('Detalle de Factura de venta');
	</script>
	<?php 
    	$permisos = $_SESSION['permisos']; 
	?>
	<div class="container-fluid spark-screen">
		<div class="row">
			<div class="col-md-12 col-md-offset-0">
				<!-- Default box -->
				<div class="box box-success">
					<div class="box-header with-border">
						<h3 class="box-title"> Ver Factura de venta</h3>

						<div class="box-tools pull-right">
							<button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
								<i class="fa fa-minus"></i></button>
							<button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
								<i class="fa fa-times"></i></button>
						</div>
					</div>

					<div class="box-body">

						<div class="row">
						<div class="form-group col-md-3 col-md-offset-0">
							<label for="numero">Numero</label>
							<input class="form-control" name="numero" id="numero" type="text" value="{{ $factura->numero }}" disabled="disabled">
						</div>
						<div class="form-group col-md-3 col-md-offset-0">
							<label for="id_contacto">Cliente</label>
							<select class="form-control" name="id_contacto" id="id_contacto" disabled="disabled">
							@foreach($clientes as $cliente)
								@if($cliente->nit)
					        	<option value="{{ $cliente->id_contacto }}">{{ $cliente->nombre.' ('.$cliente->nit.')'}}</option>
					        	@else
					        	<option value="{{ $cliente->id_contacto }}">{{ $cliente->nombre}}</option>
					        	@endif
					        @endforeach
		                  	</select>
						</div>
						<div class="form-group col-md-3 col-md-offset-0">
							<label for="id_vendedor">Vendedor</label>
							<select class="form-control" name="id_vendedor" id="id_vendedor" disabled="disabled">
							@foreach($vendedores as $vendedor)
					        	<option value="{{ $vendedor->id_usuario }}">{{ $vendedor->nombre.' ('.$vendedor->identificacion.')'}}</option>
					        @endforeach
		                  	</select>
						</div>
						<div class="form-group col-md-2 col-md-offset-0">
							<label for="id_contacto">Bodega</label>
							<select class="form-control" name="id_bodega" id="id_bodega" disabled="">
							@foreach($bodegas as $bodega)
								<?php $selected =($bodega->id_bodega == $factura->id_bodega) ? 'selected="selected"': '';?>
					        	<option value="{{ $bodega->id_bodega }}" {{$selected}}>{{ $bodega->nombre}}</option>
					        @endforeach
		                  	</select>
						</div>
						</div>
						
						<div class="row">
						<div class="form-group col-md-3 col-md-offset-0">
							<label for="fechaI">Fecha inicio</label>
							<div class="input-group date" id="date1">
			                  <div class="input-group-addon">
			                    <i class="fa fa-calendar"></i>
			                  </div>

			                  <input class="form-control pull-right" id="fechaI" type="text" name="fechaI" value="{{ $factura->fechaI}}" disabled="disabled">
			                </div>
						</div>
					
						<div class="form-group col-md-3 col-md-offset-0">
							<label for="fechaF">Fecha vencimiento</label>
							<div class="input-group date">
			                  <div class="input-group-addon">
			                    <i class="fa fa-calendar"></i>
			                  </div>
			                  <input class="form-control pull-right" id="fechaF" type="text" name="fechaF" value=" {{ $factura->fechaF}}" disabled="disabled">
			                </div>
						</div>
						
						<div class="form-group col-md-3 col-md-offset-0">
							<label for="notas">Notas</label>
							<textarea class="form-control" rows="2" name="notas" id="notas" disabled="disabled">{{ $factura->notas}}</textarea>
						</div>
						</div>



				
						@include('gresos.editGresosFacturas')

						<div class="form-group col-md-4 col-md-offset-0">
							<?php $checked = ($factura->deleted_at !=null) ? 'checked=checked':'';?>
							<label for="eliminadoPregunta">¿Eliminado?</label><br>
							<input type="checkbox" value="1" {{$checked}} disabled="disabled">
						</div>
						<!-- totalTotal box -->
              			<br>
              			<div class="col-md-4 col-sm-6 col-xs-12 col-md-offset-4">
				          <div class="info-box" style="box-shadow: 1px 1px 5px #888888;">
				            <span class="info-box-icon bg-green"><i class="fa fa-money" aria-hidden="true" style="margin-top:20%;"></i></span>

				            <div class="info-box-content">
				              <span class="info-box-text">Total</span>
				              <span class="info-box-number" id="totalTotalSpan">{{$factura->total}}</span>
				              <input type="hidden" name="total" id="totalTotal" value="{{$factura->total}}">
				            </div>
				            <!-- /.info-box-content -->
				          </div>
				          <!-- /.info-box -->
				        </div>						
				          <!-- /.totalTotal box -->
									

				        @if(isset($permisos[18]) && $permisos[18]->ver==1)
						<h4 class="box-title form-group col-md-12 col-md-offset-0">Versiones</h3>
						<table class="table table-bordered table-hover dataTable" 
							data-toggle="table"
							data-pagination="true"       						
							>
			                <thead>
				                <tr>
				                	<th>Numero</th>
				                  	<th>Cliente</th>
				                  	<th>Total</th>
				                  	<th>Fecha</th>
				                  	<th>Version</th>
				                  	<th>Responsable</th>
				                  	<th>Detalle</th>
				                </tr>
				            </thead>
				        	<tbody>
				                @foreach($versiones as $version) 
				                	<?php  $referenciaPrincipal = ($version->id_greso==$factura->id_greso) ? 'id=referenciaPrincipal' : ''; 
				                	?>
						    		<tr {{$referenciaPrincipal}} >
						    			<td> {{$version->numero }} 		</td>
						    			<td> {{$version->contacto }} 		</td>
						    			<td> {{$version->total }} 		</td>
						    			<td> {{$version->updated_at }} </td>
						    			<td> {{$version->numeroVersion }} </td>
						    			<td> <a href="{{ url('/verUsuario/'.$version->responsableID) }}">  {{$version->responsableNombre.' ('.$version->responsableIdentificacion.')' }}</a> </td>
						    			<td> 
						    				<a class="btn btn-info" style="padding: 2px 10px;" href="{{ url('/verFacturaI/'.$version->id_greso) }}" aria-label="Ver Detalle">
  												<i class="fa fa-eye" aria-hidden="true"></i>
  											</a>
						    			</td>
						    		</tr>
								@endforeach
			              	</tbody>	
              			</table>
              			@endif					
					</div>
				</div>
				<!-- /.box -->
			</div>
		</div>
	</div>

	<script type="text/javascript">
	
		//setear para vista
		$(function () {			
			$("#id_contacto").val({{$factura->id_contacto}});
			$("#id_vendedor").val({{$factura->id_vendedor}});

			$("#fechaI").datepicker({dateFormat: "yy-mm-dd"});
        	$("#fechaF").datepicker({dateFormat: "yy-mm-dd"});
        	$('.images').magnificPopup({type:'image'});

        	$('.item').prop("disabled", true);
        	$('.valor').prop("disabled", true);
        	$('.cantidad').prop("disabled", true);
        	$('.referencia').prop("disabled", true);
        	$('.descuento').prop("disabled", true);
        	$('.impuesto').prop("disabled", true);
        	$('.descuento').prop("disabled", true);
        	$('.apunte').prop("disabled", true);
        	$('.btones').remove();

        	$("#referenciaPrincipal").css("background-color", "#DDFCDA");
        	$("#app").css("display","");

        	$( ".impuesto" ).select2({theme: "bootstrap"});
        	

        	//formato number
			$('.valor').number(true, 2, '.', ',');
			$('.subtotal').number(true, 2, '.', ',');
			$('.total').number(true, 2, '.', ',');
			$('#subtotal').number(true, 2, '.', ',');
			$('.impuestoFinal').number(true, 2, '.', ',');
			$('#totalTotalSpan').number(true, 2, '.', ',');

        });

	</script>
@endsection
