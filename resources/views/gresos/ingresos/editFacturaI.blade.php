@extends('adminlte::layouts.app')

@section('htmlheader_title')
	{{ trans('adminlte_lang::message.home') }}
@endsection


@section('main-content')
	<script type="text/javascript">
		$("#contentHeader1").append('<a href={{url("facturasI")}}><i class="fa fa-tags"></i> Facturas de venta</a>');
		$("#contentHeader2").text('Modificar Factura de venta');
	</script>
	<div class="container-fluid spark-screen">
		<div class="row">
			<div class="col-md-12 col-md-offset-0">
				<!-- Default box -->
				<div class="box box-success">
					<div class="box-header with-border">
						<h3 class="box-title"> Actualizar Factura de venta</h3>

						<div class="box-tools pull-right">
							<button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
								<i class="fa fa-minus"></i></button>
							<button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
								<i class="fa fa-times"></i></button>
						</div>
					</div>

					<form action="{{ url('/editFacturaI') }}" method="post" onsubmit="customdNumber(false);">
					<div class="box-body">

						@if(count($errors)>0)
						<div class="form-group col-md-12 col-md-offset-0">
							<div class="alert alert-danger alert-dismissible">
				                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
				                <h4><i class="icon fa fa-ban"></i> Error en el formulario</h4>
				                @foreach ($errors->all() as $message) 
	    							<span > {{$message}}</span><br>
								@endforeach
				             </div>
						</div>
						@endif

						<!-- Bloque para mostrar que se actualizo la empresa correctamente-->
						@if(session('mensaje'))
						<div class="form-group col-md-12 col-md-offset-0">
							<div class="alert alert-success alert-dismissible">
						        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
						        <h4><i class="icon fa fa-check"></i> {{ session('mensaje') }}</h4>
						    </div>
						</div>
						@endif

						
						<div class="row">
						<div class="form-group col-md-3 col-md-offset-0">
							<label for="numero">Numero</label>
							<input class="form-control" name="numero" id="numero" type="text" value="{{ $factura->numero }}" disabled="disabled">
						</div>
						<div class="form-group col-md-3 col-md-offset-0">
							<label for="id_contacto">Cliente</label>
							<select class="form-control" name="id_contacto" id="id_contacto">
							@foreach($clientes as $cliente)
								@if($cliente->nit)
					        	<option value="{{ $cliente->id_contacto }}">{{ $cliente->nombre.' ('.$cliente->nit.')'}}</option>
					        	@else
					        	<option value="{{ $cliente->id_contacto }}">{{ $cliente->nombre}}</option>
					        	@endif
					        @endforeach
		                  	</select>
						</div>
						<div class="form-group col-md-3 col-md-offset-0">
							<label for="id_vendedor">Vendedor</label>
							<select class="form-control" name="id_vendedor" id="id_vendedor" disabled="disabled">
							@foreach($vendedores as $vendedor)
							<?php $selected = ($vendedor->id_usuario== Auth::user()->id_usuario) ? 'selected=selected':''; ?>
					        	<option value="{{ $vendedor->id_usuario }}" {{$selected}} >{{ $vendedor->nombre.' ('.$vendedor->identificacion.')'}}</option>
					        @endforeach
		                  	</select>
		                  	<input type="hidden" name="id_vendedor" value="{{Auth::user()->id_usuario}}">
						</div>
						<div class="form-group col-md-2 col-md-offset-0">
							<label for="id_contacto">Bodega</label>
							<select class="form-control" name="id_bodega" id="id_bodega" onchange="bodegaAjax($(this).val());">
							@foreach($bodegas as $bodega)
								<?php $selected =($bodega->id_bodega == $factura->id_bodega) ? 'selected="selected"': '';?>
					        	<option value="{{ $bodega->id_bodega }}" {{$selected}}>{{ $bodega->nombre}}</option>
					        @endforeach
		                  	</select>
						</div>
						</div>



						<div class="row">
						<div class="form-group col-md-3 col-md-offset-0">
							<label for="fechaI">Fecha inicio</label>
							<div class="input-group date" id="date1">
			                  <div class="input-group-addon">
			                    <i class="fa fa-calendar"></i>
			                  </div>

			                  <input class="form-control pull-right" id="fechaI" type="text" name="fechaI" value="{{ $factura->fechaI}}">
			                </div>
						</div>
						<div class="form-group col-md-3 col-md-offset-0">
							<label for="id_terminoPago">Termino de pago</label>
							<select class="form-control" name="terminoPagoDias" id="id_terminoPago" onchange="terminoDePago($(this).val());">
								<option value="0">Contado</option>
								
								@foreach($terminosPago as $termino)
									@if($termino->activo==1)
									<?php $selected= ($termino->dias==$factura->terminoPagoDias)?'selected="selected"':''; ?>
					        		<option value="{{ $termino->dias}}" {{$selected}}>{{ $termino->nombre.' ('.$termino->dias.' dias)'}}</option>
					        		@endif
					        	@endforeach
					        	<option value="Manual">Manual</option>
					        	
		                  	</select>
						</div>
						
						
						<div class="form-group col-md-3 col-md-offset-0">
							<label for="fechaF">Fecha vencimiento</label>
							<div class="input-group date">
			                  <div class="input-group-addon">
			                    <i class="fa fa-calendar"></i>
			                  </div>
			                  <input class="form-control pull-right" id="fechaF" type="text" name="fechaF" value=" {{ $factura->fechaF}}" onchange="$('#id_terminoPago').val('Manual');">
			                </div>
						</div>
						
						<div class="form-group col-md-3 col-md-offset-0">
							<label for="notas">Notas</label>
							<textarea class="form-control" rows="2" name="notas" id="notas" >{{ $factura->notas}}</textarea>
						</div>
						</div>

						@include('gresos.editGresosFacturas')

						<!-- totalTotal box -->
              			<br>
              			<div class="col-md-4 col-sm-6 col-xs-12 col-md-offset-8">
				          <div class="info-box" style="box-shadow: 1px 1px 5px #888888;">
				            <span class="info-box-icon bg-green"><i class="fa fa-money" aria-hidden="true" style="margin-top:20%;"></i></span>

				            <div class="info-box-content">
				              <span class="info-box-text">Total</span>
				              <span class="info-box-number" id="totalTotalSpan">{{$factura->total}}</span>
				              <input type="hidden" name="total" id="totalTotal" value="{{$factura->total}}">
				            </div>
				            <!-- /.info-box-content -->
				          </div>
				          <!-- /.info-box -->
				        </div>						
				          <!-- /.totalTotal box -->
						

					</div>
					<div class="box-footer">
                		<div class="col-md-2 col-md-offset-10">
							<button type="submit" class="btn btn-primary btn-block btn-flat" id="submit">Actualizar</button>
						</div>
						<input type="hidden" name="_token" value="{{ csrf_token() }}">
						<input type="hidden" name="id_greso" value="{{ $factura->id_greso}}">
						<input type="hidden" name="id_empresa" value="{{ Auth::user()->id_empresa }}">

              		</div>
              		</form>
					
				</div>
				<!-- /.box -->
			</div>
		</div>
	</div>
	

	<script type="text/javascript">
		
		var row;
		$(function () {			
			$("#id_contacto").val({{$factura->id_contacto}});
			row = $("#r").val()-1;
        });

        var itemsXimpuestos = [[],[]];
		/*	usado como una tabla dentro del cliente javascript para luego recuperar los valores segun los items
			0 = id_item
			1 = id_impuesto
		*/
		@foreach($itemsXimpuestos as $itemXimpuesto)
			itemsXimpuestos[0].push({{$itemXimpuesto->id_item}});
			itemsXimpuestos[1].push({{$itemXimpuesto->id_impuesto}});
		@endforeach

		var urlBodegaAjax = "{{ url('/bodegaAjax')}}";
		
	</script>
	<script type="text/javascript" src="{{ asset('/js/Gresos_js/gresosFactura.js') }}"></script>
@endsection
