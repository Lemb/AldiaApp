<html>
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
  <title>PDFAldia</title>
  <link href="{{ asset('logo.png') }}" rel="icon">

  
  <style>

    body{
      font-family: sans
      -serif;
      font-size: 10px;
    }
    @page {
      margin: 10px 0px;
    }
    header {
      position: static; 
      left: 0px;
      top: -160px;
      right: 0px;
      height: 100px;
      text-align: center;
    }
    header h1{
      margin: 10px 0;
    }
    header h2{
      margin: 0 0 10px 0;
    }
    footer {
      position: fixed;
      left: 0px;
      bottom: -50px;
      right: 0px;
      height: 40px;
      border-bottom: 2px solid #ddd;
    }
    footer .page:after {
      content: counter(page);
    }
    footer table {
      width: 100%;
    }
 


    #membrete{
        width: 100%;
        background-color: #ddd;
        margin-bottom:  0px;
        
    }
    #membretee{
        width: 100%;
        margin-bottom:  0px;
    }
    
    #facturaD{
      width: 100%;
      text-align: center;
      margin-bottom: 4px;
      
    }
    #facturaN{    
    	background-color: #8296AB;
      	width: 100%;
      	color: white;
      	margin-bottom: 9em;
    }
    .facturaN{    
    	background-color: #8296AB;
      	width: 50%;
      	text-align: right;
      	color: white;
      	float: left;
    }

    #table{
      margin-top: 10px;
      width: 100%;
    }
    #table table{
      width: 100%;
      text-align: center;
     
    }
    #notas{
      text-align: center;
    }
    #total{
      text-align: center;
      float: right;
    }
    #footTableTerms{
      margin-top: 15px;
    }

    
</style>
<body>
  <header style="">
    <div id="membrete">
      <h2>{{$empresa->nombre}}</h2>
      <span>Nit: {{$empresa->nit}}</span><br>
      <span>Telefono: {{$empresa->telefono}}</span><br>
      <span>Ciudad: {{$empresa->ciudad}}</span><br>
      <span>Direccion: {{$empresa->direccion}}</span><br>
      
    </div>



  <div id="content">
    <div id="info">
      
      <div id="facturaD">
        <h3 style="background-color: #8296AB; color: white; margin-bottom: 3px;">Facturar A</h3>
        <span>{{$factura->clienteNombre}}</span><br>
        @if(isset($factura->clienteNit))
        <span>Nit: {{$factura->clienteNit}}</span><br>
        @endif
        @if(isset($factura->clienteTelefono))
        <span>Teléfono: {{$factura->clienteTelefono}}</span><br>
        @endif
        @if(isset($factura->clienteCiudad))
        <span>Ciudad: {{$factura->clienteCiudad}}</span><br>
        @endif
        @if(isset($factura->clienteDireccion))
        <span>Direccion: {{$factura->clienteDireccion}}</span><br>
        @endif
        
      </div>
      <div id="facturaN">
	      <div class="facturaN" style="height: 8em;">
	        <h4 style="margin: 3px 10px;">Factura: {{$factura->numero}}</h4>
	        <h4 style="margin: 3px 10px;">Vendedor: {{$factura->usuarioNombre.' '.$factura->usuarioApellido.' ('.$factura->usuarioIdentificacion.')'}}</h4>
	      </div>
	      <div class="facturaN" style="height: 8em; text-align: left;" >
	        <h4 style="margin: 3px 10px;">Inicio: {{$factura->fechaI}}</h4>
	        <h4 style="margin: 3px 10px;">Plazo: {{$factura->fechaF}}</h4>
	      </div>
      </div>

    </div>
  </header>
  <!--
  <footer>
    <table>
      <tr>
        <td>
            <p class="izq">
              Desarrolloweb.com
            </p>
        </td>
        <td>
          <p class="page">
            Página
          </p>
        </td>
      </tr>
    </table>
  </footer>
-->



    <div id="table">
      <table>
        <thead>
          <tr style="background-color: #8296AB; color: white; width: 100%;" >
            
            <th style="width: 15%;">Concepto</th>
            <th style="width: 9%;">Valor/Cantidad</th>
            <th style="width: 9%;">Total</th>
          </tr>
        </thead>
        <tbody>
          @foreach($itemsX as $item)
          <tr>
            @if(isset($item->referencia))
            <td>{{$item->itemNombre}} ({{$item->referencia}})</td>
            @else
            <td>{{$item->itemNombre}}</td>
            @endif
            <td>${{number_format($item->valor,2)}}/{{$item->cantidad}}</td>
            <td style="text-align: right;">${{number_format($item->total,2)}}</td>
          </tr>
          @endforeach
          
        </tbody>    
      </table>
    </div>

    <div id="footTable">
      @if(isset($factura->notas))
      <div id="notas">
        <table style="width: 56%;">
          <thead>
            <tr>
              <th style="background-color: #ddd; text-align: center;"><span>NOTAS</span></td>
            </tr>
          </thead>
          <tbody>
            <tr>
              <td>{{$factura->notas}}</td>
            </tr>
          </tbody>
        </table>
      </div>
      @endif
      
  
      <div id="total">
        <table style="width: 30%;">
          <tbody>
            
            <tr >
              <td style="width: 20%;">SUBTOTAL:</td>
              <td style="width: 10%; text-align: center;">${{number_format($factura->subtotal,2)}}</td>
            </tr>
            <tr >
              <td style="width: 20%;">IMPUESTOS:</td>
              <td style="width: 10%; text-align: center;">${{number_format($factura->total-$factura->subtotal,2)}}</td>
            </tr>
            <tr style="background-color: #ddd;">
              <td style="width: 20%;">TOTAL:</td>
              <td style="width: 17%; text-align: center;">${{number_format($factura->total,2)}}</td>
            </tr>
            @if($factura->pagar!=0)
            <tr style="background-color: #ddd;">
              <td style="width: 20%;">Pagado:</td>
              <td style="width: 17%; text-align: center;">${{number_format($factura->pagado,2)}}</td>
            </tr>
            <tr style="background-color: #ddd;">
              <td style="width: 20%;">Por Pagar:</td>
              <td style="width: 17%; text-align: center;">${{number_format($factura->pagar,2)}}</td>
            </tr>
            @endif
          </tbody>
        </table>
      </div>

    </div>

    <br>
    @if(isset($empresa->fac_terminos))
    <div id="footTableTerms" >
      <div id="terminos">
        <table style="width: 56%;">
          <thead>
            <tr>
              <th ><span>Terminos y condiciones</span></td>
            </tr>
          </thead>
          <tbody>
            <tr>
              <td>{{$empresa->fac_terminos}}</td>
            </tr>
          </tbody>
        </table>
      </div>
    </div>
    @endif

  </div>
</body>
</html>
