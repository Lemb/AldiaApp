<h4 id="h4Factura">Facturas de venta</h4>
						
<div id="facturas">
<?php $index=0;?>
@foreach($gresoXgresosRef as $gresoXgresoRef)
<div class="row" >

<input type="hidden" value="{{$gresoXgresoRef->id_gresoRef}}" name="id_gresoRef[]">
<div class="form-group col-md-2 col-md-offset-0">
	<label for="numeroF">Numero</label>
	<input class="form-control" name="numeroF[]" id="numeroF" type="text" value="{{$gresoXgresoRef->numero}}" readonly="readonly">
</div>
<div class="form-group col-md-2 col-md-offset-0">
	<label for="totalF">Total</label>
	<input class="form-control totalF" name="totalF[]" id="totalF" type="text" value="{{$gresoXgresoRef->total}}" disabled="disabled">
</div>
@if(!isset($pago) || $pago->versionActual==1)
<div class="form-group col-md-2 col-md-offset-0">
	<label for="pagadoF">Pagado</label>
	<input class="form-control pagadoF" name="pagadoF[]" id="pagadoF" type="text" value="{{$gresoXgresoRef->pagado}}" disabled="disabled">
</div>

<div class="form-group col-md-2 col-md-offset-0">
	<label for="porpagarF">Por pagar</label>
	<input class="form-control porpagarF" name="porpagarF[]" id="porpagarF" type="text" value="{{$gresoXgresoRef->pagar}}" disabled="disabled">
</div>
@endif
<div class="form-group col-md-2 col-md-offset-0">
	<label for="valorF">Valor a recibir</label>
	<input class="form-control valorF" name="valorF[]" id="valorF" type="text" value="{{$gresoXgresoRef->valor}}" onkeyup="calcularTotal();" onchange="calcularTotal();">
	
</div>

<div class="form-group col-md-1 col-md-offset-0 maximo">
	<label for="valorF">Maximo</label>
	<button type="button" class="btn btn-block btn-success" onclick="igualar('{{$gresoXgresoRef->pagar}}', {{$index}});"><i class="fa fa-eject" aria-hidden="true"></i></button>
</div>

</div>
<?php $index++;?>
@endforeach
</div>