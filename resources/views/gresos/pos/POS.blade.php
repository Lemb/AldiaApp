@extends('adminlte::layouts.errors')

@section('htmlheader_title')
	{{ trans('adminlte_lang::message.home') }}
@endsection


@section('main-content')

@include('adminlte::layouts.partials.mainheader2')
@include('adminlte::layouts.partials.controlsidebar')

<div class="box-body">
	<div class="col-md-4 col-md-offset-0" style="height :90%; position: absolute; width: 36%">
		<div class="col-md-6 col-md-offset-0" style="height: 11%;">
			<label>Cliente</label>
			<select class="form-control" name="id_contacto" id="id_contacto">
				
			@foreach($clientes as $cliente)
				
				@if($cliente->nit)
	        	<option value="{{ $cliente->id_contacto }}">{{ $cliente->nombre.' ('.$cliente->nit.')'}}</option>
	        	@else
	        	<option value="{{ $cliente->id_contacto }}">{{ $cliente->nombre}}</option>
	        	@endif
	        @endforeach
          	</select>
		</div>
		<div class="form-group col-md-6 col-md-offset-0">
			<label for="tipo">Numeracion (<span id="numero"></span>)</label>
			<select class="form-control" name="numeracion"  id="numeracion" onchange="numeracion1();">
			@foreach($numeraciones as $numeracion)
				<?php $selected = ($numeracion->preterminada==1) ? "selected='selected'" :"";//selected?>
				@if($numeracion->activa==1)
	        	<option value="{{$numeracion->id_numeracion}}" id="num{{$numeracion->id_numeracion}}" class="{{$numeracion->prefijo.$numeracion->numeroA}}" {{$selected}}>{{ $numeracion->nombre}}</option>
	        	@endif
	        @endforeach
          	</select>
		</div>
		<ul class="col-md-12 col-md-offset-0" id="listaAVender" style="overflow-y: auto; height: 69%;">
	
		</ul>
		<div class="col-md-12 col-md-offset-0" style="height: 19%;">
			<div>
				<span style="float: left;">Subtotal:</span> 
				<span style="float: right;">$<span id="subtotalOperacion">00.00</span></span>
			</div><br>
			<div>
				<span style="float: left;">Impuestos:</span> 
				<span style="float: right;">$<span id="impuestoOperacion">00.00</span></span>
			</div><br>
			<button class="btn btn-success btn-block btn-lg" id="venderBoton" disabled="" onclick="ajaxVender();">
				<span style="float: left;">Vender</span> 
				<span style="float: right;">$<span id="totalOperacion">0.00</span></span>
			</button>
			<button type="button" class="btn btn-secondary btn-block" onclick="cancelar();">
				<span style="float: left;">Cancelar</span>
				<i class="fa fa-trash" style="float: right;"></i>
			</button>
		</div>
	</div>
</div>
<!-- /.box -->

<div class="col-md-8 col-md-offset-0" style="height :90%; position: absolute; right: 0;">
	
	<div class="input-group margin barraBusqueda">
        <div class="input-group-btn">
          <button class="btn btn-default active" id="botonTexto"  onclick="buscadorBotones('texto');"><i class="fa fa-font" aria-hidden="true" enabled> Texto/Referencia</i></button>
        </div>
        <div class="input-group-btn">
          <button class="btn btn-default" id="botonBarras" onclick="buscadorBotones('barras');" ><i class="fa fa-barcode" aria-hidden="true"></i> Codigo de Barras</button>
        </div>
        <!-- /btn-group -->
        <input type="text" id="buscador" class="buscador form-control" placeholder="Buscador..." />
        <span class="input-group-addon"><i class="fa fa-search" aria-hidden="true"></i></span>
    </div>

	<ul class="col-md-12 col-md-offset-0" id="itemsUl" style="overflow-y: auto; height :90%;" >
		@foreach($items as $item)
			<li class="list-group-item justify-content-between liItemPOS col-md-3 col-lg-2 col-md-offset-0 {{$item->codigoBarras}} idLi{{$item->id_item}}" style="display: inline-block; margin-bottom: 7px; height: 250px; "  onclick="agregarItem($(this));">

				<span style="color: #3c8dbc;" class="referenciaItem">{{$item->referencia}} .</span> 
	        	@if($item->imagen)
				<img src="{{ URL::to('/').'/storage/imagenes/'.$item->id_item.$item->imagen }}" width="100%" height="160px" style="object-fit: cover; border-radius: 40px 8px 8px;">
				<input type="hidden" class="imagenItem" value="{{URL::to('/').'/storage/imagenes/'.$item->id_item.$item->imagen}}">
				@else										
				<img src="{{ URL::to('/').'/storage/imagenes/predeterminadoAldiaApp0.png' }}" width="100%" height="160px" style="object-fit: cover; border-radius: 40px 8px 8px;">
				<input type="hidden" class="imagenItem" value="{{URL::to('/').'/storage/imagenes/predeterminadoAldiaApp0.png'}}">
	        	@endif
	        	<small class="label bg-green" style="position: absolute; bottom:5em; right:  9%;">$<span class="precioItem">{{$item->precio}}</span></small>
				<span class="nombreItem">{{$item->nombre}}</span>
				@foreach($impuestos as $impuesto)
					@if($impuesto->id_impuesto== $item->id_impuesto)
						<input type="hidden" class="impuestoItem" id="{{$item->id_impuesto}}" value="{{$impuesto->porcentaje}}">
					@endif
				@endforeach
				
				<input type="hidden" class="id_item" value="{{$item->id_item}}">
			</li>
        @endforeach
	</ul>
</div>




<!-- Small modal -->
<div class="modal fade modalApunte" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">
  <div class="modal-dialog modal-sm" role="document">
    <div class="modal-content">
    	<div class="modal-header">
        	<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        	<h4 class="modal-title">Apunte</h4>
		</div>
		<div class="modal-body">
		    <textarea id="apunte" placeholder="Tu apunte.." style="width: 100%;" maxlength="199"></textarea>
		</div>
		<div class="modal-footer">
		    <button type="button" class="btn btn-primary" onclick="modalApunteGuardar();" data-dismiss="modal">Guardar</button>
		</div>
    </div>
  </div>
</div>



<!-- Small modal -->
<div class="modal fade modalCambioUsuario" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">
  <div class="modal-dialog modal-sm" role="document">
    <div class="modal-content">
    	<div class="modal-header">
        	<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        	<h4 class="modal-title">Cambio de usuario</h4>
		</div>
		<div class="modal-body">
		    <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <div class="form-group input-group has-feedback">
                <input type="email" class="form-control" placeholder="{{ trans('adminlte_lang::message.email') }}" id="email" />
                <span class="input-group-addon"><i class="fa fa-envelope-o" aria-hidden="true"></i></span>
            </div>
            <div class="form-group input-group has-feedback">
                <input type="password" class="form-control" placeholder="{{ trans('adminlte_lang::message.password') }}" id="password" />
                <span class="input-group-addon"><i class="fa fa-key" aria-hidden="true"></i></span>
            </div>
		</div>
		<div class="modal-footer">
		    <button type="button" class="btn btn-primary" onclick="cambioUsuarioAjax();" data-dismiss="modal">Cambio de usuario</button>
		</div>
    </div>
  </div>
</div>


<script type="text/javascript">
	var urlAjax = "{{ url('posAjax')}}";
	var urlPDF1 = '{{url("/PDFFacturaI/")}}';
	var urlLogin = "{{ url('/loginAjax') }}";
</script>
<script type="text/javascript" src="{{ asset('/js/Gresos_js/POS.js') }}"></script>
@endsection
