@include('adminlte::layouts.partials.functions')
<div id="categoriasDiv" style="display: none;">
	<h4>Categorias</h4>

	<div class="col-md-1 col-md-offset-0" style="margin-bottom: 5px;">
	<button type="button" class="btn btn-block btn-success " onclick="nuevaCategoria();"><i class="fa fa-plus-circle" aria-hidden="true"></i></button>
	</div>
	<div class="col-md-1 col-md-offset-0" style="margin-bottom: 5px;">
	<button type="button" class="btn btn-block btn-danger " onclick="menosCategoria();"><i class="fa fa-minus-circle" aria-hidden="true"></i></button>
	</div>

	<div class="col-md-11 col-md-offset-2">
	</div>
	<div class="row rowCategoria r0" id="rowCategoria">
		<div class="form-group col-md-3 col-md-offset-0">
			<label for="id_categoria">Categoria</label><br>
			<input type="hidden" name="id_categoria[]" class="form-control input-sm categoriaID r0">
			<input class="form-control input-sm categoria r0" data-toggle="modal" data-target="#myModal" type="text" readonly="readonly" onclick="alistarModificacionCategoria($(this));">
		</div>
				
		<div class="form-group col-md-2 col-md-offset-0">
			<label for="valor">Valor</label>
			<input class="form-control input-sm valor r0" name="valor[]" id="valor" type="text" onkeyup="posicion($(this));" onchange="posicion($(this));" step="any" min="0">
			<label>Cantidad</label>
			<input class="form-control input-sm cantidad r0" type="number" name="cantidad[]" value="1" onchange="posicion($(this));" step="any" min="0">
		</div>
		
		<div class="form-group col-md-2 col-md-offset-0">
			<label for="id_descuento">Descuento %</label>
			<input class="form-control input-sm descuento r0" name="descuento[]" id="descuento" type="number" value="0" onkeyup="posicion($(this));" onchange="posicion($(this));" step="any">

			<label for="id_impuesto">Impuesto</label>
			<select class="form-control input-sm impuesto r0" name="id_impuesto[]" id="0" onchange="posicion($(this), $(this).find('option:selected').attr('class'));">
			<option value="6" class="0">0%</option>
			@foreach($impuestos as $impuesto)
				@if($impuesto->activa==1)
	        	<option value="{{ $impuesto->id_impuesto }}" id="{{ $impuesto->id_impuesto }}" class="{{$impuesto->porcentaje}}" >{{ $impuesto->nombre.' ('.$impuesto->porcentaje.'%)'}}</option>
	        	@endif
	        	
	        @endforeach
          	</select>
		</div>
		<div class="form-group col-md-2 col-md-offset-0">
			<label for="apunte">Apunte</label>
			<textarea class="form-control input-sm apunte r0" rows="3" name="apunte[]" id="apunte"></textarea>
		</div>
		<div class="form-group col-md-1 col-md-offset-0">
			<br>
		</div>
		<div class="form-group col-md-2 col-md-offset-0">
			<label>Subtotal</label>
			<input class="form-control input-sm subtotal r0" type="text" name="subtotalI[]" value="0" readonly="readonly">
			<label>Total</label>
			<input class="form-control input-sm total r0" type="text" name="totalI[]" value="0" readonly="readonly">
		</div>
	</div>

	<div class="form-group col-md-2 col-md-offset-10">
		<label>Subtotal:</label>
		<input class="form-control input-sm" type="text" name="subtotal" id="subtotal" value="0" readonly="readonly">
		<div id="impuestoDiv" class="impuestoDiv r0">
			<label>Impuestos</label>
			<input class="form-control input-sm impuestoFinal r0" type="text" value="0" disabled="disabled">
		</div>
	</div>
</div>










<!-- modal -->
	<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
		<div class="modal-dialog modal-lg" role="document">
	    	<div class="modal-content">
	      		<div class="modal-header">
	        		<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
	        		</button>
	        		<h4 class="modal-title" id="myModalLabel">Categorias</h4>	        
	      		</div>
	      
	    		<div class="modal-body">
	    			
	      			<div >
				    @foreach($categorias as $categoria) 
				    	@if( empty($categoria->id_categoriaP))
				    	<ul>
	                		<li class="list-group-item justify-content-between"><i class="fa fa-plus-square-o" aria-hidden="true" style="cursor:pointer;" id="i{{$categoria->id_categoria}}" onclick="expandir({{$categoria->id_categoria}});"></i> {{$categoria->nombre}}
	                			<span class="badge badge-default badge-pill" style="background-color: white; padding: 0 0; ">
		                			<a class='btn' data-dismiss="modal" style='padding: 2px 10px; margin-right:4px;' onclick='seleccionarCategoria({{ $categoria->id_categoria }}, "{{ $categoria->nombre }}");'>
										<i class='fa fa-chevron-circle-up fa-lg' aria-hidden='true' style='background-color:white; color:black;'></i>
									</a>
								</span>
							</li>
							{!!subcategorias($categoria, $categorias, "items")!!}
							
	                	</ul>
				    	@endif
					@endforeach
					</div>
					
	      		</div>
				<div class="modal-footer">	      	

				</div>
	      
	    	</div>
		</div>
	</div>
	<!-- /modal -->
