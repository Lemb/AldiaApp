@extends('adminlte::layouts.app')

@section('htmlheader_title')
	{{ trans('adminlte_lang::message.home') }}
@endsection


@section('main-content')
	<script type="text/javascript">
		$("#contentHeader1").append('<a href={{url("pagosE")}}><i class="fa fa-money"></i> Pagos realizados</a>');
		$("#contentHeader2").text('Crear nuevo Pago');
	</script>
	<div class="container-fluid spark-screen">
		<div class="row">
			<div class="col-md-12 col-md-offset-0">
				<!-- Default box -->
				<div class="box box-danger">
					<div class="box-header with-border">
						<h3 class="box-title">Nuevo Egreso</h3>

						<div class="box-tools pull-right">
							<button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
								<i class="fa fa-minus"></i></button>
							<button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
								<i class="fa fa-times"></i></button>
						</div>
					</div>

					<form action="{{ url('/nuevoPagoE') }}" method="post" onsubmit="customdNumber(false);">
					<div class="box-body">

						<!--Errores y exito mensajes-->
						@if(count($errors)>0)
						<div class="form-group col-md-12 col-md-offset-0">
							<div class="alert alert-danger alert-dismissible">
				                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
				                <h4><i class="icon fa fa-ban"></i> Error en el formulario</h4>
				                @foreach ($errors->all() as $message) 
	    							<span > {{$message}}</span><br>
								@endforeach
				             </div>
						</div>
						@endif

						
						<div class="row">
						<div class="form-group col-md-3 col-md-offset-0">
							<label for="numero">Numero</label>
							<input class="form-control" name="numero" id="numero" type="text" value="{{$numeracion->SigNum_comprobantePago}}" readonly="readonly">
						</div>
						<div class="form-group col-md-3 col-md-offset-0">
							<label for="id_contacto">Proveedor</label>
							<select class="form-control" name="id_contacto" id="id_contacto" onchange="ajaxClient($(this).val(), 2);">
							<option></option>
							@foreach($proveedores as $proveedor)
								@if($proveedor->nit)
					        	<option value="{{ $proveedor->id_contacto }}" onclick="ajaxClient({{$proveedor->id_contacto}});">{{ $proveedor->nombre.' ('.$proveedor->nit.')'}}</option>
					        	@else
					        	<option value="{{ $proveedor->id_contacto }}" onclick="ajaxClient({{$proveedor->id_contacto}});">{{ $proveedor->nombre}}</option>
					        	@endif
					        @endforeach
		                  	</select>
						</div>
						<div class="form-group col-md-3 col-md-offset-0">
							<label for="id_banco">Banco</label>
							<select class="form-control" name="id_banco" id="id_banco">
							@foreach($bancos as $banco)
								<?php $selected = ($banco->predeterminado == 1) ? "selected='selected'":""; ?>
					        	<option value="{{ $banco->id_banco }}" {{$selected}}>{{ $banco->nombre}}</option>
					        @endforeach
		                  	</select>
						</div>
						<div class="form-group col-md-3 col-md-offset-0">
							<label for="id_metodoPago">Metodo de pago</label>
							<select class="form-control" name="id_metodoPago" id="id_metodoPago">
							@foreach($metodosPago as $metodos)
					        	<option value="{{ $metodos->id_metodoPago }}">{{ $metodos->nombre}}</option>
					        @endforeach
		                  	</select>
						</div>
						<div class="form-group col-md-3 col-md-offset-0">
							<label for="fechaI">Fecha</label>
							<div class="input-group date" id="date1">
			                  <div class="input-group-addon">
			                    <i class="fa fa-calendar"></i>
			                  </div>

			                  <input class="form-control pull-right" id="fechaI" type="text" name="fechaI" value="{{ $date}}">
			                </div>
						</div>
						<div class="form-group col-md-3 col-md-offset-0">
							<label for="id_vendedor">Encargado</label>
							<select class="form-control" name="id_vendedor" id="id_vendedor" disabled="disabled">
							@foreach($vendedores as $vendedor)
								<?php $selected = ($vendedor->id_usuario== Auth::user()->id_usuario) ? 'selected=selected':''; ?>
					        	<option value="{{ $vendedor->id_usuario }}" {{$selected}}>{{ $vendedor->nombre.' ('.$vendedor->identificacion.')'}}</option>
					        @endforeach
		                  	</select>
		                  	<input type="hidden" name="id_vendedor" value="{{Auth::user()->id_usuario}}">
						</div>
						
						<div class="form-group col-md-3 col-md-offset-0">
							<label for="notas">Notas</label>
							<textarea class="form-control" rows="2" name="notas" id="notas" >{{ Input::old('notas')}}</textarea>
						</div>
						<div class="form-group col-md-3 col-md-offset-0" id="preguntaFacturacion" style="display: none;">
							<label for="notas">¿Agregar pago de Factura?</label><br>
							
							<input type="radio" name="facturaCategoriaPregunta" class="facturaCategoriaPregunta" value="Si" > Si<br>
							<input type="radio" name="facturaCategoriaPregunta" class="facturaCategoriaPregunta" value="No" > No
						</div>
						</div>




				









						<h4 id="h4Factura" style="display: none;">Facturas de compra</h4>
						<h4 id="h4FacturaNoRelacion" style="display: none;">Este proveedor no tiene facturas de compra a pagar</h4>

						<div id="facturas" style="display: none;"></div>

						@include('gresos.nuevoGresoPagoCategorias')
              			<div class="col-md-3 col-sm-6 col-xs-12 col-md-offset-9">
				          <div class="info-box" style="box-shadow: 1px 1px 5px #888888;">
				            <span class="info-box-icon bg-red"><i class="fa fa-money" aria-hidden="true"></i></span>

				            <div class="info-box-content">
				              <span class="info-box-text">Total</span>
				              <span class="info-box-number" id="totalTotalSpan">0</span>
				              <input type="hidden" name="total" id="totalTotal" value="0">
				            </div>
				            <!-- /.info-box-content -->
				          </div>
				          <!-- /.info-box -->
				        </div>						
				          <!-- /.totalTotal box -->
						
					


					</div>
					<div class="box-footer">
                		<div class="col-md-2 col-md-offset-10">
							<button type="submit" class="btn btn-primary btn-block btn-flat" id="submit" disabled="disabled">Crear</button>
						</div>
						<input type="hidden" name="_token" value="{{ csrf_token() }}">
						<input type="hidden" name="id_empresa" value="{{ Auth::user()->id_empresa }}">

              		</div>
              		</form>
					
				</div>
				<!-- /.box -->
			</div>
		</div>
	</div>
	

	<script type="text/javascript">
		var urlAjaxClient = "{{ url('/ajaxClienteE')}}";
	</script>
	<script type="text/javascript" src="{{ asset('/js/Gresos_js/gresosPagos.js') }}"></script>
	
@endsection
