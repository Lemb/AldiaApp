@extends('adminlte::layouts.app')

@section('htmlheader_title')
	{{ trans('adminlte_lang::message.home') }}
@endsection


@section('main-content')
	<script type="text/javascript">
		$("#contentHeader1").append('<a href={{url("facturasE")}}><i class="fa fa-tags"></i> Facturas de compra</a>');
		$("#contentHeader2").text('Modificar Factura de compra');
	</script>

	<div class="container-fluid spark-screen">
		<div class="row">
			<div class="col-md-12 col-md-offset-0">
				<!-- Default box -->
				<div class="box box-danger">
					<div class="box-header with-border">
						<h3 class="box-title"> Actualizar Factura de compra</h3>

						<div class="box-tools pull-right">
							<button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
								<i class="fa fa-minus"></i></button>
							<button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
								<i class="fa fa-times"></i></button>
						</div>
					</div>

					<form action="{{ url('/editFacturaE') }}" method="post" onsubmit="customdNumber(false);">
					<div class="box-body">
						<p class="help-block">Campos marcados con asterisco (<i class="fa fa-asterisk" style="color: #DD4B39;"></i>) son obligatorios.</p>
						@if(count($errors)>0)
						<div class="form-group col-md-12 col-md-offset-0">
							<div class="alert alert-danger alert-dismissible">
				                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
				                <h4><i class="icon fa fa-ban"></i> Error en el formulario</h4>
				                @foreach ($errors->all() as $message) 
	    							<span > {{$message}}</span><br>
								@endforeach
				             </div>
						</div>
						@endif

						<!-- Bloque para mostrar que se actualizo la empresa correctamente-->
						@if(session('mensaje'))
						<div class="form-group col-md-12 col-md-offset-0">
							<div class="alert alert-success alert-dismissible">
						        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
						        <h4><i class="icon fa fa-check"></i> {{ session('mensaje') }}</h4>
						    </div>
						</div>
						@endif

						
						<div class="row">
						<div class="form-group col-md-3 col-md-offset-0">
							<label for="numero">Numero de factura</label> (<i class="fa fa-asterisk" style="color: #DD4B39;"></i>)
							<input class="form-control" name="numero" id="numero" type="text" value="{{ $factura->numero }}" required="required">
						</div>
						<div class="form-group col-md-3 col-md-offset-0">
							<label for="id_contacto">Proveedor</label>
							<select class="form-control" name="id_contacto" id="id_contacto">
							@foreach($proveedores as $proveedor)
								@if($proveedor->nit)
					        	<option value="{{ $proveedor->id_contacto }}">{{ $proveedor->nombre.' ('.$proveedor->nit.')'}}</option>
					        	@else
					        	<option value="{{ $proveedor->id_contacto }}">{{ $proveedor->nombre}}</option>
					        	@endif
					        @endforeach
		                  	</select>
						</div>
						<div class="form-group col-md-3 col-md-offset-0">
							<label for="id_vendedor">Encargado</label>
							<select class="form-control" name="id_vendedor" id="id_vendedor" disabled="disabled">
							@foreach($encargados as $encargado)
							<?php $selected = ($encargado->id_usuario== Auth::user()->id_usuario) ? 'selected=selected':''; ?>
					        	<option value="{{ $encargado->id_usuario }}" {{$selected}}>{{ $encargado->nombre.' ('.$encargado->identificacion.')'}}</option>
					        @endforeach
		                  	</select>
		                  	<input type="hidden" name="id_vendedor" value="{{Auth::user()->id_usuario}}">
						</div>
						</div>


						<div class="row">
						<div class="form-group col-md-3 col-md-offset-0">
							<label for="fechaI">Fecha inicio</label>
							<div class="input-group date" id="date1">
			                  <div class="input-group-addon">
			                    <i class="fa fa-calendar"></i>
			                  </div>

			                  <input class="form-control pull-right" id="fechaI" type="text" name="fechaI" value="{{ $factura->fechaI}}">
			                </div>
						</div>
						<div class="form-group col-md-3 col-md-offset-0">
							<label for="id_terminoPago">Termino de pago</label>
							<select class="form-control" name="terminoPagoDias" id="id_terminoPago" onchange="terminoDePago($(this).val());">
								<option value="0">Contado</option>
								@foreach($terminosPago as $termino)
									@if($termino->activo==1)
									<?php $selected = ($termino->dias==$factura->terminoPagoDias)?'selected="selected"':''; ?>
					        		<option value="{{ $termino->dias}}" {{$selected}}>{{ $termino->nombre.' ('.$termino->dias.' dias)'}}</option>
					        		@endif
					       		@endforeach
					        	<option value="Manual">Manual</option>
		                  	</select>
						</div>
						
						
						<div class="form-group col-md-3 col-md-offset-0">
							<label for="fechaF">Fecha vencimiento</label>
							<div class="input-group date">
			                  <div class="input-group-addon">
			                    <i class="fa fa-calendar"></i>
			                  </div>
			                  <input class="form-control pull-right" id="fechaF" type="text" name="fechaF" value=" {{ $factura->fechaF}}" onchange="$('#id_terminoPago').val('Manual');">
			                </div>
						</div>
						
						<div class="form-group col-md-3 col-md-offset-0">
							<label for="notas">Notas</label>
							<textarea class="form-control" rows="2" name="notas" id="notas" >{{ $factura->notas}}</textarea>
						</div>
						</div>

						@include('gresos.editGresosFacturas')

						<!-- totalTotal box -->
              			<br>
              			<div class="col-md-3 col-sm-6 col-xs-12 col-md-offset-9">
				          <div class="info-box" style="box-shadow: 1px 1px 5px #888888;">
				            <span class="info-box-icon bg-red"><i class="fa fa-money" aria-hidden="true"></i></span>

				            <div class="info-box-content">
				              <span class="info-box-text">Total</span>
				              <span class="info-box-number" id="totalTotalSpan">{{$factura->total}}</span>
				              <input type="hidden" name="total" id="totalTotal" value="{{$factura->total}}">
				            </div>
				            <!-- /.info-box-content -->
				          </div>
				          <!-- /.info-box -->
				        </div>						
				          <!-- /.totalTotal box -->
						
					


					</div>
					<div class="box-footer">
                		<div class="col-md-2 col-md-offset-10">
							<button type="submit" class="btn btn-primary btn-block btn-flat" id="submit" >Actualizar</button>
						</div>
						<input type="hidden" name="_token" value="{{ csrf_token() }}">
						<input type="hidden" name="id_greso" value="{{ $factura->id_greso}}">
						<input type="hidden" name="id_empresa" value="{{ Auth::user()->id_empresa }}">

              		</div>
              		</form>
					
				</div>
				<!-- /.box -->
			</div>
		</div>
	</div>
	

	<script type="text/javascript">
		var row;
		$(function () {			
			$("#id_contacto").val({{$factura->id_contacto}});
        	/*para editar facturas unicamente, establecer el id del select igual a la clase del option seleccionado
        	permitiendo al aplicativo calcular el porcentaje de impuesto sin haber fijado un nuevo impuesto*/
        	for(var i=0; i< $(".rowItem").length;i++){
        		$(".impuesto.r"+i).attr("id", $(".impuesto #"+$(".impuesto.r"+i).val()).attr("class"));
			}
        	row = $("#r").val()-1;
        });		
	</script>

	<script type="text/javascript" src="{{ asset('/js/Gresos_js/gresosFactura.js') }}"></script>
@endsection
