@extends('adminlte::layouts.app')

@section('htmlheader_title')
	{{ trans('adminlte_lang::message.home') }}
@endsection


@section('main-content')
	<script type="text/javascript">
		$("#contentHeader1").append('<a href={{url("facturasE")}}><i class="fa fa-tags"></i> Facturas de compra</a>');
		$("#contentHeader2").text('Detalle de Factura de compra');
	</script>
	<?php 
    	$permisos = $_SESSION['permisos']; 
	?>
	<div class="container-fluid spark-screen">
		<div class="row">
			<div class="col-md-12 col-md-offset-0">
				<!-- Default box -->
				<div class="box box-danger">
					<div class="box-header with-border">
						<h3 class="box-title"> Ver Factura de compra</h3>

						<div class="box-tools pull-right">
							<button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
								<i class="fa fa-minus"></i></button>
							<button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
								<i class="fa fa-times"></i></button>
						</div>
					</div>
					
					<div class="box-body">
						
						<div class="row">
						<div class="form-group col-md-3 col-md-offset-0">
							<label for="numero">Numero de factura</label>
							<input class="form-control" name="numero" id="numero" type="text" value="{{ $factura->numero }}" disabled="disabled">
						</div>
						<div class="form-group col-md-3 col-md-offset-0">
							<label for="id_contacto">Proveedor</label>
							<select class="form-control" name="id_contacto" id="id_contacto" disabled="disabled">
							@foreach($proveedores as $proveedor)
								@if($proveedor->nit)
					        	<option value="{{ $proveedor->id_contacto }}">{{ $proveedor->nombre.' ('.$proveedor->nit.')'}}</option>
					        	@else
					        	<option value="{{ $proveedor->id_contacto }}">{{ $proveedor->nombre}}</option>
					        	@endif
					        @endforeach
		                  	</select>
						</div>
						<div class="form-group col-md-3 col-md-offset-0">
							<label for="id_vendedor">Encargado</label>
							<select class="form-control" name="id_vendedor" id="id_vendedor" disabled="disabled">
							@foreach($encargados as $encargado)
					        	<option value="{{ $encargado->id_usuario }}">{{ $encargado->nombre.' ('.$encargado->identificacion.')'}}</option>
					        @endforeach
		                  	</select>
						</div>
						</div>


						<div class="row">
						<div class="form-group col-md-3 col-md-offset-0">
							<label for="fechaI">Fecha inicio</label>
							<div class="input-group date" id="date1">
			                  <div class="input-group-addon">
			                    <i class="fa fa-calendar"></i>
			                  </div>

			                  <input class="form-control pull-right" id="fechaI" type="text" name="fechaI" value="{{ $factura->fechaI}}" disabled="disabled">
			                </div>
						</div>
						
						<div class="form-group col-md-3 col-md-offset-0">
							<label for="fechaF">Fecha vencimiento</label>
							<div class="input-group date">
			                  <div class="input-group-addon">
			                    <i class="fa fa-calendar"></i>
			                  </div>
			                  <input class="form-control pull-right" id="fechaF" type="text" name="fechaF" value=" {{ $factura->fechaF}}" disabled="disabled">
			                </div>
						</div>
						
						<div class="form-group col-md-3 col-md-offset-0">
							<label for="notas">Notas</label>
							<textarea class="form-control" rows="2" name="notas" id="notas" disabled="disabled">{{ $factura->notas}}</textarea>
						</div>
						</div>




				









						<h4>Items de compra</h4>
			
						<?php $r=0; ?>
						@foreach($itemsXgreso as $itemsX)

						<div class="row rowItem r0" id="rowItem">
							<div class="form-group col-md-3 col-md-offset-0">
								<label for="id_item">Item</label>
								<select class="form-control input-sm item r{{$r}}" name="id_item[]" id="id_item" disabled="disabled">
								<option></option>
								@foreach($items as $item)
									<?php $selected = ($item->id_item == $itemsX->id_item) ? "selected='selected'" : '';?>

									@if($item->referencia)
						        	<option value="{{ $item->id_item }}" onclick="item($(this).parent(), '{{$item->referencia}}',{{$item->costo}},{{$item->id_impuesto}});" {{$selected}}>{{ $item->nombre.' ('.$item->referencia.')'}}</option>
						        	@else
						        	<option value="{{ $item->id_item }}" onclick="item($(this).parent(), '{{$item->referencia}}',{{$item->costo}},{{$item->id_impuesto}});" {{$selected}}>{{ $item->nombre}}</option>
						        	@endif
						        @endforeach
			                  	</select>
			                
								<label for="referencia">Referencia</label>
								<input class="form-control input-sm referencia r{{$r}}" name="referencia[]" id="referencia" type="text" value="{{$itemsX->referencia}}" disabled="disabled">
							
							</div>
							
							<div class="form-group col-md-2 col-md-offset-0">
								<label for="valor">Valor</label>
								<input class="form-control input-sm valor r{{$r}}" name="valor[]" id="valor" type="number" value="{{$itemsX->valor}}" onchange="posicion($(this));" disabled="disabled">
							</div>
							
							<div class="form-group col-md-2 col-md-offset-0">
								<label for="id_descuento">Descuento %</label>
								<input class="form-control input-sm descuento r{{$r}}" name="descuento[]" id="descuento" type="text" value="{{$itemsX->descuento}}" onchange="posicion($(this));" disabled="disabled">

								<label for="id_impuesto">Impuesto</label>
								<select class="form-control input-sm impuesto r{{$r}}" name="id_impuesto[]" id="0" disabled="disabled">
									<option value="6" onclick="posicion($(this).parent(), 0);">0%</option>
								@foreach($impuestos as $impuesto)

									@if($impuesto->activa==1)
									<?php $selected = ($impuesto->id_impuesto == $itemsX->id_impuesto) ? "selected='selected'" : '';?>
						        	<option value="{{ $impuesto->id_impuesto }}" id="{{ $impuesto->id_impuesto }}" class="{{$impuesto->porcentaje}}" onclick="posicion($(this).parent(), {{$impuesto->porcentaje}});" {{$selected}}>{{ $impuesto->nombre.' ('.$impuesto->porcentaje.'%)'}}</option>
						        	@endif
						        	
						        @endforeach
			                  	</select>
							</div>
							<div class="form-group col-md-2 col-md-offset-0">
								<label for="apunte">Apunte</label>
								<textarea class="form-control input-sm apunte r{{$r}}" rows="3" name="apunte[]" id="apunte" disabled="disabled">{{$itemsX->apunte}}</textarea>
							</div>
							<div class="form-group col-md-1 col-md-offset-0">
								<label>Cantidad</label>
								<input class="form-control input-sm cantidad r{{$r}}" type="number" name="cantidad[]" value="{{$itemsX->cantidad}}"  disabled="disabled">
							</div>
							<div class="form-group col-md-2 col-md-offset-0">
								<label>Subtotal</label>
								<input class="form-control input-sm subtotal r{{$r}}" type="number" name="subtotalI[]" value="{{$itemsX->subtotal}}" disabled="disabled">
								<label>Total</label>
								<input class="form-control input-sm total r{{$r}}" type="number" name="totalI[]" value="{{$itemsX->total}}" disabled="disabled">
								
							</div>
						</div>
						<?php $r++; ?>
						@endforeach

						<div class="form-group col-md-2 col-md-offset-10">
							<label>Subtotal:</label>
							<input class="form-control input-sm" type="number" name="subtotal" id="subtotal" value="{{$factura->subtotal}}" readonly="readonly">
							<div id="impuestoDiv" class="impuestoDiv r0">
								<label>Impuestos</label>
								<input class="form-control input-sm impuestoFinal r0" type="number" value="{{$factura->total-$factura->subtotal}}" disabled="disabled">
							</div>
						</div>

						<!-- totalTotal box -->
              			<br>
              			<div class="form-group col-md-4 col-md-offset-0">
							<?php $checked = ($factura->deleted_at !=null) ? 'checked=checked':'';?>
							<label for="eliminadoPregunta">¿Eliminado?</label><br>
							<input type="checkbox" value="1" {{$checked}} disabled="disabled">
						</div>
              			<div class="col-md-3 col-sm-6 col-xs-12 col-md-offset-5">
				          <div class="info-box" style="box-shadow: 1px 1px 5px #888888;">
				            <span class="info-box-icon bg-red"><i class="fa fa-money" aria-hidden="true"></i></span>

				            <div class="info-box-content">
				              <span class="info-box-text">Total</span>
				              <span class="info-box-number" id="totalTotalSpan">{{$factura->total}}</span>
				              <input type="hidden" name="total" id="totalTotal" value="{{$factura->total}}">
				            </div>
				            <!-- /.info-box-content -->
				          </div>
				          <!-- /.info-box -->
				        </div>						
				          <!-- /.totalTotal box -->
						
					

				    	@if(isset($permisos[18]) && $permisos[18]->ver==1)
						<h4 class="box-title form-group col-md-12 col-md-offset-0">Versiones</h3>
						<table class="table table-bordered table-hover dataTable" 
							data-toggle="table"
							data-pagination="true"       						
							>
			                <thead>
				                <tr>
				                	<th>Numero</th>
				                  	<th>Cliente</th>
				                  	<th>Total</th>
				                  	<th>Fecha</th>
				                  	<th>Version</th>
				                  	<th>Responsable</th>
				                  	<th>Detalle</th>
				                </tr>
				            </thead>
				        	<tbody>
				                @foreach($versiones as $version) 
				                	<?php  $referenciaPrincipal = ($version->id_greso==$factura->id_greso) ? 'id=referenciaPrincipal' : ''; 
				                	?>
						    		<tr {{$referenciaPrincipal}} >
						    			<td> {{$version->numero }} 		</td>
						    			<td> {{$version->contacto }} 		</td>
						    			<td> {{$version->total }} 		</td>
						    			<td> {{$version->updated_at }} </td>
						    			<td> {{$version->numeroVersion }} </td>
						    			<td> <a href="{{ url('/verUsuario/'.$version->responsableID) }}">  {{$version->responsableNombre.' ('.$version->responsableIdentificacion.')' }}</a> </td>
						    			<td> 
						    				<a class="btn btn-info" style="padding: 2px 10px;" href="{{ url('/verFacturaE/'.$version->id_greso) }}" aria-label="Ver Detalle">
  												<i class="fa fa-eye" aria-hidden="true"></i>
  											</a>
						    			</td>
						    		</tr>
								@endforeach
			              	</tbody>	
              			</table>
              			@endif					
					</div>
					
					
				</div>
				<!-- /.box -->
			</div>
		</div>
	</div>
	

	<script type="text/javascript">
	
		$(function () {			
			$("#id_contacto").val({{$factura->id_contacto}});
			$("#id_vendedor").val({{$factura->id_vendedor}});

			$("#fechaI").datepicker({dateFormat: "yy-mm-dd"});
        	$("#fechaF").datepicker({dateFormat: "yy-mm-dd"});
        	$("#referenciaPrincipal").css("background-color", "#DDFCDA");
        });

		
	</script>
@endsection
