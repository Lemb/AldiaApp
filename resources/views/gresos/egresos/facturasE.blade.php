@extends('adminlte::layouts.app')

@section('htmlheader_title')
	{{ trans('adminlte_lang::message.home') }}
@endsection


@section('main-content')
	<script type="text/javascript">
		$("#contentHeader2").append('<i class="fa fa-tags"></i> Facturas de compra');
	</script>
	<?php $permisos = $_SESSION['permisos']; ?>
	<div class="container-fluid spark-screen">
		<div class="row">
			<div class="col-md-12 col-md-offset-0">
				<!-- Default box -->
				<div class="box box-danger">
					<div class="box-header with-border">
						<h3 class="box-title">Facturas de Compra</h3>

						<div class="box-tools pull-right">
							<button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
								<i class="fa fa-minus"></i></button>
							<button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
								<i class="fa fa-times"></i></button>
						</div>
					</div>
					<div class="box-body">
						@if($permisos['15']->insertar==1)
						<div class="col-md-2 col-md-offset-0" style="margin-bottom: 10px;">
							<a class="btn btn-primary btn-block btn-flat" href="{{ url('nuevaFacturaE')}}"><i class="fa fa-minus" aria-hidden="true"></i> Nuevo</a>
						</div>
						@endif
						

						<!--Errores y exito mensajes-->
						@if(count($errors)>0)
						<div class="form-group col-md-8 col-md-offset-2">
							<div class="alert alert-danger alert-dismissible">
				                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
				                <h4><i class="icon fa fa-ban"></i> Error en el formulario</h4>
				                @foreach ($errors->all() as $message) 
	    							<span > {{$message}}</span><br>
								@endforeach
				             </div>
						</div>
						@endif
						@if(session('mensaje'))
						<div class="form-group col-md-8 col-md-offset-2">
							<div class="alert alert-success alert-dismissible">
						        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
						        <h4><i class="icon fa fa-check"></i> {{ session('mensaje') }}</h4>
						    </div>
						</div>
						@endif


						<table class="table table-bordered table-hover dataTable" 
							data-toggle="table"
							data-pagination="true"
							data-search="true"
							data-show-refresh="true"
       						data-show-columns="true">
			                <thead>
				                <tr>
				                  <th>Numero</th>
				                  <th>Proveedor</th>
				                  <th>Inicio</th>
				                  <th>Vencimiento</th>
				                  <th>Total</th>
				                  <th class="pagado">Pagado</th>
				                  <th class="pagar">Por pagar</th>
				                  <th>Estado</th>
				                  <th data-halign="center" data-align="center" data-width="17%">Acciones</th>
				                </tr>
				            </thead>
				        	<tbody>
				        		@isset($facturas)
				                @foreach($facturas as $factura) 
				                	<tr>
						    			<td> {{$factura->numero }} 		</td>
						    			<td> {{$factura->contacto }} 	</td>
						    			<td> {{$factura->fechaI }} </td>
						    			<td> {{$factura->fechaF }} </td>
						    			<td> {{$factura->total }} </td>
						    			<td> {{$factura->pagado}}  </td>
						    			<td> {{$factura->pagar}}  </td>
						    			<td> {{$factura->estado }} </td>
						    			<td>
						    				@if($permisos['15']->ver==1)
						    				<a class="btn btn-success" style="padding: 2px 10px;" href="{{ url('/verFacturaE/'.$factura->id_greso) }}">
  												<i class="fa fa-eye" aria-hidden="true"></i>
  											</a>
  											@endif
  											@if($permisos['15']->modificar==1)
											<a class="btn btn-primary" style="padding: 2px 10px;" href="{{ url('/editFacturaE/'.$factura->id_greso) }}" aria-label="editar">
											  <i class="fa fa-pencil" aria-hidden="true"></i>
											</a>
											@endif
											@if($factura->pagado==0 && $permisos['15']->eliminar==1)
											<a class="btn btn-danger" style="padding: 2px 10px;" href="{{ url('/eliminarFacturaE/'.$factura->id_greso) }}" aria-label="Eliminar" id="eliminar">
											 <i class="fa fa-times" aria-hidden="true"></i>
											</a>
											@endif
										</td>
						    		</tr>
								@endforeach
								@endisset
			              	</tbody>	
              			</table>						
					</div>
					
				</div>
				<!-- /.box -->
			</div>
		</div>
	</div>

	
	
	<script type="text/javascript">
		
		$( document ).ready(function() {
			$(".pagar").css("color", "red");
			$(".pagado").css("color", "green");
			

  			$('#eliminar').click(function(event) {
		    	if(!confirm('¿Deseas realmente eliminar esta factura?'))
					event.preventDefault();
			});

		});
	</script>
@endsection

