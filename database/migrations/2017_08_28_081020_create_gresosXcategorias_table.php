<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGresosXcategoriasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('gresosXcategorias', function (Blueprint $table) {
            $table->bigInteger('id_greso')->unsigned();
            $table->foreign('id_greso')->references('id_greso')->on('gresos');
            $table->bigInteger('id_categoria')->unsigned();
            $table->foreign('id_categoria')->references('id_categoria')->on('categorias');
            $table->double('valor');
            $table->double('cantidad');
            $table->double('total');
            $table->string('apunte', 200);
            $table->primary(['id_greso', 'id_categoria']);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('gresosXcategorias');
    }
}

