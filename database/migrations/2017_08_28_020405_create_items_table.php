<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('items', function (Blueprint $table) {
            $table->bigIncrements('id_item');
            $table->string('nombre', 100);
            $table->string('referencia', 50)->nullable();
            $table->double('costo')->nullable();
            $table->double('precio');
            $table->string('descripcion', 100)->nullable();
            $table->string('codigoBarras', 250)->nullable();
            $table->string('imagen', 250)->nullable();
            $table->boolean('inventariable');//saber si es un producto o bien un servicio

            $table->smallInteger('id_medida')->unsigned()->nullable();
            $table->foreign('id_medida')->references('id_medida')->on('medidas');
            $table->bigInteger('id_categoria')->unsigned();
            $table->foreign('id_categoria')->references('id_categoria')->on('categorias');
            $table->bigInteger('id_empresa')->unsigned();
            $table->foreign('id_empresa')->references('id_empresa')->on('empresas');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('items');
    }
}
