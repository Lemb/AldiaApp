<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateItemsXbodegasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('itemsXbodegas', function (Blueprint $table) {
            $table->bigIncrements('id_itemsXbodegas');
            $table->bigInteger('id_item')->unsigned();
            $table->foreign('id_item')->references('id_item')->on('items');
            $table->integer('id_bodega')->unsigned();
            $table->foreign('id_bodega')->references('id_bodega')->on('bodegas');
            $table->bigInteger('cantidad');//cantidad de items en esa bodega
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('itemsXbodegas');
    }
}
