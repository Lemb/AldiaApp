<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGresosXimpuestosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('gresosxitemsXImpuestos', function (Blueprint $table) {
            $table->bigIncrements('id_gresosxitemsXImpuesto');
            $table->bigInteger('id_gresosXitems')->unsigned();
            $table->foreign('id_gresosXitems')->references('id_gresosXitems')->on('gresosXitems');
            $table->bigInteger('id_impuesto')->unsigned();
            $table->foreign('id_impuesto')->references('id_impuesto')->on('impuestos');
            $table->double('valor')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('gresosXimpuestos');
    }
}
