<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateItemsXimpuestosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        
        Schema::create('itemsXimpuestos', function (Blueprint $table) {
            $table->bigIncrements('id_itemXimpuesto');
            $table->bigInteger('id_item')->unsigned();
            $table->foreign('id_item')->references('id_item')->on('items');
            $table->bigInteger('id_impuesto')->unsigned()->nullable();
            $table->foreign('id_impuesto')->references('id_impuesto')->on('impuestos');
            $table->timestamps();
        });        
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
