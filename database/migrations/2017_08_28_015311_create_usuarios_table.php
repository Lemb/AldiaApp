<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsuariosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('usuarios', function (Blueprint $table) {
            $table->bigIncrements('id_usuario');
            $table->integer('identificacion')->nullable();
            $table->string('nombre', 80)->nullable();
            $table->string('apellido', 80)->nullable();
            $table->string('celular', 80)->nullable();
            $table->string('telefono', 80)->nullable();
            $table->string('direccion', 80)->nullable();
            $table->string('email', 100);
            $table->string('password', 100);
            $table->boolean('permisosU')->nullable();
            $table->boolean('activo');
            $table->boolean('login');//para saber si el usuario se puede logear

            $table->bigInteger('id_perfil')->unsigned();
            $table->foreign('id_perfil')->references('id_perfil')->on('perfiles');
            $table->bigInteger('id_empresa')->unsigned();
            $table->foreign('id_empresa')->references('id_empresa')->on('empresas');

            $table->rememberToken();
            $table->timestamps();
            $table->softDeletes();
            $table->boolean('versionActual')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('usuarios');
    }
}
