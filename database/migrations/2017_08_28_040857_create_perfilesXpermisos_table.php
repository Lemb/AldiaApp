<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePerfilesXpermisosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('perfilesXpermisos', function (Blueprint $table) {
            $table->bigInteger('id_perfil')->unsigned();
            $table->foreign('id_perfil')->references('id_perfil')->on('perfiles');
            $table->integer('id_permiso')->unsigned();
            $table->foreign('id_permiso')->references('id_permiso')->on('permisos');
            $table->boolean('insertar')->nullable();
            $table->boolean('ver')->nullable();
            $table->boolean('modificar')->nullable();
            $table->boolean('eliminar')->nullable();
            $table->primary(['id_perfil', 'id_permiso']);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('perfilesXpermisos');
    }
}
