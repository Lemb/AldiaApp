<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVersionesModulosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        
        Schema::create('versionesModulos', function (Blueprint $table) {
            $table->bigIncrements('id_versionModulo');
            $table->bigInteger('id_referencia')->unsigned();
            $table->bigInteger('id_referenciaInicial')->unsigned();
            $table->integer('numeroVersion')->unsigned();
            $table->integer('modulo')->unsigned();
            $table->bigInteger('id_usuario')->unsigned();
            $table->string('razonDeVersion', 254)->nullable();
            
        });
     
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
