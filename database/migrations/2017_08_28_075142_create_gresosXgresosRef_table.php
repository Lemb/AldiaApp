<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGresosXgresosRefTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('gresosXgresosRef', function (Blueprint $table) {
            $table->bigInteger('id_greso')->unsigned();
            $table->foreign('id_greso')->references('id_greso')->on('gresos');
            $table->bigInteger('id_gresoRef')->unsigned();
            $table->foreign('id_gresoRef')->references('id_greso')->on('gresos');
            $table->double('retencion')->nullable();
            $table->double('valor');
            $table->primary(['id_greso', 'id_gresoRef']);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('gresosXgresosRef');
    }
}
