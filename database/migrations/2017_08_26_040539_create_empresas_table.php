<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEmpresasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('empresas', function (Blueprint $table) {
            $table->bigIncrements('id_empresa');
            $table->string('nombre', 120);
            $table->string('nit', 40)->nullable();
            $table->string('telefono', 30)->nullable();
            $table->string('ciudad', 70)->nullable();
            $table->string('direccion', 80)->nullable();
            $table->string('email', 90)->nullable();
            $table->smallInteger('id_moneda')->unsigned();
            $table->foreign('id_moneda')->references('id_moneda')->on('monedas');
            $table->smallInteger('id_documentoFacturacion')->unsigned();
            $table->foreign('id_documentoFacturacion')->references('id_documentoFacturacion')->on('documentosFacturacion');
            $table->smallInteger('id_regimen')->unsigned();
            $table->foreign('id_regimen')->references('id_regimen')->on('regimenes');
            $table->string('logo', 250)->nullable();
            $table->bigInteger('id_configuracion')->unsigned();
            $table->foreign('id_configuracion')->references('id_configuracion')->on('configuraciones');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('empresas');
    }
}
