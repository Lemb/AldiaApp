<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGresosXitemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('gresosXitems', function (Blueprint $table) {
            $table->bigIncrements('id_gresosXitems');
            $table->bigInteger('id_greso')->unsigned();
            $table->foreign('id_greso')->references('id_greso')->on('gresos');
            $table->bigInteger('id_item')->unsigned()->nullable();
            $table->foreign('id_item')->references('id_item')->on('items');
            $table->bigInteger('id_categoria')->unsigned()->nullable();
            $table->foreign('id_categoria')->references('id_categoria')->on('categorias');
            $table->string('referencia', 60)->nullable();
            $table->double('valor');
            $table->double('cantidad');
            $table->double('descuento')->nullable();
            $table->double('subtotal')->nullable();
            $table->double('total');
            $table->string('apunte', 200)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('gresosXitems');
    }
}
