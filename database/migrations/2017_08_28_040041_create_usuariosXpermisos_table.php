<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsuariosXpermisosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('usuariosXpermisos', function (Blueprint $table) {
            $table->bigInteger('id_usuario')->unsigned();
            $table->foreign('id_usuario')->references('id_usuario')->on('usuarios');
            $table->integer('id_permiso')->unsigned();
            $table->foreign('id_permiso')->references('id_permiso')->on('permisos');
            $table->boolean('insertar')->nullable();
            $table->boolean('ver')->nullable();
            $table->boolean('modificar')->nullable();
            $table->boolean('eliminar')->nullable();
            $table->primary(['id_usuario', 'id_permiso']);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('usuariosXpermisos');
    }
}
