<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateConfiguracionesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('configuraciones', function (Blueprint $table) {
            $table->bigIncrements('id_configuracion');
            //opciones de facturas
            $table->string('fac_terminos', 250);
            $table->string('fac_notas', 250);
            $table->boolean('fac_estadoCuentaCliente');

            //siguiente numero para y la descripcion
            $table->bigInteger('SigNum_reciboCaja');
            $table->bigInteger('SigNum_comprobantePago');
            $table->bigInteger('SigNum_notaCredito');
            $table->bigInteger('SigNum_remision');
            $table->bigInteger('SigNum_cotizacion');
            $table->bigInteger('SigNum_ordenCompra');

            $table->boolean('multimoneda');
            $table->boolean('codigoBarras');
            $table->char('separadorDecimal', 2);
            $table->char('precisionDecimal', 2);

            //plantillas facturas
            $table->smallInteger('id_planFac_opcion1')->unsigned();
            $table->smallInteger('id_planFac_opcion2')->unsigned();
            $table->foreign('id_planFac_opcion1')->references('id_plantilla')->on('plantillas');
            $table->foreign('id_planFac_opcion2')->references('id_plantilla')->on('plantillas');
            $table->boolean('planFac_totalLetras');
            $table->boolean('planFac_retenciones_sugeridas');
            $table->boolean('planFac_saltosLineas_desc');
            $table->smallInteger('id_planFac_fuente')->unsigned();
            $table->foreign('id_planFac_fuente')->references('id_fuente')->on('fuentes');
            $table->smallInteger('planFac_fuenteTamanio');
            $table->smallInteger('planFac_espaciado');//espaciado entre cada item
            $table->boolean('planFac_totalItems');//mostrar total items
            $table->string('planFac_firma', 250);

            //otras plantillas
            $table->smallInteger('id_planCotiz')->unsigned();
            $table->smallInteger('id_planRemi')->unsigned();
            $table->smallInteger('id_planTrans')->unsigned();
            $table->smallInteger('id_planOrdenCompra')->unsigned();
            $table->foreign('id_planCotiz')->references('id_plantilla')->on('plantillas');
            $table->foreign('id_planRemi')->references('id_plantilla')->on('plantillas');
            $table->foreign('id_planTrans')->references('id_plantilla')->on('plantillas');
            $table->foreign('id_planOrdenCompra')->references('id_plantilla')->on('plantillas');

            //correos por defecto
            $table->text('cor_factura');
            $table->text('cor_cotizacion');
            $table->text('cor_notaCredito');
            $table->text('cor_remision');
            $table->text('cor_caja');
            $table->text('cor_egreso');
            $table->text('cor_compra');
            $table->text('cor_estadoCuenta');
            $table->boolean('pref_reporte');//preferencias reporte mensual
            

            //notificaciones de factura dias antes, presente y despues de la factura
            $table->boolean('notFac_antesActiva');
            $table->smallInteger('notFac_antesDias');
            $table->text('notFac_antesCorreo');
            $table->boolean('notFac_diaActiva');
            $table->text('notFac_diaCorreo');
            $table->boolean('notFac_despuesActiva');
            $table->smallInteger('notFac_despuesDias');
            $table->text('notFac_despuesCorreo');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('configuraciones');
    }
}
