<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGresosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('gresos', function (Blueprint $table) {
            $table->bigIncrements('id_greso');
            $table->smallInteger('id_operacion')->unsigned();//determina que tipo de greso es(ejem: factura, gasto, etc..)
            $table->foreign('id_operacion')->references('id_operacion')->on('operaciones');
            $table->dateTime('fechaI')->nullable();//fecha inicial de la factura
            $table->string('terminoPagoDias', 10)->nullable();
            $table->dateTime('fechaF')->nullable();//fecha en que expira la factura
            $table->string('numero', 100)->nullable();
            $table->text('observaciones')->nullable();
            $table->text('notas')->nullable();//las notas a diferencia de las observaciones aparecen en la factura impresa
            $table->double('subtotal')->nullable();
            $table->double('total');
            $table->string('adjunto', 254)->nullable();
            $table->string('frecuencia', 60)->nullable();

            $table->smallInteger('id_estado')->unsigned()->nullable();
            $table->foreign('id_estado')->references('id_estado')->on('estados');
            $table->integer('id_bodega')->unsigned()->nullable();
            $table->foreign('id_bodega')->references('id_bodega')->on('bodegas');
            $table->bigInteger('id_vendedor')->unsigned()->nullable();
            $table->foreign('id_vendedor')->references('id_usuario')->on('usuarios');
            $table->smallInteger('id_metodoPago')->unsigned()->nullable();
            $table->foreign('id_metodoPago')->references('id_metodoPago')->on('metodosPago');
            $table->bigInteger('id_lista')->unsigned()->nullable();
            $table->foreign('id_lista')->references('id_lista')->on('listas');
            $table->bigInteger('id_conciliacion')->unsigned()->nullable();
            $table->foreign('id_conciliacion')->references('id_conciliacion')->on('conciliaciones');
            $table->bigInteger('id_banco')->unsigned()->nullable();
            $table->foreign('id_banco')->references('id_banco')->on('bancos');
            $table->bigInteger('id_contacto')->unsigned();
            $table->foreign('id_contacto')->references('id_contacto')->on('contactos');
            $table->bigInteger('id_empresa')->unsigned();
            $table->foreign('id_empresa')->references('id_empresa')->on('empresas');
            $table->timestamps();
            $table->softDeletes();
            $table->boolean('versionActual');
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('gresos');
    }
}
