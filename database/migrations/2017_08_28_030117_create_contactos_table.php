<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateContactosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('contactos', function (Blueprint $table) {
            $table->bigIncrements('id_contacto');
            $table->string('nombre', 80);
            $table->string('nit', 50)->nullable();
            $table->string('direccion', 80)->nullable();
            $table->string('ciudad', 60)->nullable();
            $table->string('email', 90)->nullable();
            $table->string('telefono', 30)->nullable();
            $table->string('telefono2', 30)->nullable();
            $table->string('celular', 30)->nullable();
            $table->string('fax', 100)->nullable();
            $table->string('observaciones', 150)->nullable();
            
            //saber si es cliente y/o proveedor 
            $table->boolean('cliente');
            $table->boolean('proveedor');
            $table->boolean('estadoCuenta')->nullable();//estado de cuenta en las facturas.
            $table->boolean('notificacion')->nullable();//notificaciones al correo por facturas

            $table->bigInteger('id_terminoPago')->unsigned()->nullable();//termino de pago preterminado
            $table->foreign('id_terminoPago')->references('id_terminoPago')->on('terminosPago');
            $table->bigInteger('id_vendedor')->unsigned()->nullable();//vendedor preterminado
            $table->foreign('id_vendedor')->references('id_usuario')->on('usuarios');
            $table->bigInteger('id_lista')->unsigned()->nullable();//lista preterminada
            $table->foreign('id_lista')->references('id_lista')->on('listas');
            $table->bigInteger('id_empresa')->unsigned();
            $table->foreign('id_empresa')->references('id_empresa')->on('empresas');

            $table->timestamps();
            $table->softDeletes();
            $table->boolean('versionActual');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('contactos');
    }
}

