<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateConciliacionesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('conciliaciones', function (Blueprint $table) {
            $table->bigIncrements('id_conciliacion');
            $table->double('saldoI');//saldo inicial
            $table->double('saldoF');//saldo final
            $table->double('gastosB');//gastos bancarios
            $table->double('impuestosB');//impuestos bancarios
            $table->double('entradasB');//entradas bancarias
            
            $table->smallInteger('id_estado')->unsigned();
            $table->foreign('id_estado')->references('id_estado')->on('estados');
            $table->bigInteger('id_banco')->unsigned();
            $table->foreign('id_banco')->references('id_banco')->on('bancos');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('conciliaciones');
    }
}
