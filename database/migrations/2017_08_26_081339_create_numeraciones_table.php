<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNumeracionesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('numeraciones', function (Blueprint $table) {
            $table->bigIncrements('id_numeracion');
            $table->string('nombre', 80);
            $table->string('prefijo', 15);
            $table->integer('numeroI');//numero inicial
            $table->integer('numeroA');//numero actual
            $table->integer('numeroF');//numero final
            $table->string('resolucion', 200)->nullable();
            $table->boolean('preterminada')->nullable();
            $table->boolean('activa');
            $table->bigInteger('id_empresa')->unsigned();
            $table->foreign('id_empresa')->references('id_empresa')->on('empresas');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('numeraciones');
    }
}

