<?php

use Illuminate\Database\Seeder;

class PerfilesXPermisosTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('perfilesXpermisos')->insert([
            ['id_perfil' => '1', 'id_permiso' => '2',
            'insertar' => '0', 'ver' => '1',
            'modificar' => '1', 'eliminar' => '0'],

        	['id_perfil' => '1', 'id_permiso' => '3',
        	'insertar' => '1', 'ver' => '1',
        	'modificar' => '1', 'eliminar' => '1'],

        	['id_perfil' => '1', 'id_permiso' => '4',
        	'insertar' => '1', 'ver' => '1',
        	'modificar' => '1', 'eliminar' => '1'],

        	['id_perfil' => '1', 'id_permiso' => '5',
        	'insertar' => '1', 'ver' => '1',
        	'modificar' => '1', 'eliminar' => '1'],

        	['id_perfil' => '1', 'id_permiso' => '6',
        	'insertar' => '1', 'ver' => '1',
        	'modificar' => '1', 'eliminar' => '1'],

        	['id_perfil' => '1', 'id_permiso' => '7',
        	'insertar' => '1', 'ver' => '1',
        	'modificar' => '1', 'eliminar' => '1'],

        	['id_perfil' => '1', 'id_permiso' => '8',
        	'insertar' => '1', 'ver' => '1',
        	'modificar' => '1', 'eliminar' => '1'],

        	['id_perfil' => '1', 'id_permiso' => '9',
        	'insertar' => '1', 'ver' => '1',
        	'modificar' => '1', 'eliminar' => '1'],

        	['id_perfil' => '1', 'id_permiso' => '10',
        	'insertar' => '1', 'ver' => '1',
        	'modificar' => '1', 'eliminar' => '1'],

        	['id_perfil' => '1', 'id_permiso' => '11',
        	'insertar' => '1', 'ver' => '1',
        	'modificar' => '1', 'eliminar' => '1'],

        	['id_perfil' => '1', 'id_permiso' => '12',
        	'insertar' => '1', 'ver' => '1',
        	'modificar' => '1', 'eliminar' => '1'],

        	['id_perfil' => '1', 'id_permiso' => '13',
        	'insertar' => '1', 'ver' => '1',
        	'modificar' => '1', 'eliminar' => '1'],

        	['id_perfil' => '1', 'id_permiso' => '14',
        	'insertar' => '1', 'ver' => '1',
        	'modificar' => '1', 'eliminar' => '1'],

        	['id_perfil' => '1', 'id_permiso' => '15',
        	'insertar' => '1', 'ver' => '1',
        	'modificar' => '1', 'eliminar' => '1'],

        	['id_perfil' => '1', 'id_permiso' => '16',
        	'insertar' => '1', 'ver' => '1',
        	'modificar' => '1', 'eliminar' => '1'],
            
            ['id_perfil' => '1', 'id_permiso' => '17',
            'insertar' => '0', 'ver' => '1',
            'modificar' => '0', 'eliminar' => '0'],
            
             ['id_perfil' => '1', 'id_permiso' => '18',
            'insertar' => '0', 'ver' => '1',
            'modificar' => '0', 'eliminar' => '0'],

            ['id_perfil' => '1', 'id_permiso' => '19',
            'insertar' => '1', 'ver' => '0',
            'modificar' => '0', 'eliminar' => '0'],

            ['id_perfil' => '1', 'id_permiso' => '20',
            'insertar' => '0', 'ver' => '1',
            'modificar' => '0', 'eliminar' => '0'],

            ['id_perfil' => '1', 'id_permiso' => '21',
            'insertar' => '1', 'ver' => '1',
            'modificar' => '1', 'eliminar' => '1']

        ]);
    }
}
