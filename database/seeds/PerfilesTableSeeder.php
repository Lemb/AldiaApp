<?php

use Illuminate\Database\Seeder;

class PerfilesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	DB::table('perfiles')->insert([
        	[	'nombre' => 'Administrador',
        		'descripcion' => 'Todos los permisos',
        		'id_empresa' => '1'
        	]
        ]);
    }
}
