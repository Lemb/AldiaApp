<?php

use Illuminate\Database\Seeder;

class RegimenesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('regimenes')->insert([
        	['nombre' => 'Regimen Comun'],
        	['nombre' => 'Regimen Simplificado']
        ]);
    }
}
