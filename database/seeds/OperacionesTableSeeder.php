<?php

use Illuminate\Database\Seeder;

class OperacionesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('operaciones')->insert([
        	[
        		'id_operacion' => '1',
	        	'nombre' => 'Factura de Venta',
	        ],
	        
	        [
	        	'id_operacion' => '3',
		        'nombre' => 'Pagos Recibidos',
	        ],
	        [
	        	'id_operacion' => '7',
		        'nombre' => 'Factura de Compra',
	        ],
	        [
	        	'id_operacion' => '9',
		        'nombre' => 'Pagos Realizados',
	        ]
        ]);
    }
}
