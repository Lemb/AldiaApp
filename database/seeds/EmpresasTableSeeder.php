<?php

use Illuminate\Database\Seeder;

class EmpresasTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('empresas')->insert([
        	[
            'id_empresa' => '1',
            'nombre' => 'aldiaApp',
        	'nit' => '12345',
        	'telefono' => '3153392105',
        	'ciudad' => 'Cali',
        	'direccion' => 'aun no establecido',
        	'email' => 'aldia@gmail.com',
        	'id_moneda' => '1' ,
        	'id_documentoFacturacion' => '1',
        	'id_regimen' => '1' ,
        	'logo' => 'aldiaApp' ,
        	'id_configuracion' => '1'
        	]
        ]);
    }
}
