<?php

use Illuminate\Database\Seeder;

class EstadosTable extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('estados')->insert([
        	['id_estado' => '1', 'nombre' => 'Abierta'],
        	['id_estado' => '2', 'nombre' => 'Cerrada'],
            ['id_estado' => '3', 'nombre' => 'Pendiente']
        ]);
    }
}
