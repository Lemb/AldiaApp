<?php

use Illuminate\Database\Seeder;

class DocumentosTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('documentosFacturacion')->insert([
        	['nombre' => 'Factura de Venta', 'descripcion' => 'Factura de Venta'],
        	['nombre' => 'Remision', 'descripcion' => 'Remision']
        ]);
    }
}
