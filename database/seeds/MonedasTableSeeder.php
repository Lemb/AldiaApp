<?php

use Illuminate\Database\Seeder;

class MonedasTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('monedas')->insert([
        	[ 'id_moneda'=> '1',
               'nombre' => 'Pesos',
        	   'simbolo' => 'COP']
        ]);
    }
}
