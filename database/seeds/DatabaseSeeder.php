<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call([
        	PermisosTablaSeeder::class,
        	FuentesTableSeeder::class,
        	DocumentosTableSeeder::class,
        	MonedasTableSeeder::class,
        	PlantillasTableSeeder::class,
        	RegimenesTableSeeder::class,
        	ConfiguracionesTableSeeder::class,
        	EmpresasTableSeeder::class,
            ContactosTableSeeder::class,
        	PerfilesTableSeeder::class,
        	PerfilesXPermisosTableSeeder::class,
            CategoriasTableSeeder::class,
            ImpuestosTableSeeder::class,
            MedidasTableSeeder::class,
            MetodosPagoTableSeeder::class,
            OperacionesTableSeeder::class,
            EstadosTable::class,

        ]);
        
    }
}
