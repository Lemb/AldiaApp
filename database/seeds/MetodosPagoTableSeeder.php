<?php

use Illuminate\Database\Seeder;

class MetodosPagoTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('metodosPago')->insert([
        	['nombre' => 'Efectivo'],
        	['nombre' => 'Consignacion'],
        	['nombre' => 'Transferencia'],
        	['nombre' => 'Cheque'],
        	['nombre' => 'Tarjeta de crédito'],
        	['nombre' => 'Tarjeta de débito']
        ]);
    }
}
