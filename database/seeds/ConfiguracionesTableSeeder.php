<?php

use Illuminate\Database\Seeder;

class ConfiguracionesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	DB::table('configuraciones')->insert([
        	[
        	'fac_terminos' => 'aldiaApp',
        	'fac_notas' => 'aldiaApp',
        	'fac_estadoCuentaCliente' => '1',
        	'SigNum_reciboCaja' => '1',
        	'SigNum_comprobantePago' => '1',
        	'SigNum_notaCredito' => '1',
        	'SigNum_remision' => '1',
        	'SigNum_cotizacion' => '1',
        	'SigNum_ordenCompra' => '1',
        	'multimoneda' => '1',
        	'codigoBarras' => '1',
        	'separadorDecimal' => ',',
        	'precisionDecimal' => '2',
        	'id_planFac_opcion1' => '1',
        	'id_planFac_opcion2' => '1',
        	'planFac_totalLetras' => '1',
        	'planFac_retenciones_sugeridas' => '1',
        	'planFac_saltosLineas_desc' => '1',
        	'id_planFac_fuente' => '1',
        	'planFac_fuenteTamanio' => '1',
        	'planFac_espaciado' => '1',
        	'planFac_totalItems' => '1',
        	'planFac_firma' => '1',
        	'id_planCotiz' => '1',
        	'id_planRemi' => '1',
        	'id_planTrans' => '1',
        	'id_planOrdenCompra' => '1',
        	'cor_factura' => 'aldiaApp',
        	'cor_cotizacion' => 'aldiaApp',
        	'cor_notaCredito' => 'aldiaApp',
        	'cor_remision' => 'aldiaApp',
        	'cor_caja' => 'aldiaApp',
        	'cor_egreso' => 'aldiaApp',
        	'cor_compra' => 'aldiaApp',
        	'cor_estadoCuenta' => 'aldiaApp',
        	'pref_reporte' => '1',
        	'notFac_antesActiva' => '1',
        	'notFac_antesDias' => '1',
        	'notFac_antesCorreo' => 'aldiaApp',
        	'notFac_diaActiva' => '1',
        	'notFac_diaCorreo' => 'aldiaApp',
        	'notFac_despuesActiva' => '1',
        	'notFac_despuesDias' => '1',
        	'notFac_despuesCorreo' => 'aldiaApp']
        ]);
    }
}
