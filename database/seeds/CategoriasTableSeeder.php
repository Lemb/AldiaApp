<?php

use Illuminate\Database\Seeder;

class CategoriasTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
       DB::table('categorias')->insert([
        	['id_categoria' => '1',
        	 'nombre' => 'Activos',
        	 'id_categoriaP' => null,
        	 'id_empresa' => '1',
        	 'versionActual' => '1',],

        	['id_categoria' => '2',
        	 'nombre' => 'Egresos',
        	 'id_categoriaP' => null,
        	 'id_empresa' => '1',
        	 'versionActual' => '1',],

        	['id_categoria' => '3',
        	 'nombre' => 'Ingresos',
        	 'id_categoriaP' => null,
        	 'id_empresa' => '1',
        	 'versionActual' => '1',],

        	['id_categoria' => '4',
        	 'nombre' => 'Pasivos',
        	 'id_categoriaP' => null,
        	 'id_empresa' => '1',
        	 'versionActual' => '1',],

        	['id_categoria' => '5',
        	 'nombre' => 'Patrimonios',
        	 'id_categoriaP' => null,
        	 'id_empresa' => '1',
        	 'versionActual' => '1',],

        	['id_categoria' => '6',
        	 'nombre' => 'Transferencias Bancarias',
        	 'id_categoriaP' => null,
        	 'id_empresa' => '1',
        	 'versionActual' => '1',],

        	['id_categoria' => '7',
        	 'nombre' => 'Activos corrientes',
        	 'id_categoriaP' => '1',
        	 'id_empresa' => '1',
        	 'versionActual' => '1',],

        	['id_categoria' => '8',
        	 'nombre' => 'Activos no corrientes',
        	 'id_categoriaP' => '1',
        	 'id_empresa' => '1',
        	 'versionActual' => '1',],

        	['id_categoria' => '9',
        	 'nombre' => 'Cuentas por cobrar',
        	 'id_categoriaP' => '7',
        	 'id_empresa' => '1',
        	 'versionActual' => '1',],

        	['id_categoria' => '10',
        	 'nombre' => 'Inventarios',
        	 'id_categoriaP' => '7',
        	 'id_empresa' => '1',
        	 'versionActual' => '1',],

        	['id_categoria' => '11',
        	 'nombre' => 'Inversiones a corto plazo',
        	 'id_categoriaP' => '7',
        	 'id_empresa' => '1',
        	 'versionActual' => '1',],

        	['id_categoria' => '12',
        	 'nombre' => 'Otros activos corrientes',
        	 'id_categoriaP' => '7',
        	 'id_empresa' => '1',
        	 'versionActual' => '1',],

        	['id_categoria' => '13',
        	 'nombre' => 'Activos por impuestos corrientes',
        	 'id_categoriaP' => '9',
        	 'id_empresa' => '1',
        	 'versionActual' => '1',],

        	['id_categoria' => '14',
        	 'nombre' => 'Avances y anticipos entregados',
        	 'id_categoriaP' => '9',
        	 'id_empresa' => '1',
        	 'versionActual' => '1',],

        	['id_categoria' => '15',
        	 'nombre' => 'Cuentas por cobrar clientes',
        	 'id_categoriaP' => '9',
        	 'id_empresa' => '1',
        	 'versionActual' => '1',],

        	['id_categoria' => '16',
        	 'nombre' => 'Otras cuentas por cobrar',
        	 'id_categoriaP' => '9',
        	 'id_empresa' => '1',
        	 'versionActual' => '1',],

        	['id_categoria' => '17',
        	 'nombre' => 'Impuestos a favor',
        	 'id_categoriaP' => '13',
        	 'id_empresa' => '1',
        	 'versionActual' => '1',],

        	['id_categoria' => '18',
        	 'nombre' => 'Retenciones a favor',
        	 'id_categoriaP' => '13',
        	 'id_empresa' => '1',
        	 'versionActual' => '1',],

        	['id_categoria' => '19',
        	 'nombre' => 'Cuentas por cobrar a empleados',
        	 'id_categoriaP' => '16',
        	 'id_empresa' => '1',
        	 'versionActual' => '1',],

        	['id_categoria' => '20',
        	 'nombre' => 'Devoluciones a proveedores',
        	 'id_categoriaP' => '16',
        	 'id_empresa' => '1',
        	 'versionActual' => '1',],

        	['id_categoria' => '21',
        	 'nombre' => 'Prestamos a terceros',
        	 'id_categoriaP' => '16',
        	 'id_empresa' => '1',
        	 'versionActual' => '1',],

        	['id_categoria' => '22',
        	 'nombre' => 'Otros activos no corrientes',
        	 'id_categoriaP' => '8',
        	 'id_empresa' => '1',
        	 'versionActual' => '1',],

        	['id_categoria' => '23',
        	 'nombre' => 'Propiedad, planta y equipo',
        	 'id_categoriaP' => '8',
        	 'id_empresa' => '1',
        	 'versionActual' => '1',],

        	['id_categoria' => '24',
        	 'nombre' => 'Costos de ventas y operacion',
        	 'id_categoriaP' => '2',
        	 'id_empresa' => '1',
        	 'versionActual' => '1',],

        	['id_categoria' => '25',
        	 'nombre' => 'Depreciaciones, amortizaciones y deterioros',
        	 'id_categoriaP' => '2',
        	 'id_empresa' => '1',
        	 'versionActual' => '1',],

        	['id_categoria' => '26',
        	 'nombre' => 'Gastos de administracion',
        	 'id_categoriaP' => '2',
        	 'id_empresa' => '1',
        	 'versionActual' => '1',],

        	['id_categoria' => '27',
        	 'nombre' => 'Gastos por impuestos',
        	 'id_categoriaP' => '2',
        	 'id_empresa' => '1',
        	 'versionActual' => '1',],

        	['id_categoria' => '28',
        	 'nombre' => 'Costos de los servicios vendidos',
        	 'id_categoriaP' => '24',
        	 'id_empresa' => '1',
        	 'versionActual' => '1',],

        	['id_categoria' => '29',
        	 'nombre' => 'Costos de la mercancia vendida',
        	 'id_categoriaP' => '24',
        	 'id_empresa' => '1',
        	 'versionActual' => '1',],

        	['id_categoria' => '30',
        	 'nombre' => 'Ajustes al inventario',
        	 'id_categoriaP' => '29',
        	 'id_empresa' => '1',
        	 'versionActual' => '1',],

        	['id_categoria' => '31',
        	 'nombre' => 'Costos del inventario',
        	 'id_categoriaP' => '29',
        	 'id_empresa' => '1',
        	 'versionActual' => '1',],

        	['id_categoria' => '32',
        	 'nombre' => 'Descuentos financieros',
        	 'id_categoriaP' => '29',
        	 'id_empresa' => '1',
        	 'versionActual' => '1',],

        	['id_categoria' => '33',
        	 'nombre' => 'Devoluciones en compras de inventario',
        	 'id_categoriaP' => '29',
        	 'id_empresa' => '1',
        	 'versionActual' => '1',],

        	['id_categoria' => '34',
        	 'nombre' => 'Depreciacion de propiedad, planta y equipo',
        	 'id_categoriaP' => '25',
        	 'id_empresa' => '1',
        	 'versionActual' => '1',],

        	['id_categoria' => '35',
        	 'nombre' => 'Deterioro de cuentas por cobrar',
        	 'id_categoriaP' => '25',
        	 'id_empresa' => '1',
        	 'versionActual' => '1',],

        	['id_categoria' => '36',
        	 'nombre' => 'Gastos de personal',
        	 'id_categoriaP' => '26',
        	 'id_empresa' => '1',
        	 'versionActual' => '1',],

        	['id_categoria' => '37',
        	 'nombre' => 'Gastos generales',
        	 'id_categoriaP' => '26',
        	 'id_empresa' => '1',
        	 'versionActual' => '1',],

        	['id_categoria' => '38',
        	 'nombre' => 'Sueldos',
        	 'id_categoriaP' => '36',
        	 'id_empresa' => '1',
        	 'versionActual' => '1',],

        	['id_categoria' => '39',
        	 'nombre' => 'Arrendamientos',
        	 'id_categoriaP' => '37',
        	 'id_empresa' => '1',
        	 'versionActual' => '1',],

        	['id_categoria' => '40',
        	 'nombre' => 'Combustibles y lubricantes',
        	 'id_categoriaP' => '37',
        	 'id_empresa' => '1',
        	 'versionActual' => '1',],

        	['id_categoria' => '41',
        	 'nombre' => 'Comisiones, honorarios y servicios',
        	 'id_categoriaP' => '37',
        	 'id_empresa' => '1',
        	 'versionActual' => '1',],

        	['id_categoria' => '42',
        	 'nombre' => 'Otros gatos generales',
        	 'id_categoriaP' => '37',
        	 'id_empresa' => '1',
        	 'versionActual' => '1',],

        	['id_categoria' => '43',
        	 'nombre' => 'Papeleria',
        	 'id_categoriaP' => '37',
        	 'id_empresa' => '1',
        	 'versionActual' => '1',],

        	['id_categoria' => '44',
        	 'nombre' => 'Publicidad',
        	 'id_categoriaP' => '37',
        	 'id_empresa' => '1',
        	 'versionActual' => '1',],

        	['id_categoria' => '45',
        	 'nombre' => 'Seguros generales',
        	 'id_categoriaP' => '37',
        	 'id_empresa' => '1',
        	 'versionActual' => '1',],

        	['id_categoria' => '46',
        	 'nombre' => 'Servicos de aseo, cafeteria, restaurante y/o lavanderia',
        	 'id_categoriaP' => '37',
        	 'id_empresa' => '1',
        	 'versionActual' => '1',],

        	['id_categoria' => '47',
        	 'nombre' => 'Servicios publicos',
        	 'id_categoriaP' => '37',
        	 'id_empresa' => '1',
        	 'versionActual' => '1',],

        	['id_categoria' => '48',
        	 'nombre' => 'Vigilancia',
        	 'id_categoriaP' => '37',
        	 'id_empresa' => '1',
        	 'versionActual' => '1',],

        	['id_categoria' => '49',
        	 'nombre' => 'Servicios publicos',
        	 'id_categoriaP' => '37',
        	 'id_empresa' => '1',
        	 'versionActual' => '1',],

        	['id_categoria' => '50',
        	 'nombre' => 'Otros gastos',
        	 'id_categoriaP' => '2',
        	 'id_empresa' => '1',
        	 'versionActual' => '1',],

        	['id_categoria' => '51',
        	 'nombre' => 'Ingresos de actividades ordinarias',
        	 'id_categoriaP' => '3',
        	 'id_empresa' => '1',
        	 'versionActual' => '1',],

        	['id_categoria' => '52',
        	 'nombre' => 'Otros ingresos',
        	 'id_categoriaP' => '3',
        	 'id_empresa' => '1',
        	 'versionActual' => '1',],

        	['id_categoria' => '53',
        	 'nombre' => 'Ventas',
        	 'id_categoriaP' => '51',
        	 'id_empresa' => '1',
        	 'versionActual' => '1',],

        	['id_categoria' => '54',
        	 'nombre' => 'Ingresos financieros',
        	 'id_categoriaP' => '52',
        	 'id_empresa' => '1',
        	 'versionActual' => '1',],

        	['id_categoria' => '55',
        	 'nombre' => 'Otros ingresos diversos',
        	 'id_categoriaP' => '52',
        	 'id_empresa' => '1',
        	 'versionActual' => '1',],

        	['id_categoria' => '56',
        	 'nombre' => 'Pasivos corrientes',
        	 'id_categoriaP' => '4',
        	 'id_empresa' => '1',
        	 'versionActual' => '1',],

        	['id_categoria' => '57',
        	 'nombre' => 'Pasivos no corrientes',
        	 'id_categoriaP' => '4',
        	 'id_empresa' => '1',
        	 'versionActual' => '1',],

        	['id_categoria' => '58',
        	 'nombre' => 'Cuentas por pagar',
        	 'id_categoriaP' => '56',
        	 'id_empresa' => '1',
        	 'versionActual' => '1',],

        	['id_categoria' => '59',
        	 'nombre' => 'Obligaciones financieras',
        	 'id_categoriaP' => '56',
        	 'id_empresa' => '1',
        	 'versionActual' => '1',],

        	['id_categoria' => '60',
        	 'nombre' => 'Obligaciones laborales y de seguridad social',
        	 'id_categoriaP' => '56',
        	 'id_empresa' => '1',
        	 'versionActual' => '1',],

        	['id_categoria' => '61',
        	 'nombre' => 'Otros pasivos corrientes',
        	 'id_categoriaP' => '56',
        	 'id_empresa' => '1',
        	 'versionActual' => '1',],

        	['id_categoria' => '62',
        	 'nombre' => 'Pasivos por impuestos corrientes',
        	 'id_categoriaP' => '56',
        	 'id_empresa' => '1',
        	 'versionActual' => '1',],

        	['id_categoria' => '63',
        	 'nombre' => 'Avances y anticipos recibidos',
        	 'id_categoriaP' => '58',
        	 'id_empresa' => '1',
        	 'versionActual' => '1',],

        	['id_categoria' => '64',
        	 'nombre' => 'Cuentas por pagar a proveedores',
        	 'id_categoriaP' => '58',
        	 'id_empresa' => '1',
        	 'versionActual' => '1',],

        	['id_categoria' => '65',
        	 'nombre' => 'Otras cuentas por pagar',
        	 'id_categoriaP' => '58',
        	 'id_empresa' => '1',
        	 'versionActual' => '1',],

        	['id_categoria' => '66',
        	 'nombre' => 'Devoluciones a clientes',
        	 'id_categoriaP' => '65',
        	 'id_empresa' => '1',
        	 'versionActual' => '1',],

        	['id_categoria' => '67',
        	 'nombre' => 'Tarjetas de credito',
        	 'id_categoriaP' => '59',
        	 'id_empresa' => '1',
        	 'versionActual' => '1',],

        	['id_categoria' => '68',
        	 'nombre' => 'Salarios y prestaciones sociales',
        	 'id_categoriaP' => '60',
        	 'id_empresa' => '1',
        	 'versionActual' => '1',],

        	['id_categoria' => '69',
        	 'nombre' => 'Impuestos por pagar',
        	 'id_categoriaP' => '62',
        	 'id_empresa' => '1',
        	 'versionActual' => '1',],

        	['id_categoria' => '70',
        	 'nombre' => 'Retenciones por pagar',
        	 'id_categoriaP' => '62',
        	 'id_empresa' => '1',
        	 'versionActual' => '1',],

        	['id_categoria' => '71',
        	 'nombre' => 'Otros pasivos no corrientes',
        	 'id_categoriaP' => '57',
        	 'id_empresa' => '1',
        	 'versionActual' => '1',],

        	['id_categoria' => '72',
        	 'nombre' => 'Prestamos a largo plazo',
        	 'id_categoriaP' => '57',
        	 'id_empresa' => '1',
        	 'versionActual' => '1',],

        	['id_categoria' => '73',
        	 'nombre' => 'Ajustes por saldos iniciales',
        	 'id_categoriaP' => '5',
        	 'id_empresa' => '1',
        	 'versionActual' => '1',],

        	['id_categoria' => '74',
        	 'nombre' => 'Capital social',
        	 'id_categoriaP' => '5',
        	 'id_empresa' => '1',
        	 'versionActual' => '1',],

        	['id_categoria' => '75',
        	 'nombre' => 'Ganancias acumuladas',
        	 'id_categoriaP' => '5',
        	 'id_empresa' => '1',
        	 'versionActual' => '1',],

        	['id_categoria' => '76',
        	 'nombre' => 'Ajustes iniciales en bancos',
        	 'id_categoriaP' => '73',
        	 'id_empresa' => '1',
        	 'versionActual' => '1',],

        	['id_categoria' => '77',
        	 'nombre' => 'Ajustes iniciales en inventario',
        	 'id_categoriaP' => '5',
        	 'id_empresa' => '1',
        	 'versionActual' => '1',]

        ]);
    }
}
