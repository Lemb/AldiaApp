<?php

use Illuminate\Database\Seeder;

class FuentesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('fuentes')->insert([
        	['nombre' => 'aldiaApp']
        ]);
    }
}
