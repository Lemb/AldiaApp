<?php

use Illuminate\Database\Seeder;

class PermisosTablaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('permisos')->insert([
            ['id_permiso' => '2',
            'descripcion' => 'MiPerfil',
            'insertar' => '0', 'ver' => '1',
            'modificar' => '1', 'eliminar' => '0'],
        	['id_permiso' => '3',
            'descripcion' => 'Empresa',
            'insertar' => '0', 'ver' => '1',
            'modificar' => '1', 'eliminar' => '0'],
        	['descripcion' => 'Usuarios', 'id_permiso' => '4',
            'insertar' => '1', 'ver' => '1',
            'modificar' => '1', 'eliminar' => '1'],
        	['descripcion' => 'Perfiles', 'id_permiso' => '5',
            'insertar' => '1', 'ver' => '1',
            'modificar' => '1', 'eliminar' => '1'],
        	['descripcion' => 'Numeraciones', 'id_permiso' => '6',
            'insertar' => '1', 'ver' => '1',
            'modificar' => '1', 'eliminar' => '1'],
        	['descripcion' => 'Impuestos', 'id_permiso' => '7',
            'insertar' => '1', 'ver' => '1',
            'modificar' => '1', 'eliminar' => '1'],
        	['descripcion' => 'Terminos de Pago', 'id_permiso' => '8',
            'insertar' => '1', 'ver' => '1',
            'modificar' => '1', 'eliminar' => '1'],
        	['descripcion' => 'Categorias', 'id_permiso' => '9',
            'insertar' => '1', 'ver' => '1',
            'modificar' => '1', 'eliminar' => '1'],
        	['descripcion' => 'Bancos', 'id_permiso' => '10',
            'insertar' => '1', 'ver' => '1',
            'modificar' => '1', 'eliminar' => '1'],
        	['descripcion' => 'Contactos', 'id_permiso' => '11',
            'insertar' => '1', 'ver' => '1',
            'modificar' => '1', 'eliminar' => '1'],
        	['descripcion' => 'Items', 'id_permiso' => '12',
            'insertar' => '1', 'ver' => '1',
            'modificar' => '1', 'eliminar' => '1'],
        	['descripcion' => 'Facturas de Venta', 'id_permiso' => '13',
            'insertar' => '1', 'ver' => '1',
            'modificar' => '1', 'eliminar' => '1'],
        	['descripcion' => 'Pagos Recibidos', 'id_permiso' => '14',
            'insertar' => '1', 'ver' => '1',
            'modificar' => '1', 'eliminar' => '1'],
        	['descripcion' => 'Facturas de Compra', 'id_permiso' => '15',
            'insertar' => '1', 'ver' => '1',
            'modificar' => '1', 'eliminar' => '1'],
        	['descripcion' => 'Pagos Realizados', 'id_permiso' => '16',
            'insertar' => '1', 'ver' => '1',
            'modificar' => '1', 'eliminar' => '1'],
            ['descripcion' => 'Historiales de Acciones', 'id_permiso' => '17',
            'insertar' => '0', 'ver' => '1',
            'modificar' => '0', 'eliminar' => '0'],
            ['descripcion' => 'Historiales de Versiones', 'id_permiso' => '18',
            'insertar' => '0', 'ver' => '1',
            'modificar' => '0', 'eliminar' => '0'],
            ['descripcion' => 'POS', 'id_permiso' => '19',
            'insertar' => '1', 'ver' => '0',
            'modificar' => '0', 'eliminar' => '0'],

            ['descripcion' => 'Reportes', 'id_permiso' => '20',
            'insertar' => '0', 'ver' => '1',
            'modificar' => '0', 'eliminar' => '0'],

            ['descripcion' => 'Bodegas', 'id_permiso' => '21',
            'insertar' => '1', 'ver' => '1',
            'modificar' => '1', 'eliminar' => '1']
        ]);
    }
}
