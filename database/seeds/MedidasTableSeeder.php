<?php

use Illuminate\Database\Seeder;

class MedidasTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('medidas')->insert([
        	['nombre' => 'N/A', 'descripcion' => 'No aplica'],
        	['nombre' => 'Unidad', 'descripcion' => 'Una pieza del articulo'],
        	['nombre' => 'Metros', 'descripcion' => ''],
        	['nombre' => 'Centimetros', 'descripcion' => ''],
        	['nombre' => 'Pies', 'descripcion' => 'una pieza del articulo']
        ]);
    }
}
