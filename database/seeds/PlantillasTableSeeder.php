<?php

use Illuminate\Database\Seeder;

class PlantillasTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('plantillas')->insert([
        	['descripcion' => 'aldiaApp']
        ]);
    }
}
