<?php

use Illuminate\Database\Seeder;

class ContactosTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
       DB::table('contactos')->insert([
        	[
	        	'id_contacto' => '0', 
	        	'nombre' => 'POS',
	        	'cliente' => '1',
	        	'proveedor' => '1',
	        	'id_empresa' => '1',
	        	'versionActual' => '1'
        	]
        ]);
    }
}
