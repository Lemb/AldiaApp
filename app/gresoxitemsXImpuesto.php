<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class gresoxitemsXImpuesto extends Model
{
    //

    //
    protected $fillable = [
    	'id_gresosXitems',
    	'id_impuesto',
    	'valor'
    ];
    protected $hidden = [];
    protected $table = 'gresosxitemsXImpuestos';
	protected $primaryKey = 'id_gresosxitemsXImpuesto';
}
