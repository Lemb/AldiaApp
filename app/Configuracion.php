<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

//usados manualmente
use Illuminate\Support\Facades\DB;
use Validator;

class Configuracion extends Model
{
  
  //metodo encargado de actualizar las opciones de facturacion en general
  public static function facturacionGeneral($input){
    $respuesta = array();

    $reglas =  array(
    );
    $validator = Validator::make($input, $reglas);

    //asignar id_empresa para redirigir de nuevo al formulario
    $respuesta['id_empresa'] = $input['id_empresa'];

    if ($validator->fails()){
        $respuesta['mensaje'] = $validator;
        $respuesta['error']   = true;
    }else{
        // llamar al metodo unset para deshacerse del token como parte del input
        unset($input['_token']);

        DB::table('configuraciones')
        ->join('empresas', 'empresas.id_configuracion', '=', 'configuraciones.id_configuracion')
        ->where('id_empresa', $input['id_empresa'])
        ->update($input);

        $respuesta['error']   = false;
        $respuesta['mensaje'] = "Actualizado Correctamente";
        $respuesta['id_empresa']=$input['id_empresa'];
    }     

    return $respuesta; 
  }
      
	protected $fillable = [
	        'fac_terminos',
          'fac_notas',
			    'fac_estadoCuentaCliente',
    		  'SigNum_reciboCaja',
			    'SigNum_comprobantePago',
   			  'SigNum_notaCredito',
      		'SigNum_remision',
    		  'SigNum_cotizacion',
   			  'SigNum_ordenCompra',
          'multimoneda',
         	'codigoBarras',
     		  'separadorDecimal',
     		  'precisionDecimal',
   			  'id_planFac_opcion1',
		      'id_planFac_opcion2',
  			  'planFac_totalLetras',
			    'planFac_retenciones_sugeridas',
			    'planFac_saltosLineas_desc',
     		  'id_planFac_fuente',
			    'planFac_fuenteTamanio',
    		  'planFac_espaciado',
   			  'planFac_totalItems',
        	'planFac_firma',
         	'id_planCotiz',
          'id_planRemi',
         	'id_planTrans',
   			  'id_planOrdenCompra',
          'cor_factura',
       		'cor_cotizacion',
      		'cor_notaCredito',
         	'cor_remision',
          'cor_caja',
          'cor_egreso',
          'cor_compra',
      		'cor_estadoCuenta',
         	'pref_reporte',
   			  'notFac_antesActiva',
       		'notFac_antesDias',
     			'notFac_antesCorreo',
       		'notFac_diaActiva',
       		'notFac_diaCorreo',
 			    'notFac_despuesActiva',
    		  'notFac_despuesDias',
 			    'notFac_despuesCorreo'];
    protected $hidden = [];
    protected $table = 'configuraciones';
	  protected $primaryKey = 'id_configuracion';

}
