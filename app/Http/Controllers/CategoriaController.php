<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
//manualmente
use App\Categoria;
use App\Accion;
use Illuminate\Support\Facades\Input;
use Illuminate\Http\RedirectResponse;

session_start();
class CategoriaController extends Controller
{
    //
    public function __construct(){
        if(isset($_SESSION['id_usuario'])){
            $this->middleware(\App\Http\Middleware\autenticar::class.':'.$_SESSION['id_usuario'].',9');
        }
    }
    public function selectCategorias($mensaje=null){
    	$categorias = Categoria::selectCategorias($_SESSION['id_empresa']);
    	return view('categorias/categorias' )->with('categorias', $categorias)->with('mensaje', $mensaje)->with('sidebarActive', 'categoriasSidebar');
    }
    public function nuevaCategoria(){
    	$respuesta = Categoria::nuevaCategoria(Input::all(), $_SESSION['id_usuario']); 
    	if ($respuesta['error']){
            return redirect('categorias')->with('respuesta', 'Error')->withErrors($respuesta['mensaje']);
        }else{
            Accion::crearAccion('1', $_SESSION['id_usuario'], $respuesta, 9);
            return redirect('categorias')->with('mensaje', $respuesta['mensaje'] );
        }    
    }
    public function verCategoria($id_categoria){
        $categoria = Categoria::categoria($id_categoria, $_SESSION['id_empresa']);
        $versiones = Categoria::versiones($id_categoria, $_SESSION['id_empresa']);     
        return view('categorias/verCategoria')->with('categoria', $categoria)->with('versiones', $versiones)->with('sidebarActive', 'categoriasSidebar');
    }

    public function editCategoria(){
    	$respuesta = Categoria::actualizarCategoria(Input::all(), $_SESSION['id_usuario']);

    	if ($respuesta['error']){
            return redirect('categorias/'.$respuesta['id_empresa'])->with('respuesta', 'Error')->withErrors($respuesta['mensaje']);
        }else{
            Accion::crearAccion('3', $_SESSION['id_usuario'], $respuesta, 9);
            return redirect('categorias')->with('mensaje', $respuesta['mensaje'] );
        }
    }
    public function eliminarCategoria($id_categoria){
        $respuesta = Categoria::eliminarCategoria($id_categoria, $_SESSION['id_usuario'], $_SESSION['id_empresa']);
        Accion::crearAccion('4', $_SESSION['id_usuario'], $respuesta, 9);

        return redirect('categorias')->with('mensaje', $respuesta['mensaje']);
    }
}
session_abort();