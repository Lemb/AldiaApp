<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
//manualmente
use App\Usuario;
use App\Accion;
use Illuminate\Support\Facades\Input;
use Illuminate\Http\RedirectResponse;
use Illuminate\Support\Facades\Auth;
session_start();

class UsuarioController extends Controller
{

    public function __construct(){
        if(isset($_SESSION['id_usuario']))
            $this->middleware(\App\Http\Middleware\autenticar::class.':'.$_SESSION['id_usuario'].',4');
    }
    //encargado de actualizar el usuario es decir el perfil del activo
    public function actualizar(Request $request){
        
    	$respuesta = Usuario::actualizar(Input::all(), $_SESSION['id_usuario']);
    
        if ($respuesta['error']){
            return redirect('empresa')->with('respuesta', 'Error')->withErrors($respuesta['mensaje'] ,'errores');
        }else{
            
            Accion::crearAccion('3', $_SESSION['id_usuario'], $respuesta, 4);
            return redirect('empresa')->with('mensajeU', $respuesta['mensaje'] );
        }
    }

    public static function loginAjax(){
        $userdata = array(
            'email' => (isset($_GET['ajax'])) ? $_GET['email'] : Input::get('email'),
            'password' => (isset($_GET['ajax'])) ? $_GET['password'] : Input::get('password'),
            'deleted_at' => null,
            'versionActual' => 1,
        ); 

        if(Usuario::validar($userdata)){
            if (isset($_GET['ajax'])) {
                $result['mensaje']          = "Cambio de usuario exitoso";
                $result['nombre']           = Auth::user()->nombre;
                $result['apellido']         = Auth::user()->apellido;
                $result['identificacion']   = Auth::user()->identificacion;
                $result['id_usuario']       = Auth::user()->id_usuario;

                $_SESSION['id_usuario']     = Auth::user()->id_usuario;
                echo json_encode($result);
            }
            else
                echo 'error';
        }
    }

    
    //5 ACCIONES BASICAS
    public function selectUsuarios($mensaje=null){
        $usuarios = Usuario::selectUsuarios($_SESSION['id_empresa']);
        return view('configuracion/usuarios/usuarios' )->with('usuarios', $usuarios)->with('mensaje', $mensaje)->with('sidebarActive', 'configuracionSidebar');
    }



    public function nuevoUsuario(){
    	$perfiles = Usuario::perfiles($_SESSION['id_empresa']); 
    	return view('configuracion/usuarios/nuevoUsuario')->with('perfiles', $perfiles)->with('sidebarActive', 'configuracionSidebar');	
    }
    public function nuevoUsuarioPost(Request $request){
        
    	$respuesta = Usuario::nuevoUsuario(Input::all(), $_SESSION['id_usuario']);
        if ($respuesta['error']){
            return redirect('nuevoUsuario')->with('respuesta', 'Error')->withErrors($respuesta['mensaje']);
        }else{
            Accion::crearAccion('1', $_SESSION['id_usuario'], $respuesta, 4);
            return redirect('usuarios/'.$respuesta['mensaje']);
        } 
        
    }


    //metodos Usuario
    public function verUsuario($id_usuario, $id_usuario2=null){

        $perfiles = Usuario::perfiles($_SESSION['id_empresa']);
        $usuario  = Usuario::usuario($id_usuario, 'ver', $_SESSION['id_empresa']);    
        $usuario2  = (isset($id_usuario2)) ? Usuario::usuario($id_usuario2, 'ver'):null;    
        $versiones = Usuario::versiones($id_usuario);

        return view('configuracion/usuarios/verUsuario')->with('usuario', $usuario)->with('usuario2', $usuario2)->with('perfiles', $perfiles)->with('versiones', $versiones)->with('sidebarActive', 'configuracionSidebar');
    }



    public function editUsuario($id_usuario){
    	$perfiles = Usuario::perfiles($_SESSION['id_empresa']);
    	$usuario  = Usuario::usuario($id_usuario, 'editar', $_SESSION['id_empresa']);
    	return view('configuracion/usuarios/editUsuario')->with('usuario', $usuario)->with('perfiles', $perfiles)->with('sidebarActive', 'configuracionSidebar');	
    }
    public function editUsuarioPost(Request $request){
    	$respuesta = Usuario::actualizarUsuario(Input::all(), $_SESSION['id_usuario'], $_SESSION['id_empresa']);

    	if ($respuesta['error']){
            return redirect('editUsuario/'.$respuesta['id_usuario'])->with('respuesta', 'Error')->withErrors($respuesta['mensaje']);
        }else{
            Accion::crearAccion('3', $_SESSION['id_usuario'], $respuesta, 4);
            return redirect('editUsuario/'.$respuesta['id_usuario'])->with('mensaje', $respuesta['mensaje'] );
        }
    }


    public function eliminarUsuario($id_usuario){
        $respuesta = Usuario::eliminarUsuario($id_usuario, $_SESSION['id_usuario'], $_SESSION['id_empresa']);
        Accion::crearAccion('4', $_SESSION['id_usuario'], $respuesta, 4);
        
        return redirect('usuarios/'.$respuesta['respuesta']);
    }


}
session_abort();