<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
//manualmente
use App\Banco;
use App\Accion;
use Illuminate\Support\Facades\Input;
use Illuminate\Http\RedirectResponse;
session_start();

class BancoController extends Controller
{
    //
    public function __construct(){
        if(isset($_SESSION['id_usuario']))
            $this->middleware(\App\Http\Middleware\autenticar::class.':'.$_SESSION['id_usuario'].',10');
    }
    public function selectBancos(){
    	$bancos = Banco::selectBancos($_SESSION['id_empresa']);
    	return view('bancos/bancos' )->with('bancos', $bancos)->with('sidebarActive', 'bancosSidebar');
    }

    public function nuevoBanco(){
    	$respuesta = Banco::nuevoBanco(Input::all(), $_SESSION['id_usuario']); 
    	if ($respuesta['error']){
            return redirect('bancos/'.$respuesta['id_empresa'])->with('respuesta', 'Error')->withErrors($respuesta['mensaje']);
        }else{
            Accion::crearAccion('1', $_SESSION['id_usuario'], $respuesta, 10);
            return redirect('bancos')->with('mensaje', $respuesta['mensaje'] );
        }
    }

    //metodos Usuario
    public function verBanco($id_banco){
        $banco  = Banco::banco($id_banco, $_SESSION['id_empresa']);    
        $versiones = Banco::versiones($id_banco);

        return view('bancos/verBanco')->with('banco', $banco)->with('versiones', $versiones)->with('sidebarActive', 'bancosSidebar');
    }


    public function editBanco(){
    	$respuesta = Banco::actualizarBanco(Input::all(), $_SESSION['id_usuario']);

    	if ($respuesta['error']){
            return redirect('bancos/'.$respuesta['id_empresa'])->with('respuesta', 'Error')->withErrors($respuesta['mensaje']);
        }else{
            Accion::crearAccion('3', $_SESSION['id_usuario'], $respuesta, 10);
            return redirect('bancos')->with('mensaje', $respuesta['mensaje'] );
        }
    }

    public function eliminarBanco($id_banco){
        $respuesta = Banco::eliminarBanco($id_banco, $_SESSION['id_usuario'], $_SESSION['id_empresa']);
        Accion::crearAccion('4', $_SESSION['id_usuario'], $respuesta, 10);
        return redirect('bancos')->with('mensaje', $respuesta['mensaje']);
    }
}
session_abort();