<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

//manualmente
use App\Accion;
use App\Bodega;
use App\Permiso;
use Illuminate\Support\Facades\Input;
use Illuminate\Http\RedirectResponse;
use Illuminate\Support\Facades\Auth;

session_start();
class BodegaController extends Controller
{
	public function __construct(){
        if(isset($_SESSION['id_usuario'])){
            $this->middleware(\App\Http\Middleware\autenticar::class.':'.$_SESSION['id_usuario'].',21');
        }
    }
    public function selectBodegas($mensaje=null){
    	$bodegas = Bodega::selectBodegas($_SESSION['id_empresa']);
    	return view('configuracion/bodegas/bodegas' )->with('bodegas', $bodegas)->with('mensaje', $mensaje)->with('sidebarActive', 'configuracionSidebar');
    }
   
    public function nuevaBodegaPost(Request $request){
        $respuesta = Bodega::nuevaBodega(Input::all(), $_SESSION['id_usuario']);
        if ($respuesta['error']){
            return redirect('bodegas/'.$respuesta['mensaje'])->with('respuesta', 'Error')->withErrors($respuesta['mensaje']);
        }else{
            Accion::crearAccion('1', $_SESSION['id_usuario'], $respuesta, 21);
            return redirect('bodegas/'.$respuesta['mensaje']);
        } 
    }

    public function verBodega($id_bodega){
    	$bodega 	= Bodega::bodega($id_bodega, $_SESSION['id_empresa']);
    	$versiones  = Bodega::versiones($id_bodega);
        
    	return view('configuracion/bodegas/verBodega')->with('bodega', $bodega)->with('sidebarActive', 'configuracionSidebar')->with('versiones', $versiones);
    }
    /*
    public function nuevaBodega(){
        $permisos = Permiso::selectPermisos();
        return view('configuracion/bodegas/nuevaBodega')->with('permisos', $permisos)->with('sidebarActive', 'configuracionSidebar'); 
    }
    
    public function editBodega($id_bodega){
    	$bodega  = Bodega::bodega($id_bodega, $_SESSION['id_empresa']);
        $permisos = Permiso::selectPermisos();
        $permisosEdit = Permiso::selectPermisosEdit($id_bodega);
    	return view('configuracion/bodegas/editBodegas')->with('bodega', $bodega)->with('permisos', $permisos)->with('permisosEdit', $permisosEdit)->with('sidebarActive', 'configuracionSidebar');	
    }
	*/
    public function editBodegaPost(Request $request){
    	$respuesta = Bodega::actualizarBodega(Input::all(), $_SESSION['id_usuario']);

    	if ($respuesta['error']){
            return redirect('bodegas/'.$respuesta['mensaje'])->with('respuesta', 'Error')->withErrors($respuesta['mensaje']);
        }else{Accion::crearAccion('3', $_SESSION['id_usuario'], $respuesta, 21);
            return redirect('bodegas/'.$respuesta['mensaje'])->with('mensaje', $respuesta['mensaje'] );
        }
    }


    public function eliminarBodega($id_bodega){
        $respuesta = Bodega::eliminarBodega($id_bodega, $_SESSION['id_empresa']);
        Accion::crearAccion('4', $_SESSION['id_usuario'], $respuesta, 21);

        return redirect('bodegas/'.$respuesta['mensaje']); 
    }

    public function ajaxBodega($id_bodega){
        echo json_encode(Bodega::itemsXbodega($id_bodega));
    }

}
session_abort();
