<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

//manualmente
use App\Empresa;
use App\Accion;
use Illuminate\Support\Facades\Input;
use Illuminate\Http\RedirectResponse;
session_start();

class EmpresaController extends Controller
{   
    public function __construct(){
        if(isset($_SESSION['id_usuario']))
            $this->middleware(\App\Http\Middleware\autenticar::class.':'.$_SESSION['id_usuario'].',11');
    }
    
    //metodo encargado de actualizar una nueva empresa cuando en el formulario se unda click en actualizar en la seccion de empresa
    public function actualizar(Request $request){
        
    	$respuesta = Empresa::actualizar(Input::all());
    
        if ($respuesta['error']){
            return redirect('empresa')->with('respuesta', 'Error')->withErrors($respuesta['mensaje']);
        }else{
            Accion::crearAccion('3', $_SESSION['id_usuario'], $respuesta, 3);
            return redirect('empresa')->with('mensaje', $respuesta['mensaje'] );
        }
    }

    //metodo encargado de cargar los datos para el formulario de configuraciones/empresa
    public function form(){
    	$empresa = Empresa::form($_SESSION['id_empresa']);
    	return view('configuracion/empresa')->with('empresa', $empresa)->with('sidebarActive', 'configuracionSidebar');
    }
}
session_abort();
