<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

//manualmente
use App\Accion;
use App\Perfiles;
use App\Permiso;
use Illuminate\Support\Facades\Input;
use Illuminate\Http\RedirectResponse;
use Illuminate\Support\Facades\Auth;
session_start();

class PerfilesController extends Controller
{
    public function __construct(){

        if(isset($_SESSION['id_usuario'])){
            $this->middleware(\App\Http\Middleware\autenticar::class.':'.$_SESSION['id_usuario'].',5');
        }
    }
    public function selectPerfiles($mensaje=null){
    	$perfiles = Perfiles::selectPerfiles($_SESSION['id_empresa']);
    	return view('configuracion/perfiles/perfiles' )->with('perfiles', $perfiles)->with('mensaje', $mensaje)->with('sidebarActive', 'configuracionSidebar');
    }

    public function nuevoPerfil(){
        $permisos = Permiso::selectPermisos();
        return view('configuracion/perfiles/nuevoPerfil')->with('permisos', $permisos)->with('sidebarActive', 'configuracionSidebar'); 
    }
    public function nuevoPerfilPost(Request $request){
        $respuesta = Perfiles::nuevoPerfil(Input::all());
        if ($respuesta['error']){
            return redirect('nuevoPerfil')->with('respuesta', 'Error')->withErrors($respuesta['mensaje']);
        }else{
            Accion::crearAccion('1', $_SESSION['id_usuario'], $respuesta, 5);
            return redirect('perfiles/'.$respuesta['mensaje']);
        } 
    }


    public function verPerfil($id_perfil){
    	$perfil  = Perfiles::perfil($id_perfil, $_SESSION['id_empresa']);
        $permisos = Permiso::selectPermisos();
        $permisosEdit = Permiso::selectPermisosEdit($id_perfil);
    	return view('configuracion/perfiles/verPerfil')->with('perfil', $perfil)->with('permisos', $permisos)->with('permisosEdit', $permisosEdit)->with('sidebarActive', 'configuracionSidebar');
    }
    

    public function editPerfil($id_perfil){
    	$perfil  = Perfiles::perfil($id_perfil, $_SESSION['id_empresa']);
        $permisos = Permiso::selectPermisos();
        $permisosEdit = Permiso::selectPermisosEdit($id_perfil);
    	return view('configuracion/perfiles/editPerfil')->with('perfil', $perfil)->with('permisos', $permisos)->with('permisosEdit', $permisosEdit)->with('sidebarActive', 'configuracionSidebar');	
    }
    public function editPerfilPost(Request $request){
    	$respuesta = Perfiles::actualizarPerfil(Input::all());

    	if ($respuesta['error']){
            return redirect('editPerfil/'.$respuesta['id_perfil'])->with('respuesta', 'Error')->withErrors($respuesta['mensaje']);
        }else{Accion::crearAccion('3', $_SESSION['id_usuario'], $respuesta, 5);
            
            return redirect('editPerfil/'.$respuesta['id_perfil'])->with('mensaje', $respuesta['mensaje'] );
        }
    }



    public function eliminarPerfil($id_perfil){
        $respuesta = Perfiles::eliminarPerfil($id_perfil, $_SESSION['id_empresa']);
        Accion::crearAccion('4', $_SESSION['id_usuario'], $respuesta, 5);

        return redirect('perfiles/'.$respuesta['respuesta']); 
    }

}
session_abort();
