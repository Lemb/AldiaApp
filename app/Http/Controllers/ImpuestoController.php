<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

//manualmente
use App\Accion;
use App\Impuesto;
use Illuminate\Support\Facades\Input;
use Illuminate\Http\RedirectResponse;
session_start();
class ImpuestoController extends Controller
{
    public function __construct(){
        if(isset($_SESSION['id_usuario']))
            $this->middleware(\App\Http\Middleware\autenticar::class.':'.$_SESSION['id_usuario'].',7');
    }
    public function selectImpuestos($mensaje=null){
    	$impuestos   = Impuesto::selectImpuestos($_SESSION['id_empresa']);
    	$retenciones = Impuesto::selectRetenciones($_SESSION['id_empresa']);

       	return view('configuracion/impuestos/impuestos' )->with('impuestos', $impuestos)->with('retenciones', $retenciones)->with('mensaje', $mensaje)->with('sidebarActive', 'configuracionSidebar');
    }
    
    public function nuevoImpuestoPost(){
    	$respuesta = Impuesto::nuevoImpuesto(Input::all()); 
    	if ($respuesta['error']){
            return redirect('impuestos')->with('respuesta', 'Error')->withErrors($respuesta['mensaje'], $respuesta['cualidad']);
        }else{
            Accion::crearAccion('1', $_SESSION['id_usuario'], $respuesta, 7);
            return redirect('impuestos/'.$respuesta['respuesta']); 
        }
    }
    public function verImpuesto($id_impuesto){
        $impuesto  = Impuesto::impuesto($id_impuesto, $_SESSION['id_empresa']);
        return view('configuracion/impuestos/verImpuesto')->with('impuesto', $impuesto)->with('sidebarActive', 'configuracionSidebar');
    }
    public function editImpuestoPost(){
    	$respuesta = Impuesto::actualizarImpuesto(Input::all());

    	if ($respuesta['error']){
            return redirect('impuestos')->with('respuesta', 'Error')->withErrors($respuesta['mensaje'], $respuesta['cualidad']);
        }else{
            Accion::crearAccion('3', $_SESSION['id_usuario'], $respuesta, 7);
            return redirect('impuestos/'.$respuesta['respuesta']); 
        }
    }
    public function eliminarImpuesto($id_impuesto){
        $respuesta = Impuesto::eliminarImpuesto($id_impuesto, $_SESSION['id_empresa']);
        Accion::crearAccion('4', $_SESSION['id_usuario'], $respuesta, 7);
        return redirect('impuestos/'.$respuesta['respuesta']); 
    }
}
session_abort();