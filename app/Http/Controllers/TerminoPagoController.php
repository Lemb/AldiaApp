<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

//manualmente
use App\Accion;
use App\TerminoPago;
use Illuminate\Support\Facades\Input;
use Illuminate\Http\RedirectResponse;
session_start();

class TerminoPagoController extends Controller
{
    //
    public function __construct(){
        if(isset($_SESSION['id_usuario']))
            $this->middleware(\App\Http\Middleware\autenticar::class.':'.$_SESSION['id_usuario'].',8');
    }
    public function selectTerminosPagos($mensaje=null){
    	$terminos = TerminoPago::selectTerminos($_SESSION['id_empresa']);
    	return view('configuracion/terminosPago/terminosPago' )->with('terminos', $terminos)->with('mensaje', $mensaje)->with('sidebarActive', 'configuracionSidebar');
    }


    public function nuevoTerminoPagoPost(){
    	$respuesta = TerminoPago::nuevoTerminoPago(Input::all()); 
    	if ($respuesta['error']){
            return redirect('terminosPago/'.$respuesta['id_empresa'])->with('respuesta', 'Error')->withErrors($respuesta['mensaje']);
        }else{
            Accion::crearAccion('1', $_SESSION['id_usuario'], $respuesta, 8);
            return redirect('terminosPago')->with('mensaje', $respuesta['mensaje'] );
        }
    }
    public function verTerminoPago($id_terminoPago){
        $terminoPago  = TerminoPago::terminoPago($id_terminoPago, $_SESSION['id_empresa']);
        return view('configuracion/terminosPago/verTerminoPago')->with('terminoPago', $terminoPago)->with('sidebarActive', 'configuracionSidebar');
    }

    public function editTerminoPagoPost(){
    	$respuesta = TerminoPago::actualizarTerminoPago(Input::all());

    	if ($respuesta['error']){
            return redirect('terminosPago')->with('respuesta', 'Error')->withErrors($respuesta['mensaje']);
        }else{
            Accion::crearAccion('3', $_SESSION['id_usuario'], $respuesta, 8);
            return redirect('terminosPago')->with('mensaje', $respuesta['mensaje'] );
        }
    }
    public function eliminarTerminoPago($id_terminoPago){
        $respuesta = TerminoPago::eliminarTerminoPago($id_terminoPago, $_SESSION['id_empresa']);
        Accion::crearAccion('4', $_SESSION['id_usuario'], $respuesta, 8);
        return redirect('terminosPago')->with('mensaje', $respuesta['respuesta']);        
        
    }
}
session_abort();