<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

//manualmente
use App\Accion;
use App\Contacto;
use App\Usuario;
use App\TerminoPago;
use Illuminate\Support\Facades\Input;
use Illuminate\Http\RedirectResponse;

session_start();
class ContactoController extends Controller
{
    //
    public function __construct(){
        if(isset($_SESSION['id_usuario']))
            $this->middleware(\App\Http\Middleware\autenticar::class.':'.$_SESSION['id_usuario'].',11');
    }
    public function selectContactos(){
    	$contactos = Contacto::selectContactos($_SESSION['id_empresa']);
    	return view('contactos/contactos' )->with('contactos', $contactos)->with('sidebarActive', 'contactosSidebar');
    }


    public function nuevoContacto(){
    	$vendedores = Usuario::selectUsuarios($_SESSION['id_empresa']);
    	$terminos   = TerminoPago::selectTerminos($_SESSION['id_empresa']);
    	return view('contactos/nuevoContacto')->with('vendedores', $vendedores )->with('terminos', $terminos)->with('sidebarActive', 'contactosSidebar');	
    }
    public function nuevoContactoPost(){
    	$respuesta = Contacto::nuevoContacto(Input::all(), $_SESSION['id_usuario']);

    	if ($respuesta['error']){
            return redirect('nuevoContacto')->with('respuesta', 'Error')->withErrors($respuesta['mensaje'])->withInput();
        }else{
            Accion::crearAccion('1', $_SESSION['id_usuario'], $respuesta, 11);
            return redirect('contactos')->with('mensaje', $respuesta['mensaje'] );
        } 
    }

    public function verContacto($id_contacto){
        $contacto   = Contacto::contacto($id_contacto, $_SESSION['id_empresa'], 'ver');
        $personas   = Contacto::personas($id_contacto);
        $vendedores = Usuario::selectUsuarios($_SESSION['id_empresa']);
        $terminos   = TerminoPago::selectTerminos($_SESSION['id_empresa']);
        $versiones  = Contacto::versiones($id_contacto);
   
        return view('contactos/verContacto')->with('vendedores', $vendedores )->with('terminos', $terminos)->with("contacto", $contacto)->with("personas", $personas)->with('versiones', $versiones)->with('sidebarActive', 'contactosSidebar');  
    }

    public function editContacto($id_contacto){
    	$contacto 	= Contacto::contacto($id_contacto, $_SESSION['id_empresa'], 'editar');
    	$personas	= Contacto::personas($id_contacto);
    	$vendedores = Usuario::selectUsuarios($_SESSION['id_empresa']);
    	$terminos   = TerminoPago::selectTerminos($_SESSION['id_empresa']);
    	
    	return view('contactos/editContacto')->with('vendedores', $vendedores )->with('terminos', $terminos)->with("contacto", $contacto)->with("personas", $personas)->with('sidebarActive', 'contactosSidebar');	
    }
    public function editContactoPost(){
    	$respuesta = Contacto::actualizarContacto(Input::all(), $_SESSION['id_usuario']);

    	if ($respuesta['error']){
            return redirect('editContacto/'.$respuesta['id_contacto'])->with('respuesta', 'Error')->withErrors($respuesta['mensaje']);
        }else{
            Accion::crearAccion('3', $_SESSION['id_usuario'], $respuesta, 11);
            return redirect('editContacto/'.$respuesta['id_contacto'])->with('mensaje', $respuesta['mensaje'] );
        }
    }

    public function eliminarContacto($id_contacto){
        $respuesta = Contacto::eliminarContacto($id_contacto, $_SESSION['id_usuario'], $_SESSION['id_empresa']);
        Accion::crearAccion('4', $_SESSION['id_usuario'], $respuesta, 11);
        return redirect('contactos')->with('mensaje', $respuesta['mensaje']);
    }

}
session_abort();