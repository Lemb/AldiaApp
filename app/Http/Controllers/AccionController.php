<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
//manualmente
use App\Accion;
use App\Usuario;
use Illuminate\Support\Facades\Input;
use Illuminate\Http\RedirectResponse;


class AccionController extends Controller
{
    //
    public function __construct(){
        if(isset($_SESSION['id_usuario']))
            $this->middleware(\App\Http\Middleware\autenticar::class.':'.$_SESSION['id_usuario'].',17');
    }
    public function selectHistorialAcciones($id_usuario=null){
    	if($id_usuario == null)
    		$id_usuario = $_SESSION['id_usuario'];
    	
		$usuario = Usuario::usuario($id_usuario, 'ver', $_SESSION['id_empresa']);
		if($usuario->id_empresa!=$_SESSION['id_empresa'])
			return redirect('home');

    	$acciones = Accion::selectAcciones($id_usuario);
		$nombreUsuario = $usuario->nombre.' '.$usuario->apellido.' ('.$usuario->identificacion.')';
    	return view('configuracion/historialAcciones/historialAcciones' )->with('acciones', $acciones)->with('nombreUsuario', $nombreUsuario)->with('sidebarActive', 'configuracionSidebar');
    }

}
