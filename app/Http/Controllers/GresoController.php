<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

//manualmente

use App\Accion;
use App\Bodega;
use App\Empresa;
use App\Greso;
use App\Usuario;
use App\Numeracion;
use App\Contacto;
use App\Item;
use App\TerminoPago;
use App\Impuesto;
use App\Banco;
use App\Categoria;
use Illuminate\Support\Facades\Input;
use Barryvdh\DomPDF\Facade;
use Illuminate\Http\RedirectResponse;


session_start();
class GresoController extends Controller
{
    
	//FACTURASI
	public function selectFacturasI(){
    	$facturas = Greso::selectFacturas($_SESSION['id_empresa'], 1);
    	return view('gresos/ingresos/facturasI' )->with('facturas', $facturas)->with('sidebarActive', 'ingresosSidebar');
    }
    public function POS(){
        $facturas       = Greso::selectFacturas($_SESSION['id_empresa'], 1);
        $numeraciones   = Numeracion::selectNumeraciones($_SESSION['id_empresa']);
        $clientes       = Contacto::selectContactosFacturas($_SESSION['id_empresa'], 'cliente');
        $terminosPago   = TerminoPago::selectTerminos($_SESSION['id_empresa']);
        $vendedores     = Usuario::selectUsuarios($_SESSION['id_empresa']);
        $items          = Item::selectItems($_SESSION['id_empresa']);
        $impuestos      = Impuesto::selectImpuestos($_SESSION['id_empresa']);
        
        return view('gresos/pos/POS' )->with('facturas', $facturas)->with('numeraciones', $numeraciones)->with('clientes', $clientes)->with('terminosPago', $terminosPago)->with('vendedores', $vendedores)->with('items', $items)->with('impuestos', $impuestos)->with('date', date("Y-m-d"));
    }
    //usado para registrar en la base de datos mediante ajax cuando se usa la opcion de pos
    public function POSAjax(){
        $input = array();

        $input['id_item']       = explode("," , $_GET['id_items']);
        $input['referencia']    = explode("," , $_GET['referencias']);
        $input['valor']         = explode("," , $_GET['valores']);
        $input['cantidad']      = explode("," , $_GET['cantidades']);
        $input['id_impuesto']   = explode("," , $_GET['impuestos']);
        $input['subtotalI']     = explode("," , $_GET['subtotales']);
        $input['totalI']        = explode("," , $_GET['totales']);
        $input['apunte']        = explode("," , $_GET['apuntes']);


        $input['id_empresa']    = $_SESSION['id_empresa'];
        $input['id_vendedor']   = $_GET['id_vendedor'];
        $input['id_contacto']   = $_GET['cliente'];
        $input['numeracion']    = $_GET['numeracion'];
        $input['numero']        = $_GET['numero'];
        $input['subtotal']      = array_sum($input['subtotalI']);
        $input['total']         = array_sum($input['totalI']);
        $input['fechaI']        = date("Y-m-d H:i:s");
        $input['fechaF']        = date("Y-m-d H:i:s");
        $input['agregarPago']   = 1;

        $respuesta = Greso::nuevaFactura($input, 'ingreso', $input['id_vendedor'],13);
        Accion::crearAccion('1', $input['id_vendedor'], $respuesta, 13);

        $respuestaFinal['mensaje']  = $respuesta['mensaje'].' con numero '.$respuesta['numero'];
        $respuestaFinal['numero']   = $respuesta['numero'];
        $respuestaFinal['numeroS']  = $respuesta['numeroS'];
        $respuestaFinal['id_greso'] = $respuesta['factura']->id_greso;
        echo json_encode($respuestaFinal);
    }


    public function nuevaFacturaI(){
     	$numeraciones 	= Numeracion::selectNumeraciones($_SESSION['id_empresa']);
     	$clientes 		= Contacto::selectContactosFacturas($_SESSION['id_empresa'], 'cliente');
     	$terminosPago 	= TerminoPago::selectTerminos($_SESSION['id_empresa']);
     	$vendedores 	= Usuario::selectUsuarios($_SESSION['id_empresa']);
     	$items 			= Item::selectItems($_SESSION['id_empresa'], true);
     	$impuestos		= Impuesto::selectImpuestos($_SESSION['id_empresa']);
        $bodegas        = Bodega::selectBodegas($_SESSION['id_empresa']);
        $allItemsXimpuestos = Greso::selectItemsXimpuestos($_SESSION['id_empresa']);


    	return view('gresos/ingresos/nuevaFacturaI')->with('numeraciones', $numeraciones)->with('clientes', $clientes)->with('terminosPago', $terminosPago)->with('vendedores', $vendedores)->with('items', $items)->with('impuestos', $impuestos)->with('date', date("Y-m-d H:i"))->with('sidebarActive', 'ingresosSidebar')->with('itemsXimpuestos',$allItemsXimpuestos)->with('bodegas', $bodegas);	

    }

    public function nuevaFacturaIPost(){
    	$respuesta = Greso::nuevaFactura(Input::all(), 'ingreso', $_SESSION['id_usuario'],13);
    	if ($respuesta['error']){
            return redirect('nuevaFacturaI/'.$respuesta['id_empresa'])->with('respuesta', 'Error')->withErrors($respuesta['mensaje'])->withInput();
        }else{
            Accion::crearAccion('1', $_SESSION['id_usuario'], $respuesta, 13);
            return redirect('facturasI')->with('mensaje', $respuesta['mensaje'] );
        } 
    }

    public function verFacturaI($id_greso){

        $numeraciones   = Numeracion::selectNumeraciones($_SESSION['id_empresa']);
        $clientes       = Contacto::selectContactosFacturas($_SESSION['id_empresa'], 'cliente');
        $vendedores     = Usuario::selectUsuarios($_SESSION['id_empresa']);
        $items          = Item::selectItems($_SESSION['id_empresa']);
        $impuestos      = Impuesto::selectImpuestos($_SESSION['id_empresa']);
        $bodegas        = Bodega::selectBodegas($_SESSION['id_empresa']);
        $gresosXimpuestos = Greso::selectGresosXimpuestos($id_greso, $_SESSION['id_empresa']);

        $facturaI       = Greso::factura($id_greso, $_SESSION['id_empresa'], 'ver');
        $itemsXgreso    = Greso::itemsXgreso($id_greso);
        $versiones      = Greso::versiones($id_greso, 13);


        return view('gresos/ingresos/verFacturaI')->with('numeraciones', $numeraciones)->with('clientes', $clientes)->with('vendedores', $vendedores)->with('items', $items)->with('impuestos', $impuestos)->with('factura', $facturaI)->with('itemsXgreso', $itemsXgreso)->with('versiones', $versiones)->with('sidebarActive', 'ingresosSidebar')->with('gresosXimpuestos',$gresosXimpuestos)->with('bodegas', $bodegas);    
    }

    public function editFacturaI($id_greso){
    
    	$numeraciones 	= Numeracion::selectNumeraciones($_SESSION['id_empresa']);
     	$clientes 		= Contacto::selectContactosFacturas($_SESSION['id_empresa'], 'cliente');
     	$terminosPago 	= TerminoPago::selectTerminos($_SESSION['id_empresa']);
     	$vendedores 	= Usuario::selectUsuarios($_SESSION['id_empresa']);
     	$items 			= Item::selectItems($_SESSION['id_empresa']);
     	$impuestos		= Impuesto::selectImpuestos($_SESSION['id_empresa']);
        $bodegas        = Bodega::selectBodegas($_SESSION['id_empresa']);
        $allItemsXimpuestos = Greso::selectItemsXimpuestos($_SESSION['id_empresa']);
        $gresosXimpuestos = Greso::selectGresosXimpuestos($id_greso, $_SESSION['id_empresa']);

     	$facturaI 		= Greso::factura($id_greso, $_SESSION['id_empresa'], 'editar');
     	$itemsXgreso	= Greso::itemsXgreso($id_greso);

    	return view('gresos/ingresos/editFacturaI')->with('numeraciones', $numeraciones)->with('clientes', $clientes)->with('terminosPago', $terminosPago)->with('vendedores', $vendedores)->with('items', $items)->with('impuestos', $impuestos)->with('factura', $facturaI)->with('itemsXgreso', $itemsXgreso)->with('sidebarActive', 'ingresosSidebar')->with('gresosXimpuestos',$gresosXimpuestos)->with('itemsXimpuestos',$allItemsXimpuestos)->with('bodegas', $bodegas);		
    }
    public function editFacturaIPost(){
    	$respuesta = Greso::actualizarFactura(Input::all(), 'ingreso', $_SESSION['id_usuario'], 13);
    	if ($respuesta['error']){
            return redirect('editFacturaI/'.$respuesta['id_greso'].'/'.$respuesta['id_empresa'])->with('respuesta', 'Error')->withErrors($respuesta['mensaje'])->withInput();
        }else{
            Accion::crearAccion('3', $_SESSION['id_usuario'], $respuesta, 13);
            return redirect('editFacturaI/'.$respuesta['id_greso'])->with('mensaje', $respuesta['mensaje'] );
        } 
    }

    public function eliminarFacturaI($id_greso){
        $respuesta = Greso::eliminarFactura($id_greso, 'ingreso', $_SESSION['id_usuario'], $_SESSION['id_empresa'], 13);
        Accion::crearAccion('4', $_SESSION['id_usuario'], $respuesta, 13);
        return redirect('facturasI')->with('mensaje', $respuesta['mensaje']);
    }


    public function PDFFacturaI($id_greso, $plantilla){
        
        $facturaI       = Greso::PDF($id_greso, $_SESSION['id_empresa']);
        $itemsXgreso    = Greso::itemsXgreso($id_greso);
        $empresa        = Empresa::form($_SESSION['id_empresa']);

        $data['factura']    = $facturaI;
        $data['itemsX']     = $itemsXgreso;
        $data['empresa']    = $empresa;

        if($plantilla==1){
            $pdf = Facade::loadView('gresos/ingresos/pdf', $data);
        }
        else if($plantilla == 2){
            $paper_size = array(0,0,190,1000);
            $pdf = Facade::loadView('gresos/ingresos/pdf2', $data)->setPaper($paper_size);   
        }

        return $pdf->stream($facturaI->numero.'.pdf');
    }//continuar con pdf









    //PAGOS I

    public function selectPagosI(){
        $pagos = Greso::selectPagos($_SESSION['id_empresa'], 3);
        return view('gresos/ingresos/pagosI' )->with('pagos', $pagos)->with('sidebarActive', 'ingresosSidebar');;
    }

    public function nuevoPagoI(){
        
        $numeracion     = Numeracion::selectReciboCaja($_SESSION['id_empresa'], 'ingreso');
        $clientes       = Contacto::selectContactosFacturas($_SESSION['id_empresa'], 'cliente');        
        $bancos         = Banco::selectBancos($_SESSION['id_empresa']);
        $metodosPago    = Greso::selectMetodosPago();
        $categorias     = Categoria::selectCategorias($_SESSION['id_empresa']);
        $impuestos      = Impuesto::selectImpuestos($_SESSION['id_empresa']);
        $vendedores     = Usuario::selectUsuarios($_SESSION['id_empresa']);

        
        return view('gresos/ingresos/nuevoPagoI')->with('numeracion', $numeracion)->with('clientes', $clientes)->with('categorias', $categorias)->with('impuestos', $impuestos)->with('metodosPago', $metodosPago)->with('bancos', $bancos)->with('date', date("Y-m-d H:i"))->with('sidebarActive', 'ingresosSidebar')->with('vendedores', $vendedores);    
    }
    public function ajaxClienteI($id_contacto){
        echo json_encode(Greso::selectAjaxFacturas($id_contacto, 1));
    }

    public function nuevoPagoIPost(){
        $respuesta = Greso::nuevoPago(Input::all(), 'ingreso', $_SESSION['id_usuario'], 14);
        if ($respuesta['error']){
            return redirect('nuevoPagoI')->with('respuesta', 'Error')->withErrors($respuesta['mensaje'])->withInput();
        }else{
            Accion::crearAccion('1', $_SESSION['id_usuario'], $respuesta, 14);
            return redirect('pagosI')->with('mensaje', $respuesta['mensaje'] );
        } 
    }

    public function verPagoI($id_greso){
        $clientes       = Contacto::selectContactosFacturas($_SESSION['id_empresa'], 'cliente');        
        $bancos         = Banco::selectBancos($_SESSION['id_empresa']);
        $metodosPago    = Greso::selectMetodosPago();
        $categorias     = Categoria::selectCategorias($_SESSION['id_empresa']);
        $impuestos      = Impuesto::selectImpuestos($_SESSION['id_empresa']);
        $vendedores     = Usuario::selectUsuarios($_SESSION['id_empresa']);

        $pagoI             = Greso::pago($id_greso, $_SESSION['id_empresa'], 'ver');
        $gresoXgresosRef   = Greso::gresoXgresosRef($id_greso);
        $gresosXcategorias = '';
        $versiones      = Greso::versiones($id_greso, 14);
        
        if (count($gresoXgresosRef)<=0)//encaso de que el pago no este relacionado a una factura
            $gresosXcategorias    = Greso::categoriasXgreso($id_greso);

        return view('gresos/ingresos/verPagoI')->with('clientes', $clientes)->with('metodosPago', $metodosPago)->with('bancos', $bancos)->with('pago', $pagoI)->with('gresoXgresosRef', $gresoXgresosRef)->with('gresosXcategorias', $gresosXcategorias)->with('impuestos', $impuestos)->with('categorias', $categorias)->with('versiones', $versiones)->with('sidebarActive', 'ingresosSidebar')->with('vendedores', $vendedores);    
    }

    public function editPagoI($id_greso){
        $clientes       = Contacto::selectContactosFacturas($_SESSION['id_empresa'], 'cliente');        
        $bancos         = Banco::selectBancos($_SESSION['id_empresa']);
        $metodosPago    = Greso::selectMetodosPago();
        $categorias     = Categoria::selectCategorias($_SESSION['id_empresa']);
        $impuestos      = Impuesto::selectImpuestos($_SESSION['id_empresa']);
        $vendedores     = Usuario::selectUsuarios($_SESSION['id_empresa']);

        $pagoI             = Greso::pago($id_greso, $_SESSION['id_empresa'], 'editar');
        $gresoXgresosRef   = Greso::gresoXgresosRef($id_greso);
        $gresosXcategorias = '';
        
        if (count($gresoXgresosRef)<=0) //encaso de que el pago no este relacionado a una factura
            $gresosXcategorias    = Greso::categoriasXgreso($id_greso);
      
        return view('gresos/ingresos/editPagoI')->with('clientes', $clientes)->with('metodosPago', $metodosPago)->with('bancos', $bancos)->with('pago', $pagoI)->with('gresoXgresosRef', $gresoXgresosRef)->with('gresosXcategorias', $gresosXcategorias)->with('impuestos', $impuestos)->with('categorias', $categorias)->with('sidebarActive', 'ingresosSidebar')->with('vendedores', $vendedores);    
    }

    public function editPagoIPost(){
        $respuesta = Greso::editPago(Input::all(), 'ingreso', $_SESSION['id_usuario'], 14);
        if ($respuesta['error']){
            return redirect('editPagoI/'.$respuesta['id_greso'])->with('respuesta', 'Error')->withErrors($respuesta['mensaje'])->withInput();
        }else{
            Accion::crearAccion('3', $_SESSION['id_usuario'], $respuesta, 14);
            return redirect('editPagoI/'.$respuesta['id_greso'])->with('mensaje', $respuesta['mensaje'] );
        } 
    }
    

    public function eliminarPagoI($id_greso){
        $respuesta = Greso::eliminarPago($id_greso, 'ingreso', $_SESSION['id_usuario'], $_SESSION['id_empresa'], 14);
        Accion::crearAccion('4', $_SESSION['id_usuario'], $respuesta, 14);
        return redirect('pagosI')->with('mensaje', $respuesta['mensaje']);
    }    
    public function PDFPagoI($id_greso){
        $pagoI                  = Greso::PDF($id_greso, $_SESSION['id_empresa']);
        $gresoXgresos           = Greso::gresoXgresosRef($id_greso);
        $gresosXcategorias      = Greso::categoriasXgreso($id_greso);
        $empresa                = Empresa::form($_SESSION['id_empresa']);
            
        $data['pago']       = $pagoI;
        $data['gresosX']    = $gresoXgresos;
        $data['categoriasX']= $gresosXcategorias;
        $data['empresa']    = $empresa;

        $pdf = Facade::loadView('gresos/ingresos/pdfPagoI', $data);
        
        return $pdf->stream($pagoI->numero.'.pdf');
    }//continuar con pdf








    //facturas E

    public function selectFacturasE(){
        $facturas = Greso::selectFacturas($_SESSION['id_empresa'], 7);
        return view('gresos/egresos/facturasE' )->with('facturas', $facturas)->with('sidebarActive', 'egresosSidebar');
    }

    public function nuevaFacturaE(){
        $proveedores    = Contacto::selectContactosFacturas($_SESSION['id_empresa'], 'proveedor');
        $terminosPago   = TerminoPago::selectTerminos($_SESSION['id_empresa']);
        $encargados     = Usuario::selectUsuarios($_SESSION['id_empresa']);
        $items          = Item::selectItems($_SESSION['id_empresa']);
        $impuestos      = Impuesto::selectImpuestos($_SESSION['id_empresa']);
        $allItemsXimpuestos = Greso::selectItemsXimpuestos($_SESSION['id_empresa']);

        return view('gresos/egresos/nuevaFacturaE')->with('proveedores', $proveedores)->with('terminosPago', $terminosPago)->with('encargados', $encargados)->with('items', $items)->with('impuestos', $impuestos)->with('date', date("Y-m-d H:i"))->with('sidebarActive', 'egresosSidebar')->with('itemsXimpuestos',$allItemsXimpuestos);    
    }

    public function nuevaFacturaEPost(){
        $respuesta = Greso::nuevaFactura(Input::all(), 'egreso', $_SESSION['id_usuario'], 15);

        if ($respuesta['error']){
            return redirect('nuevaFacturaE')->with('respuesta', 'Error')->withErrors($respuesta['mensaje'])->withInput();
        }else{
            Accion::crearAccion('1', $_SESSION['id_usuario'], $respuesta, 13);
            return redirect('facturasE')->with('mensaje', $respuesta['mensaje'] );
        } 
    }
    public function verFacturaE($id_greso){

        $proveedores    = Contacto::selectContactosFacturas($_SESSION['id_empresa'], 'proveedor');
        $encargados     = Usuario::selectUsuarios($_SESSION['id_empresa']);
        $items          = Item::selectItems($_SESSION['id_empresa']);
        $impuestos      = Impuesto::selectImpuestos($_SESSION['id_empresa']);

        $facturaE       = Greso::factura($id_greso, $_SESSION['id_empresa'], 'ver');
        $itemsXgreso    = Greso::itemsXgreso($id_greso);
        $versiones      = Greso::versiones($id_greso,15);

        return view('gresos/egresos/verFacturaE')->with('proveedores', $proveedores)->with('encargados', $encargados)->with('items', $items)->with('impuestos', $impuestos)->with('factura', $facturaE)->with('itemsXgreso', $itemsXgreso)->with('versiones', $versiones)->with('sidebarActive', 'egresosSidebar');        
    }

    public function editFacturaE($id_greso){
        
        $proveedores    = Contacto::selectContactosFacturas($_SESSION['id_empresa'], 'proveedor');
        $terminosPago   = TerminoPago::selectTerminos($_SESSION['id_empresa']);
        $encargados     = Usuario::selectUsuarios($_SESSION['id_empresa']);
        $items          = Item::selectItems($_SESSION['id_empresa']);
        $impuestos      = Impuesto::selectImpuestos($_SESSION['id_empresa']);

        $facturaE       = Greso::factura($id_greso, $_SESSION['id_empresa'], 'editar');
        $itemsXgreso    = Greso::itemsXgreso($id_greso);

        return view('gresos/egresos/editFacturaE')->with('proveedores', $proveedores)->with('terminosPago', $terminosPago)->with('encargados', $encargados)->with('items', $items)->with('impuestos', $impuestos)->with('factura', $facturaE)->with('itemsXgreso', $itemsXgreso)->with('sidebarActive', 'egresosSidebar');      
    }
    public function editFacturaEPost(){
        $respuesta = Greso::actualizarFactura(Input::all(), 'egreso', $_SESSION['id_usuario'], 15);

        if ($respuesta['error']){
            return redirect('editFacturaE/'.$respuesta['id_greso'])->with('respuesta', 'Error')->withErrors($respuesta['mensaje'])->withInput();
        }else{
            Accion::crearAccion('3', $_SESSION['id_usuario'], $respuesta, 15);
            return redirect('editFacturaE/'.$respuesta['id_greso'])->with('mensaje', $respuesta['mensaje']);
        } 
    }

    public function eliminarFacturaE($id_greso){
        $respuesta = Greso::eliminarFactura($id_greso, 'egreso', $_SESSION['id_usuario'], $_SESSION['id_empresa'], 15);
        Accion::crearAccion('4', $_SESSION['id_usuario'], $respuesta, 15);
        return redirect('facturasE')->with('mensaje', $respuesta['mensaje']);
    }














    //pagosE

    public function selectPagosE(){
        $pagos = Greso::selectPagos($_SESSION['id_empresa'], 9);
        return view('gresos/egresos/pagosE' )->with('pagos', $pagos)->with('sidebarActive', 'egresosSidebar');
    }

    public function nuevoPagoE(){
        
        $numeracion     = Numeracion::selectReciboCaja($_SESSION['id_empresa'], 'egreso');
        $proveedores    = Contacto::selectContactosFacturas($_SESSION['id_empresa'], 'proveedor');        
        $bancos         = Banco::selectBancos($_SESSION['id_empresa']);
        $metodosPago    = Greso::selectMetodosPago();
        $categorias     = Categoria::selectCategorias($_SESSION['id_empresa']);
        $impuestos      = Impuesto::selectImpuestos($_SESSION['id_empresa']);
        $vendedores     = Usuario::selectUsuarios($_SESSION['id_empresa']);

        return view('gresos/egresos/nuevoPagoE')->with('numeracion', $numeracion)->with('proveedores', $proveedores)->with('metodosPago', $metodosPago)->with('bancos', $bancos)->with('date', date("Y-m-d H:i"))->with('categorias', $categorias)->with('impuestos',$impuestos)->with('sidebarActive', 'egresosSidebar')->with('vendedores', $vendedores);    
    }
    public function ajaxClienteE($id_contacto){
        echo json_encode(Greso::selectAjaxFacturas($id_contacto, 7));
    }

    public function nuevoPagoEPost(){
        $respuesta = Greso::nuevoPago(Input::all(), 'egreso', $_SESSION['id_usuario'], 16);

        if ($respuesta['error']){
            return redirect('nuevoPagoE/'.$respuesta['id_empresa'])->with('respuesta', 'Error')->withErrors($respuesta['mensaje'])->withInput();
        }else{
            Accion::crearAccion('1', $_SESSION['id_usuario'], $respuesta, 16);
            return redirect('pagosE')->with('mensaje', $respuesta['mensaje'] );
        } 
    }
    public function verPagoE($id_greso){
        $proveedores    = Contacto::selectContactosFacturas($_SESSION['id_empresa'], 'proveedor');        
        $bancos         = Banco::selectBancos($_SESSION['id_empresa']);
        $metodosPago    = Greso::selectMetodosPago();

        $categorias     = Categoria::selectCategorias($_SESSION['id_empresa']);
        $impuestos      = Impuesto::selectImpuestos($_SESSION['id_empresa']);

        $pagoE             = Greso::pago($id_greso, $_SESSION['id_empresa'], 'ver');
        $gresoXgresosRef   = Greso::gresoXgresosRef($id_greso);
        $gresosXcategorias = '';
        $versiones      = Greso::versiones($id_greso, 16);
        $vendedores     = Usuario::selectUsuarios($_SESSION['id_empresa']);
        
        if (count($gresoXgresosRef)<=0) {//encaso de que el pago no este relacionado a una factura
            $gresosXcategorias    = Greso::categoriasXgreso($id_greso);
        }

        return view('gresos/egresos/verPagoE')->with('proveedores', $proveedores)->with('metodosPago', $metodosPago)->with('bancos', $bancos)->with('pago', $pagoE)->with('gresoXgresosRef', $gresoXgresosRef)->with('gresosXcategorias', $gresosXcategorias)->with('impuestos', $impuestos)->with('categorias', $categorias)->with('versiones', $versiones)->with('sidebarActive', 'egresosSidebar')->with('vendedores', $vendedores); 
    }


    public function editPagoE($id_greso){
        $proveedores    = Contacto::selectContactosFacturas($_SESSION['id_empresa'], 'proveedor');        
        $bancos         = Banco::selectBancos($_SESSION['id_empresa']);
        $metodosPago    = Greso::selectMetodosPago();
        $categorias     = Categoria::selectCategorias($_SESSION['id_empresa']);
        $impuestos      = Impuesto::selectImpuestos($_SESSION['id_empresa']);
        

        $pagoE             = Greso::pago($id_greso, $_SESSION['id_empresa'], 'editar');
        $gresoXgresosRef   = Greso::gresoXgresosRef($id_greso);
        $gresosXcategorias = '';
        $vendedores     = Usuario::selectUsuarios($_SESSION['id_empresa']);
        
        if (count($gresoXgresosRef)<=0) {//encaso de que el pago no este relacionado a una factura
            $gresosXcategorias    = Greso::categoriasXgreso($id_greso);
        }
      

        return view('gresos/egresos/editPagoE')->with('proveedores', $proveedores)->with('metodosPago', $metodosPago)->with('bancos', $bancos)->with('pago', $pagoE)->with('gresoXgresosRef', $gresoXgresosRef)->with('gresosXcategorias', $gresosXcategorias)->with('impuestos', $impuestos)->with('categorias', $categorias)->with('sidebarActive', 'egresosSidebar')->with('vendedores', $vendedores);    
    }

    public function editPagoEPost(){
        $respuesta = Greso::editPago(Input::all(), 'egreso', $_SESSION['id_usuario'], 16);

        if ($respuesta['error']){
            return redirect('editPagoE/'.$respuesta['id_greso'])->with('respuesta', 'Error')->withErrors($respuesta['mensaje'])->withInput();
        }else{
            Accion::crearAccion('3', $_SESSION['id_usuario'], $respuesta, 16);
            return redirect('editPagoE/'.$respuesta['id_greso'])->with('mensaje', $respuesta['mensaje'] );
        } 
    }

    public function eliminarPagoE($id_greso){
        $respuesta = Greso::eliminarPago($id_greso, 'egreso', $_SESSION['id_usuario'], $_SESSION['id_empresa'], 16);
        Accion::crearAccion('4', $_SESSION['id_usuario'], $respuesta, 16);
        return redirect('pagosE')->with('mensaje', $respuesta['mensaje']);
    }

    public function PDFPagoE($id_greso){
        $pagoE                  = Greso::PDF($id_greso, $_SESSION['id_empresa']);
        $gresoXgresos           = Greso::gresoXgresosRef($id_greso);
        $gresosXcategorias      = Greso::categoriasXgreso($id_greso);
        $empresa                = Empresa::form($_SESSION['id_empresa']);

        $data['pago']       = $pagoE;
        $data['gresosX']    = $gresoXgresos;
        $data['categoriasX']= $gresosXcategorias;
        $data['empresa']    = $empresa;

        $pdf = Facade::loadView('gresos/egresos/pdfPagoE', $data);
        
        return $pdf->stream($pagoE->numero.'.pdf');
    }//continuar con pdf

}
session_abort();