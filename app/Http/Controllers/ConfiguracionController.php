<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

//manualmente
use App\Configuracion;
use App\Accion;
use Illuminate\Support\Facades\Input;
use Illuminate\Http\RedirectResponse;

session_start();
class ConfiguracionController extends Controller
{
    //metodo encargado de actualizar las opciones de facturacion en general en el menu configuraciones empresas
    public function facturacionGeneral(){
    	
    	$respuesta = Configuracion::facturacionGeneral(Input::all());
    
        if ($respuesta['error']){
            return redirect('empresa')->with('respuesta', 'Error')->withErrors($respuesta['mensaje'] ,'errorF');
        }else{
            Accion::crearAccion('3', $_SESSION['id_usuario'], $respuesta, 3, 'terminosFacturacion');
            return redirect('empresa')->with('mensajeF', $respuesta['mensaje'] );
        }

    }
}
session_abort();
