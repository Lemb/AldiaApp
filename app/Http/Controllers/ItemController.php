<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

//manualmente
use App\Accion;
use App\Item;
use App\Impuesto;
use App\Categoria;
use App\Bodega;
use Illuminate\Support\Facades\Input;
use Illuminate\Http\RedirectResponse;

session_start();
class ItemController extends Controller
{
    public function __construct(){
        if(isset($_SESSION['id_usuario'])){
            $this->middleware(\App\Http\Middleware\autenticar::class.':'.$_SESSION['id_usuario'].',12');
        }
    }
    public function selectItems(){
    	$items = Item::selectItems($_SESSION['id_empresa']);
    	return view('items/items' )->with('items', $items)->with('sidebarActive', 'itemsSidebar');
    }

     public function nuevoItem(){
     	$impuestos  = Impuesto::selectImpuestos($_SESSION['id_empresa']);
     	$categorias = Categoria::selectCategorias($_SESSION['id_empresa']);
     	$medidas	= Item::selectMedidas();
        $bodegas    = Bodega::selectBodegas($_SESSION['id_empresa']);
        
    	return view('items/nuevoItem')->with('categorias', $categorias )->with('impuestos', $impuestos)->with('medidas', $medidas)->with('sidebarActive', 'itemsSidebar')->with('bodegas', $bodegas);	
    }
    public function nuevoItemPost(){
    	$respuesta = Item::nuevoItem(Input::all(), Input::file("imagen"));

    	if ($respuesta['error']){
            return redirect('nuevoItem')->with('respuesta', 'Error')->withErrors($respuesta['mensaje'])->withInput();
        }else{
            Accion::crearAccion('1', $_SESSION['id_usuario'], $respuesta, 12);
            return redirect('items')->with('mensaje', $respuesta['mensaje'] );
        } 
    }
    public function verItem($id_item){
        $item            = Item::item($id_item, $_SESSION['id_empresa'], 'ver');
        $impuestos       = Impuesto::selectImpuestos($_SESSION['id_empresa']);
        $categorias      = Categoria::selectCategorias($_SESSION['id_empresa']);
        $medidas         = Item::selectMedidas();
        $itemsXbodegas   = Item::selectItemsXbodegas($id_item, $_SESSION['id_empresa']);
        $itemsXimpuestos = Item::selectItemsXimpuestos($id_item, $_SESSION['id_empresa']);

        return view('items/verItem')->with('item', $item )->with('impuestos', $impuestos)->with('categorias', $categorias)->with('medidas', $medidas)->with('sidebarActive', 'itemsSidebar')->with('itemsXbodegas', $itemsXbodegas)->with('itemsXimpuestos', $itemsXimpuestos);      
    }

    public function editItem($id_item){
    	$item 		= Item::item($id_item, $_SESSION['id_empresa'], 'editar');
    	$impuestos  = Impuesto::selectImpuestos($_SESSION['id_empresa']);
     	$categorias = Categoria::selectCategorias($_SESSION['id_empresa']);
     	$medidas	= Item::selectMedidas();
        $itemsXbodegas = Item::selectItemsXbodegas($id_item, $_SESSION['id_empresa']);
        $itemsXimpuestos = Item::selectItemsXimpuestos($id_item, $_SESSION['id_empresa']);

    	return view('items/editItem')->with('item', $item )->with('impuestos', $impuestos)->with('categorias', $categorias)->with('medidas', $medidas)->with('sidebarActive', 'itemsSidebar')->with('itemsXbodegas', $itemsXbodegas)->with('itemsXimpuestos', $itemsXimpuestos);	
    }
    public function editItemPost(){
    	$respuesta = Item::actualizarItem(Input::all(), Input::file("imagen"));

    	if ($respuesta['error']){
            return redirect('editItem/'.$respuesta['id_item'])->with('respuesta', 'Error')->withErrors($respuesta['mensaje']);
        }else{
            Accion::crearAccion('3', $_SESSION['id_usuario'], $respuesta, 12);
            return redirect('editItem/'.$respuesta['id_item'])->with('mensaje', $respuesta['mensaje'] );
        }
    }

    public function eliminarItem($id_item){
        $respuesta = Item::eliminarItem($id_item, $_SESSION['id_empresa']);
        Accion::crearAccion('4', $_SESSION['id_usuario'], $respuesta, 12);
        return redirect('items')->with('mensaje', $respuesta['mensaje']);
    }
    public function codigoBarras(){
        return view('modulos/codigoBarras/codigoBarras')->with('sidebarActive', 'modulosSidebar');
    }
}
session_abort();