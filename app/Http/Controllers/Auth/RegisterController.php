<?php

namespace App\Http\Controllers\Auth;

use App\User;
use Validator;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\RegistersUsers;

//agregados manualmente
use App\Usuario;
use App\Empresa;
use App\Configuracion;
use App\VersionModulo;
/**
 * Class RegisterController
 * @package %%NAMESPACE%%\Http\Controllers\Auth
 */
class RegisterController extends Controller
{//proximo mañana primero de septiembre de 2017 realizar el registro en register controller
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Show the application registration form.
     *
     * @return \Illuminate\Http\Response
     */
    public function showRegistrationForm()
    {
        return view('adminlte::auth.register');
    }

    /**
     * Where to redirect users after login / registration.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name'     => 'required|max:255',
            'username' => 'sometimes|required|max:255',
            'email'    => 'required|email|max:255',
            'password' => 'required|min:6|confirmed',
            'terms'    => 'required',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return User
     */
    protected function create(array $data)
    {   $configuracion = [
            'fac_terminos' => 'Aldia Terminos y condiciones',
            'fac_notas' => 'Aldia notas',
            'fac_estadoCuentaCliente' => false,
            'SigNum_reciboCaja' => 1,
            'SigNum_comprobantePago' => 1,
            'SigNum_notaCredito' => 1,
            'SigNum_remision' => 1,
            'SigNum_cotizacion' => 1,
            'SigNum_ordenCompra' => 1,
            'multimoneda' => false,
            'codigoBarras' => false,
            'separadorDecimal' => ',',
            'precisionDecimal' => '2',
            'id_planFac_opcion1' => '1',
            'id_planFac_opcion2' => '1',
            'planFac_totalLetras'=> true,
            'planFac_retenciones_sugeridas' => true, 
            'planFac_saltosLineas_desc' => true,
            'id_planFac_fuente' => 1,
            'planFac_fuenteTamanio' => 11,
            'planFac_espaciado' => 2,
            'planFac_totalItems' => true,
            'planFac_firma' => 'aldiaExample.png',
            'id_planCotiz' => 1,
            'id_planRemi' => 1,
            'id_planTrans' => 1,
            'id_planOrdenCompra' => 1,
            'cor_factura' => 'aldia Correo Factura',
            'cor_cotizacion' => 'aldia Correo cotizacion',
            'cor_notaCredito' => 'aldia Correo nota de credito',
            'cor_remision' => 'aldia Correo remision',
            'cor_caja' => 'aldia Correo caja',
            'cor_egreso' => 'aldia Correo egreso',
            'cor_compra' => 'aldia Correo compra',
            'cor_estadoCuenta' => 'aldia Correo estado de cuenta',
            'pref_reporte' => true,
            'notFac_antesActiva'=> false,
            'notFac_antesDias'=> 5,
            'notFac_antesCorreo'=> 'notificacion de factura correo',
            'notFac_diaActiva'=> false,
            'notFac_diaCorreo'=> 'notificacion de factura correo',
            'notFac_despuesActiva'=> false,
            'notFac_despuesDias'=> 5,
            'notFac_despuesCorreo'=> 'notificacion de factura correo',

        ];
        $configuracion = Configuracion::create($configuracion);
        if($configuracion){
            $empresa =[
                'nombre' => $data['nombreEmp'],                
                'email' => $data['email'],
                'id_moneda' => 1,
                'id_documentoFacturacion' => 1,
                'id_regimen' => 1,
                'logo' => 'aldiaExample.png',
                'id_configuracion' => $configuracion['id_configuracion'],

            ];
            $empresa = Empresa::create($empresa);
            if ($empresa) {
                $usuario = array(
                    'nombre'   => $data['name'],
                    'email'    => $data['email'],
                    'activo'   => true,
                    'login'   => 2,
                    'id_perfil'   => 1,
                    'id_empresa'   => $empresa['id_empresa'],
                    'versionActual' => 1
                );
                $usuario['password']= bcrypt($data['password']);
                $usuario = Usuario::create($usuario);
                VersionModulo::insertVersionesModulosManejo($usuario, $usuario->id_usuario, 4, 'id_usuario');//registrar version
                return $usuario;
            }

        }
        
    }
}

