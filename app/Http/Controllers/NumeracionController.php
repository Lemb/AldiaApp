<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

//manualmente
use App\Numeracion;
use App\Accion;
use Illuminate\Support\Facades\Input;
use Illuminate\Http\RedirectResponse;
session_start();

class NumeracionController extends Controller
{   
    public function __construct(){
        if(isset($_SESSION['id_usuario']))
            $this->middleware(\App\Http\Middleware\autenticar::class.':'.$_SESSION['id_usuario'].',6');
    }
    public function selectNumeraciones($mensaje=null){
    	$numeraciones = Numeracion::selectNumeraciones($_SESSION['id_empresa']);
    	$SigNum 	  = Numeracion::selectSigNum($_SESSION['id_empresa']);

    	return view('configuracion/numeraciones/numeraciones' )->with('numeraciones', $numeraciones)->with('mensaje', $mensaje)
    	->with('SigNum', $SigNum)->with('sidebarActive', 'configuracionSidebar');
    }
    

    public function nuevaNumeracion(){
    	return view('configuracion/numeraciones/nuevaNumeracion')->with('sidebarActive', 'configuracionSidebar');	
    }
    public function nuevaNumeracionPost(Request $request){
    	$respuesta = Numeracion::nuevaNumeracion(Input::all()); 
        if ($respuesta['error']){
            return redirect('numeraciones')->with('erroress', 'Error')->withErrors($respuesta['mensaje'], 'errorF');
        }else{
            Accion::crearAccion('1', $_SESSION['id_usuario'], $respuesta, 6);
            return redirect('numeraciones/'.$respuesta['mensaje']);
        }
    		
    }

    public function verNumeracion($id_numeracion){
        $numeracion  = Numeracion::numeracion($id_numeracion, $_SESSION['id_empresa']);
        return view('configuracion/numeraciones/verNumeracion')->with('numeracion', $numeracion)->with('sidebarActive', 'configuracionSidebar');
    }

    public function editNumeracion($id_numeracion){
    	$numeracion  = Numeracion::numeracion($id_numeracion, $_SESSION['id_empresa']);
    	return view('configuracion/numeraciones/editNumeracion')->with('numeracion', $numeracion)->with('sidebarActive', 'configuracionSidebar');	
    }
    public function editNumeracionPost(Request $request){
    	$respuesta = Numeracion::actualizarNumeracion(Input::all());

    	if ($respuesta['error']){
            return redirect('numeraciones')->with('erroress', 'Error')->withErrors($respuesta['mensaje'], 'errorF');
        }else{
            Accion::crearAccion('3', $_SESSION['id_usuario'], $respuesta, 6);
            return redirect('numeraciones/'.$respuesta['mensaje']);
        }
    }
    public function editNumeracionDoc(){
		$respuesta = Numeracion::actualizarNumeracionesDoc(Input::all());
		if ($respuesta['error']){
	        return redirect('numeraciones')->with('respuesta', 'Error')->withErrors($respuesta['mensaje']);
	    }else{
            
            Accion::crearAccion('3', $_SESSION['id_usuario'], $respuesta, 6, 'numeracionEdit');
	        return redirect('numeraciones')->with('mensajeD', $respuesta['mensaje'] );
	    }
    }

    public function eliminarNumeracion($id_numeracion){
        $respuesta = Numeracion::eliminarNumeracion($id_numeracion, $_SESSION['id_empresa']);
        
        Accion::crearAccion('4', $_SESSION['id_usuario'], $respuesta, 6);
        return redirect('numeraciones/'.$respuesta['respuesta']); 
    }
}

session_abort();