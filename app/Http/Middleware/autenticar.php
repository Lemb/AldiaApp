<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\DB;

class autenticar
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next, $id_usuario, $id_permiso, $tipo=null)    
    {
        if($id_usuario==0)//si es indefinido
            return $next($request);

        $checkPermiso = DB::table('usuarios')
        ->join('perfilesXpermisos', 'perfilesXpermisos.id_perfil', '=', 'usuarios.id_perfil')
        ->where('id_usuario', $id_usuario)        
        ->where('id_permiso', $id_permiso)        
        ->first();  

        
        if($tipo==null){        
            if(isset($checkPermiso) && ($checkPermiso->insertar==1 || $checkPermiso->ver==1 || $checkPermiso->modificar==1 || $checkPermiso->eliminar==1)) 
                return $next($request);     
        }
        else{
            if(isset($checkPermiso)){
                if($tipo==1){
                    if ($checkPermiso->insertar==1) 
                        return $next($request);         
                }
                else if($tipo==2){
                    if ($checkPermiso->ver==1) 
                        return $next($request);            
                }
                else if($tipo==3){
                    if ($checkPermiso->modificar==1) 
                        return $next($request);            
                }
                else if($tipo==4){
                    if ($checkPermiso->eliminar==1) 
                        return $next($request);            
                }
            }
        }
        return redirect('/home');
    }
}
