<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

//manualmente
use Illuminate\Support\Facades\DB;
use Validator;
class Empresa extends Model
{
  
  //metodo encargado de actualizar los datos de la empresa
  public static function actualizar($input){

    $respuesta = array();

    $reglas =  array(
        'id_empresa' =>array('required'),
        'nombre'  => array('required', 'max:120'),
        'nit'  => array('numeric', 'nullable'),
        'telefono'  => array('numeric', 'nullable'),
        'email'  => array('max:90', 'email'),
        'id_regimen'  => array('required'),
        'id_documentoFacturacion'  => array('required'),
        'id_moneda' => array('required')
    );
    $validator = Validator::make($input, $reglas);
    
    //asignar id_empresa para redirigir de nuevo al formulario
    $respuesta['id_empresa'] = $input['id_empresa'];

    if ($validator->fails()){
        $respuesta['mensaje'] = $validator;
        $respuesta['error']   = true;
    }else{
        // llamar al metodo unset para deshacerse del token como parte del input
        unset($input['_token']);

        $empresa = Empresa::find($input['id_empresa']);
        $empresa->fill($input);
        $empresa->save();

        $respuesta['error']   = false;
        $respuesta['mensaje'] = "Datos Actualizados";
        $respuesta['empresa'] = $empresa;
    }     

    return $respuesta; 
  }

  //metodo encargado de cargar los datos para el formulario de configuraciones/empresa
  public static function form($id_empresa){
    return DB::table('empresas')
          ->join('configuraciones', 'empresas.id_configuracion', '=', 'configuraciones.id_configuracion')
          ->where('id_empresa', $id_empresa)
          ->select('empresas.*', 'configuraciones.fac_notas','configuraciones.fac_terminos')
          ->first();
  }

  protected $fillable = [
            'nombre',
            'nit',
          	'telefono',
         		'direccion',
            'email',
         		'id_moneda',
			      'id_documentoFacturacion',
        		'id_regimen',
            'logo',
  			    'id_configuracion',
            'ciudad'];
  protected $hidden = [];
  protected $table = 'empresas';
	protected $primaryKey = 'id_empresa';
}
