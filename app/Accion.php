<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

//usados manualmente
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\DB;
use Validator;

class Accion extends Model
{
    
	//hacer una seleccion de todos las asociadas a un usuario
  public static function selectAcciones($id_usuario){
   	return DB::table('acciones')
      ->where('id_usuario', $id_usuario)
      ->orderBy('id_accion', 'desc')
      ->get();
  }

  //crear una accion para el historial de acciones
  public static function crearAccion($tipoAccion, $id_usuario, $respuesta, $modulo, $excepcion=null){
    $referencia               = Accion::referencias($respuesta, $modulo, $excepcion);

    $input['descripcion']     = Accion::descripcion($tipoAccion, $referencia);
    $input['modulo']          = $modulo;
    $input['tipoAccion']      = $tipoAccion;
    $input['id_referencia']   = $referencia['id_referencia'];
    $input['id_usuario']      = $id_usuario;
    Accion::create($input);
  }

  public static function descripcion($tipoAccion, $referencia){
    switch ($tipoAccion) {// >>texto de identificacion ||texto a mostrar<< significa que es un link
      case '1':
        $descripcion = 'Registró >>'.$referencia['interna'].'||'.$referencia['visual'].'<<'; 
        break;
      case '2':
        $descripcion = 'Visualizó >>'.$referencia['interna'].'||'.$referencia['visual'].'<<'; 
        break;
      case '3':
        $descripcion = 'Actualizó >>'.$referencia['interna'].'||'.$referencia['visual'].'<<'; 
        break;
      case '4':
        $descripcion = 'Eliminó >>'.$referencia['interna'].'||'.$referencia['visual'].'<<'; 
        break;
      default:
        # code...
        break;
    }
    return $descripcion;
  }

  //metodo usado para determinar el valor de las referencias con respecto al modoulo a tratar
  public static function referencias($respuesta, $modulo, $excepcion){
    switch ($modulo) {
      case '3':
      
        $referencia['interna']        = 'empresa';
        if($excepcion=='terminosFacturacion'){
          $referencia['visual']         = 'Terminos de facturacion ';
          $referencia['id_referencia']  = $respuesta['id_empresa'];
        }
        else{
          $referencia['visual']         = 'Empresa '.$respuesta['empresa']->nombre;
          $referencia['id_referencia']  = $respuesta['empresa']->id_empresa;
        }
        
        break;
      case '4':
        $referencia['interna']        = 'verUsuario/'.$respuesta['usuario']->id_usuario1;
        $referencia['visual']         = 'Usuario '.$respuesta['usuario']->nombre.' '.$respuesta['usuario']->apellido.' ('.$respuesta['usuario']->identificacion.')';
        $referencia['id_referencia']  = $respuesta['usuario']->id_usuario; 
        break;
      case '5':
        $referencia['interna']        = 'verPerfil/'.$respuesta['perfil']->id_perfil;
        $referencia['visual']         = 'Perfil '.$respuesta['perfil']->nombre;//mensaje o como se vera finalmente en el historial
        $referencia['id_referencia']  = $respuesta['perfil']->id_perfil; 
        break;
      case '6':
        if($excepcion=='numeracionEdit'){
          $referencia['interna']  = 'numeraciones';
          $referencia['visual']   = 'Siguientes numeraciones';
        }
        else{
          $referencia['interna']  = 'verNumeracion/'.$respuesta['numeracion']->id_numeracion;
          $referencia['visual']   = 'Numeracion '.$respuesta['numeracion']->nombre.' ('.$respuesta['numeracion']->prefijo.')';
        }
        $referencia['id_referencia']  = '0'; 
        break;
      case '7':
        $cualidad                     = $respuesta['impuesto']->cualidad==1 ? 'Impuesto' : 'Retencion' ;
        $referencia['interna']        = 'verImpuesto/'.$respuesta['impuesto']->id_impuesto;
        $referencia['visual']         = $cualidad.' '.$respuesta['impuesto']->nombre.' ('.$respuesta['impuesto']->porcentaje.'%)';
        $referencia['id_referencia']  = $respuesta['impuesto']->id_impuesto; 
        break;
      case '8':
        $referencia['interna']        = 'verTerminoPago/'.$respuesta['terminoPago']->id_terminoPago;
        $referencia['visual']         = 'Termino de pago '.$respuesta['terminoPago']->nombre.' ('.$respuesta['terminoPago']->dias.')';
        $referencia['id_referencia']  = $respuesta['terminoPago']->id_terminoPago;
        break;
      case '9':
        $referencia['interna']        = 'verCategoria/'.$respuesta['categoria']->id_categoria;
        $referencia['visual']         = 'Categoria '.$respuesta['categoria']->nombre;
        $referencia['id_referencia']  = $respuesta['categoria']->id_categoria;
        break;
      case '10':
        $referencia['interna']        = 'verBanco/'.$respuesta['banco']->id_banco;
        $referencia['visual']         = 'Banco '.$respuesta['banco']->nombre.' ($'.$respuesta['banco']->saldo.')';
        $referencia['id_referencia']  = $respuesta['banco']->id_banco;
        break;
      case '11':
        $referencia['interna']        = 'verContacto/'.$respuesta['contacto']->id_contacto;
        $referencia['visual']         = 'Contacto '.$respuesta['contacto']->nombre.' ('.$respuesta['contacto']->nit.')';
        $referencia['id_referencia']  = $respuesta['contacto']->id_contacto;
        break;
      case '12':
        $referencia['interna']        = 'verItem/'.$respuesta['item']->id_item;
        $referencia['visual']         = 'Item '.$respuesta['item']->nombre.' ('.$respuesta['item']->referencia.') con precio de venta de '.$respuesta['item']->precio;
        $referencia['id_referencia']  = $respuesta['item']->id_item;
        break;
      case '13':
        $referencia['interna']        = 'verFacturaI/'.$respuesta['factura']->id_greso;
        $referencia['visual']         = 'Factura de venta '.$respuesta['factura']->numero.' ($'.$respuesta['factura']->total.')';
        $referencia['id_referencia']  = $respuesta['factura']->id_greso;
        break;
      case '14':
        $referencia['interna']        = 'verPagoI/'.$respuesta['pago']->id_greso;
        $referencia['visual']         = '('.$respuesta['pago']->numero.')Pago Recibido '.$respuesta['pago']->observaciones.' ($'.$respuesta['pago']->total.')';
        $referencia['id_referencia']  = $respuesta['pago']->id_greso;
        break;
      case '15':
        $referencia['interna']        = 'verFacturaE/'.$respuesta['factura']->id_greso;
        $referencia['visual']         = 'Factura de compra '.$respuesta['factura']->numero.' ($'.$respuesta['factura']->total.')';
        $referencia['id_referencia']  = $respuesta['factura']->id_greso;
        break;
      case '16':
        $referencia['interna']        = 'verPagoE/'.$respuesta['pago']->id_greso;
        $referencia['visual']         = '('.$respuesta['pago']->numero.')Pago Realizado '.$respuesta['pago']->observaciones.' ($'.$respuesta['pago']->total.')';
        $referencia['id_referencia']  = $respuesta['pago']->id_greso;
        break;
      case '21':
        $referencia['interna']        = 'verBodega/'.$respuesta['bodega']->id_bodega;
        $referencia['visual']         = 'Bodega '.$respuesta['bodega']->nombre;
        $referencia['id_referencia']  = $respuesta['bodega']->id_bodega;
        break;
      default:
        # code...
        break;
    }
    return $referencia;
  }

  

  protected $fillable = [
  	'descripcion',
  	'id_usuario',
    'tipoAccion',
    'id_referencia',
    'modulo',
  ];
  protected $hidden = [];
  protected $table = 'acciones';
	protected $primaryKey = 'id_accion';
}
