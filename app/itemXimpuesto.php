<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class itemXimpuesto extends Model
{
    //
    protected $fillable = [
    	'id_item',
    	'id_impuesto'
    ];
    protected $hidden = [];
    protected $table = 'itemsXimpuestos';
	protected $primaryKey = 'id_itemXimpuesto';
}
