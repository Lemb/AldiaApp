<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class gresoXgresoRef extends Model
{
    //}

    protected $fillable = [
    	'id_greso',
    	'id_gresoRef',
    	'retencion',
    	'valor'
    ];
    
    protected $hidden = [];
    protected $table = 'gresosXgresosRef';
	protected $primaryKey = 'id_greso';
}
