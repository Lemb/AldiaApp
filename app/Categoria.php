<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

//usados manualmente
use App\VersionModulo;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\SoftDeletes;
use Validator;
class Categoria extends Model
{
    use SoftDeletes;
    
    public static function nuevaCategoria($input, $sessionUsuario){
    
      $respuesta = array();
      $reglas =  array(
          'nombre'  => array('required'),
      );
      $validator = Validator::make($input, $reglas);

      //asignar id_empresa para redirigir de nuevo al formulario
      $respuesta['id_empresa'] = $input['id_empresa'];

      if ($validator->fails()){
          $respuesta['mensaje'] = $validator;
          $respuesta['error']   = true;
      }else{
        $input['versionActual'] = 1;//ultima version

        $categoria = Categoria::create($input);
        $respuesta['categoria'] = $categoria;
        VersionModulo::insertVersionesModulosManejo($categoria, $sessionUsuario, 9, 'id_categoria');//registrar version

        $respuesta['error']   = false;
        $respuesta['mensaje'] = "Categoria creada";
      }     
      return $respuesta; 
    }


    //funcion usada para actualizar una categoria
    public static function actualizarCategoria($input, $sessionUsuario){

      $respuesta = array();

      $reglas =  array(
          'nombre'  => array('required'),
      );
      $validator = Validator::make($input, $reglas);

      $respuesta['id_empresa'] = $input['id_empresa'];
      if ($validator->fails()){
          $respuesta['mensaje'] = $validator;
          $respuesta['error']   = true;
      }else{
          // llamar al metqodo unset para deshacerse del token como parte del input
          unset($input['_token']);
          if(!isset($input['activo']))
            $input['activo'] = false;

          $categoria = Categoria::find($input['id_categoria']);
          $respuesta['categoria'] = $categoria;
          VersionModulo::editVersionesModulosManejo($categoria, $sessionUsuario, 9, 'id_categoria');//registrar nueva version 

          $categoria->fill($input);
          $categoria->save();
          
          $respuesta['error']   = false;
          $respuesta['mensaje'] = "Categoria Actualizada";
      }     

      return $respuesta; 
    }

    public static function eliminarCategoria($id_categoria, $sessionUsuario, $id_empresa){
      $categoria = Categoria::where('id_empresa', $id_empresa)->where('id_categoria', $id_categoria)->first();
      $respuesta['categoria'] = $categoria;
      VersionModulo::editVersionesModulosManejo($categoria, $sessionUsuario, 9, 'id_categoria');//tecnicamente es una edicion
      $categoria->delete();

      $respuesta['categoria'] = $categoria;
      $respuesta['mensaje'] = 'Categoria Eliminada';

      return $respuesta;
    }






    //hacer una seleccion de todas las categorias asociadas a la empresa y a aldia
    public static function selectCategorias($id_empresa){
        return DB::table('categorias')
          ->whereIn('id_empresa', [1, $id_empresa])
          ->whereNull('deleted_at')
          ->where('categorias.versionActual', 1)
          ->orderBy('nombre')
          ->get();
    }

    public static function categoria($id_categoria,  $id_empresa){      
        return DB::table('categorias')
        ->join('categorias as ca2', 'ca2.id_categoria', '=', 'categorias.id_categoriaP')
        ->where('categorias.id_categoria', $id_categoria)
        ->where('categorias.id_empresa', $id_empresa)      
        ->select('categorias.*', 'ca2.nombre AS nombrePadre')
        ->first();
    }


    public static function versiones($id_categoria, $id_empresa){//usada para consultar las versiones del usuario
      return DB::table('categorias')
      ->join('categorias as ca2', 'ca2.id_categoria', '=', 'categorias.id_categoriaP')
      ->join('versionesModulos', 'versionesModulos.id_referencia', '=', 'categorias.id_categoria')
      ->join('usuarios as responsable', 'versionesModulos.id_usuario', '=', 'responsable.id_usuario')
      ->whereIn('versionesModulos.id_referenciaInicial', function ($query) use($id_categoria) {
                $query->select('id_referenciaInicial')
                      ->from('versionesModulos')
                      ->where('id_referencia', $id_categoria)
                      ->where('modulo', 9);
      })
      ->where('versionesModulos.modulo', 9)
      ->orderBy('numeroVersion','desc')
      ->select('categorias.*', 'ca2.nombre AS nombrePadre', 'versionesModulos.*', 'responsable.identificacion as responsableIdentificacion', 'responsable.nombre as responsableNombre', 'responsable.id_usuario as responsableID')
      ->get();

    }


    protected $fillable = [
    	'nombre',
    	'descripcion',
    	'id_categoriaP',
      'id_categoriaPP',
    	'id_empresa',
      'versionActual'
    ];
    protected $dates = ['deleted_at'];
    protected $hidden = [];
    protected $table = 'categorias';
	  protected $primaryKey = 'id_categoria';
}


