<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class gresoXitem extends Model
{
    //

    protected $fillable = [
    	'id_greso',
    	'id_item',
        'id_categoria',
    	'referencia',
    	'valor',
    	'cantidad',
        'id_impuesto',
        'descuento',
        'subtotal',
        'total',
        'apunte'
    ];
    
    protected $hidden = [];
    protected $table = 'gresosXitems';
	protected $primaryKey = 'id_gresosXitems';
}
