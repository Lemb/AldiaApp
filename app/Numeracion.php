<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

//usados manualmente
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\SoftDeletes;
use Validator;

class Numeracion extends Model
{
    use SoftDeletes;

    

    public static function nuevaNumeracion($input){

      $respuesta = array();

      $reglas =  array(
          'nombre'  => array('required'),
          'numeroI'  => array('numeric'),
          'numeroF'  => array('numeric'),

      );
      $messages = [
            'numeroI.numeric'    => 'El campo numero inicial debe ser un número',
            'numeroF.numeric'    => 'El campo numero final debe ser un número',
        ];
      $validator = Validator::make($input, $reglas, $messages);

      //asignar id_empresa para redirigir de nuevo al formulario
      $respuesta['id_empresa'] = $input['id_empresa'];

      if ($validator->fails()){
          $respuesta['mensaje'] = $validator;
          $respuesta['error']   = true;
      }else{
          // llamar al metqodo unset para deshacerse del token como parte del input
          unset($input['_token']);

          if(!isset($input['preterminada']))
            $input['preterminada'] = false;
          else if($input['preterminada']==1){//usada para tener solo una numeracion predeterminada
            DB::table('numeraciones')
            ->where('id_empresa', $input['id_empresa'])
            ->update(['preterminada' => false]);
          }

          $input['numeroA'] = $input['numeroI'];
          $numeracion = Numeracion::create($input);

          $respuesta['id_numeracion'] = $numeracion->id_numeracion; 
          $respuesta['error']   = false;
          $respuesta['mensaje'] = "Numeracion creada";
          $respuesta['numeracion'] = $numeracion;
      }     

      return $respuesta; 
    }

    //funcion usada para actualizar las numeraciones relacionadas a documentos diferentes a facturacion
    public static function actualizarNumeracionesDoc($input){
      $respuesta = array();

      $reglas =  array(
          'SigNum_reciboCaja' =>array('numeric'),
          'SigNum_comprobantePago' =>array('numeric'),
          'SigNum_notaCredito' =>array('numeric'),
          'SigNum_remision' =>array('numeric'),
          'SigNum_cotizacion' =>array('numeric'),
          'SigNum_ordenCompra' =>array('numeric'),
      );
      $messages = [
            'SigNum_reciboCaja.numeric'    => 'El campo Siguiente numero de recibo de caja no es un número',
            'SigNum_comprobantePago.numeric'    => 'El campo Siguiente numero de comprobante de pago no es un número',
            'SigNum_notaCredito.numeric'    => 'El campo Siguiente numero de nota de credito no es un número',
            'SigNum_remision.numeric'    => 'El campo Siguiente numero de remisiones no es un número',
            'SigNum_cotizacion.numeric'    => 'El campo Siguiente numero de cotizaciones no es un número',
            'SigNum_ordenCompra.numeric'    => 'El campo Siguiente numero de órdenes de compra no es un número',
      ];
      $validator = Validator::make($input, $reglas, $messages);

      $respuesta['id_empresa'] = $input['id_empresa'];
      if ($validator->fails()){
          $respuesta['mensaje'] = $validator;
          $respuesta['error']   = true;
      }else{
          
          //bloque para recoger el id_configuracion correspondiente a la empresa 
          //del usuario de la sesion actual
          $id_configuracion = DB::table('empresas')
                    				  ->where('id_empresa', $input['id_empresa'])
                    				  ->select('id_configuracion')
                    				  ->first();

          // llamar al metqodo unset para deshacerse del token como parte del input
          unset($input['id_empresa']);
          unset($input['_token']);

          DB::table('configuraciones')
          ->where('id_configuracion', $id_configuracion->id_configuracion)
          ->update($input);
          
          $respuesta['error']   = false;
          $respuesta['mensaje'] = "Numeraciones Actualizadas";

      }     

      return $respuesta; 
    }

    //funcion usada para actualizar una numeracion
    public static function actualizarNumeracion($input){
     
      $respuesta = array();

      $reglas =  array(
          'nombre'  => array('required'),
          'numeroI'  => array('numeric'),
          'numeroA'  => array('numeric'),
          'numeroF'  => array('numeric'),

      );
      $messages = [
            'numeroI.numeric'    => 'El campo numero inicial debe ser un número',
            'numeroA.numeric'    => 'El campo numero actual debe ser un número',
            'numeroF.numeric'    => 'El campo numero final debe ser un número',
        ];
      $validator = Validator::make($input, $reglas, $messages);

      $respuesta['id_empresa'] = $input['id_empresa'];
      if ($validator->fails()){
          $respuesta['mensaje'] = $validator;
          $respuesta['error']   = true;
      }else{
          // llamar al metqodo unset para deshacerse del token como parte del input
          unset($input['_token']);

          //determinar si no vienen valores de los checkbox para colocarlos como falsos
          if(!isset($input['activa']))
            $input['activa'] = false;
          if(!isset($input['preterminada']))
            $input['preterminada'] = false;
          else if($input['preterminada']==1){//usada para tener solo una numeracion predeterminada
            DB::table('numeraciones')
            ->where('id_empresa', $input['id_empresa'])
            ->update(['preterminada' => false]);
          }
          
          $numeracion = Numeracion::find($input['id_numeracion']);
          $numeracion->fill($input);
          $numeracion->save();
          
          $respuesta['error']   = false;
          $respuesta['mensaje'] = "Numeracion Actualizada";
          $respuesta['numeracion'] = $numeracion;
      }     

      return $respuesta; 
    }

    public static function eliminarNumeracion($id_numeracion, $id_empresa){
      $numeracion = Numeracion::find(1)->where('id_empresa', $id_empresa)->where('id_numeracion', $id_numeracion)->first();
      $numeracion->delete();

      $respuesta['respuesta'] = 'Numeracion Eliminada';
      $respuesta['numeracion'] = $numeracion;

      return $respuesta;
    }





    //obtener un numeracion especifica
    public static function numeracion($id_numeracion, $id_empresa){
    	return DB::table('numeraciones')
          ->where('id_numeracion', $id_numeracion)
          ->where('id_empresa', $id_empresa)
          ->first();
    }

    //hacer una seleccion de todas las numeraciones de factura
    public static function selectNumeraciones($id_empresa){
        return DB::table('numeraciones')
          ->where('id_empresa', $id_empresa)
          ->whereNull('deleted_at')
          ->orderBy('id_numeracion', 'desc')
          ->get();
    }
    //usada para traer todas las siguientes numeros de otros documentos
    public static function selectSigNum($id_empresa){
        return DB::table('empresas')
          ->join('configuraciones', 'configuraciones.id_configuracion', '=', 'empresas.id_configuracion')
          ->where('id_empresa', $id_empresa)
          ->whereNull('empresas.deleted_at')
          ->select('SigNum_reciboCaja', 'SigNum_comprobantePago', 'SigNum_notaCredito', 'SigNum_remision', 
            'SigNum_cotizacion', 'SigNum_ordenCompra')
          ->first();
    }
    //usada para traer todas las siguientes numeros de otros documentos
    public static function selectReciboCaja($id_empresa, $tipo){
      if($tipo == 'ingreso')
        $select ='SigNum_reciboCaja';
      else if($tipo=='egreso')
        $select ='SigNum_comprobantePago';
      
        return DB::table('empresas')
          ->join('configuraciones', 'configuraciones.id_configuracion', '=', 'empresas.id_configuracion')
          ->where('id_empresa', $id_empresa)
          ->whereNull('empresas.deleted_at')
          ->select($select)
          ->first();//first
    }
    
    protected $fillable = [
    	'nombre',
    	'prefijo',
    	'numeroI',
    	'numeroA',
    	'numeroF',
    	'resolucion',
    	'preterminada',
    	'activa',
    	'id_empresa'
    ];
    protected $dates = ['deleted_at'];
    protected $hidden = [];
    protected $table = 'numeraciones';
	  protected $primaryKey = 'id_numeracion';
}
