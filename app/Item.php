<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

//usados manualmente
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\SoftDeletes;
use Validator;
use App\itemXbodega;
use App\itemXimpuesto;
class Item extends Model
{
 	use SoftDeletes;

  public static function nuevoItem($input, $file){

		$respuesta = array();
		$reglas =  array(
			  'nombre'  => array('required', 'max:99'),
	    	'descripcion'=> array('max:179'),
	    	'costo'  => array('numeric', 'nullable'),
	    	'precio'  => array('numeric'),
	    	'cantidad'=>array('required_if:inventariable, 1')
	    	
		);
		$messages = [
  		'cantidad.required_if' => 'El campo cantidad inicial es obligatorio en caso de ser un producto inventariable'
	];
		$validator = Validator::make($input, $reglas, $messages);
 
		//asignar id_empresa para redirigir de nuevo al formulario
		$respuesta['id_empresa'] = $input['id_empresa'];

		if ($validator->fails()){
	    	$respuesta['mensaje'] = $validator;
	    	$respuesta['error']   = true;
		}else{
			//comprobar si se marco los checkboxes y luego mandar false en caso de no estar check
			if(!isset($input['inventariable'])){
        	$input['inventariable'] = false;
       		$input['id_medida'] = 1;
          $input['id_bodega'] = 0;
       		unset($input['cantidad']);
			}
			if(isset($file))
				$input['imagen'] = $file->getClientOriginalName();

    	$item = Item::create($input);
    	//guardar foto	
    	if(isset($file)){
    		//obtenemos el nombre del archivo
       	$nombre = $item->id_item.$file->getClientOriginalName();
   			//indicamos que queremos guardar un nuevo archivo en el disco local
   			\Storage::disk('local')->put(str_replace(" ","",$nombre),  \File::get($file));
    	}
      if(isset($input['id_impuesto'])){
        for($i=0; $i<count($input['id_impuesto']);$i++){
          $inputII['id_item']     = $item->id_item;
          $inputII['id_impuesto'] = $input['id_impuesto'][$i];
          itemXimpuesto::create($inputII);
        }
      }
      //si contiene items en cantidad entonces insertar en itemXbodega
    	if(isset($input['cantidad'])){
        for($i=0; $i<count($input['cantidad']);$i++) {
          $inputIB['cantidad']   = $input['cantidad'][$i];
          $inputIB['id_item']    = $item->id_item;
          $inputIB['id_bodega']  = $input['id_bodega'][$i];
          itemXbodega::create($inputIB);
        }
    	} 

    	$respuesta['error']   = false;
    	$respuesta['mensaje'] = "Item creado";
      $respuesta['item']    = $item;
	  }    
		return $respuesta; 
  }

   //funcion usada para actualizar un banco
  public static function actualizarItem($input, $file){
  
  	$respuesta = array();

  	$reglas =  array(
    		'nombre'  => array('required', 'max:99'),
	    	'descripcion'=> array('max:179'),
	    	'costo'  => array('numeric', 'nullable'),
	    	'precio'  => array('numeric',  'nullable')
    	);
    $validator = Validator::make($input, $reglas);

    //asignar id_empresa para redirigir de nuevo al formulario
		$respuesta['id_empresa'] = $input['id_empresa'];
		$respuesta['id_item'] = $input['id_item'];
  	if ($validator->fails()){
  		$respuesta['mensaje'] = $validator;
      $respuesta['error']   = true;
  	}else{

			$item = Item::find($input['id_item']);//encontrar el item correspondiente
			if(isset($file)){//en caso de llegar una imagen borrar la anterior y asignar un nombre al input
				\Storage::disk('local')->delete($item->id_item.$item->imagen);
				$input['imagen']=$file->getClientOriginalName();
			}
      $item->fill($input);
      $item->save();

      //guardar foto	
    	if(isset($file)){
    		//obtenemos el nombre del archivo
       	$nombre = $input['id_item'].$file->getClientOriginalName();
   			//indicamos que queremos guardar un nuevo archivo en el disco local
   			\Storage::disk('local')->put(str_replace(" ","",$nombre),  \File::get($file));
    	}

      //borrar todas las relaciones de los items con los impuestos para recrear los correspondientes
      DB::table('itemsXimpuestos')
        ->where('id_item', $item->id_item)
        ->delete();
      if(isset($input['id_impuesto'])){
        for($i=0; $i<count($input['id_impuesto']);$i++){
          $inputII['id_item']     = $item->id_item;
          $inputII['id_impuesto'] = $input['id_impuesto'][$i];
          itemXimpuesto::create($inputII);
        }
      }

      
      $respuesta['error']   = false;
      $respuesta['mensaje'] = "Item actualizado";
      $respuesta['item'] = $item;
  	}     

  	return $respuesta; 
  }
   //eliminar el item
  public static function eliminarItem($id_item, $id_empresa){
   	$item = Item::where('id_item', $id_item)->where('id_empresa', $id_empresa)->first();
  	$item->delete();

    $respuesta['item'] = $item;
    $respuesta['mensaje'] = 'Item eliminado';
    return $respuesta;
  }


  //hacer una seleccion de todos los items asociados a la empresa
  public static function selectItems($id_empresa, $disponibilidad=false){
    return DB::table('items')
      ->join('categorias', 'categorias.id_categoria', '=', 'items.id_categoria')
      ->join('medidas', 'medidas.id_medida', '=', 'items.id_medida')
      ->where('items.id_empresa', $id_empresa)
      ->when($disponibilidad, function ($query) {//usado para no traer los items que no tienen disponibilidad
          return $query->where(function ($query) {
                $query->whereRaw('(select sum(cantidad) from itemsXbodegas where id_item = items.id_item) > 0')->orWhere('items.id_medida', '=', '1');
            });
      })
      ->whereNull('items.deleted_at')
      ->select('items.*', 'categorias.nombre as categoria', 'items.id_item as itemID', DB::raw('(select sum(cantidad) from itemsXbodegas where id_item = itemID) AS cantidad'),  'medidas.nombre as medida')
      ->orderBy('items.nombre')
      ->get();
  }
  public static function selectItemsXbodegas($id_item, $id_empresa){
    return DB::table('itemsXbodegas')
    ->join('bodegas','bodegas.id_bodega', '=', 'itemsXbodegas.id_bodega')
    ->where('bodegas.id_empresa', $id_empresa)
    ->where('itemsXbodegas.id_item', $id_item)
    ->select('bodegas.nombre as bodegaNombre', 'itemsXbodegas.*')
    ->get();
  }
  public static function selectItemsXimpuestos($id_item, $id_empresa){
    return DB::table('itemsXimpuestos')
    ->join('impuestos','impuestos.id_impuesto', '=', 'itemsXimpuestos.id_impuesto')
    ->where('impuestos.id_empresa', $id_empresa)
    ->where('itemsXimpuestos.id_item', $id_item)
    ->select('impuestos.nombre as impuestoNombre', 'impuestos.porcentaje as impuestoPorcentaje', 'itemsXimpuestos.*')
    ->get();
  }
  public static function selectMedidas(){
    return DB::table('medidas')
      ->get();
  }
  //obtener un item especifico
  public static function item($id_item, $id_empresa, $accion){
    if($accion=='editar'){
    	return DB::table('items')
    	  ->join('categorias', 'categorias.id_categoria', '=', 'items.id_categoria')
        ->where('items.id_item', $id_item)
        ->where('items.id_empresa', $id_empresa)
        ->whereNull('items.deleted_at')
        ->select('items.*', 'categorias.nombre as nombreCategoria', 'categorias.id_categoria as idCategoria', DB::raw('(select sum(cantidad) from itemsXbodegas where id_item ='.$id_item.') as cantidad'))
        ->first();
    }
    else if($accion=='ver'){
      return DB::table('items')
        ->join('categorias', 'categorias.id_categoria', '=', 'items.id_categoria')
        ->where('items.id_item', $id_item)
        ->where('items.id_empresa', $id_empresa)
        ->select('items.*', 'categorias.nombre as nombreCategoria', 'categorias.id_categoria as idCategoria', DB::raw('(select sum(cantidad) from itemsXbodegas where id_item ='.$id_item.') as cantidad'))
        ->first(); 
    }
  }

  protected $fillable = [
  	'nombre',
  	'referencia',
  	'costo',
  	'precio',
  	'descripcion',
  	'codigoBarras',
  	'imagen',
  	'inventariable',
  	'id_medida',
  	'id_categoria',
  	'id_empresa'
  ];
  protected $dates = ['deleted_at'];
  protected $hidden = [];
  protected $table = 'items';
  protected $primaryKey = 'id_item';
}
