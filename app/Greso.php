<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

//usados manualmente
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\SoftDeletes;
use Validator;

use App\VersionModulo;
use App\gresoXitem;
use App\gresoXgresoRef;
use App\Banco;
use App\itemXbodega;
use App\gresoxitemsXImpuesto;

class Greso extends Model
{
    use SoftDeletes;

    public static function nuevaFactura($input, $tipo, $sessionUsuario, $modulo){
        
        $respuesta = array();
        $reglas =  array(
            'fechaI'  => array('date'),
            'fechaF' => array('date', 'after_or_equal:fechaI'),
            'id_contacto' => array('required'),
            'descuento[]' => array('number', 'min:0'),
            'cantidad[]' => array('number', 'min:0'),
            'total[]' => array('number', 'min:0')
        );
        $messages = [
            'fechaI.date'    => 'La fecha de inicio no corresponde con una fecha válida.',
            'fechaF.after_or_equal'    => 'La fecha de vencimiento debe ser posterior a la ,´9{mfecha de inicio.',
            'fechaF.date'    => 'La fecha de vencimiento no corresponde con una fecha válida.',
        ];
        $validator = Validator::make($input, $reglas, $messages);

        //asignar id_empresa para redirigir de nuevo al formulario
        $respuesta['id_empresa'] = $input['id_empresa'];

        if ($validator->fails()){
            $respuesta['mensaje'] = $validator;
            $respuesta['error']   = true;
        }else{
            if($tipo=='ingreso'){
                $input['id_operacion'] = 1;
                //actualizar numeracion para agregarle uno para la siguiente factura
                $numeracion = Numeracion::find($input['numeracion']);
                $respuesta['numeroS'] =$numeracion->prefijo.($numeracion->numeroA+1);//usado para mostrar en pos numero siguiente a esta factura
                $input['numero']   = $numeracion->prefijo.$numeracion->numeroA;
                $inputN['numeroA'] = $numeracion->numeroA+1;
                
                $numeracion->fill($inputN);
                $numeracion->save();
            }
            else if($tipo=='egreso'){
                $input['id_operacion'] = 7;                
            }
            $input['id_estado'] = 1;
            $input['versionActual'] = 1;

            $facturaI               = Greso::create($input);
            $respuesta['factura']   = $facturaI;
            $input['id_greso']      = $facturaI;
            VersionModulo::insertVersionesModulosManejo($facturaI, $sessionUsuario, $modulo, 'id_greso');//registrar version

            Greso::crearItems($input, $tipo);

            if($input['agregarPago']==1){
                $banco = DB::table('bancos')
                ->where('id_empresa', $input['id_empresa'])
                ->where('predeterminado', 1)
                ->select('id_banco', 'tipo')
                ->first();//consultar banco predeterminado de la empresa
                
                $value = ($tipo=='ingreso') ? 'SigNum_reciboCaja' : 'SigNum_comprobantePago';
                $numero = DB::table('empresas')
                ->join('configuraciones', 'configuraciones.id_configuracion' , '=', 'empresas.id_configuracion')
                ->where('id_empresa', $input['id_empresa'])
                ->value($value);//consultarSigNumRecibiodecajaocomprobantedepago

                $inputP['numero']           = $numero;
                $inputP['id_contacto']      = $input['id_contacto'];
                $inputP['id_banco']         = $banco->id_banco;
                $inputP['id_metodoPago']    = $banco->tipo;
                $inputP['fechaI']           = $input['fechaI'];
                $inputP['total']            = $input['total'];
                $inputP['id_empresa']       = $input['id_empresa'];
                
                $inputP['id_gresoRef'][0]   = $input['id_greso']->id_greso;
                $inputP['numeroF'][0]       = $input['numero'];
                $inputP['valorF'][0]        = $input['total'];

                Greso::nuevoPago($inputP, $tipo, $sessionUsuario, 14);
            }


            $respuesta['error']   = false;
            $respuesta['mensaje'] = ($tipo=='ingreso') ? "Factura de venta creada" : "Factura de compra creada";
            $respuesta['numero']  = $input['numero'];
            
        }    
        return $respuesta; 
    }

    public static function nuevoPago($input, $tipo, $sessionUsuario, $modulo){
      
        $respuesta = array();
        $reglas =  array(
            'numero' => array('required'),
            'fechaI'  => array('date'),            
            'id_contacto' => array('required'),
            'valorF[]' => array('number', 'min:0'),
            
        );
        $messages = [
            'fechaI.date'    => 'La fecha inicio no corresponde con una fecha válida.',
            'id_contacto.required'    => 'El campo cliente es obligatorio.',
        ];
        $validator = Validator::make($input, $reglas, $messages);

        //asignar id_empresa para redirigir de nuevo al formulario
        $respuesta['id_empresa'] = $input['id_empresa'];

        if ($validator->fails()){
            $respuesta['mensaje'] = $validator;
            $respuesta['error']   = true;
        }else{
            
            if(isset($input['facturaCategoriaPregunta']) && $input['facturaCategoriaPregunta']=='No'){ 
                $input['numeraciones'][0] = "Concepto/s";               
            }

            else{                
                for($i=0;$i<count($input['numeroF']);$i++){
                    if(isset($input['valorF'][$i]) && $input['valorF'][$i] >0)
                        $input['numeraciones'][$i] = $input['numeroF'][$i];
                }
            }
            
            $input['versionActual'] = 1;
            $input['id_operacion'] = ($tipo=='ingreso') ? 3 : 9;
            $input['observaciones'] = implode(", ", $input['numeraciones']); 
            $pago = Greso::create($input);
            $respuesta['pago'] = $pago;
            $input['id_greso'] = $pago->id_greso;

            VersionModulo::insertVersionesModulosManejo($pago, $sessionUsuario, $modulo, 'id_greso');//registrar version

            //agregar el total de lo depositado en el banco correspondiente
            $banco = Banco::find($input['id_banco']);
            $inputBanco['saldo'] = ($tipo=='ingreso') ? $banco->saldo+$input['total'] : $banco->saldo-$input['total'];
            $banco->fill($inputBanco);
            $banco->save();
            if(isset($input['facturaCategoriaPregunta']) && $input['facturaCategoriaPregunta']=='No'){
                Greso::crearCategorias($input);        
            }
            else{
                Greso::crearGresosRef($input);    
            }

            
            //actualizar numeracion para agregarle uno para el siguiente comprobante
            $increments = ($tipo=='ingreso') ? 'SigNum_reciboCaja' : 'SigNum_comprobantePago';
            DB::table('empresas')
            ->join('configuraciones', 'configuraciones.id_configuracion' , '=', 'empresas.id_configuracion')
            ->where('id_empresa', $input['id_empresa'])
            ->increment($increments);
        
            $respuesta['error']   = false;
            $respuesta['mensaje'] = ($tipo=='ingreso') ? "Pago recibido" : "Pago realizado";
        }    
        return $respuesta; 
    }

    public static function actualizarFactura($input, $tipo, $sessionUsuario, $modulo){

        $respuesta = array();
        $reglas =  array(
            'fechaI'  => array('date'),
            'fechaF' => array('date', 'after_or_equal:fechaI'),
            'id_contacto' => array('required'),
            'descuento[]' => array('number', 'min:0'),
            'cantidad[]' => array('number', 'min:0'),
            'total[]' => array('number', 'min:0')
        );
        $messages = [
            'fechaI.date'    => 'La fecha inicio no corresponde con una fecha válida.',
            'fechaF.after_or_equal'    => 'La fecha de vencimiento debe ser posterior a la fecha de inicio.',
            'fechaF.date'    => 'La fecha de vencimiento no corresponde con una fecha válida.',
        ];
        $validator = Validator::make($input, $reglas, $messages);

        //asignar id_empresa para redirigir de nuevo al formulario
        $respuesta['id_empresa'] = $input['id_empresa'];
        $respuesta['id_greso'] = $input['id_greso'];

        if ($validator->fails()){
            $respuesta['mensaje'] = $validator;
            $respuesta['error']   = true;
        }else{
            $facturaI = Greso::find($input['id_greso']);
            $facturaIAntigua = VersionModulo::editVersionesModulosManejo($facturaI, $sessionUsuario, $modulo, 'id_greso');//registrar nueva version 
            $gresoXitems = DB::table('gresosXitems')->where('id_greso', $facturaI->id_greso)->get();
            //pasarle un objeto y una id para asignar esos objetos a la id en base de datos
            Greso::crearItemsObjeto($gresoXitems, $facturaIAntigua->id_greso, $tipo, $facturaI->id_bodega);

            $facturaI->fill($input);
            $facturaI->save();

            foreach ($gresoXitems as $gresoXitem) {
                gresoxitemsXImpuesto::where('id_gresosXitems', $gresoXitem->id_gresosXitems)->delete();
            }
            //finporcion de codigo para eliminar los impuestos relacionados a cada item del greso correspondiente
            
            //eliminar todos los items relacionado al id_greso
            gresoXitem::where('id_greso', $input['id_greso'])->delete();
            //asignar objeto de factura completo y en la funcion crear items rescatar valores
            $input['id_greso'] = $facturaI;

            Greso::crearItems($input, $tipo, $gresoXitems);
            Greso::cambiarEstadoFactura($facturaI->id_greso);

            $respuesta['error']   = false;
            $respuesta['mensaje'] = ($tipo=='ingreso') ? "Factura de venta actualizada" : "Factura de compra actualizada";
            $respuesta['factura'] = $facturaI;
        }    
        return $respuesta; 
    }


    public static function editPago($input, $tipo, $sessionUsuario, $modulo){
  
        $respuesta = array();
        $reglas =  array(
            'fechaI'  => array('date'),            
            'id_contacto' => array('required'),
            'valorF[]' => array('number', 'min:0'),
            
        );
        $messages = [
            'fechaI.date'    => 'La fecha inicio no corresponde con una fecha válida.',
            'id_contacto.required'    => 'El campo cliente es obligatorio.',
        ];
        $validator = Validator::make($input, $reglas, $messages);

        //asignar id_empresa para redirigir de nuevo al formulario
        $respuesta['id_empresa'] = $input['id_empresa'];
        $respuesta['id_greso'] = $input['id_greso'];

        if ($validator->fails()){
            $respuesta['mensaje'] = $validator;
            $respuesta['error']   = true;
        }else{
            if($input['facturaCategoriaPregunta']=='Si'){                
                for($i=0;$i<count($input['numeroF']);$i++){
                    if(isset($input['valorF'][$i]) && $input['valorF'][$i] >0)
                        $input['numeraciones'][$i] = $input['numeroF'][$i];
                }
            }
            else if($input['facturaCategoriaPregunta']=='No'){                
                $input['numeraciones'][0] = "Concepto/s";
            }

            $input['observaciones'] = implode(", ", $input['numeraciones']);
             
            $greso = Greso::find($input['id_greso']);

            //INICIO DE REGISTRO DE VERSIONAMIENTO
            $pagoAntiguo = VersionModulo::editVersionesModulosManejo($greso, $sessionUsuario, $modulo, 'id_greso');//registrar nueva version 
            $gresoXgresosRef   = Greso::gresoXgresosRef($greso->id_greso);
            //$gresosXcategorias = '';
            if (count($gresoXgresosRef)<=0) {//encaso de que el pago no este relacionado a una factura
                //$gresosXcategorias    = Greso::categoriasXgreso($id_greso);
                $gresosXcategorias = DB::table('gresosXitems')->where('id_greso', $greso->id_greso)->get();
                Greso::crearCategoriasObjeto($gresosXcategorias, $pagoAntiguo->id_greso);//pasarle un pbjeto
            }
            else{
               Greso::crearGresosRefObjeto($gresoXgresosRef, $pagoAntiguo->id_greso);
            }
            //FINAL DE REGISTRO DE VERSIONAMIENTO
            
            //buscar el anterior banco y restarle el total anterior
            $banco = Banco::find($greso->id_banco);
            $inputBanco['saldo'] = ($tipo=='ingreso') ? $banco->saldo-$input['totalAntiguo']: $banco->saldo+$input['totalAntiguo'];
            $banco->fill($inputBanco);
            $banco->save();            
            
            $input['id_operacion'] = ($tipo=='ingreso') ? 3: 9;
            $greso->fill($input);
            $greso->save();//guardar todo lo de gresos
            $respuesta['pago'] = $greso;

            //agregar el total de lo depositado en el banco correspondiente
            $banco = Banco::find($input['id_banco']);
            $inputBanco['saldo'] = ($tipo=='ingreso') ? $banco->saldo+$input['total'] : $banco->saldo-$input['total'];
            $banco->fill($inputBanco);
            $banco->save();

            if($input['facturaCategoriaPregunta']=='Si'){
                gresoXgresoRef::where('id_greso', $input['id_greso'])->delete(); //eliminar todos los items relacionado al id_greso
                Greso::crearGresosRef($input); //agregar pagos a las facturas de ventas relacionadas con el cliente
            }
            else if($input['facturaCategoriaPregunta']=='No'){
                gresoXitem::where('id_greso', $input['id_greso'])->delete();//eliminar todas las categorias relacionadas
                Greso::crearCategorias($input);//recrear las categorias
            }

        
            $respuesta['error']   = false;
            $respuesta['mensaje'] = "Pago actualizado";
        }    
        return $respuesta; 
    }

    //eliminar la factura de venta
    public static function eliminarFactura($id_greso, $tipo, $sessionUsuario, $id_empresa, $modulo){
        $facturaI = Greso::where('id_greso', $id_greso)->where('id_empresa', $id_empresa)->first();
        $facturaIAntigua = VersionModulo::editVersionesModulosManejo($facturaI, $sessionUsuario, $modulo, 'id_greso');//registrar nueva version 
        $gresoXitems = DB::table('gresosXitems')->where('id_greso', $facturaI->id_greso)->get();
        Greso::crearItemsObjeto($gresoXitems, $facturaIAntigua->id_greso, $tipo, $input['id_bodega'],'eliminarFactura');//pasarle un pbjeto

        $facturaI->delete();

        $respuesta['mensaje'] = ($tipo=='ingreso') ? 'Factura de venta eliminada': 'Factura de compra eliminada';
        $respuesta['factura'] = $facturaI;
        return $respuesta;
    }
   

    //eliminar pago recibido
    public static function eliminarPago($id_greso, $tipoOperacion, $sessionUsuario, $id_empresa, $modulo){
        $pago = Greso::where('id_greso', $id_greso)->where('id_empresa', $id_empresa)->first();
        $pagoAntiguo = VersionModulo::editVersionesModulosManejo($pago, $sessionUsuario, $modulo, 'id_greso');//registrar nueva version 

        $gresoXgresosRef = Greso::gresoXgresosRef($pago->id_greso);
        //$gresosXcategorias = '';
        
        if (count($gresoXgresosRef)<=0) {//encaso de que el pago no este relacionado a una factura
            //$gresosXcategorias    = Greso::categoriasXgreso($id_greso);
            $gresosXcategorias = DB::table('gresosXitems')->where('id_greso', $pago->id_greso)->get();
            Greso::crearCategoriasObjeto($gresosXcategorias, $pagoAntiguo->id_greso);//pasarle un pbjeto
        }
        else{
            Greso::crearGresosRefObjeto($gresoXgresosRef, $pagoAntiguo->id_greso);
        }

        //restarle el saldo al banco
        $banco = Banco::find($pago->id_banco);
        $inputBanco['saldo'] = ($tipoOperacion =='ingreso') ? $banco->saldo-$pago->total : $banco->saldo+$pago->total;
        $banco->fill($inputBanco);
        $banco->save();

        $pago->delete();

        if(count($gresoXgresosRef)>=0){
            foreach ($gresoXgresosRef as $gresoXgresoRef) {
                Greso::cambiarEstadoFactura($gresoXgresoRef->id_gresoRef);
            }
        }

        $respuesta['mensaje'] = 'Pago Anulado';
        $respuesta['pago'] = $pago;
        return $respuesta;
    }




    public static function crearGresosRef($input){
        for($i=0; $i <count($input['id_gresoRef']); $i++){
            if (isset($input['valorF'][$i]) && $input['valorF'][$i] >0) {
                $inputG = array();
                $inputG['id_greso']     = $input['id_greso'];
                $inputG['id_gresoRef']  = $input['id_gresoRef'][$i];
                $inputG['valor']        = $input['valorF'][$i];

                gresoXgresoRef::create($inputG);//crear cada uno de los aportes a facturas
                Greso::cambiarEstadoFactura($input['id_gresoRef'][$i]);
            }
        }
    }
    
    public static function crearGresosRefObjeto($gresoXgresosRef, $id_greso){
        foreach($gresoXgresosRef as $gresoXgresoRef){
            $inputG = array();
            $inputG['id_greso']     = $id_greso;
            $inputG['id_gresoRef']  = $gresoXgresoRef->id_gresoRef;
            $inputG['valor']        = $gresoXgresoRef->valor;

            gresoXgresoRef::create($inputG);//crear cada uno de los aportes a facturas
            Greso::cambiarEstadoFactura($gresoXgresoRef->id_gresoRef);
        }
    }
    //funcion para cambiar el estado de la factura que reciba
    public static function cambiarEstadoFactura($id_greso){
        $factura = Greso::find($id_greso);

        $porPagar = DB::table('gresosXgresosRef')
                    ->join('gresos', 'gresosXgresosRef.id_greso' , '=', 'gresos.id_greso')
                    ->where('id_gresoRef', '=', $id_greso)
                    ->where('gresos.versionActual','=', 1)
                    ->whereNull('gresos.deleted_at')
                    ->select(DB::raw('IFNULL(SUM(valor), 0) AS porPagar'))
                    ->first();

        $estado = array();
        if($factura->total-$porPagar->porPagar > 0)
            $estado['id_estado'] = 1; 
        else if($factura->total-$porPagar->porPagar == 0)
            $estado['id_estado'] = 2;
        else if($factura->total-$porPagar->porPagar<0)
            $estado['id_estado'] = 3; 

        $factura->fill($estado);
        $factura->save();

    }


    //crear los items asociados a un greso
    public static function crearItems($input, $tipo, $gresoXitemsAntiguos=null){
        $inputI = array();
        for ($i=0; $i < count($input['id_item']); $i++) { 
            if(isset($input['id_item'][$i])){//si no hay ningun item seleccionado no lo toma en cuenta
                $inputI['id_greso']         = $input['id_greso']->id_greso;
                $inputI['id_item']          = $input['id_item'][$i];
                $inputI['referencia']       = $input['referencia'][$i];
                $inputI['valor']            = $input['valor'][$i];
                $inputI['cantidad']         = $input['cantidad'][$i];
                $inputI['descuento']        = isset($input['descuento'][$i]) ? $input['descuento'][$i]: null;
                $inputI['subtotal']         = $input['subtotalI'][$i];
                $inputI['total']            = $input['totalI'][$i];
                $inputI['apunte']           = $input['apunte'][$i];

                $gresoXitem = gresoXitem::create($inputI);//obtener item recien insertado como parte de greso
                
                if(isset($input['id_impuesto'.$i]))//porcion para insertar impuestos correspondientes
                    Greso::asignarImpuestosGresoXItems($input['id_impuesto'.$i], $gresoXitem->id_gresosXitems);

                //aumentar o disminuir items de facturas 
                $registrado = false;
                if(isset($gresoXitemsAntiguos)){
                    foreach ($gresoXitemsAntiguos as $gresoXitemAntiguo) {
                        if($gresoXitemAntiguo->id_item == $input['id_item'][$i]){
                            $cantidadAntigua  = $gresoXitemAntiguo->cantidad;
                            Greso::aumentarDisminuirCantidadItemsBodega($input['id_item'][$i], $input['id_bodega'], $input['cantidad'][$i], $tipo, $cantidadAntigua);    
                            $registrado = true;
                            $gresoXitemAntiguo->considerado = true;
                        }
                    }
                }
                if($registrado !=true ){
                    Greso::aumentarDisminuirCantidadItemsBodega($input['id_item'][$i], $input['id_bodega'], $input['cantidad'][$i], $tipo);
                }
                //finaumentar o disminuir items de facturas 
            }
        }
        //aumentar o disminuir aquellos items que se borraron del nuevogreso y que no fueron considerados
        if(isset($gresoXitemsAntiguos)){
            foreach ($gresoXitemsAntiguos as $gresoXitemAntiguo) {
                if(!isset($gresoXitemAntiguo->considerado))
                    Greso::aumentarDisminuirCantidadItemsBodega($gresoXitemAntiguo->id_item, $input['id_bodega'], 0, $tipo, $gresoXitemAntiguo->cantidad);   
            }
        }
    }
    
    //crear los items asociados a un greso
    //funcion para crear los items a un greso antiguo
    public static function crearItemsObjeto($gresoXitems, $id_greso, $tipo, $id_bodega, $accion=null){
        $inputI = array();
        foreach($gresoXitems as $gresoXitem){
            $inputI['id_greso']         = $id_greso;
            $inputI['id_item']          = $gresoXitem->id_item;
            $inputI['referencia']       = $gresoXitem->referencia;
            $inputI['valor']            = $gresoXitem->valor;
            $inputI['cantidad']         = $gresoXitem->cantidad;
            $inputI['descuento']        = $gresoXitem->descuento;
            $inputI['subtotal']         = $gresoXitem->subtotal;
            $inputI['total']            = $gresoXitem->total;
            $inputI['apunte']           = $gresoXitem->apunte;

            $gresoXitemCreado = gresoXitem::create($inputI);

            //insertar impuestos
            $impuestos =  DB::table('gresosxitemsXImpuestos')
                         ->where('id_gresosXitems', $gresoXitem->id_gresosXitems)
                         ->select('id_impuesto')
                         ->get();//recuperar todos los impuestos relacionados a gresosXitems
            Greso::asignarImpuestosGresoXItems($impuestos, $gresoXitemCreado->id_gresosXitems);
            //fininsertar impuestos
            if($accion=='eliminarFactura')
                Greso::aumentarDisminuirCantidadItemsBodega($gresoXitem->id_item, $id_bodega,$gresoXitem->cantidad, $tipo, 0, $accion);
        }
    }
    public static function asignarImpuestosGresoXItems($impuestos, $id_gresosXitems){

        foreach($impuestos as $impuesto){//insertar todos los impuestos relacionados al gresoXitem 
            $impuesto = (isset($impuesto->id_impuesto)) ? $impuesto->id_impuesto : $impuesto;
            $gresosxitemsXImpuestos = ["id_impuesto" => $impuesto, 
                                "id_gresosXitems" => $id_gresosXitems];//extraer id del item recien registrado
            gresoxitemsXImpuesto::create($gresosxitemsXImpuestos);
        }
    }
    public static function aumentarDisminuirCantidadItemsBodega($id_item, $id_bodega, $cantidadItem, $tipoOperacion, $cantidadAntiguaItem=0, $accion=null){
        $item = itemXbodega::where('id_bodega', $id_bodega)->where('id_item', $id_item)->first();//encontrar el item correspondiente
        /*si el item existe en bodega aumentar la cantidad de items o disminuir la cantidad de items
         dependiendo si se esta registrando un ingreso u egreso*/
       
        if(isset($item)){
            if($accion=='eliminarFactura'){
                $inputItem['cantidad'] = ($tipoOperacion == 'ingreso') ? $item->cantidad+($cantidadItem) : $item->cantidad-($cantidadItem);    
            }
            else{
                $inputItem['cantidad'] = ($tipoOperacion == 'ingreso') 
                ? $item->cantidad-($cantidadItem-$cantidadAntiguaItem) : $item->cantidad+($cantidadItem-$cantidadAntiguaItem);
            }

            $item->fill($inputItem);
            $item->save();
        }
    }


    //crear las categorias asociadas a un greso
    public static function crearCategorias($input){
        $inputI = array();
        for ($i=0; $i < count($input['id_categoria']); $i++) { 
            if(isset($input['id_categoria'][$i])){//si no hay ninguna categoria seleccionado no lo toma en cuenta
                $inputI['id_greso']         = $input['id_greso'];
                $inputI['id_categoria']     = $input['id_categoria'][$i];
                $inputI['valor']            = $input['valor'][$i];
                $inputI['cantidad']         = $input['cantidad'][$i];
                $inputI['id_impuesto']      = $input['id_impuesto'][$i];
                $inputI['descuento']        = $input['descuento'][$i];
                $inputI['subtotal']         = $input['subtotalI'][$i];
                $inputI['total']            = $input['totalI'][$i];
                $inputI['apunte']           = $input['apunte'][$i];

                gresoXitem::create($inputI);
            }
        }
    }
     //crear las categorias pero en base a un objeto pasado en vez de un input
    public static function crearCategoriasObjeto($gresoXitems, $id_greso){
        $inputI = array();
        foreach ($gresoXitems as $gresoXitem) {
        
            $inputI['id_greso']         = $id_greso;
            $inputI['id_categoria']     = $gresoXitem->id_categoria;
            $inputI['valor']            = $gresoXitem->valor;
            $inputI['cantidad']         = $gresoXitem->cantidad;
            $inputI['id_impuesto']      = $gresoXitem->id_impuesto;
            $inputI['descuento']        = $gresoXitem->descuento;
            $inputI['subtotal']         = $gresoXitem->subtotal;
            $inputI['total']            = $gresoXitem->total;
            $inputI['apunte']           = $gresoXitem->apunte;

            gresoXitem::create($inputI);
        }
    }

    


    //hacer una seleccion de todas las facturas de ventas asociadas a la empresa
    public static function selectFacturas($id_empresa, $operacion){
        if(Greso::where('id_empresa', '=', $id_empresa)->exists()){
            return DB::table('gresos')
                ->join('estados', 'estados.id_estado', '=', 'gresos.id_estado')
                ->join('contactos', 'contactos.id_contacto', '=', 'gresos.id_contacto')
                ->where('gresos.id_empresa', $id_empresa)
                ->where('gresos.id_operacion', $operacion)
                ->where('gresos.versionActual', 1)
                ->whereNull('gresos.deleted_at')
                ->orderBy('gresos.id_greso', 'desc')
                ->select('gresos.*', 'estados.nombre as estado', 'contactos.nombre as contacto', 'gresos.id_greso AS greso',
                    DB::raw('IFNULL((SELECT SUM(valor) FROM gresosXgresosRef  INNER JOIN gresos on gresosXgresosRef.id_greso = gresos.id_greso WHERE id_gresoRef = greso AND gresos.versionActual = 1 AND gresos.deleted_at IS NULL), 0) AS pagado'),
                    DB::raw('gresos.total-IFNULL((SELECT SUM(valor) FROM gresosXgresosRef INNER JOIN gresos on gresosXgresosRef.id_greso = gresos.id_greso WHERE id_gresoRef = greso AND gresos.versionActual = 1 AND gresos.deleted_at IS NULL), 0) AS pagar'))
                ->get();

        }
        else
            return null;
    }
     //hacer una seleccion de todas las facturas de ventas asociadas a el contacto consultadas por ajax
    public static function selectAjaxFacturas($id_contacto, $operacion){
        return DB::table('gresos')
            ->where('gresos.id_contacto', $id_contacto)
            ->where('gresos.id_estado', 1)
            ->where('gresos.versionActual', 1)
            ->where('gresos.id_operacion', $operacion)
            ->whereNull('gresos.deleted_at')
            ->select('gresos.*', 'gresos.id_greso AS greso', 
                DB::raw('IFNULL((SELECT SUM(valor) FROM gresosXgresosRef INNER JOIN gresos on gresosXgresosRef.id_greso = gresos.id_greso WHERE id_gresoRef = greso AND gresos.versionActual = 1 AND gresos.deleted_at IS NULL), 0) AS pagado'),
                DB::raw('gresos.total-IFNULL((SELECT SUM(valor) FROM gresosXgresosRef INNER JOIN gresos on gresosXgresosRef.id_greso = gresos.id_greso WHERE id_gresoRef = greso AND gresos.versionActual = 1 AND gresos.deleted_at IS NULL), 0) AS pagar'))
            ->get();
    }

     //hacer una seleccion de todos los pagos recibidos
    public static function selectPagos($id_empresa, $tipo){
        return DB::table('gresos')
            ->leftJoin('estados', 'estados.id_estado', '=', 'gresos.id_estado')
            ->join('contactos', 'contactos.id_contacto', '=', 'gresos.id_contacto')
            ->join('bancos', 'bancos.id_banco', '=', 'gresos.id_banco')
            ->where('gresos.id_empresa', $id_empresa)
            ->where('gresos.versionActual', 1)
            ->where('gresos.id_operacion', $tipo)
            ->whereNull('gresos.deleted_at')
            ->orderBy('id_greso', 'desc')
            ->select('gresos.*', 'estados.nombre as estado', 'contactos.nombre as contacto', 'bancos.nombre as banco')
            ->get();
    }
     //hacer una seleccion de todos los metodos de pago
    public static function selectMetodosPago(){
        return DB::table('metodosPago')
            ->get();
    }
    public static function selectItemsXimpuestos($id_empresa){
        return DB::table('itemsXimpuestos')
            ->join('impuestos', 'impuestos.id_impuesto', '=', 'itemsXimpuestos.id_impuesto')
            ->where('impuestos.id_empresa', $id_empresa)
            ->select('itemsXimpuestos.*')
            ->get();
    }
    public static function selectGresosXimpuestos($id_greso, $id_empresa){
        return DB::table('gresosxitemsXImpuestos')
            ->join('impuestos', 'impuestos.id_impuesto', '=', 'gresosxitemsXImpuestos.id_impuesto')
            ->join('gresosXitems','gresosXitems.id_gresosXitems', '=', 'gresosxitemsXImpuestos.id_gresosXitems')
            ->where('impuestos.id_empresa', $id_empresa)
            ->where('gresosXitems.id_greso', $id_greso)
            ->select('gresosxitemsXImpuestos.*')
            ->get();
    }

    
    public static function versiones($id_greso, $modulo){//usada para consultar las versiones del greso
        return DB::table('gresos')
        ->join('contactos', 'contactos.id_contacto', '=', 'gresos.id_contacto')
        ->join('versionesModulos', 'versionesModulos.id_referencia', '=', 'gresos.id_greso')
        ->join('usuarios as responsable', 'versionesModulos.id_usuario', '=', 'responsable.id_usuario')
        ->whereIn('versionesModulos.id_referenciaInicial', function ($query) use($id_greso, $modulo) {
                  $query->select('id_referenciaInicial')
                        ->from('versionesModulos')
                        ->where('id_referencia', $id_greso)
                        ->where('modulo', $modulo);
        })
        ->where('versionesModulos.modulo', $modulo)
        ->orderBy('numeroVersion','desc')
        ->select('gresos.*', 'versionesModulos.*', 'responsable.identificacion as responsableIdentificacion', 'responsable.nombre as responsableNombre', 'responsable.id_usuario as responsableID', 'contactos.nombre as contacto')
        ->get();
    }

     //DATOS A PASAR A PDF
    public static function PDF($id_greso, $id_empresa){
        return DB::table('gresos')
          ->join('contactos', 'contactos.id_contacto', '=', 'gresos.id_contacto')
          ->join('usuarios', 'gresos.id_vendedor', '=', 'usuarios.id_usuario')
          ->where('gresos.id_greso', $id_greso)
          ->where('gresos.id_empresa', $id_empresa)
          ->whereNull('gresos.deleted_at')
          ->select('gresos.*', 'contactos.nombre AS clienteNombre', 'contactos.nit AS clienteNit', 
            'contactos.direccion AS clienteDireccion', 'contactos.ciudad AS clienteCiudad', 'contactos.telefono AS clienteTelefono',
            'usuarios.nombre as usuarioNombre', 'usuarios.apellido as usuarioApellido', 'usuarios.identificacion as usuarioIdentificacion'
                , 'gresos.id_greso AS greso',
                DB::raw('IFNULL((SELECT SUM(valor) FROM gresosXgresosRef  INNER JOIN gresos on gresosXgresosRef.id_greso = gresos.id_greso WHERE id_gresoRef = greso AND gresos.versionActual = 1 AND gresos.deleted_at IS NULL), 0) AS pagado'),
                DB::raw('gresos.total-IFNULL((SELECT SUM(valor) FROM gresosXgresosRef INNER JOIN gresos on gresosXgresosRef.id_greso = gresos.id_greso WHERE id_gresoRef = greso AND gresos.versionActual = 1 AND gresos.deleted_at IS NULL), 0) AS pagar'))
          ->first();
    }
     //obtener items de un greso
    public static function itemsXgreso($id_greso){
        return DB::table('gresosXitems')
          ->join('items', 'items.id_item', '=', 'gresosXitems.id_item')
          ->where('id_greso', $id_greso)
          ->select('gresosXitems.*', 'items.nombre AS itemNombre', 'items.imagen as imagen')
          ->get();
    }
    //obtener categorias de un greso
    public static function categoriasXgreso($id_greso){
        return DB::table('gresosXitems')
          ->join('categorias', 'categorias.id_categoria', '=', 'gresosXitems.id_categoria')
          ->where('id_greso', $id_greso)
          ->select('gresosXitems.*', 'categorias.nombre AS categoriaNombre')
          ->get();
    }

     //obtener una factura de venta especifica
    public static function factura($id_greso, $id_empresa, $accion){
        if($accion=='editar'){
            return DB::table('gresos')
              ->where('id_greso', $id_greso)
              ->where('id_empresa', $id_empresa)
              ->where('versionActual', 1)
              ->whereNull('deleted_at')
              ->first();
        }
        else if($accion == 'ver'){
            return DB::table('gresos')
              ->where('id_empresa', $id_empresa)
              ->where('id_greso', $id_greso)
              ->first();   
        }
    }
     //obtener un pago especifico
    public static function pago($id_greso, $id_empresa, $accion){
        if($accion=='editar'){
            return DB::table('gresos')
              ->where('id_greso', $id_greso)
              ->where('id_empresa', $id_empresa)
              ->where('versionActual', 1)
              ->whereNull('deleted_at')
              ->first();
        }
        else if($accion == 'ver'){
            return DB::table('gresos')
              ->where('id_greso', $id_greso)
              ->where('id_empresa', $id_empresa)
              ->first();
        }
    }
     //obtener facturas asociadas a un pago
    public static function gresoXgresosRef($id_greso){
        return DB::table('gresosXgresosRef')
          ->join('gresos', 'gresos.id_greso', '=', 'gresosXgresosRef.id_gresoRef')
          ->where('gresosXgresosRef.id_greso', $id_greso)
          ->select('gresosXgresosRef.*', 'gresos.numero as numero','gresos.total as total', 'gresosXgresosRef.id_gresoRef AS greso', 'gresosXgresosRef.id_greso AS gresoID', 
                DB::raw('IFNULL((SELECT SUM(valor) FROM gresosXgresosRef INNER JOIN gresos on gresosXgresosRef.id_greso = gresos.id_greso WHERE id_gresoRef = greso AND gresos.versionActual = 1 AND gresosXgresosRef.id_greso<=gresoID AND gresos.deleted_at IS NULL), 0)-gresosXgresosRef.valor AS pagado'),
                DB::raw('gresosXgresosRef.valor+gresos.total-IFNULL((SELECT SUM(valor) FROM gresosXgresosRef INNER JOIN gresos on gresosXgresosRef.id_greso = gresos.id_greso WHERE id_gresoRef = greso AND gresos.versionActual = 1 AND gresosXgresosRef.id_greso<=gresoID AND gresos.deleted_at IS NULL), 0) AS pagar'))
          ->get();
    }


    protected $fillable = [
    	'id_operacion',
    	'fechaI',
        'terminoPagoDias',
    	'fechaF',
    	'numero',
        'observaciones',
        'notas',
        'subtotal',
        'total',
        'adjunto',
        'frecuencia',
        'id_estado',
        'id_bodega',
        'id_vendedor',
        'id_metodoPago',
        'id_lista',
        'id_conciliacion',
        'id_banco',
        'id_contacto',
    	'id_empresa',
        'versionActual'
    ];
    protected $dates = ['deleted_at'];
    protected $hidden = [];
    protected $table = 'gresos';
	protected $primaryKey = 'id_greso';
}



