<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

//usados manualmente
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\SoftDeletes;
use Validator;

class Impuesto extends Model
{
	use SoftDeletes;

    //hacer una seleccion de todos los impuestos pertenecientes a la empresa
    public static function selectImpuestos($id_empresa){
      	return DB::table('impuestos')
          ->where('id_empresa', $id_empresa)
          ->where('cualidad', 1)
          ->whereNull('deleted_at')
          ->orderBy('id_impuesto', 'desc')
          ->get();
    }

    //hacer una seleccion de todos los retenciones pertenecientes a la empresa
    public static function selectRetenciones($id_empresa){
      	return DB::table('impuestos')
          ->where('id_empresa', $id_empresa)
          ->where('cualidad', 2)
          ->whereNull('deleted_at')
          ->orderBy('id_impuesto', 'desc')
          ->get();
    }

     public static function nuevoImpuesto($input){

      $respuesta = array();

      $reglas =  array(
          'cualidad'  => array('required'),
          'nombre'  => array('required'),
          'porcentaje'  => array('numeric'),
          
      );
      $validator = Validator::make($input, $reglas);


      //asignar id_empresa para redirigir de nuevo al formulario
      $respuesta['id_empresa'] = $input['id_empresa'];

      if ($validator->fails()){
          $respuesta['mensaje'] = $validator;
          $respuesta['error']   = true;
      }else{
      	  
          // llamar al metqodo unset para deshacerse del token como parte del input
          unset($input['_token']);
          
          $impuesto = Impuesto::create($input);

          $respuesta['impuesto'] = $impuesto;
          $respuesta['error']   = false;
          $respuesta['respuesta'] = $impuesto->cualidad==1 ? 'Impuesto Creado' : 'Retencion Creada';
      }     

      return $respuesta; 
    }

    //funcion usada para actualizar Impuestos
    public static function actualizarImpuesto($input){
     
      $respuesta = array();

      $reglas =  array(
          'cualidad'  => array('required'),
          'nombre'  => array('required'),
          'porcentaje'  => array('numeric'),
      );
      
      $validator = Validator::make($input, $reglas);

      $respuesta['id_empresa'] = $input['id_empresa'];
      $respuesta['cualidad'] = ($input['cualidad']=='1') ?"EImpuesto":"ERetencion";
      if ($validator->fails()){
      	  $respuesta['mensaje'] = $validator;
          $respuesta['error']   = true;
      }else{
          // llamar al metqodo unset para deshacerse del token como parte del input
          unset($input['_token']);

          //determinar si no vienen valores de los checkbox para colocarlos como falsos
          if(!isset($input['activa']))
            $input['activa'] = false;

          $impuesto = Impuesto::find($input['id_impuesto']);
          $impuesto->fill($input);
          $impuesto->save();

          $respuesta['impuesto'] = $impuesto;
          $respuesta['error']   = false;
          $respuesta['respuesta'] = $impuesto->cualidad==1 ? 'Impuesto actualizado' : 'Retencion actualizada';
      }     

      return $respuesta; 
    }
    public static function eliminarImpuesto($id_impuesto, $id_empresa){
  		$impuesto = Impuesto::find(1)->where('id_empresa', $id_empresa)->where('id_impuesto', $id_impuesto)->first();
  		$impuesto->delete();

      $respuesta['respuesta'] = $impuesto->cualidad==1 ? 'Impuesto Eliminado' : 'Retencion Eliminada';
      $respuesta['impuesto'] = $impuesto;
  		
  		return $respuesta;	
    }

    //obtener un impuesto especifico
    public static function impuesto($id_impuesto, $id_empresa){
    	return DB::table('impuestos')
          ->where('id_impuesto', $id_impuesto)
          ->where('id_empresa', $id_empresa)
          ->first();
    }

    protected $fillable = [
    	'cualidad',
    	'nombre',
    	'porcentaje',
    	'descripcion',
    	'activa',
    	'id_empresa'
    ];
    protected $dates = ['deleted_at'];
    protected $hidden = [];
    protected $table = 'impuestos';
	  protected $primaryKey = 'id_impuesto';
}
