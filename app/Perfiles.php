<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

//usados manualmente
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\SoftDeletes;
use Validator;

use App\PerfilXPermiso;

class Perfiles extends Model
{
 	use SoftDeletes;

  public static function nuevoPerfil($input){
    $respuesta = array();
    $reglas =  array(
        'nombre'  => array('required'),
    );
    $validator = Validator::make($input, $reglas);

    //asignar id_empresa para redirigir de nuevo al formulario
    $respuesta['id_empresa'] = $input['id_empresa'];

    if ($validator->fails()){
        $respuesta['mensaje'] = $validator;
        $respuesta['error']   = true;
    }else{
        // llamar al metqodo unset para deshacerse del token como parte del input
        unset($input['_token']);
        $perfil = Perfiles::create($input);
        $input['id_perfil'] = $perfil->id_perfil;
        Perfiles::crearPerfilXPermisos($input);
        
        $respuesta['perfil'] = $perfil;                   
        $respuesta['error']   = false;
        $respuesta['mensaje'] = "Perfil creado";
    }     

    return $respuesta; 
  }


  //funcion usada para actualizar un perfil de manera individual y especifica
  public static function actualizarPerfil($input){
   
    $respuesta = array();

    $reglas =  array(
        'nombre' =>array('required'),
    );
    $validator = Validator::make($input, $reglas);
    $respuesta['id_perfil'] = $input['id_perfil'];

    if ($validator->fails()){
      $respuesta['mensaje'] = $validator;
      $respuesta['error']   = true;
    }else{
      // llamar al metqodo unset para deshacerse del token como parte del input
      unset($input['_token']);
      
      $perfil = Perfiles::find($input['id_perfil']);
      $perfil->fill($input);
      $perfil->save();

      PerfilXPermiso::where('id_perfil', $input['id_perfil'])->delete();
      Perfiles::crearPerfilXPermisos($input);
      

      $respuesta['error']   = false;
      $respuesta['mensaje'] = "Perfil Actualizado";
      $respuesta['perfil'] = $perfil;
    }     

    return $respuesta; 
  }

  public static function eliminarPerfil($id_perfil, $id_empresa){
  	$perfil = Perfiles::find(1)->where('id_empresa', $id_empresa)->where('id_perfil', $id_perfil)->first();
    $perfil->delete();

    $respuesta['respuesta'] = 'Perfil Eliminado';
    $respuesta['perfil'] = $perfil;
    return $respuesta;
  }



  public static function selectPerfiles($id_empresa){
    return DB::table('perfiles')
        ->where('id_empresa', $id_empresa)
        ->whereNull('deleted_at')
        ->get();
  }

 	//obtener un perfil especifico
  public static function perfil($id_perfil, $id_empresa){
  	return DB::table('perfiles')
        ->where('id_perfil', $id_perfil)
        ->where('id_empresa', $id_empresa)
        ->first();
  }
  //obtener permsisos de un perfil especifico
  public static function perfilXpermisos($id_perfil){
    return DB::table('perfilesXpermisos')
        ->where('id_perfil', $id_perfil)
        ->whereNull('deleted_at')
        ->first();
  }

  public static function crearPerfilXPermisos($input){
    if (!isset($input['insertar']))
      $input['insertar'] = array();
    if (!isset($input['ver']))
      $input['ver'] = array();
    if (!isset($input['modificar']))
      $input['modificar'] = array();
    if (!isset($input['eliminar']))
      $input['eliminar'] = array();

    $arreglos = array_merge($input['insertar'], $input['ver'], $input['modificar'], $input['eliminar']);//unir todos los arreglos
    $arreglos = array_unique($arreglos);//agruparlos por valor
    sort($arreglos);//ordenarlos por valor y reordenar el indice
    
    $inputI = array();
    $inputI['id_perfil'] = $input['id_perfil'];
    

    for($i=0; $i< count($arreglos); $i++){
      $inputI['insertar']   = false;
      $inputI['ver']        = false;
      $inputI['modificar']  = false;
      $inputI['eliminar']   = false;

      foreach ($input['insertar'] as $key) {
        if($arreglos[$i]==$key){
          $inputI['insertar'] = true;
          $inputI['id_permiso'] = $key;
        }
      }
    
      foreach ($input['ver'] as $key) {
        if($arreglos[$i]==$key){
          $inputI['ver'] = true;
          $inputI['id_permiso'] = $key;
        }
      }

      foreach ($input['modificar'] as $key) {
        if($arreglos[$i]==$key){
          $inputI['modificar'] = true;
          $inputI['id_permiso'] = $key;
        }
      }
    
      foreach ($input['eliminar'] as $key) {
        if($arreglos[$i]==$key){
          $inputI['eliminar'] = true;
          $inputI['id_permiso'] = $key;
        }
      }
      
      PerfilXPermiso::create($inputI);
    }
  }



  protected $fillable = [
  	'nombre',
  	'descripcion',
  	'id_empresa'
  ];
  protected $dates = ['deleted_at'];
  protected $hidden = [];
  protected $table = 'perfiles';
  protected $primaryKey = 'id_perfil';
}
