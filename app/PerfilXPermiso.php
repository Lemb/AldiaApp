<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PerfilXPermiso extends Model
{
    //
    protected $fillable = [
    	'id_perfil',
    	'id_permiso',
    	'insertar',
    	'ver',
    	'modificar',
    	'eliminar',
    ];
    protected $hidden = [];
    protected $table = 'perfilesXpermisos';
	protected $primaryKey = 'id_perfil';
}
