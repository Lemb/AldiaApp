<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

//usados manualmente
use App\VersionModulo;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\SoftDeletes;
use Validator;

class Banco extends Model
{
  use SoftDeletes;
 
  public static function nuevoBanco($input, $sessionUsuario){

    $respuesta = array();
    $reglas =  array(
        'nombre'  => array('required'),
        'descripcion'=> array('max:149'),
        'saldo'  => array('numeric'),
    );
    $validator = Validator::make($input, $reglas);

    //asignar id_empresa para redirigir de nuevo al formulario
    $respuesta['id_empresa'] = $input['id_empresa'];

    if ($validator->fails()){
        $respuesta['mensaje'] = $validator;
        $respuesta['error']   = true;
    }else{
      $input['versionActual'] = 1;//ultima version
      // llamar al metqodo unset para deshacerse del token como parte del input
      unset($input['_token']);
      if( isset($input['predeterminado']) && $input['predeterminado']==1){//usado para tener solo un banco predeterminado
        DB::table('bancos')
        ->where('id_empresa', $input['id_empresa'])
        ->update(['predeterminado' => false]);
      }
      $banco = Banco::create($input);
      $respuesta['banco'] = $banco;
      VersionModulo::insertVersionesModulosManejo($banco, $sessionUsuario, 10, 'id_banco');//registrar version
      
      $respuesta['error']   = false;
      $respuesta['mensaje'] = "Banco creado";
    }     
    return $respuesta; 
  }

  //funcion usada para actualizar un banco
  public static function actualizarBanco($input, $sessionUsuario){

    $respuesta = array();

    $reglas =  array(
        'nombre'  => array('required'),
        'descripcion'=> array('max:149'),
        'saldo'  => array('numeric'),
    );
    $validator = Validator::make($input, $reglas);

    $respuesta['id_empresa'] = $input['id_empresa'];
    if ($validator->fails()){
        $respuesta['mensaje'] = $validator;
        $respuesta['error']   = true;
    }else{
        // llamar al metqodo unset para deshacerse del token como parte del input
        unset($input['_token']);
        unset($input['saldo']);
        if(isset($input['predeterminado']) && $input['predeterminado']==1){//usado para tener solo un banco predeterminado
          DB::table('bancos')
          ->where('id_empresa', $input['id_empresa'])
          ->update(['predeterminado' => false]);
        }

        $banco = Banco::find($input['id_banco']);          
        VersionModulo::editVersionesModulosManejo($banco, $sessionUsuario, 10, 'id_banco');//registrar nueva version 
        $banco->fill($input);
        $banco->save();
        
        $respuesta['banco'] = $banco;
        $respuesta['error']   = false;
        $respuesta['mensaje'] = "Banco actualizado";
    }     

    return $respuesta; 
  }

  public static function eliminarBanco($id_banco, $sessionUsuario, $id_empresa){
   	$banco = Banco::where('id_empresa', $id_empresa)->where('id_banco', $id_banco)->first();
    VersionModulo::editVersionesModulosManejo($banco, $sessionUsuario, 10, 'id_banco');//registrar nueva version 
  	$banco->delete();

    $respuesta['banco'] = $banco;
    $respuesta['mensaje'] = 'Banco eliminado';

    return $respuesta;
  }


   //hacer una seleccion de todos los bancos asociados a la empresa
  public static function selectBancos($id_empresa){
    return DB::table('bancos')
      ->where('id_empresa', $id_empresa)
      ->whereNull('deleted_at')
      ->where('versionActual', 1)
      ->orderBy('id_banco', 'desc')
      ->get();
  }

  //consultaar banco basado en empresa
  public static function banco($id_banco, $id_empresa){
    return DB::table('bancos')
    ->where('id_banco', $id_banco)
    ->where('id_empresa', $id_empresa)      
    ->select('bancos.*')
    ->first();
  }

  public static function versiones($id_banco){//usada para consultar las versiones del banco
    return DB::table('bancos')
    ->join('versionesModulos', 'versionesModulos.id_referencia', '=', 'bancos.id_banco')
    ->join('usuarios as responsable', 'versionesModulos.id_usuario', '=', 'responsable.id_usuario')
    ->whereIn('versionesModulos.id_referenciaInicial', function ($query) use($id_banco) {
              $query->select('id_referenciaInicial')
                    ->from('versionesModulos')
                    ->where('id_referencia', $id_banco)
                    ->where('modulo', 10);
    })
    ->where('versionesModulos.modulo', 10)
    ->orderBy('numeroVersion','desc')
    ->select('bancos.*', 'versionesModulos.*',
      'responsable.identificacion as responsableIdentificacion', 'responsable.nombre as responsableNombre', 'responsable.id_usuario as responsableID')
    ->get();

  }




  protected $fillable = [
  	'nombre',
  	'tipo',
  	'saldo',
  	'descripcion',
    'predeterminado',
  	'id_empresa',
    'versionActual'
  ];
  protected $dates = ['deleted_at'];
  protected $hidden = [];
  protected $table = 'bancos';
  protected $primaryKey = 'id_banco';
}
