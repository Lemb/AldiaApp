<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

//usados manualmente
use App\VersionModulo;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\SoftDeletes;
use Validator;
use App\Persona;

class Contacto extends Model
{
  use SoftDeletes;

  public static function nuevoContacto($input, $sessionUsuario){
  	
		$respuesta = array();
		$reglas =  array(
			'nombre'  => array('required', 'max:79'),
			'nit' => array('max:49'),
    	'observaciones'=> array('max:149'),
    	'telefono'  => array('numeric', 'digits_between:0,29', 'nullable'),
    	'telefono2'  => array('numeric', 'digits_between:0,29', 'nullable'),
    	'celular'  => array('numeric', 'digits_between:0,29', 'nullable'),
    	'direccion'  => array('max:79'),
    	'email'  => array('max:89'),
    	'ciudad'  => array('max:49'),
    	'fax'  => array('max:99'),
		);
		$validator = Validator::make($input, $reglas);

		//asignar id_empresa para redirigir de nuevo al formulario
		$respuesta['id_empresa'] = $input['id_empresa'];

		if ($validator->fails()){
	    	$respuesta['mensaje'] = $validator;
	    	$respuesta['error']   = true;
		}else{
			//comprobar si se marco los checkboxes y luego mandar false en caso de no estar check
			if(!isset($input['cliente']))
        $input['cliente'] = false;
		  if(!isset($input['proveedor']))
	    	$input['proveedor'] = false;
      if(!isset($input['notificacion']))
      	$input['notificacion'] = false;

    	// llamar al metqodo unset para deshacerse del token como parte del input
    	unset($input['_token']);
      $input['versionActual'] = 1;
    	$contacto = Contacto::create($input);
      $input['id_contacto'] = $contacto->id_contacto;

      VersionModulo::insertVersionesModulosManejo($contacto, $sessionUsuario, 11, 'id_contacto');//registrar version
    	//fragmento para agregar todas las personas relacionadas al contacto
    	Contacto::agregarPersonasInput($input);

      $respuesta['contacto'] = $contacto;
    	$respuesta['error']   = false;
    	$respuesta['mensaje'] = "Contacto creado";
	  }    
		return $respuesta; 
  }

   //funcion usada para actualizar un banco
  public static function actualizarContacto($input, $sessionUsuario){

  	$respuesta = array();

  	$reglas =  array(
  		'nombre'  => array('required', 'max:79'),
		  'nit' => array('max:49'),
    	'observaciones'=> array('max:149'),
    	'telefono'  => array('numeric', 'digits_between:0,29', 'nullable'),
    	'telefono2'  => array('numeric', 'digits_between:0,29', 'nullable'),
    	'celular'  => array('numeric', 'digits_between:0,29', 'nullable'),
    	'direccion'  => array('max:79'),
    	'email'  => array('max:89'),
    	'ciudad'  => array('max:49'),
    	'fax'  => array('max:99'),
  	);
  	$validator = Validator::make($input, $reglas);

  	$respuesta['id_empresa'] = $input['id_empresa'];
  	$respuesta['id_contacto'] = $input['id_contacto'];
  	if ($validator->fails()){
  		$respuesta['mensaje'] = $validator;
      	$respuesta['error']   = true;
  	}else{
  		// llamar al metqodo unset para deshacerse del token como parte del input
  		//comprobar si se marco los checkboxes y luego mandar false en caso de no estar check
		  if(!isset($input['cliente']))
   		 $input['cliente'] = false;
	    if(!isset($input['proveedor']))
    	  $input['proveedor'] = false;
      if(!isset($input['notificacion']))
      	$input['notificacion'] = false;

    	$contacto = Contacto::find($input['id_contacto']);
      $contactoAntiguo = VersionModulo::editVersionesModulosManejo($contacto, $sessionUsuario, 11, 'id_contacto');

      $personas = DB::table('personas')->where('id_contacto', $contacto->id_contacto)->get();//obtenertodaslaspersonas
      //igualar el id de contacto al de el registro nuevo(que referencia version previa)
      Contacto::agregarPersonas($personas, $contactoAntiguo->id_contacto);//agregar a ese nuevo registro las personas anteriores

      //continuar con el flujo de modificacion
    	$contacto->fill($input);
    	$contacto->save();

    	//eliminar todas las personas relacionadas al contacto y volver a crear las provenientes
    	Persona::where('id_contacto', $input['id_contacto'])->delete();
    	$input['id_contacto'] = $contacto->id_contacto;
    	Contacto::agregarPersonasInput($input);
    
      $respuesta['contacto'] = $contacto;
    	$respuesta['error']   = false;
    	$respuesta['mensaje'] = "Contacto actualizado";
  	}     

  	return $respuesta; 
  }

  //eliminar el contacto
  public static function eliminarContacto($id_contacto, $sessionUsuario, $id_empresa){
    
   	$contacto = Contacto::where('id_empresa', $id_empresa)->where('id_contacto', $id_contacto)->first();
    $contactoAntiguo = VersionModulo::editVersionesModulosManejo($contacto, $sessionUsuario, 11, 'id_contacto');
    $personas = DB::table('personas')->where('id_contacto', $contacto->id_contacto)->get();//obtenertodaslaspersonas
    //igualar el id de contacto al del registro nuevo(que referencia version previa)
    Contacto::agregarPersonas($personas, $contactoAntiguo->id_contacto);//agregar a ese nuevo registro las personas anteriores
  	$contacto->delete();

    $respuesta['mensaje']   = 'Contacto eliminado';
    $respuesta['contacto']  = $contacto;

    return $respuesta;
  }






  public static function agregarPersonasInput($input){//agregar personas relacionadas al contacto
    for ($i=0; $i < count($input['nombreP']); $i++) { 
      if ($input['nombreP'][$i] != null || $input['apellidoP'][$i] != null || $input['emailP'][$i] != null || $input['telefonoP'][$i] != null  || $input['celularP'][$i] != null) {
        $inputP['nombre']     = $input['nombreP'][$i];
        $inputP['apellido']   = $input['apellidoP'][$i];
        $inputP['email']      = $input['emailP'][$i];
        $inputP['telefono']   = $input['telefonoP'][$i];
        $inputP['celular']    = $input['celularP'][$i];
        if(!isset($input['notificacionP'][$i]))
          $inputP['notificacion'] = false;
        else
          $inputP['notificacion'] = $input['notificacionP'][$i];

        $inputP['id_contacto'] = $input['id_contacto'];

        Persona::create($inputP);
      }
    }
  }
  public static function agregarPersonas($personas, $id_contacto){//agregar personas relacionadas al contacto
    foreach ($personas as $persona) {
        $inputP['nombre']     = $persona->nombre;
        $inputP['apellido']   = $persona->apellido;
        $inputP['email']      = $persona->email;
        $inputP['telefono']   = $persona->telefono;
        $inputP['celular']    = $persona->celular;
        $inputP['notificacion'] = false;
        $inputP['id_contacto'] = $id_contacto;
        Persona::create($inputP);
    }
  }

  //hacer una seleccion de todos los contactos asociados a la empresa
  public static function selectContactos($id_empresa){
    return DB::table('contactos')
      ->where('id_empresa', $id_empresa)
      ->where('versionActual', 1)
      ->whereNull('deleted_at')
      ->orderBy('id_contacto', 'desc')
      ->get();
  }
  //hacer una seleccion de todos los clientes o proveedores asociados a la empresa
  public static function selectContactosFacturas($id_empresa, $tipo){
    return DB::table('contactos')
      ->where('id_empresa', $id_empresa)
      ->where('versionActual', 1)
      ->where($tipo, 1)
      ->whereNull('deleted_at')
      ->orWhere('id_contacto', 1)
      ->orderBy('id_contacto', 'asc')
      ->get();
  }
  //obtener un contacto especifica
  public static function contacto($id_contacto, $id_empresa, $accion){
    if($accion=='editar'){      
      return DB::table('contactos')
          ->where('id_contacto', $id_contacto)
          ->where('id_empresa', $id_empresa)
          ->whereNull('deleted_at')
          ->where('versionActual', 1)
          ->first();
    }
    else if($accion=='ver'){
    	return DB::table('contactos')
          ->where('id_contacto', $id_contacto)
          ->where('id_empresa', $id_empresa)
          ->first();
    }
  }
   //obtener personas de un contacto especifica
  public static function personas($id_contacto){
  	return DB::table('personas')
        ->where('id_contacto', $id_contacto)
        ->get();
  }

  public static function versiones($id_contacto){//usada para consultar las versiones del contacto
    return DB::table('contactos')
    ->join('versionesModulos', 'versionesModulos.id_referencia', '=', 'contactos.id_contacto')
    ->join('usuarios as responsable', 'versionesModulos.id_usuario', '=', 'responsable.id_usuario')
    ->whereIn('versionesModulos.id_referenciaInicial', function ($query) use($id_contacto) {
              $query->select('id_referenciaInicial')
                    ->from('versionesModulos')
                    ->where('id_referencia', $id_contacto)
                    ->where('modulo', 11);
    })
    ->where('versionesModulos.modulo', 11)
    ->orderBy('numeroVersion','desc')
    ->select('contactos.*', 'versionesModulos.*', 'responsable.identificacion as responsableIdentificacion', 'responsable.nombre as responsableNombre', 'responsable.id_usuario as responsableID')
    ->get();
  }


  protected $fillable = [
  	'nombre',
  	'nit',
  	'direccion',
  	'ciudad',
  	'email',
  	'telefono',
  	'telefono2',
  	'fax',
  	'celular',
  	'observaciones',
  	'cliente',
  	'proveedor',
  	'estadoCuenta',
  	'notificacion',
  	'id_terminoPago',
  	'id_vendedor',
  	'id_lista',
  	'id_empresa',
    'versionActual'
  ];
  protected $dates = ['deleted_at'];
  protected $hidden = [];
  protected $table = 'contactos';
	protected $primaryKey = 'id_contacto';
}


