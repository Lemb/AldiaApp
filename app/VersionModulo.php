<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

//usados manualmente
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\DB;
use Validator;

class VersionModulo extends Model
{
	public $timestamps = false;

	public static function createVersionModulo($datos){//crear versiones del modulo
    $input['id_referencia']         = $datos->id;
    $input['id_referenciaInicial']  = $datos->id_inicial;
    $input['numeroVersion'] 		= $datos->numeroVersion;
    $input['modulo'] 				= $datos->modulo;
    $input['id_usuario']            = $datos->id_usuario;

    VersionModulo::create($input);
  }


  public static function insertVersionesModulosManejo ($referencia, $sessionUsuario, $modulo, $nombreID){
    //establecer y crear primera version de la referencia
    $referencia->id            = $referencia->$nombreID;
    $referencia->id_inicial    = $referencia->$nombreID;
    $referencia->numeroVersion = 1;
    $referencia->id_usuario    = $sessionUsuario;
    $referencia->modulo        = $modulo;
    VersionModulo::createVersionModulo($referencia);
  }
  public static function editVersionesModulosManejo ($referencia, $sessionUsuario, $modulo, $nombreID){
    //crear un duplicado del categoria y setear como parte de una version anterior al categoria
    $referenciaAntigua = $referencia->replicate();
    $referenciaAntigua->versionActual = 0;
    $referenciaAntigua->updated_at = $referencia->updated_at;
    $referenciaAntigua->save();

     //buscar la version relacionada a la categoria primera
    $version = VersionModulo::where('id_referencia', $referencia->$nombreID)->where('modulo', $modulo)->first();
    //sacar el numero de la version actual de la categoria al cual se duplico
    $numeroVersion = VersionModulo::where('id_referenciaInicial', $referencia->$nombreID)->max('numeroVersion');

    //hacer referencia al categoria duplicado y sacar sus datos
    $referenciaAntigua->id             = $referenciaAntigua->$nombreID;
    $referenciaAntigua->id_inicial     = $referencia->$nombreID;
    $referenciaAntigua->numeroVersion  = $numeroVersion;
    $referenciaAntigua->modulo         = $modulo;
    $referenciaAntigua->id_usuario     = $version->id_usuario;
    
    VersionModulo::createVersionModulo($referenciaAntigua);//registrarlo como parte de una version anterior

    $inputV['id_usuario']     = $sessionUsuario;//usuario quien emprendio la accion
    $inputV['numeroVersion']  = $numeroVersion+1;
    $version->fill($inputV); 
    $version->save(); 

    return $referenciaAntigua;
  }


	protected $fillable = [
		'id_referencia',
		'id_referenciaInicial',
		'numeroVersion',
		'modulo',
		'id_usuario',

	];
	protected $hidden = [];
	protected $table = 'versionesModulos';
	protected $primaryKey = 'id_versionModulo';
}
