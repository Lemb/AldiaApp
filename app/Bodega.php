<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
//usados manualmente
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\SoftDeletes;
use Validator;

use App\VersionModulo;
class Bodega extends Model
{
  	use SoftDeletes;

	public static function nuevaBodega($input, $sessionUsuario){
	    $respuesta = array();
	    $reglas =  array(
	        'nombre'  => array('required'),
	    );
	    $validator = Validator::make($input, $reglas);

	    //asignar id_empresa para redirigir de nuevo al formulario
	    $respuesta['id_empresa'] = $input['id_empresa'];

	    if ($validator->fails()){
	        $respuesta['mensaje'] = $validator;
	        $respuesta['error']   = true;
	    }else{
	        // llamar al metqodo unset para deshacerse del token como parte del input
	        unset($input['_token']);
		    if( isset($input['predeterminada']) && $input['predeterminada']==1){//usado para tener solo una bodega predeterminada
		        DB::table('bodegas')
		        ->where('id_empresa', $input['id_empresa'])
		        ->update(['predeterminada' => false]);
      		}

	        $input['versionActual'] = 1;

	        $bodega = Bodega::create($input);
	        VersionModulo::insertVersionesModulosManejo($bodega, $sessionUsuario, 21, 'id_bodega');

	        $respuesta['bodega'] = $bodega;                   
	        $respuesta['error']   = false;
	        $respuesta['mensaje'] = "Bodega creada";
	    }     

	    return $respuesta; 
  	}


  	//funcion usada para actualizar un bodega de manera individual y especifica
	public static function actualizarBodega($input, $sessionUsuario){

		$respuesta = array();

		$reglas =  array(
		    'nombre' =>array('required'),
		);
		$validator = Validator::make($input, $reglas);
		$respuesta['id_bodega'] = $input['id_bodega'];

		if ($validator->fails()){
			$respuesta['mensaje'] = $validator;
			$respuesta['error']   = true;
		}else{
			// llamar al metqodo unset para deshacerse del token como parte del input
			unset($input['_token']);

			if( isset($input['predeterminada']) && $input['predeterminada']==1){//usado para tener solo una bodega predeterminada
		        DB::table('bodegas')
		        ->where('id_empresa', $input['id_empresa'])
		        ->update(['predeterminada' => false]);
      		}

			$bodega = Bodega::find($input['id_bodega']);
			VersionModulo::editVersionesModulosManejo($bodega, $sessionUsuario, 21, 'id_bodega');
			$bodega->fill($input);
			$bodega->save();
			

			$respuesta['error']   = false;
			$respuesta['mensaje'] = "Bodega Actualizada";
			$respuesta['bodega'] = $bodega;
		}     

		return $respuesta; 
	}

	public static function eliminarBodega($id_bodega, $id_empresa){
		$bodega = Bodega::where('id_empresa', $id_empresa)->where('id_bodega', $id_bodega)->first();
		$bodega->delete();

		$respuesta['mensaje'] = 'Bodega Eliminada';
		$respuesta['bodega']    = $bodega;
		return $respuesta;
	}



	public static function selectBodegas($id_empresa){
		return DB::table('bodegas')
	    ->where('id_empresa', $id_empresa)
	    ->whereNull('deleted_at')
	    ->where('versionActual', 1)
	    ->get();
	}
	//obtener un bodega especifico
	public static function bodega($id_bodega, $id_empresa){
		return DB::table('bodegas')
	    ->where('id_bodega', $id_bodega)
	    ->where('id_empresa', $id_empresa)
	    ->first();
	}
	public static function itemsXbodega($id_bodega){
		return DB::table('itemsXbodegas')
		->where('id_bodega', $id_bodega)
		->get();
	}

	public static function versiones($id_bodega){//usada para consultar las versiones del usuario
	    return DB::table('bodegas')
	    ->join('versionesModulos', 'versionesModulos.id_referencia', '=', 'bodegas.id_bodega')
	    ->join('usuarios as responsable', 'versionesModulos.id_usuario', '=', 'responsable.id_usuario')
	    ->whereIn('versionesModulos.id_referenciaInicial', function ($query) use($id_bodega) {
	              $query->select('id_referenciaInicial')
	                    ->from('versionesModulos')
	                    ->where('id_referencia', $id_bodega)
	                    ->where('modulo', 21);
	    })
	    ->where('versionesModulos.modulo', 21)
	    ->orderBy('numeroVersion','desc')
	    ->select('bodegas.*', 'versionesModulos.*', 
	      'responsable.identificacion as responsableIdentificacion', 'responsable.nombre as responsableNombre', 'responsable.id_usuario as responsableID')
	    ->get();

  	}

	protected $fillable = [
		'nombre',
		'direccion',
		'telefono',
		'observaciones',
		'id_empresa',
		'predeterminada',
		'versionActual'
	];
	protected $dates = ['deleted_at'];
	protected $hidden = [];
	protected $table = 'bodegas';
	protected $primaryKey = 'id_bodega';
}
