<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class itemXbodega extends Model
{
    //
    protected $fillable = [
    	'id_item',
    	'id_bodega',
    	'cantidad'
    ];
    protected $hidden = [];
    protected $table = 'itemsXbodegas';
	protected $primaryKey = 'id_itemsXbodegas';
}
