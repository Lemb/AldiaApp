<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

//usados manualmente
use App\VersionModulo;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\SoftDeletes;
use Validator;

class Usuario extends Authenticatable
{
  use Notifiable;
  use SoftDeletes;

  //metodo encargado de actualizar el usuario actual de la sesion, esto desde el menu empresa
  public static function actualizar($input, $sessionUsuario){

    $respuesta = array();

    $reglas =  array(
        'id_usuario' =>array('required'),
        'id_empresa' =>array('required'),
        'celular'  => array('numeric', 'nullable'),
        'telefono'  => array('numeric', 'nullable'),
        'email'  => array('max:90', 'email', 'required'),
    );
    $validator = Validator::make($input, $reglas);

    //asignar id_empresa para redirigir de nuevo al formulario
    $respuesta['id_empresa'] = $input['id_empresa'];

    if ($validator->fails()){
        $respuesta['mensaje'] = $validator;
        $respuesta['error']   = true;
    }else{
        // llamar al metodo unset para deshacerse del token como parte del input
        unset($input['_token']);

        $usuario = Usuario::find($input['id_usuario']);
        $respuesta['id_usuario'] = $usuario->id_usuario;//id_usuario para registrar en el historialde acciones
        VersionModulo::editVersionesModulosManejo($usuario, $sessionUsuario, 4, 'id_usuario');
        $usuario->fill($input);
        $usuario->save();
        
        $respuesta['usuario'] = $usuario;
        $respuesta['usuario']->id_usuario1 = $usuario->id_usuario;
        $respuesta['error']   = false;
        $respuesta['mensaje'] = "Perfil Actualizado";

    }     

    return $respuesta; 
  }

  //metodo para crear un nuevo usuario desde el aplicativo
  public static function nuevoUsuario($input, $sessionUsuario){

    $respuesta = array();

    $reglas =  array(
        'identificacion'  => array('numeric'),
        'celular'  => array('numeric', 'nullable'),
        'telefono'  => array('numeric', 'nullable'),
        'email'  => array('max:90', 'email', 'required'),
        'password' => array('required')
    );
    $validator = Validator::make($input, $reglas);

    //asignar id_empresa para redirigir de nuevo al formulario
    $respuesta['id_empresa'] = $input['id_empresa'];

    if ($validator->fails()){
        $respuesta['mensaje'] = $validator;
        $respuesta['error']   = true;
    }else{

        $input['versionActual'] = 1;//ultima version
        $input['password'] = bcrypt($input['password']);

        $usuario = Usuario::create($input);
        $respuesta['usuario'] = $usuario;
        $respuesta['usuario']->id_usuario1 = $usuario->id_usuario;
        VersionModulo::insertVersionesModulosManejo($usuario, $sessionUsuario, 4, 'id_usuario');//registrar version

        $respuesta['error']   = false;
        $respuesta['mensaje'] = "Usuario creado";
    }     
    return $respuesta; 
  }
  

  //funcion usada para actualizar un usuario de manera individual y especifica
  public static function actualizarUsuario($input, $sessionUsuario, $id_empresa){
   
    $respuesta = array();

    $reglas =  array(
        'id_usuario' =>array('required'),
        'identificacion'  => array('numeric'),
        'celular'  => array('numeric', 'nullable'),
        'telefono'  => array('numeric', 'nullable'),
        'email'  => array('max:90', 'email', 'required'),
    );
    $validator = Validator::make($input, $reglas);

    //asignar id_empresa para redirigir de nuevo al formulario
    $respuesta['id_usuario'] = $input['id_usuario'];
    $respuesta['id_empresa'] = $input['id_empresa'];

    if ($validator->fails()){
        $respuesta['mensaje'] = $validator;
        $respuesta['error']   = true;
    }else{
        // llamar al metqodo unset para deshacerse del token como parte del input
        unset($input['_token']);
        
        $usuario = Usuario::where('id_empresa', $id_empresa)->where('id_usuario', $input['id_usuario'])->first();

        VersionModulo::editVersionesModulosManejo($usuario, $sessionUsuario, 4, 'id_usuario');//registrar nueva version 
        $respuesta['id_usuario'] = $usuario->id_usuario;//id_usuario para registrar en el historial de acciones
        
        $usuario->fill($input);
        $usuario->save();

        $respuesta['error']   = false;
        $respuesta['mensaje'] = "Usuario Actualizado";
        $respuesta['usuario'] = $usuario;
        $respuesta['usuario']->id_usuario1 = $usuario->id_usuario;
    }     

    return $respuesta; 
  }


  public static function eliminarUsuario($id_usuario, $sessionUsuario, $id_empresa){
    $usuario = Usuario::find(1)->where('id_empresa', $id_empresa)->where('id_usuario', $id_usuario)->first();
    $respuesta['id_usuario'] = $usuario->id_usuario;
    VersionModulo::editVersionesModulosManejo($usuario, $sessionUsuario, 4, 'id_usuario');//registrar nueva version 
    $usuario->delete();

    $respuesta['respuesta'] = 'Usuario Eliminado';
    $respuesta['usuario'] = $usuario;
    $respuesta['usuario']->id_usuario1 = $usuario->id_usuario;
    return $respuesta;
  }


  //validar usuario
  public static function validar($userdata){
  	return Auth::attempt($userdata);
  }


  //traer todos los usuarios de version final y no eliminados por empresa
  public static function selectUsuarios($id_empresa){
    return DB::table('usuarios')
    ->join('perfiles', 'usuarios.id_perfil', '=', 'perfiles.id_perfil')
    ->where('usuarios.id_empresa', $id_empresa)
    ->whereNull('usuarios.deleted_at')
    ->where('usuarios.versionActual', 1)
    ->select('usuarios.*', 'perfiles.nombre as perfil')
    ->orderBy('usuarios.id_usuario', 'desc')
    ->get();
  }

  //consultar perfiles
  public static function perfiles($id_empresa){
    return DB::table('perfiles')
    ->where('id_empresa', '1')
    ->orWhere(function ($query) use ($id_empresa) {
      $query->where('id_empresa', $id_empresa)
      ->whereNull('deleted_at');
    })
    ->get();
  }


  //consultaar usuario basado en empresa y la accion que se desea hacer
  public static function usuario($id_usuario, $accion, $id_empresa){
    if($accion=='editar'){
      return DB::table('usuarios')
      ->join('perfiles', 'usuarios.id_perfil', '=', 'perfiles.id_perfil')
      ->where('usuarios.id_usuario', $id_usuario)
      ->where('usuarios.id_empresa', $id_empresa)
      ->whereNull('usuarios.deleted_at')
      ->where('usuarios.versionActual', 1)
      ->select('usuarios.*', 'perfiles.nombre as perfil')
      ->first();
    }
    else if($accion=='ver'){
      return DB::table('usuarios')
      ->join('perfiles', 'usuarios.id_perfil', '=', 'perfiles.id_perfil')
      ->where('usuarios.id_usuario', $id_usuario)
      ->where('usuarios.id_empresa', $id_empresa)      
      ->select('usuarios.*', 'perfiles.nombre as perfil')
      ->first();
    }

  }


  public static function versiones($id_usuario){//usada para consultar las versiones del usuario
    return DB::table('usuarios')
    ->join('perfiles', 'usuarios.id_perfil', '=', 'perfiles.id_perfil')
    ->join('versionesModulos', 'versionesModulos.id_referencia', '=', 'usuarios.id_usuario')
    ->join('usuarios as responsable', 'versionesModulos.id_usuario', '=', 'responsable.id_usuario')
    ->whereIn('versionesModulos.id_referenciaInicial', function ($query) use($id_usuario) {
              $query->select('id_referenciaInicial')
                    ->from('versionesModulos')
                    ->where('id_referencia', $id_usuario)
                    ->where('modulo', 4);
    })
    ->where('versionesModulos.modulo', 4)
    ->orderBy('numeroVersion','desc')
    ->select('usuarios.*', 'perfiles.nombre as perfil', 'versionesModulos.*', 'usuarios.id_usuario as usuario', 
      'responsable.identificacion as responsableIdentificacion', 'responsable.nombre as responsableNombre', 'responsable.id_usuario as responsableID')
    ->get();

  }

  protected $fillable = [
    'identificacion',
		'nombre',
		'apellido',
		'celular',
		'telefono',
		'direccion',
		'email',
		'password',
    'permisosU',
		'activo',
		'login',
		'id_perfil',
		'id_empresa',
    'versionActual',
  ];
  protected $dates = ['deleted_at'];
  protected $hidden = [];
  protected $table = 'usuarios';
  protected $primaryKey = 'id_usuario';
}
