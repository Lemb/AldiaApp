<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

//usados manualmente

use Illuminate\Support\Facades\DB;



class Permiso extends Model
{
    //

    public static function selectPermisos(){
        return DB::table('permisos')->get();
    }
    public static function selectPermisosEdit($id_perfil){
        return DB::table('permisos')
                ->join('perfilesXpermisos', 'perfilesXpermisos.id_permiso', '=', 'permisos.id_permiso')
                ->where('id_perfil', $id_perfil)
                ->select('permisos.descripcion', 'perfilesXpermisos.*')
                ->get();   
    }

    protected $fillable = [
    	'descripcion'
    ];
    
    protected $hidden = [];
    protected $table = 'permisos';
	protected $primaryKey = 'id_permiso';
}
