<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Persona extends Model
{
    //

    protected $fillable = [
    	'nombre',
    	'apellido',
    	'email',
    	'telefono',
    	'celular',
    	'notificacion',
    	'id_contacto'
    ];
    protected $dates = ['deleted_at'];
    protected $hidden = [];
    protected $table = 'personas';
	protected $primaryKey = 'id_persona';
}
