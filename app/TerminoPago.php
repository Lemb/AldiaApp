<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

//usados manualmente
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\SoftDeletes;
use Validator;

class TerminoPago extends Model
{
    use SoftDeletes;
    //hacer una seleccion de todos los terminos
    public static function selectTerminos($id_empresa){
      	return DB::table('terminosPago')
          ->whereIn('id_empresa', [1, $id_empresa])
          ->whereNull('deleted_at')
          ->orderBy('id_terminoPago', 'desc')
          ->get();
    }

    public static function nuevoTerminoPago($input){

		$respuesta = array();

		$reglas =  array(
	    	'nombre'  => array('required'),
	    	'dias'  => array('numeric'),
		);
		$validator = Validator::make($input, $reglas);

		//asignar id_empresa para redirigir de nuevo al formulario
		$respuesta['id_empresa'] = $input['id_empresa'];

		if ($validator->fails()){
	    	$respuesta['mensaje'] = $validator;
	    	$respuesta['error']   = true;
		}else{
	    	// llamar al metqodo unset para deshacerse del token como parte del input
    	unset($input['_token']);
    	$terminoPago = TerminoPago::create($input);

      $respuesta['terminoPago'] = $terminoPago;
			$respuesta['error']   = false;
    	$respuesta['mensaje'] = "Termino de pago creado";
		}     

    	return $respuesta; 
    }
    //funcion usada para actualizar una numeracion
    public static function actualizarTerminoPago($input){
     
      $respuesta = array();

      $reglas =  array(
          'nombre'  => array('required'),
          'dias'  => array('numeric'),
      );
      $validator = Validator::make($input, $reglas);

      $respuesta['id_empresa'] = $input['id_empresa'];
      if ($validator->fails()){
          $respuesta['mensaje'] = $validator;
          $respuesta['error']   = true;
      }else{
          // llamar al metqodo unset para deshacerse del token como parte del input
          unset($input['_token']);

          if(!isset($input['activo']))
          	$input['activo'] = false;

          $terminoPago = TerminoPago::find($input['id_terminoPago']);
          $terminoPago->fill($input);
          $terminoPago->save();
          
          $respuesta['terminoPago'] = $terminoPago;
          $respuesta['error']   = false;
          $respuesta['mensaje'] = "Termino de Pago Actualizado";
      }     

      return $respuesta; 
    }

    public static function eliminarTerminoPago($id_terminoPago, $id_empresa){
      $terminoPago = TerminoPago::where('id_empresa', $id_empresa)->where('id_terminoPago', $id_terminoPago)->first();
      $terminoPago->delete();

      $respuesta['respuesta'] = 'Termino de pago Eliminado';
      $respuesta['terminoPago'] = $terminoPago;

      return $respuesta;
    }

    //obtener un numeracion especifica
    public static function terminoPago($id_terminoPago, $id_empresa){
    	return DB::table('terminosPago')
          ->where('id_terminoPago', $id_terminoPago)
          ->where('id_empresa', $id_empresa)
          ->first();
    }

    protected $fillable = [
    	'nombre',
    	'dias',
    	'activo',
    	'id_empresa'
    ];
    protected $dates = ['deleted_at'];
    protected $hidden = [];
    protected $table = 'terminosPago';
	  protected $primaryKey = 'id_terminoPago';
}
