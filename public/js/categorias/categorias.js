$( document ).ready(function() {
	$('#eliminar').click(function(event) {
    	if(!confirm('¿Deseas realmente eliminar esta categoria?'))
			event.preventDefault();
	});		    
});
//funcion usada para expandir la categorias relacionadas a la expandida
function expandir(id_categoriaP){
	if($( "."+id_categoriaP).css( "display")=="none"){
		$( "."+id_categoriaP).css( "display", "");
		$( "#i"+id_categoriaP ).attr( "class", "fa fa-minus-square-o");
	}
	else{
		$( "."+id_categoriaP).css( "display", "none");
		$( "#i"+id_categoriaP ).attr( "class", "fa fa-plus-square-o");
	}
}


//funcion usada para pasarle parametros al modal
function modal(id_categoria, nombre, edit){
	if(edit){
		$("#categoriaNombre").text("");
		$("#myModalLabel").text("Editar categoria");	

		$("#nombre").attr("value",nombre);
		$("#id_categoriaP").attr("name","id_categoria");				
		$("#submit").attr("value","Actualizar");
		$("#form").attr("action", editCategoriaLink);
	}
	else{
		$("#categoriaNombre").text("En "+nombre);
		$("#myModalLabel").text("Nueva categoria");	

		$("#nombre").attr("value","");
		$("#id_categoriaP").attr("name","id_categoriaP");				
		$("#submit").attr("value","Crear");
		$("#form").attr("action", nuevaCategoriaLink);
	}
	
	$("#id_categoriaP").attr("value", id_categoria);
}