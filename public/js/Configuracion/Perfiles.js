function checkingAll(clase) {
	if(clase.prop("checked"))
		$("input").prop("checked", true);//checkear toda la columna
	else if(!clase.prop("checked"))
		$("input").prop("checked", false);//Uncheckear toda la columna
}
function checking(clase) {
	if(clase.prop("checked"))
		$("."+clase.attr('class')).prop("checked", true);//checkear toda la columna
	else if(!clase.prop("checked"))
		$("."+clase.attr('class')).prop("checked", false);//Uncheckear toda la columna
	
}
function checkrow(all){

	if(all.prop("checked"))
		$( "input[value='"+all.val()+"']").prop("checked", true);
	else if(!all.prop("checked"))
		$( "input[value='"+all.val()+"']").prop("checked", false);
}
function checkrowDetect(thiss){			
	
	if(!thiss.prop("checked") && $( "input[value='"+thiss.val()+"']:checked").length<2 &&  $( "input[value='"+thiss.val()+"']").eq(0).prop("checked"))
		$( "input[value='"+thiss.val()+"']").prop("checked", false);
	else if(!thiss.prop("checked") && $( "."+thiss.attr('class')+":checked").length<2 &&  $("."+thiss.attr('class')).eq(0).prop("checked"))
		$( "."+thiss.attr('class')).eq(0).prop("checked", false);
}