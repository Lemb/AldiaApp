$(function () {
	$("#fechaI").datetimepicker({
		format: 'yyyy-mm-dd hh:ii',
		fontAwesome :true,
		todayBtn: true,
	});
	$('#id_contacto').select2({theme: "bootstrap"});

	$('.facturaCategoriaPregunta').change(function() {
		mostrarTitulos(this);
	});

	$('form').submit(function(){
		customdNumber('');//quitar las comas antes de hacer submit

		if($('.facturaCategoriaPregunta:checked').val()=="No"){
			//recorrer en busca de una categoria no definida con valores definidos para valorar el total real
			for(var i=0; i < $('.categoria').length;i++){
				if($('.categoria').eq(i).val()==""){
					$('.subtotal').eq(i).val(0);
					$('.total').eq(i).val(0);
				}
			}
			calcular(0);//calcular luego de recorrer
		}
	});

	customdNumber(',');
		
});


//funcion usada para consultar todas las facturas relacionadas a un cliente y añadirlas
function ajaxClient(cliente, tipo){

	$("#totalTotal").val(0);
	$("#totalTotalSpan").text(0);
	$("#submit").prop("disabled", true);
	$('#preguntaFacturacion').css('display', '');
	
	if (tipo==1) {
		var valor = 'Valor a Recibir';
	}
	else if (tipo == 2) {
		var valor = 'Valor a Pagar';
	}

	$.ajax({url: urlAjaxClient+"/"+cliente, 
		method: "GET",
		success: function(result){
			
			$("#facturas").empty();
			var result = $.parseJSON(result);
			var divs='';
			$.each(result, function(index, value){

				divs+='<div class="row factura" >'+
				'<input type="hidden" value="'+value.id_greso+'" name="id_gresoRef[]">'+
				'<div class="form-group col-md-2 col-md-offset-0">'+
					'<label for="numeroF">Numero</label>'+
					'<input class="form-control" name="numeroF[]" id="numeroF" type="text" value="'+value.numero+'" readonly="readonly">'+
				'</div>'+
				'<div class="form-group col-md-2 col-md-offset-0">'+
					'<label for="totalF">Total</label>'+
					'<input class="form-control totalF" name="totalF[]" id="totalF" type="text" value="'+value.total+'" disabled="disabled">'+
				'</div>'+
				'<div class="form-group col-md-2 col-md-offset-0">'+
					'<label for="pagadoF">Pagado</label>'+
					'<input class="form-control pagadoF" name="pagadoF[]" id="pagadoF" type="text" value="'+value.pagado+'" disabled="disabled">'+
				'</div>'+
				'<div class="form-group col-md-2 col-md-offset-0">'+
					'<label for="porpagarF">Por pagar</label>'+
					'<input class="form-control porpagarF" name="porpagarF[]" id="porpagarF" type="text" value="'+value.pagar+'" disabled="disabled">'+
				'</div>'+
				'<div class="form-group col-md-2 col-md-offset-0">'+
					'<label for="valorF">'+valor+'</label>'+
					'<input class="form-control valorF" name="valorF[]" id="valorF" type="text" onkeyup="calcularTotal();" onchange="calcularTotal();" step="any" min="0">'+
				'</div>'+
				'<div class="form-group col-md-1 col-md-offset-0">'+
					'<label for="valorF">Maximo</label>'+
					'<button type="button" class="btn btn-block btn-success" onclick="igualar(\''+value.pagar+'\', '+index+');"><i class="fa fa-eject" aria-hidden="true"></i></button>'+
				'</div>'+
				'</div>';
				
			});

			
			$("#facturas").append(divs);
			mostrarTitulos($('.facturaCategoriaPregunta'));
			customdNumber(',');

    	},
    	error: function (request, status, error) {
		    $('#preguntaFacturacion input').prop('checked', false);
			$("#facturas").css("display", "none");
			$("#categoriasDiv").css("display", "none");
			$("#h4Factura").css("display", "none");
			$("#h4FacturaNoRelacion").css("display", "none");

    	}
    });
}
//funcion utilizada para mostrar o esconder las facturas de venta relacionadas al contacto o panel de categorias a registrar
function mostrarTitulos(select){
	if (select.value == 'Si') {
	    $("#facturas").css("display", "");
	    $("#categoriasDiv").css("display", "none");
	    calcularTotal();
	}
	else if (select.value == 'No') {
	    $("#facturas").css("display", "none");
	    $("#categoriasDiv").css("display", "");
	    calcular(0);
	}


	if($("#categoriasDiv:visible").length>0){
		$("#h4Factura").css("display", "none");
		$("#h4FacturaNoRelacion").css("display", "none");
	}
	else if($(".factura").length>0 && $("#facturas:visible").length>0){
		$("#h4Factura").css("display", "");
		$("#h4FacturaNoRelacion").css("display", "none");
	}
	else if($("#facturas:visible").length>0){
		$("#h4Factura").css("display", "none");
		$("#h4FacturaNoRelacion").css("display", "");
	}
}

function igualar(pagar, indice){
	$('.valorF').eq(indice).val(pagar);
	calcularTotal();
}
function calcularTotal(){
	var total=0;
	var deshabilitar=false;
	var nulo=0;
	for (var i =0; i < $('.valorF').length; i++) {
		if($('.valorF').eq(i).val()>0){
			total = total+parseFloat($('.valorF').eq(i).val());//sumar total
			//preguntar si es menor a cero para deshabilitar el boton de crear o si es mayor al campo por pagar
			if(parseFloat($('.valorF').eq(i).val())<0 || parseFloat($('.valorF').eq(i).val())> parseFloat($('.porpagarF').eq(i).val()))
				deshabilitar=true;
		}
		else
			nulo++;
	}
	if($('.valorF').length<=nulo)
		deshabilitar=true;
	$("#submit").prop('disabled', deshabilitar);//deshabilitar dependiendo
	$("#totalTotal").val(total);
	$("#totalTotalSpan").text(total);

	$('#totalTotalSpan').number(true, 2, '.', ',');
}	










//CATEGORIAS

$(function () {
	$( "#myModal" ).on( "hidden.bs.modal", function(e) {
		$('#categoriaModal').removeAttr('id');
		$('#categoriaIDModal').removeAttr('id');
		$('#buscarCategoria').val('');
		$(".fa-minus-square-o").click();
	});

	

});

if(!row)
	var row = 0;
///funciones usadas para agregar o quitar rows de categorias haciendo click en los botones
function nuevaCategoria(){
	
	//clonar y sumar una row como contador
	row++;
	$( ".rowCategoria:last" ).clone().insertAfter( ".rowCategoria:last" );
	$( ".rowCategoria:last" ).attr("class", "row rowCategoria r"+row);

	
	//asignar identificadores
	$( ".rowCategoria:last .categoriaID" ).attr("class", "form-control input-sm categoriaID r"+row);
	$( ".rowCategoria:last .categoria" ).attr("class", "form-control input-sm categoria r"+row);
	$( ".rowCategoria:last .valor" ).attr("class", "form-control input-sm valor r"+row);
	$( ".rowCategoria:last .descuento" ).attr("class", "form-control input-sm descuento r"+row);
	$( ".rowCategoria:last .impuesto" ).attr("class", "form-control input-sm impuesto r"+row);
	$( ".rowCategoria:last .apunte" ).attr("class", "form-control input-sm apunte r"+row);
	$( ".rowCategoria:last .cantidad" ).attr("class", "form-control input-sm cantidad r"+row);
	$( ".rowCategoria:last .subtotal" ).attr("class", "form-control input-sm subtotal r"+row);
	$( ".rowCategoria:last .total" ).attr("class", "form-control input-sm total r"+row);
	

	//reajustar valores
	$( ".rowCategoria:last .categoriaID" ).val('');
	$( ".rowCategoria:last .categoria" ).val('');
	$( ".rowCategoria:last .impuesto" ).val("6");
	$( ".rowCategoria:last .item" ).val("0");
	$( ".rowCategoria:last .valor" ).val("");
	$( ".rowCategoria:last .descuento" ).val("0");
	$( ".rowCategoria:last .cantidad" ).val("1");
	$( ".rowCategoria:last .subtotal" ).val("0");
	$( ".rowCategoria:last .total" ).val("0");

	customdNumber(',');
	

}
//eliminar el ultimo row de items
function menosCategoria(){
	if($(".rowCategoria").length>1){
		row--;
		$(".rowCategoria:last").remove();
		calcular(row);
	}
}


//MODAL DE CATEGORIAS

//funcion usada para expandir la categorias relacionadas a la expandida
function expandir(id_categoriaP){
	if($( "."+id_categoriaP).css( "display")=="none"){
		$( "."+id_categoriaP).css( "display", "");
		$( "#i"+id_categoriaP ).attr( "class", "fa fa-minus-square-o");
	}
	else{
		$( "."+id_categoriaP).css( "display", "none");
		$( "#i"+id_categoriaP ).attr( "class", "fa fa-plus-square-o");
	}
}

function alistarModificacionCategoria(inputTCategoria){
	inputTCategoria.attr('id','categoriaModal');
	inputTCategoria.siblings('.categoriaID').attr('id','categoriaIDModal');
}
function seleccionarCategoria(id_categoria, nombre){
	$("#categoriaIDModal").val(id_categoria);
	$("#categoriaModal").val(nombre);
	habilitarSubmit();
}


//detectar la posicion del item
function posicion(clase, impuesto){
	for(var i=0; i< $(".rowCategoria").length;i++){
		if(clase.hasClass('r'+i)){
			if(impuesto!=null)
				clase.attr("id", impuesto);
			calcular(i);
		}
	}
	
}

//calcular el total en base a los campos
function calcular(posicion){
	var total = $(".valor.r"+posicion).val()-($(".valor.r"+posicion).val()*$(".descuento.r"+posicion).val())/100;//descuento
	subtotal = total*$(".cantidad.r"+posicion).val();//cantidad * total = subtotal
	$(".subtotal.r"+posicion).val(subtotal);//imprimir subtotal
	total = subtotal+(subtotal*$(".impuesto.r"+posicion).attr("id"))/100;//impuestos+subtotal=total
	$(".total.r"+posicion).val(total);//imprimir

	//reestablecer variables
	var subtotal=0;
	var total=0;
	for(var i=0; i< $(".rowCategoria").length;i++){ //sacar total y subtotal pero de todos los items
		subtotal = subtotal+parseFloat($(".subtotal.r"+i).val());
		total = total+parseFloat($(".total.r"+i).val());
	}
	$(".impuestoFinal").val(total-subtotal);
	$("#subtotal").val(subtotal);
	$("#totalTotalSpan").text(total);
	$("#totalTotal").val(total);

	habilitarSubmit();
	$('#totalTotalSpan').number(true, 2, '.', ',');
}

function habilitarSubmit(){//funcion utilizada para validar campos

	deshabilitar = false;
	var noCategoriaContador = 0;
	for(i2=0; i2<$('.cantidad').length; i2++){

		if($(".categoria").eq(i2).val()!="")//si no encuentra ningun item
			noCategoriaContador++;
		if(!$.isNumeric($(".cantidad").eq(i2).val()) || $(".cantidad").eq(i2).val() < 0)//numerico o positivo
			deshabilitar = true;
		//podria existir la posibilidad de que el usuario desee aumentar un porcentaje para ello usaria negativo
		if(!$.isNumeric($(".descuento").eq(i2).val()))
			deshabilitar = true;
		if(!$.isNumeric($(".valor").eq(i2).val()) || $(".valor").eq(i2).val() < 0)
			deshabilitar = true;
		if(!$.isNumeric($(".subtotal").eq(i2).val()) || $(".subtotal").eq(i2).val() < 0)
			deshabilitar = true;
		if(!$.isNumeric($(".total").eq(i2).val()) || $(".total").eq(i2).val() < 0)
			deshabilitar = true;
	}
	if(noCategoriaContador<=0)
		deshabilitar = true;

	$("#submit").prop('disabled', deshabilitar);//deshabilitar dependiendo	
}

function customdNumber(separador){
	//categorias
	$('.valor').number(true, 2, '.', separador);
	$('.subtotal').number(true, 2, '.', separador);
	$('.total').number(true, 2, '.', separador);
	$('#subtotal').number(true, 2, '.', separador);
	$('#impuestoFinal').number(true, 2, '.', separador);
	$('#totalTotalSpan').number(true, 2, '.', separador);

	//facturas
	$('.totalF').number(true, 2, '.', separador);
	$('.pagadoF').number(true, 2, '.', separador);
	$('.porpagarF').number(true, 2, '.', separador);
	$('.valorF').number(true, 2, '.', separador);

}




