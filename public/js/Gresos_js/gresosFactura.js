
//setear librerias para visualizacion y funcionamiento de select2, datetimepicker, customdNumber y magnificPopupmenu
$(function () {

	$("#fechaI").datetimepicker({
		format: 'yyyy-mm-dd hh:ii',
		fontAwesome :true,
		todayBtn: true,
	});
	$("#fechaF").datetimepicker({
		format: 'yyyy-mm-dd hh:ii',
		fontAwesome :true,
		todayBtn: true,
	});
	
	$('.item').select2({theme: "bootstrap"});
	$( ".impuesto" ).select2({theme: "bootstrap"});
	$('#id_vendedor').select2({theme: "bootstrap"});
	$('#numeracion').select2({theme: "bootstrap"});
	$('#id_contacto').select2({theme: "bootstrap"});

	$('.images').magnificPopup({type:'image'});

	customdNumber(',');

	$('form').submit(function(){
		customdNumber('');//quitar las comas antes de hacer submit	
		//recorrer en busca de una categoria no definida con valores definidos para valorar el total real
		for(var i=0; i < $('.item').length;i++){
			if($('.item').eq(i).val()==""){
				$('.subtotal').eq(i).val(0);
				$('.total').eq(i).val(0);
			}
		}
		calcular(0);//calcular luego de recorrer
	});
	bodegaAjax($("#id_bodega").val());

});

var cantidadesItems;
function bodegaAjax(id_bodega){
	$.ajax({url: urlBodegaAjax +"/"+id_bodega, 
		method: "GET",
		success: function(result){
			cantidadesItems = $.parseJSON(result);
		},
		error: function (request, status, error) {
		    console.log(error);
    	}
	});
}
function itemMax(input){
	if(input.val() > input.attr('max')){
		input.val(input.attr('max'));
	}
}

function terminoDePago(dias){
	if (dias!='Manual') {
		var termino = $('#fechaI').val(); 
		termino = new Date(termino);
		termino.setDate(parseInt(termino.getDate())+parseInt(dias)); 
		var mes = parseInt(termino.getMonth())+1;

		var fecha = termino.getFullYear()+'-'+('00'+mes).slice(-2)+'-'+('00'+termino.getUTCDate()).slice(-2)+' '
		+('00'+termino.getHours()).slice(-2)+':'+('00'+termino.getMinutes()).slice(-2);

		$('#fechaF').val(fecha);
	}
}

///funciones usadas para agregar o quitar rows de items haciendo click en los botones
function nuevoItem(){
	//clonar y sumar una row como contador
	row++;
	$( ".rowItem:last" ).clone().insertAfter( ".rowItem:last" );
	$( ".rowItem:last" ).attr("class", "row rowItem row"+row);

	//asignar identificadores
	$( ".rowItem:last .item" ).attr("class", "form-control input-sm item r"+row);
	$( ".rowItem:last .referencia" ).attr("class", "form-control input-sm referencia r"+row);
	$( ".rowItem:last .valor" ).attr("class", "form-control input-sm valor r"+row);
	$( ".rowItem:last .descuento" ).attr("class", "form-control input-sm descuento r"+row);
	$( ".rowItem:last .impuesto" ).attr("class", "form-control input-sm impuesto r"+row);
	$( ".rowItem:last .impuesto" ).attr("name", "id_impuesto"+row+"[]");
	$( ".rowItem:last .apunte" ).attr("class", "form-control input-sm apunte r"+row);
	$( ".rowItem:last .cantidad" ).attr("class", "form-control input-sm cantidad r"+row);
	$( ".rowItem:last .cantidad" ).val(1);
	$( ".rowItem:last .cantidad" ).prop("readonly", true);
	$( ".rowItem:last .subtotal" ).attr("class", "form-control input-sm subtotal r"+row);
	$( ".rowItem:last .total" ).attr("class", "form-control input-sm total r"+row);
	$( ".rowItem:last .images" ).attr("class", "btn form-control input-sm images r"+row);
	

	//reajustar valores
	$( ".rowItem:last .impuesto" ).val("0");
	$( ".rowItem:last .item" ).val("0");
	$( ".rowItem:last .referencia" ).val("");
	$( ".rowItem:last .valor" ).val("");
	$( ".rowItem:last .descuento" ).val("0");
	$( ".rowItem:last .cantidad" ).val("1");
	$( ".rowItem:last .subtotal" ).val("0");
	$( ".rowItem:last .total" ).val("0");
	$(".select2:last").remove();// remover el previo select2 que fue duplicado
	$(".select2:last").remove();// remover el previo select2 que fue duplicado

	$( ".rowItem:last .item" ).select2({theme: "bootstrap"});
	$( ".rowItem:last .impuesto" ).select2({theme: "bootstrap"});
	$('.rowItem:last .images').magnificPopup({type:'image'});
	
	customdNumber(',');
}
//eliminar el ultimo row de items
function menosItem(){
	if($(".rowItem").length>1){
		row--;
		$(".rowItem:last").remove();
		calcular(row);
	}
}

//funciones de items

//detectar la posicion del item
function posicion(clase){
	for(var i=0; i< $(".rowItem").length;i++){
		if(clase.hasClass('r'+i)){
			calcular(i);
		}
	}
}

//utilizado con el fin de habilitar combobox del plugin select2
function comboboxItemCorrecion(select){
	for(var i=0; i<items[0].length; i++){
		if(select.val() == items[0][i])
			item(select, items[1][i], items[2][i], items[3][i], items[4][i]);
		else if(select.val()=="")		
			habilitarSubmit();
	}

}	
//asignar a los campos en base a las propiedades del item
function item(select, referencia, precio, imagen, medida){
			
	if(precio=="")
		precio=0;
	for(var i=0; i< $(".rowItem").length;i++){
		if(select.hasClass('r'+i)){
			$(".referencia.r"+i).val(referencia);
			$(".valor.r"+i).val(precio);
			
			//impuestos
			var id_item 		= select.val();
			var impuestosItem 	= [];
			for(var i2=0 ;i2 < itemsXimpuestos[0].length; i2++){
  				if(itemsXimpuestos[0][i2]==id_item)
  					impuestosItem.push(itemsXimpuestos[1][i2]);
			}
		    $('.impuesto.r'+i).val(impuestosItem);
			$('.impuesto.r'+i).trigger('change');
			//finImpuestos

			//cantidad
			var maxSeted = false;
			$.each(cantidadesItems, function(index, value){
				if(id_item == value.id_item && medida != 'N/A'){
					$(".cantidad.r"+i).attr('max', value.cantidad);
					maxSeted = true;
				} 
			});
			if(!maxSeted)
				$(".cantidad.r"+i).prop('max', false);
			$(".cantidad.r"+i).prop("readonly", false);
			//fincantidad

			if(imagen){
				$(".images.r"+i).attr("href", imagen);
				$(".images.r"+i).prop("disabled", false);
			}
			else
				$(".images.r"+i).prop("disabled", true);
					
			calcular(i);
		}
	}
}

//calcular el total en base a los campos
function calcular(posicion){
	var total 	 = $(".valor.r"+posicion).val()-($(".valor.r"+posicion).val()*$(".descuento.r"+posicion).val())/100;//descuento
	var subtotal = total*$(".cantidad.r"+posicion).val();//cantidad * total = subtotal

	//asignar impuestos al total del item
	var impuestosSelect = $(".impuesto.r"+posicion+" option");//impuestos seleccionados para ese item
	var impuesto  = 0;
	for(var i=0; i < impuestosSelect.length; i++){
		for(i2=0 ;i2 < impuestos[0].length; i2++){
			if(impuestos[0][i2]== $(".impuesto.r"+posicion).val()[i])
				impuesto += parseInt(impuestos[2][i2]);
		}
	}
	//fin de asignar impuestos al total del item
	
	total = subtotal+(subtotal*impuesto)/100;//impuestos+subtotal=total

	$(".subtotal.r"+posicion).val(subtotal);//imprimir subtotal
	$(".total.r"+posicion).val(total);//imprimir

	//reestablecer variables
	var subtotal=0;
	var total=0;
	for(var i=0; i< $(".rowItem").length;i++){ //sacar total y subtotal pero de todos los items
		subtotal = subtotal+parseFloat($(".subtotal.r"+i).val());
		total = total+parseFloat($(".total.r"+i).val());
	}
	$(".impuestoFinal").val(total-subtotal);
	$("#subtotal").val(subtotal);
	$("#totalTotalSpan").text(total);
	$("#totalTotal").val(total);

	habilitarSubmit();
	$('#totalTotalSpan').number(true, 2, '.', ',');
}

function habilitarSubmit(){//funcion utilizada para validar campos

	deshabilitar = false;
	var noItemContador = 0;
	for(i2=0; i2<$('.cantidad').length; i2++){

		if($(".item").eq(i2).val()!="")//si no encuentra ningun item
			noItemContador++;
		if(!$.isNumeric($(".cantidad").eq(i2).val()) || $(".cantidad").eq(i2).val() < 0)//numerico o positivo
			deshabilitar = true;
		//podria existir la posibilidad de que el usuario desee aumentar un porcentaje para ello usaria negativo
		if(!$.isNumeric($(".descuento").eq(i2).val()))
			deshabilitar = true;
		if(!$.isNumeric($(".valor").eq(i2).val()) || $(".valor").eq(i2).val() < 0)
			deshabilitar = true;
		if(!$.isNumeric($(".subtotal").eq(i2).val()) || $(".subtotal").eq(i2).val() < 0)
			deshabilitar = true;
		if(!$.isNumeric($(".total").eq(i2).val()) || $(".total").eq(i2).val() < 0)
			deshabilitar = true;
	}
	if(noItemContador<=0)
		deshabilitar = true;
	
	$("#submit").prop('disabled', deshabilitar);//deshabilitar dependiendo
	$("#submit2").prop('disabled', deshabilitar);//deshabilitar dependiendo
}
function agregarPagoSubmit(){//usado como bandera para agregar el pago de la factura de venta creada
	$("#agregarPago").val("1");
}
function customdNumber(separador){
	//categorias
	$('.valor').number(true, 2, '.', separador);
	$('.subtotal').number(true, 2, '.', separador);
	$('.total').number(true, 2, '.', separador);
	$('#subtotal').number(true, 2, '.', separador);
	$('.impuestoFinal').number(true, 2, '.', separador);
	$('#totalTotalSpan').number(true, 2, '.', separador);

	//facturas
	$('.totalF').number(true, 2, '.', separador);
	$('.pagadoF').number(true, 2, '.', separador);
	$('.porpagarF').number(true, 2, '.', separador);
	$('.valorF').number(true, 2, '.', separador);

}
