//usados para extender la pagina de cuerpo completo y poner la barra de arriba del color
$('.content').css('padding', '0');
$('body').attr('class', 'skin-blue');

function agregarItem(itemLi){
	if($('.'+$('.id_item', itemLi).val()).is(":visible"))//comprobar si ya hay un item igual
		calcularItem($('.id_item', itemLi).val(),false);//ir a calcular item
	else{//sino simplemente agregar un list item del item
		var id_item			= $('.id_item', itemLi).val();
		var nombreItem		= $('.nombreItem', itemLi).text();
		var referenciaItem	= $('.referenciaItem', itemLi).text();
		var precioItem		= $('.precioItem', itemLi).text();
		var impuestoItem	= $('.impuestoItem', itemLi).val();
		var impuestoItemID	= $('.impuestoItem', itemLi).attr('id');
		var subtotalItem	= $.number(precioItem, 2 ,'.','');
		var totalItem		= $.number(parseFloat(precioItem)+parseFloat((precioItem*impuestoItem/100)), 2 ,'.','');
		var imagenItem		= $('.imagenItem', itemLi).val();

		var html = ''+
			'<li class="list-group-item justify-content-between item '+id_item+'" style=" padding-bottom: 0px; padding-right: 7px; padding-left:5px;" >'+
				'<input type="hidden" class="id_item" value="'+id_item+'">'+
				'<span style="color: black; margin:0px;" class="nombreItem">'+nombreItem+
					'<a class="btn images" style="padding:0px 4px;" href='+imagenItem+'>'+
	                	'<i class="fa fa-image"></i>'+
	              	'</a>'+
					'<span class="pull-right" style="color: gray;">$<span class="subtotalItem">'+subtotalItem+'</span></span>'+
					'<a class="btn pull-right" style="padding:0px 4px;" onclick="calcularItem('+id_item+',true);">'+
                		'<i class="fa fa-trash"></i>'+
              		'</a>'+
				'</span><br>'+
				'$<span class="precioItem">'+precioItem+'</span> | '+
				'<span style="color: #3c8dbc;" class="referenciaItem">'+referenciaItem+'</span>'+ 
				'<span class="pull-right" style="color: black;">$<span class="totalItem">'+totalItem+'</span></span>'+
				'<span class="badge bg-danger" style="margin-right: 1em;">'+
					'<span class="impuestoItem">'+impuestoItem+'</span>%'+
					'<input class="impuestoItemID" type="hidden" value="'+impuestoItemID+'">'+
				'</span>'+
				'<span class="badge bg-aqua cantidadItem" style="margin-right: 1em; font-size: medium;">1</span>'+
				'<a class="btn apunte" style="padding:0px 4px;" data-toggle="modal" data-target=".modalApunte" onclick="guardarA($(this));">'+
					'<input type="hidden" class="apunteItem">'+
	                '<i class="fa fa-edit"></i>'+
	             '</a>'+
			'</li>';
		$('#listaAVender').append(html);
		$('.images').magnificPopup({type:'image'});

		//usado para mostrar la cantidad que se llevara de cada producto en el recuadro de busqueda de items tambien
		html = '<span class="badge bg-aqua cantidadItemLi" style="font-size: medium; position: absolute; right: 0; top: 0;">1</span>';
		$('.idLi'+id_item+'.liItemPOS').prepend(html);
		
	}
	calcularTotal();
}

//agregar o reducir items segun la accion y luego calcular su subtotal y total en base al precio cantidad e impuesto
function calcularItem(id_item, reducir){
	if(reducir){
		var cantidad = parseInt($('li.'+id_item+' .cantidadItem').text())-1;
		$('.idLi'+id_item+' .cantidadItemLi').text(parseInt($('.idLi'+id_item+' .cantidadItemLi').text())-1);//aumentar en recuadro de seleccion de items
		if(cantidad<=0){
			$('.'+id_item).remove();
			$('.idLi'+id_item+' .cantidadItemLi').remove();
		}
	}
	else{
		var cantidad = parseInt($('li.'+id_item+' .cantidadItem').text())+1;
		$('.idLi'+id_item+' .cantidadItemLi').text(parseInt($('.idLi'+id_item+' .cantidadItemLi').text())+1);//aumentar en recuadro de seleccion de items
	}

	var precioItem   = $('li.'+id_item+' .precioItem').text();
	var impuestoItem = $('li.'+id_item+' .impuestoItem').text();
	var subtotalItem = cantidad*parseFloat(precioItem);
	var totalItem	 = cantidad*(parseFloat(precioItem)+parseFloat((precioItem*impuestoItem/100)));

	$('li.'+id_item+' .cantidadItem').text(cantidad);
	$('li.'+id_item+' .subtotalItem').text($.number(subtotalItem, 2 ,'.',''));
	$('li.'+id_item+' .totalItem').text($.number(totalItem, 2 ,'.',''));

	if(reducir)
		calcularTotal();
}

function calcularTotal(){//calcular el total de los items y agregarlos como impustos subtotal y total respectivamente.
	var	subtotalOperacion	= 0;
	var impuestoOperacion	= 0;
	var totalOperacion 		= 0;

	$('#listaAVender > li').each(function(){
		subtotalOperacion 	+= parseFloat($(this).find('.subtotalItem').text());
		impuestoOperacion 	+= parseFloat($(this).find('.totalItem').text())-parseFloat($(this).find('.subtotalItem').text());
		totalOperacion 		+= parseFloat($(this).find('.totalItem').text());
	});

	if(totalOperacion>0)
		$('#venderBoton').prop("disabled", false);	
	else
		$('#venderBoton').prop("disabled", true);	

	$('#subtotalOperacion').text($.number(subtotalOperacion, 2 ,'.',''));
	$('#impuestoOperacion').text($.number(impuestoOperacion, 2 ,'.',''));
	$('#totalOperacion').text($.number(totalOperacion, 2 ,'.',''));
}

function cancelar(){//cancelar factura de venta
	$('#subtotalOperacion').text('0.00');
	$('#impuestoOperacion').text('0.00');
	$('#totalOperacion').text('0.00');
	$('#listaAVender').empty();

	$('#venderBoton').prop("disabled", true);
	$('.cantidadItemLi').remove();

}


function ajaxVender(){//registrar venta por ajax
	var id_items   	= '';
	var referencias	= '';
	var valores		= '';
	var cantidades 	= '';
	var impuestos 	= '';
	var subtotales 	= '';
	var totales    	= '';
	var apuntes 	= '';

    $('.item').each(function(indice, elemento) {
    	if(indice==0)
    		var conector = '';
    	else
    		var conector = ',';

    	id_items   	+= conector+$('.id_item',elemento).val();
    	referencias += conector+$('.referenciaItem',elemento).text();
    	valores     += conector+$('.precioItem',elemento).text();
    	cantidades 	+= conector+$('.cantidadItem',elemento).text();
    	impuestos 	+= conector+$('.impuestoItemID',elemento).val();
    	subtotales 	+= conector+$('.subtotalItem',elemento).text();
    	totales 	+= conector+$('.totalItem',elemento).text();
    	apuntes 	+= conector+$('.apunteItem',elemento).val();
			
	});

	$.ajax({		
		url: urlAjax,
		method: "GET",
		data: {
			id_vendedor: $('#id_usuarioActivo').val(),
			cliente: $('#id_contacto').val(),
			numeracion:$('#numeracion').val(),
			numero: $('#numero').text(),

			id_items: id_items, 
			referencias: referencias, 
			valores: valores,
			cantidades: cantidades, 
			impuestos: impuestos, 
			subtotales: subtotales, 
			totales: totales, 
			apuntes: apuntes,
		},
		success: function(result){
			result = JSON.parse(result);
			//necesario para agregar factura al historial
			var html = ''+
			'<li class="facturaRegistrada">'+

                   
                '<a href='+urlPDF1+'/'+result['id_greso']+'/2 target="_blank">'+
                   '<i class="menu-icon fa fa-print bg-light-blue" onclick=window.open("'+urlPDF1+'/'+result['id_greso']+'/1");></i>'+
                   '<div class="menu-info">'+
                        '<h4 class="control-sidebar-subheading">'+result['numero']+'</h4>'+
                        '<p>'+$('#id_contacto option:selected').text()+'</p>'+
                        '<p>$'+$('#totalOperacion').text()+'</p>'+
                    '</div>'+
                '</a>'+
                
                
           '</li>';
           	$('#historialFacturas').prepend(html);

			//imprimir y setear variables para un nuevo registro
			alert( result['mensaje'] );	
			$('#numero').text(result['numeroS']);
			$('#numeracion option:selected').attr('class', result['numeroS']);
			cancelar();
		},
		error: function (request, status, error) {
		   alert(error+' '+request);
		}

	});
}

$(function () {
	numeracion1();
	$('.liItemPOS').mouseover(function(){
		$('.liItemPOS').css('background', 'transparent');
		$(this).css('background','#d8d8d8'); 
		$(this).css('cursor','pointer'); 
	});
	$('.liItemPOS').mouseout(function(){
		$(this).css('background', 'transparent');
	});	
	$("#id_contacto").attr("data-placeholder","bar");
	$("#id_contacto").select2();
	$("#numeracion").select2();

	//filtrador de items
	$('#buscador').keyup(function(){
		var that = this, $allListElements = $('#itemsUl > li');

		var $matchingListElements = $allListElements.filter(function(i, li){
			if (tipoDeBusqueda=='texto') 
				var listItemText = $(li).text().toUpperCase();
			else if (tipoDeBusqueda=='barras') 
		    	var listItemText = $(li).attr('class').toUpperCase();

		    var searchText = that.value.toUpperCase();
		    return ~listItemText.indexOf(searchText);
		});
		$allListElements.hide();
		$matchingListElements.show();
	});
});


var aModal;//modal de apunte
function guardarA(a){//guardar a referente al apunte
	$('#apunte').val($('.apunteItem', a).val());
	aModal = a;
} 
function modalApunteGuardar(){//guardar apuntes
	$('.apunteItem', aModal).val($('#apunte').val());
}
function numeracion1(){//encontrar numeracion
	$('#numero').text($("#num"+$('#numeracion').val()).attr("class"));
}

var tipoDeBusqueda = 'texto';
//usada para dejar visualmente los botones de busqueda presionados y setear la bandera de busqueda
function buscadorBotones(tipo){
	if(tipo=='texto'){
		$('#botonTexto').addClass('active');
		$('#botonBarras').removeClass('active');
		tipoDeBusqueda = 'texto';
	}
	else if(tipo=='barras'){
		$('#botonBarras').addClass('active');
		$('#botonTexto').removeClass('active');
		tipoDeBusqueda = 'barras';
	}	
}

function cambioUsuarioAjax(){
	$.ajax({		
		url: urlLogin,
		method: "GET",
		data: {
			email: $('#email').val(),
			password: $('#password').val(),
			ajax: 1,
		},
		success: function(result){
			result = JSON.parse(result);
			$('#id_usuarioActivo').val(result['id_usuario']);
			$('#nombreUsuarioActivo').text(result['nombre']);
			$('#descripcionUsuarioActivo').text(result['nombre']+' '+result['apellido']+' ('+result['identificacion']+')');
			alert(result['mensaje']);
		},
		error: function (request, status, error) {
		   alert(error+' '+request);
		}
	});
}