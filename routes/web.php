<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
session_start();
$id_usuario = (isset($_SESSION['id_usuario'])) ? $_SESSION['id_usuario']:'0';//comprobar sesion sino enviar 0 para saber que ni siquiera existe una sesion
session_abort();

Route::get('loginAjax', 'UsuarioController@loginAjax');

Route::get('/', function () {
    return view('welcome');
});

//CONFIGURACION---------------------------------------------------------------------------------------------




//acceder a configuracion de la empresa y actualizacion de opciones generales
Route::get('empresa', 'EmpresaController@form');
Route::post('empresa','EmpresaController@actualizar');
Route::post('perfil','UsuarioController@actualizar');
Route::post('facturacionGeneral','ConfiguracionController@facturacionGeneral');

//usuario configuracion
Route::get('usuarios','UsuarioController@selectUsuarios');
Route::get('usuarios/{mensaje}','UsuarioController@selectUsuarios');
Route::get('verUsuario/{id_usuario}','UsuarioController@verUsuario')->middleware(\App\Http\Middleware\autenticar::class.':'.$id_usuario.',4,2');
Route::get('verUsuario/{id_usuario}/{id_usuario2}','UsuarioController@verUsuario')->middleware(\App\Http\Middleware\autenticar::class.':'.$id_usuario.',4,2');
Route::get('nuevoUsuario','UsuarioController@nuevoUsuario')->middleware(\App\Http\Middleware\autenticar::class.':'.$id_usuario.',4,1');
Route::post('nuevoUsuario','UsuarioController@nuevoUsuarioPost');
Route::get('editUsuario/{id_usuario}','UsuarioController@editUsuario')->middleware(\App\Http\Middleware\autenticar::class.':'.$id_usuario.',4,3');
Route::post('editUsuario','UsuarioController@editUsuarioPost');
Route::get('eliminarUsuario/{id_usuario}','UsuarioController@eliminarUsuario')->middleware(\App\Http\Middleware\autenticar::class.':'.$id_usuario.',4,4');

//perfiles
Route::get('perfiles','PerfilesController@selectPerfiles');
Route::get('perfiles/{mensaje}','PerfilesController@selectPerfiles');
Route::get('verPerfil/{id_perfil}','PerfilesController@verPerfil')->middleware(\App\Http\Middleware\autenticar::class.':'.$id_usuario.',5,2');
Route::get('nuevoPerfil','PerfilesController@nuevoPerfil')->middleware(\App\Http\Middleware\autenticar::class.':'.$id_usuario.',5,1');
Route::post('nuevoPerfil','PerfilesController@nuevoPerfilPost');
Route::get('editPerfil/{id_perfil}','PerfilesController@editPerfil')->middleware(\App\Http\Middleware\autenticar::class.':'.$id_usuario.',5,3');
Route::post('editPerfil','PerfilesController@editPerfilPost');
Route::get('eliminarPerfil/{id_perfil}','PerfilesController@eliminarPerfil')->middleware(\App\Http\Middleware\autenticar::class.':'.$id_usuario.',5,4');

//bodegas
Route::get('bodegas','BodegaController@selectBodegas');
Route::get('bodegas/{mensaje}','BodegaController@selectBodegas');
Route::get('verBodega/{id_bodega}','BodegaController@verBodega')->middleware(\App\Http\Middleware\autenticar::class.':'.$id_usuario.',21,2');
Route::get('nuevaBodega','BodegaController@nuevaBodega')->middleware(\App\Http\Middleware\autenticar::class.':'.$id_usuario.',21,1');
Route::post('nuevaBodega','BodegaController@nuevaBodegaPost');
Route::get('editBodega/{id_bodega}','BodegaController@editBodega')->middleware(\App\Http\Middleware\autenticar::class.':'.$id_usuario.',21,3');
Route::post('editBodega','BodegaController@editBodegaPost');
Route::get('eliminarBodega/{id_bodega}','BodegaController@eliminarBodega')->middleware(\App\Http\Middleware\autenticar::class.':'.$id_usuario.',21,4');

//numeraciones
Route::get('numeraciones','NumeracionController@selectNumeraciones');
Route::get('numeraciones/{mensaje}','NumeracionController@selectNumeraciones');
Route::get('verNumeracion/{id_numeracion}','NumeracionController@verNumeracion')->middleware(\App\Http\Middleware\autenticar::class.':'.$id_usuario.',6,2');
Route::get('nuevaNumeracion','NumeracionController@nuevaNumeracion')->middleware(\App\Http\Middleware\autenticar::class.':'.$id_usuario.',6,1');
Route::post('nuevaNumeracion','NumeracionController@nuevaNumeracionPost');
Route::get('editNumeracion/{id_numeracion}','NumeracionController@editNumeracion')->middleware(\App\Http\Middleware\autenticar::class.':'.$id_usuario.',6,3');
Route::post('editNumeracion','NumeracionController@editNumeracionPost');
Route::post('editNumeracionDoc','NumeracionController@editNumeracionDoc');
Route::get('eliminarNumeracion/{id_numeracion}/{id_empresa}','NumeracionController@eliminarNumeracion')->middleware(\App\Http\Middleware\autenticar::class.':'.$id_usuario.',6,4');

//impuestos y retenciones
Route::get('impuestos','ImpuestoController@selectImpuestos');
Route::get('impuestos/{mensaje}','ImpuestoController@selectImpuestos');
Route::get('verImpuesto/{id_impuesto}','ImpuestoController@verImpuesto')->middleware(\App\Http\Middleware\autenticar::class.':'.$id_usuario.',7,2');
Route::get('nuevoImpuesto/{cualidad}','ImpuestoController@nuevoImpuesto')->middleware(\App\Http\Middleware\autenticar::class.':'.$id_usuario.',7,1');
Route::post('nuevoImpuesto','ImpuestoController@nuevoImpuestoPost');
//Route::get('editImpuesto/{id_impuesto}','ImpuestoController@editImpuesto')->middleware(\App\Http\Middleware\autenticar::class.':'.$id_usuario.',7,3');
//Route::post('editImpuesto','ImpuestoController@editImpuestoPost');
Route::get('eliminarImpuesto/{id_impuesto}','ImpuestoController@eliminarImpuesto')->middleware(\App\Http\Middleware\autenticar::class.':'.$id_usuario.',7,4');

//Terminos de pago
Route::get('terminosPago','TerminoPagoController@selectTerminosPagos');
Route::get('terminosPago/{mensaje}','TerminoPagoController@selectTerminosPagos');
Route::get('verTerminoPago/{id_terminoPago}','TerminoPagoController@verTerminoPago')->middleware(\App\Http\Middleware\autenticar::class.':'.$id_usuario.',8,2');
Route::get('nuevoTerminoPago','TerminoPagoController@nuevoTerminoPago')->middleware(\App\Http\Middleware\autenticar::class.':'.$id_usuario.',8,1');
Route::post('nuevoTerminoPago','TerminoPagoController@nuevoTerminoPagoPost');
Route::get('editTerminoPago/{id_terminoPago}','TerminoPagoController@editTerminoPago')->middleware(\App\Http\Middleware\autenticar::class.':'.$id_usuario.',8,3');
Route::post('editTerminoPago','TerminoPagoController@editTerminoPagoPost');
Route::get('eliminarTerminoPago/{id_terminoPago}','TerminoPagoController@eliminarTerminoPago')->middleware(\App\Http\Middleware\autenticar::class.':'.$id_usuario.',8,4');

Route::get('historialAcciones', 'AccionController@selectHistorialAcciones');
Route::get('historialAcciones/{id_usuario}', 'AccionController@selectHistorialAcciones');



//CATEGORIAS---------------------------------------------------------------------------------------------------------------

Route::get('categorias', 'CategoriaController@selectCategorias');
Route::post('nuevaCategoria', 'CategoriaController@nuevaCategoria');
Route::get('verCategoria/{id_categoria}','CategoriaController@verCategoria')->middleware(\App\Http\Middleware\autenticar::class.':'.$id_usuario.',9,2');
Route::post('editCategoria','CategoriaController@editCategoria');
Route::get('eliminarCategoria/{id_categoria}','CategoriaController@eliminarCategoria')->middleware(\App\Http\Middleware\autenticar::class.':'.$id_usuario.',9,4');


//BANCOS-------------------------------------------------------------------------------------------------------------------

Route::get('bancos', 'BancoController@selectBancos');
Route::post('nuevoBanco', 'BancoController@nuevoBanco');
Route::get('verBanco/{id_banco}','BancoController@verBanco')->middleware(\App\Http\Middleware\autenticar::class.':'.$id_usuario.',10,2');
Route::post('editBanco', 'BancoController@editBanco');
Route::get('eliminarBanco/{id_banco}','BancoController@eliminarBanco')->middleware(\App\Http\Middleware\autenticar::class.':'.$id_usuario.',10,4');;

//CONTACTOS-------------------------------------------------------------------------------------------------------------------

Route::get('contactos', 'ContactoController@selectContactos');
Route::get('nuevoContacto','ContactoController@nuevoContacto')->middleware(\App\Http\Middleware\autenticar::class.':'.$id_usuario.',11,1');
Route::post('nuevoContacto','ContactoController@nuevoContactoPost');
Route::get('verContacto/{id_contacto}', 'ContactoController@verContacto')->middleware(\App\Http\Middleware\autenticar::class.':'.$id_usuario.',11,2');
Route::get('editContacto/{id_contacto}', 'ContactoController@editContacto')->middleware(\App\Http\Middleware\autenticar::class.':'.$id_usuario.',11,3');
Route::post('editContacto', 'ContactoController@editContactoPost');
Route::get('eliminarContacto/{id_contacto}','ContactoController@eliminarContacto')->middleware(\App\Http\Middleware\autenticar::class.':'.$id_usuario.',11,4');


//ITEMS-------------------------------------------------------------------------------------------------------------------
Route::get('codigoBarras', 'ItemController@codigoBarras');
Route::get('items', 'ItemController@selectItems');
Route::get('nuevoItem', 'ItemController@nuevoItem')->middleware(\App\Http\Middleware\autenticar::class.':'.$id_usuario.',12,1');
Route::post('nuevoItem', 'ItemController@nuevoItemPost');
Route::get('editItem/{id_contacto}', 'ItemController@editItem')->middleware(\App\Http\Middleware\autenticar::class.':'.$id_usuario.',12,3');
Route::post('editItem', 'ItemController@editItemPost');
Route::get('verItem/{id_item}', 'ItemController@verItem')->middleware(\App\Http\Middleware\autenticar::class.':'.$id_usuario.',12,2');
Route::get('eliminarItem/{id_item}','ItemController@eliminarItem')->middleware(\App\Http\Middleware\autenticar::class.':'.$id_usuario.',12,4');


//GRESOS-------------------------------------------------------------------------------------------------------------------

//facturas de ventas
Route::get('bodegaAjax/{id_bodega}', 'BodegaController@ajaxBodega');
Route::get('pos', 'GresoController@POS')->middleware(\App\Http\Middleware\autenticar::class.':'.$id_usuario.',19,1');
Route::get('posAjax', 'GresoController@POSAjax');
Route::get('facturasI', 'GresoController@selectFacturasI')->middleware(\App\Http\Middleware\autenticar::class.':'.$id_usuario.',13');
Route::get('nuevaFacturaI', 'GresoController@nuevaFacturaI')->middleware(\App\Http\Middleware\autenticar::class.':'.$id_usuario.',13,1');
Route::post('nuevaFacturaI', 'GresoController@nuevaFacturaIPost');
Route::get('verFacturaI/{id_greso}', 'GresoController@verFacturaI')->middleware(\App\Http\Middleware\autenticar::class.':'.$id_usuario.',13,2');
Route::get('editFacturaI/{id_greso}', 'GresoController@editFacturaI')->middleware(\App\Http\Middleware\autenticar::class.':'.$id_usuario.',13,3');
Route::post('editFacturaI', 'GresoController@editFacturaIPost');
Route::get('eliminarFacturaI/{id_greso}','GresoController@eliminarFacturaI')->middleware(\App\Http\Middleware\autenticar::class.':'.$id_usuario.',13,4');
Route::get('PDFFacturaI/{id_greso}/{plantilla}','GresoController@PDFFacturaI')->middleware(\App\Http\Middleware\autenticar::class.':'.$id_usuario.',13,2');

//pagosI
Route::get('pagosI', 'GresoController@selectPagosI')->middleware(\App\Http\Middleware\autenticar::class.':'.$id_usuario.',14');
Route::get('nuevoPagoI', 'GresoController@nuevoPagoI')->middleware(\App\Http\Middleware\autenticar::class.':'.$id_usuario.',14,1');
Route::post('nuevoPagoI', 'GresoController@nuevoPagoIPost');
Route::get('ajaxClienteI/{id_cliente}', 'GresoController@ajaxClienteI');
Route::get('editPagoI/{id_greso}', 'GresoController@editPagoI')->middleware(\App\Http\Middleware\autenticar::class.':'.$id_usuario.',14,3');
Route::post('editPagoI', 'GresoController@editPagoIPost');
Route::get('verPagoI/{id_greso}', 'GresoController@verPagoI')->middleware(\App\Http\Middleware\autenticar::class.':'.$id_usuario.',14,2');
Route::get('eliminarPagoI/{id_greso}','GresoController@eliminarPagoI')->middleware(\App\Http\Middleware\autenticar::class.':'.$id_usuario.',14,4');
Route::get('PDFPagoI/{id_greso}','GresoController@PDFPagoI')->middleware(\App\Http\Middleware\autenticar::class.':'.$id_usuario.',14,2');


//facturas de compra
Route::get('facturasE', 'GresoController@selectFacturasE')->middleware(\App\Http\Middleware\autenticar::class.':'.$id_usuario.',15');
Route::get('nuevaFacturaE', 'GresoController@nuevaFacturaE')->middleware(\App\Http\Middleware\autenticar::class.':'.$id_usuario.',15,1');
Route::post('nuevaFacturaE', 'GresoController@nuevaFacturaEPost');
Route::get('verFacturaE/{id_greso}', 'GresoController@verFacturaE')->middleware(\App\Http\Middleware\autenticar::class.':'.$id_usuario.',15,2');
Route::get('editFacturaE/{id_greso}', 'GresoController@editFacturaE')->middleware(\App\Http\Middleware\autenticar::class.':'.$id_usuario.',15,3');
Route::post('editFacturaE', 'GresoController@editFacturaEPost');
Route::get('eliminarFacturaE/{id_greso}','GresoController@eliminarFacturaE')->middleware(\App\Http\Middleware\autenticar::class.':'.$id_usuario.',15,4');

//pagosE
Route::get('pagosE', 'GresoController@selectPagosE')->middleware(\App\Http\Middleware\autenticar::class.':'.$id_usuario.',16');
Route::get('nuevoPagoE', 'GresoController@nuevoPagoE')->middleware(\App\Http\Middleware\autenticar::class.':'.$id_usuario.',16,1');
Route::post('nuevoPagoE', 'GresoController@nuevoPagoEPost');
Route::get('ajaxClienteE/{id_proveedor}', 'GresoController@ajaxClienteE');
Route::get('verPagoE/{id_greso}', 'GresoController@verPagoE')->middleware(\App\Http\Middleware\autenticar::class.':'.$id_usuario.',16,2');
Route::get('editPagoE/{id_greso}', 'GresoController@editPagoE')->middleware(\App\Http\Middleware\autenticar::class.':'.$id_usuario.',16,3');
Route::post('editPagoE', 'GresoController@editPagoEPost');
Route::get('eliminarPagoE/{id_greso}','GresoController@eliminarPagoE')->middleware(\App\Http\Middleware\autenticar::class.':'.$id_usuario.',16,4');
Route::get('PDFPagoE/{id_greso}','GresoController@PDFPagoE')->middleware(\App\Http\Middleware\autenticar::class.':'.$id_usuario.',16,2');




Route::group(['middleware' => 'auth'], function () {
    //    Route::get('/link1', function ()    {
//        // Uses Auth Middleware
//    });

    //Please do not remove this if you want adminlte:route and adminlte:link commands to works correctly.
    #adminlte_routes
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
	

